"""
    The ``renders_plugins_views`` module
    ------------------------------------

    :Date: |date|
    :Revision: 1.0
    :Author: Franc SERRES <sefran007@gmail.com> from preview work of Jonathan Thomas <jonathan@openshot.org>
    :Description: Module Views Window for OpenShot Animated 3D Title plugins
    :Info: See <https://framagit.org/sefran/range-progress-bar> for more recent info.

    Use it to import Window 3D Title render animation views features.

    :class:`ListPluginsView`
    
    :class:`RendersPluginsViews`
    
    :class:`ManagerPanelPluginsView`
    
    :class:`TaskManagerPluginView`
"""
import os
import shutil

from functools import partial

from PyQt5.QtCore import QSize, Qt #, QTimer
from PyQt5.QtWidgets import QStyle, QDialog, QWidget, QListView, QTreeView, QDialogButtonBox, QLabel, QPushButton, QProgressBar, QVBoxLayout, QHBoxLayout, QColorDialog, QFileDialog, QListWidgetItem
from PyQt5.QtGui import QColor, QIcon, QPixmap, QImage

from classes import info, settings, ui_util
from classes.logger import log
from classes.app import get_app
from classes.query import File

from windows.models.renders_model import ListPluginsModel
from windows.controlers.blender_worker import *
from windows.controlers.renders_worker import ManageTaskViewPlugin


try:
    import json
except ImportError:
    import simplejson as json



class RendersPluginsViews(QDialog):
    """
    .. image:: classes/RendersPluginsViews.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class create window dialog Animated 3D titles gesture

    :param animated_3d_title_manager: Manager Animated 3D Title plugins
    :type animated_3d_title_manager: QObject
    :ivar thread_manager: Gesture of threaded renders
    :vartype thread_manager: ThreadsManager
    :ivar plugins_manager: Gesture of plugins
    :vartype plugins_manager: PluginsManager
    :ivar control_plugin: 
    :itype control_plugin: 
    :ivar list_plugins_model: 
    :itype list_plugins_model: 
    :ivar selected_template: 
    :itype selected_template: 
    :ivar preview_protected: 
    :itype preview_protected: 
    :ivar preview_unprotected: 
    :itype preview_unprotected: 
    :ivar ui_path: Path to file define ui dialog
    :itype ui_path: string
    :ivar btn_cancel: Cancel windows Animated 3D Title gesture plugins
    :itype btn_cancel: QPushButton
    
    
    .. image:: pictures/TaskPreviewPluginWidthTextTransFormAnimation.png
        :alt: Look like
        :align: center
        :width: 500px

    """
    def __init__(self, animated_3d_title_manager):
        """ Initialize class RendersPluginsViews

        :param ui_path:  
        :type ui_path:
        :param selected_template: Current Blender plugin selected
        :type selected_template:
        :rtype: QDialog
        """
        super(RendersPluginsViews, self).__init__()
        # Initialize parent class
        QDialog.__init__(self)

        # Translate text
        _ = get_app()._tr

        self.animated_3d_title_manager = animated_3d_title_manager

        self.control_plugin = None
        # Défault selected Blender plugin from Blender list plugins view
        self.list_plugins_model = None
        # Current Blender plugin selected
        self.selected_template = ""
        # Preview protected
        self.preview_protected = False
        self.preview_unprotected = not self.preview_protected
        self.style_render_config_progressbar = """
            QProgressBar{
                border: 2px solid grey;
                border-radius: 5px;
                text-align: center
            }
            
            QProgressBar::chunk {
                background-color: lightblue;
                width: 10px;
                margin: 1px;
            }
            """
        self.style_render_scene_progressbar = """
            QProgressBar{
                border: 2px solid grey;
                border-radius: 5px;
                text-align: center
            }
            
            QProgressBar::chunk {
                background-color: blue;
                width: 10px;
                margin: 1px;
            }
            """
        self.style_composing_scene_progressbar = """
            QProgressBar{
                border: 2px solid grey;
                border-radius: 5px;
                text-align: center
            }
            
            QProgressBar::chunk {
                background-color: green;
                width: 10px;
                margin: 1px;
            }
            """

        log.info("RendersPluginsViews-> " + _("Create window Animated 3D titles"))
        # Path to ui window
        self.ui_path = os.path.join(info.PATH, 'windows', 'ui', 'animated-title.ui')

        # Load UI from designer
        ui_util.load_ui(self, self.ui_path)

        # Init UI
        ui_util.init_ui(self)

        log.info("RendersPluginsViews-> " + _("Add window Animated 3D titles cancel button"))
        # Add Cancel button
        self.btn_cancel = QPushButton(_('Cancel'))
        self.buttonBox.addButton(self.btn_cancel, QDialogButtonBox.RejectRole)

        # Hide configuration widget
        self.hideConfigPluginWidget()


    def get_3d_plugins_list(self):
        """Get list of 3D plugin animated"""
        # Translate text
        _ = get_app()._tr

        log.info("RendersPluginsViews.get_3d_plugins_list-> " + _("Add widget Blender plugins tasks renders view gesture to Animated 3D titles window"))
        self.gridLayout_status_plugins.addLayout(ManagerPanelPluginsView(self), 0, 0)

        log.info("RendersPluginsViews.get_3d_plugins_list-> " + _("Add list plugins Animated 3D"))
        self.verticalLayout.addWidget(ListPluginsView(self))

        self.btnRefresh.clicked.connect(partial(self.btnRefresh_clicked))
        self.sliderPreview.valueChanged.connect(partial(self.sliderPreview_valueChanged))
        self.sliderPreview.sliderReleased.connect(partial(self.sliderPreview_sliderReleased))
    
    
    def enable_interface_preview(self):
        """Enable all controls on interface preview"""
        self.btnRefresh.setEnabled(True)
        self.sliderPreview.setEnabled(True)
        self.preview_protected = False
        self.preview_unprotected = not self.preview_protected


    def disable_interface_preview(self):
        """Disable all controls on interface preview"""
        self.btnRefresh.setDisabled(True)
        self.sliderPreview.setDisabled(True)
        self.preview_protected = True
        self.preview_unprotected = not self.preview_protected


    def showConfigPluginWidget(self):
        """ Show widget configuration active Blender plugin """
        # Translate text
        _ = get_app()._tr

        log.info("RendersPluginsViews.showConfigPluginWidget-> " + _("show config plugin"))
        self.widget_plugin_config.show()


    def hideConfigPluginWidget(self):
        """ Hide widget configuration active Blender plugin """
        # Translate text
        _ = get_app()._tr

        log.info("RendersPluginsViews.hideConfigPluginWidget-> " + _("hide config plugin"))
        self.widget_plugin_config.hide()


    def createControlConfigurationPlugin(self, id_task, list_running_plugins):
        """ Create control for active configuration Blender plugin """
        # Translate text
        _ = get_app()._tr

        log.info("RendersPluginsViews.createControlConfigurationPlugin-> " + _("Create control render for plugin object %s") % id_task)
        # Create task plugin gesture widget
        self.control_plugin = ManageTaskViewPlugin(id_task, list_running_plugins, self.widget_plugin_config, self)
        
        log.info("RendersPluginsViews.createControlConfigurationPlugin-> " + _("Active configuration for plugin object"))
        # Active widget task plugin
        self.activeConfigurationPlugin(self.control_plugin)
        

    def activeConfigurationPlugin(self, control_widget):
        """ Add widgets for configuration plugin to Configuration view """
        # Translate text
        _ = get_app()._tr

        log.info("RendersPluginsViews.addWidgetsPluginToView-> " + _("Create widgets from parameters configuration plugin object"))
        control_widget.createWidgets()
        
        log.info("RendersPluginsViews.addWidgetsPluginToView-> " + _("Active widgets configuration plugin object"))
        control_widget.activeWidgets()


    def removeControlConfigurationPlugin(self, control_widget):
        """ Remove control for a configuration plugin """
        # Translate text
        _ = get_app()._tr

        log.info("RendersPluginsViews.removeWidgetsPluginFromView-> " + _("Remove widgets from parameters configuration plugin object"))
        # Remove widgets configuration
        control_widget.deleteWidgets()
        
        # Set picture default
        #self.setPreviewPicture(image_path_default)
        
        # Set label preview control
        self.lblFrame.setText(_("No Frame"))
        
        # Hide slider
        self.sliderPreview.setVisible(False)
        
        # Hide label progress slider
        self.lblCountFrame.hide() == True
        
        # Hide button refresh
        self.btnRefresh.setVisible(False)
        
        # Delete task control plugin
        del control_widget


    def setPreviewPicture(self,image_path):
        """ Set picture preview from path file """
        image = QImage(image_path)
        scaled_image = image.scaledToHeight(self.imgPreview.height(), Qt.SmoothTransformation)
        pixmap = QPixmap.fromImage(scaled_image)
        self.imgPreview.setPixmap(pixmap)


    def setLabelPreviewControl(self, frame):
        """ Set label preview control """
        # Translate text
        _ = get_app()._tr

        if self.sliderPreview.value() != frame:
            self.sliderPreview.setValue(int(frame))
            log.info("RendersPluginsViews.setLabelPreviewControl-> Frame: %s" % frame)
        self.lblFrame.setText(_("Frame %s:" % frame))


    def setProgessRenderControl(self, id_task, percent=None, activepart=None, maxparts=None, style=None, busy=False):
        """ Set value of progress render control """
        # Translate text
        _ = get_app()._tr
        
        #log.info("RendersPluginsViews.setProgessPreviewControl-> Set progress render")
        if style:
            id_task.progress_bar.setStyleSheet(style)
        if busy:
            id_task.progress_bar.setMinimum(0)
            id_task.progress_bar.setMaximum(0)            
        else:
            id_task.progress_bar.setMinimum(0)
            id_task.progress_bar.setMaximum(100)
            if not percent is None:
                #log.info("RendersPluginsViews.setProgessPreviewControl-> " + _("Value: %s") % percent)
                id_task.progress_bar.setValue(int(percent))
            elif not (activepart is None or maxparts is None):
                id_task.progress_bar.setValue(int(activepart*100/maxparts))
            else:
                log.info("RendersPluginsViews.setProgessPreviewControl->" + _("Error: Bads parameters"))
            

    def setCountPreviewControl(self, startframe, endframe):
        """ Set label count preview control """
        # Translate text
        _ = get_app()._tr
        
        self.lblCountFrame.setText("{}/{}".format(startframe, endframe))
        self.sliderPreview.setMinimum(startframe)
        self.sliderPreview.setMaximum(endframe)


    def btnRefresh_clicked(self, checked):
        """Regenerate preview picture"""
        # Translate text
        _ = get_app()._tr

        log.info("RendersPluginsViews.btnRefresh_clicked-> " + _("Regenerate preview picture"))
        self.animated_3d_title_manager.plugins_manager.previewPlugin(self.animated_3d_title_manager.plugins_manager.edit_plugin, self.sliderPreview.value())
        
        
    def sliderPreview_valueChanged(self, new_value):
        """Get new value of preview slider, and generate preview picture"""
        # Translate text
        _ = get_app()._tr

        log.info("RendersPluginsViews.sliderPreview_valueChanged-> " + _("Update frame selected %s") % new_value)
        self.setLabelPreviewControl(new_value)
        

    def sliderPreview_sliderReleased(self):
        """Get new value of preview slider, and generate preview picture"""
        # Translate text
        _ = get_app()._tr

        log.info("RendersPluginsViews.sliderReleased-> " + _("Gegenerate preview picture %s") % self.sliderPreview.value())
        self.animated_3d_title_manager.plugins_manager.previewPlugin(self.animated_3d_title_manager.plugins_manager.edit_plugin, self.sliderPreview.value())
        

    def refresh_list_plugin_view(self):
        """Update list of Blender plugins"""
        # Translate text
        _ = get_app()._tr

        log.info("RendersPluginsViews.refresh_list_plugins_view-> update model")
        if self.list_plugins_model:
            self.list_plugins_model.update_model()


    def reject(self):
        """ Exit ask from Cancel button """
         # Translate text
        _ = get_app()._tr

        # Clear any active blender plugin animations from list running render plugins
        for task in list(self.animated_3d_title_manager.plugins_manager.list_running_plugins.keys()):
            log.info("RendersPluginsViews.reject-> " + _("Remove folder from plugin %s") % task)
            # Remove render plugin temp folder
            self.animated_3d_title_manager.plugins_manager.remove_folder_plugin(task)
            # Delete from plugin list
            self.animated_3d_title_manager.plugins_manager.list_running_plugins.pop(task, None)
            # Delete from active plugin configuration
            self.animated_3d_title_manager.plugins_manager.delete_active_plugin_configuration(task)
            del task
        
        # Close Animated3DTitleManager
        log.info("RendersPluginsViews.reject-> " + _("Close Animated 3D Title manager"))
        self.animated_3d_title_manager.close()

        # Cancel dialog
        super(RendersPluginsViews, self).reject()
        
        self.deleteLater()
        

    def __del__(self):
        """Delete object method"""
         # Translate text
        _ = get_app()._tr

        log.info("RendersPluginsViews-> " + _("Del Animated 3D Title window"))
        
        


class ManagerPanelPluginsView(QVBoxLayout):
    """
    .. image:: classes/ManagerPanelPluginsView.png
        :alt: Diagram
        :align: center
        :width: 500px

    Widget gesture render state Blender plugins view

    .. image:: pictures/MultiRenderPluginsGesture.png
        :alt: Look like
        :align: center
        :width: 500px

    """
    def __init__(self, title3DWindow):
        """ Initialize parent BlenderTaskRenderView class """
        super(ManagerPanelPluginsView, self).__init__()

        # Translate text
        _ = get_app()._tr

        log.info("ManagerPanelPluginsView-> " + _("Blender Task Plugins Render View initialization"))

        self.title3DWindow = title3DWindow

        # Add methods to animated 3D titles window
        self.title3DWindow.addTaskManagerToPlugin = self.addTaskManagerToPlugin
        self.title3DWindow.removeWidgetsFromConfigurationPluginView = self.removeWidgetsFromConfigurationPluginView

    def addTaskManagerToPlugin(self, name):
        """Add widget task manager for plugin"""
        # Translate text
        _ = get_app()._tr

        log.info("ManagerPanelPluginsView.addTaskManagerToPlugin-> " + _("create widget Plugin state view"))

        # Create widget taskmanager for selected plugin
        instance_plugin = TaskManagerPluginView(self, self.title3DWindow, name)

        # Add task manager plugin view to status run Blender plugins
        self.addLayout(instance_plugin)

        return instance_plugin

    def removeWidgetsFromConfigurationPluginView(self):
        """Remove configuration widgets into scrollArea from Blender plugins window"""
        # Translate text
        _ = get_app()._tr

        log.info("ManagerPanelPluginsView.removeWidgetsFromConfigurationPluginView-> " + _("remove widgets from config plugin view"))
        # Loop through child widgets
        for child in self.title3DWindow.settingsContainer.children():
            try:
                self.title3DWindow.settingsContainer.layout().removeWidget(child)
                child.deleteLater()
            except:
                pass
        log.info("ManagerPanelPluginsView.removeWidgetsFromConfigurationPluginView-> " + _("widgets configuration plugin removed"))




class TaskManagerPluginView(QHBoxLayout):
    """
    .. image:: classes/TaskManagerPluginView.png
        :alt: Diagram
        :align: center
        :width: 500px

    Widget tasks render Blender plugin view

    .. image:: classes/TaskManagerPluginView.png
        :alt: Look like
        :align: center
        :width: 500px

    """
    def __init__(self, widget_parent, title3DWindow, name):
        """ Initialize parent BlenderTaskRenderView class """
        super(TaskManagerPluginView, self).__init__()

        self.name_task = name
        self.widget_parent = widget_parent
        self.title3DWindow = title3DWindow
        self.animated_3d_title_manager = self.title3DWindow.animated_3d_title_manager
        self.state = None
        
        self.buttonStyleRender = """
            QPushButton {
                color:#ddd;
                border-radius:0;
                border:0;
                background-color:#e58e26;
                text-align:left;
                padding-left:50px;
                qproperty-iconSize: 20px 20px;
                
            }
            
            QPushButton:hover {
                color:#ecf0f1;
                background-color:#fad390;
            }
            
            QPushButton:pressed {
                color:#ecf0f1;
                background-color:#f6b93b;
            }
        """

        self.buttonStyleExecute = """
            QPushButton {
                color:#ddd;
                border-radius:0;
                border:0;
                background-color:#b71540;
                text-align:left;
                padding-left:50px;
                qproperty-iconSize: 20px 20px;
            }
            
            QPushButton:hover {
                color:#ecf0f1;
                background-color:#f8c291;
            }
            
            QPushButton:pressed {
                color:#ecf0f1;
                background-color:#e55039;
            }
        """

        self.buttonStyleConfigure = """
            QPushButton {
                color:#ddd;
                border-radius:0;
                border:0;
                background-color:#78e08f;
                text-align:left;
                padding-left:50px;
                qproperty-iconSize: 20px 20px;
            }
            
            QPushButton:hover {
                color:#ecf0f1;
                background-color:#b8e994;
            }
            
            QPushButton:pressed {
                color:#ecf0f1;
                background-color:#079992;
            }
        """

        self.buttonStyleNew = """
            QPushButton {
                color:#ddd;
                border-radius:0;
                border:0;
                background-color:#e58e26;
                text-align:left;
                padding-left:50px;
                qproperty-iconSize: 20px 20px;
                
            }
            
            QPushButton:hover {
                color:#ecf0f1;
                background-color:#fad390;
            }
            
            QPushButton:pressed {
                color:#ecf0f1;
                background-color:#f6b93b;
            }
        """

        self.buttonStyleTrash = """
            QPushButton {
                color:#ddd;
                border-radius:0;
                border:0;
                background-color:#b71540;
                text-align:left;
                padding-left:50px;
                qproperty-iconSize: 20px 20px;
            }
            
            QPushButton:hover {
                color:#ecf0f1;
                background-color:#f8c291;
            }
            
            QPushButton:pressed {
                color:#ecf0f1;
                background-color:#e55039;
            }
        """

        # Translate text
        _ = get_app()._tr

        log.info("TaskManagerPluginView-> " + _("Task 3D Plugins Render View initialization"))

        # Add plugin label
        self.lbl_plugin = QLabel(_(name.data()))
        # Add progress frames/part picture
        self.progress_bar = QProgressBar()
        self.progress_bar.setRange(0,100)
        self.progress_bar.setValue(0)
        # Add Render/Stop button
        self.btn_execute = QPushButton() #
        self.btn_execute.setToolTip(_('Pause'))
        self.btn_execute.setIcon(self.title3DWindow.style().standardIcon(QStyle.SP_MediaPlay))
        self.btn_execute.setStyleSheet(self.buttonStyleExecute)
        self.btn_execute.clicked.connect(partial(self.animated_3d_title_manager.plugins_manager.controlRenderPlugin, self))
        # Add Render button
        self.btn_render = QPushButton()
        self.btn_render.setToolTip(_('Render'))
        self.btn_render.setIcon(self.title3DWindow.style().standardIcon(QStyle.SP_DialogApplyButton))
        self.btn_render.setStyleSheet(self.buttonStyleRender)
        self.btn_render.clicked.connect(partial(self.animated_3d_title_manager.plugins_manager.renderPlugin, self))
        # Add configuration Blender plugin
        self.btn_conf = QPushButton()
        self.btn_conf.setToolTip(_('Configure'))
        self.btn_conf.setIcon(self.title3DWindow.style().standardIcon(QStyle.SP_DialogOpenButton))
        self.btn_conf.setStyleSheet(self.buttonStyleConfigure)
        self.btn_conf.clicked.connect(partial(self.animated_3d_title_manager.plugins_manager.activePlugin, self))
        # Add new instance Blender plugin
        self.btn_new  = QPushButton()
        self.btn_new.setToolTip(_('New'))
        self.btn_new.setIcon(self.title3DWindow.style().standardIcon(QStyle.SP_FileDialogNewFolder))
        self.btn_new.setStyleSheet(self.buttonStyleNew)
        self.btn_new.clicked.connect(partial(self.animated_3d_title_manager.plugins_manager.createPlugin, self.name_task))
        # Add Kill instance Blender plugin
        self.btn_Trash  = QPushButton()
        self.btn_Trash.setToolTip(_('Trash'))
        self.btn_Trash.setIcon(self.title3DWindow.style().standardIcon(QStyle.SP_TrashIcon))
        self.btn_Trash.setStyleSheet(self.buttonStyleTrash)
        self.btn_Trash.clicked.connect(partial(self.animated_3d_title_manager.plugins_manager.deletePlugin, self))
        #self.win.buttonBox.addButton(self.win.btn_render, QDialogButtonBox.AcceptRole)
        #
        # Create widget instance with widget status Blender plugin
        self.state_edit()

    def clear_buttons(self):
        """Erase widgets button from QHBoxLayout"""
        # Translate text
        _ = get_app()._tr

        # Remove widgets into widget
        while self.count():
            item = self.takeAt(0)
            subwidget = item.widget()
            if subwidget is not None:
                subwidget.setParent(None)
        #log.info("TaskManagerPluginView.clear_buttons-> " + _("buttons removed"))


    def state_edit(self):
        """Show edit state widget"""
        # Translate text
        _ = get_app()._tr

        # Erase alls buttons
        self.clear_buttons()
        # Add corrects buttons
        self.addWidget(self.lbl_plugin)
        self.addStretch(1)
        self.addWidget(self.btn_render)
        self.addWidget(self.btn_new)
        self.addWidget(self.btn_Trash)
        self.progress_bar.setVisible(False)
        self.btn_render.setVisible(True)
        self.btn_execute.setVisible(False)
        self.btn_conf.setVisible(False)
        self.btn_new.setVisible(True)

        # Set state edit
        self.state = 'edit'
        log.info("TaskManagerPluginView.state_edit-> " + _("State edit"))


    def state_waitconf(self):
        """Show edit state widget"""
        # Translate text
        _ = get_app()._tr

        # Erase alls buttons
        self.clear_buttons()
        # Add corrects buttons
        self.addWidget(self.lbl_plugin)
        self.addStretch(1)
        self.addWidget(self.btn_render)
        self.addWidget(self.btn_conf)
        self.addWidget(self.btn_Trash)
        self.progress_bar.setVisible(False)
        self.btn_render.setVisible(True)
        self.btn_execute.setVisible(False)
        self.btn_conf.setVisible(True)
        self.btn_new.setVisible(False)

        # Set state wait for configuration
        self.state = 'waitconf'
        log.info("TaskManagerPluginView.state_waitconf-> " + _("State wait configuration"))


    def state_render(self):
        """Show edit state widget"""
        # Translate text
        _ = get_app()._tr

        # Erase alls buttons
        self.clear_buttons()
        # Add corrects buttons
        self.addWidget(self.lbl_plugin)
        self.addWidget(self.progress_bar)
        self.addWidget(self.btn_execute)
        self.addWidget(self.btn_conf)
        self.addWidget(self.btn_Trash)
        self.progress_bar.setVisible(True)
        self.btn_render.setVisible(False)
        self.btn_execute.setVisible(True)
        self.btn_conf.setVisible(True)
        self.btn_new.setVisible(False)

        # Set state render plugin configuration
        self.state = 'render'
        log.info("TaskManagerPluginView.state_render-> " + _("State render"))

    def state_renderedit(self):
        """Show edit state widget"""
        # Translate text
        _ = get_app()._tr

        # Erase alls buttons
        self.clear_buttons()
        # Add corrects buttons
        self.addWidget(self.lbl_plugin)
        self.addWidget(self.progress_bar)
        self.addWidget(self.btn_execute)
        self.addWidget(self.btn_new)
        self.addWidget(self.btn_Trash)
        self.progress_bar.setVisible(True)
        self.btn_render.setVisible(False)
        self.btn_execute.setVisible(True)
        self.btn_conf.setVisible(False)
        self.btn_new.setVisible(True)

        # Set state render plugin configuration
        self.state = 'renderedit'
        log.info("TaskManagerPluginView.state_renderedit-> " + _("State active render edition"))
        
        
    def state_pause_render(self):
        """Show pause render state"""
        # Translate text
        _ = get_app()._tr
        
        # Set pause Icon
        self.btn_execute.setIcon(self.title3DWindow.style().standardIcon(QStyle.SP_MediaPause))
        self.btn_execute.setToolTip(_('Execute'))
        log.info("TaskManagerPluginView.state_pause_render-> " + _("State pause render"))


    def state_execute_render(self):
        """Show pause render state"""
        # Translate text
        _ = get_app()._tr
        
        # Set pause Icon
        self.btn_execute.setIcon(self.title3DWindow.style().standardIcon(QStyle.SP_MediaPlay))
        self.btn_execute.setToolTip(_('Pause'))
        log.info("TaskManagerPluginView.state_execute_render-> " + _("State execute render"))




class ListPluginsView(QListView):
    """
    .. image:: classes/ListPluginsView.png
        :alt: Diagram
        :align: center
        :width: 500px

    Widget TreeView List plugins used on the animated title window

    """
    def __init__(self, title3DWindow):
        """ Initialize class ListPluginsView """
        super(ListPluginsView, self).__init__()
        # Invoke parent init
        QTreeView.__init__(self)

        # Translate text
        _ = get_app()._tr

        #log.info("ListPluginsView-> " + _('Views list render plugin initialization'))

        # Get a reference to the window title 3D
        self.title3DWindow = title3DWindow
        self.animated_3d_title_manager = title3DWindow.animated_3d_title_manager

        # Get Model data (load list of render plugins)
        self.title3DWindow.list_plugins_model = ListPluginsModel(blender_version=self.animated_3d_title_manager.blender_version)
        self.list_plugins_model = self.title3DWindow.list_plugins_model

        log.info("ListPluginsView-> " + _('Create list plugins model'))
        # Setup header columns
        self.setModel(self.list_plugins_model.model)
        self.setIconSize(QSize(130, 110))
        #self.setGridSize(QSize(102, 92))
        self.setGridSize(QSize(130, 125))
        self.setViewMode(QListView.IconMode)
        self.setResizeMode(QListView.Adjust)
        self.setUniformItemSizes(False)
        self.setWordWrap(True)
        self.setTextElideMode(Qt.ElideRight)
        self.setStyleSheet('QTreeView::item { padding-top: 2px; }')

        # Refresh view
        self.title3DWindow.refresh_list_plugin_view()

        # Adjust dialogbox to widget
        #self.title3DWindow.adjustSize()

    def currentChanged(self, selected, deselected):
        """ Get new config from blender plugin """
        # Translate text
        _ = get_app()._tr
        
        if selected.data() and not selected.data() is None:
            log.info("ListPluginsView.currentChanged-> " + _("Change selected plugin from «%s» to «%s»" % (deselected.data(), selected.data())))
            # Is active plugin confuguration ?
            self.animated_3d_title_manager.plugins_manager.createPlugin(selected)
