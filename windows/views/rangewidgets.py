"""
    .. |date| date::

    :Date: |date|
    :Revision: 1.0
    :Author: Franc SERRES <sefran007@gmail.com>
    :Description: This is a documention for python 3 qt5 range widgets
    :Info: See <https://framagit.org/sefran/range-progress-bar> for more recent info.

    .. _formatting-text:

    The ``rangewidgets`` module
    ===========================

    Use it to import range qt widget feature.

        .. image:: classes/classes.png
            :alt: All classes
            :align: center
            :width: 800px
            :height: 1000px

    Example:
    
    .. code-block:: python
    
        from rangewidgets import QRangeProgressBar

        start_range = 1
        end_range = 45
        min_range = 1
        max_range = 100

        myObject = QRangeProgressBar(start_range, end_range, min_range, max_range)

"""
from math import log

from PyQt5 import QtCore
from PyQt5.QtCore import QObject, Qt
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QSlider
from PyQt5.QtGui import QPainter, QFont, QColor, QPen


class QRange(object):
    """
    .. image:: classes/QRange.png
        :alt: Diagram
        :align: center
        :height: 300px

    Create a range object.

    Example::

        from rangewidgets import QRange

        rangeobject = QRange(1,100,20,80)
        print('Minimum: %s' % rangeobject.getMinimum())
        print('Start: %s' % rangeobject.getStart())
        print('End: %s' % rangeobject.getEnd())
        print('Maximum: %s' % rangeobject.getMaximum())

    :param minimum: The minimum bar value.
    :type minimum: int, float
    :param maximum: The maximum bar value.
    :type maximum: int, float
    :param start: The start of range.
    :type start: int, float
    :param end: The end of range.
    :type end: int, float
    """

    def __init__(self, minimum, maximum, start, end):
        """ Initialize class QRange """
        # Initialisation parameters
        self.setMinimum(minimum)
        self.setMaximum(maximum)
        self._start = start
        self._end = end


    def setStart(self, value):
        """
        Set start range value.

        :param value: Start range value -- default value 20.
        :type value: int, float
        """
        self._nulmaxrange = False
        if value < self._minimum:
            self._start = self._minimum
        elif value > self._end:
            if self._end == self._maximum:
                self._start = self._maximum
                self._nulmaxrange = True
            else:
                self._start = self._end
        else:
            self._start = value


    def getStart(self):
        """
        Get start range value.

        :returns: Value of start range.
        :rtype: int, float
        """
        return self._start


    def setEnd(self, value):
        """
        Set end range value.

        :param value: End range value -- default value 80.
        :type value: int, float
        """
        self._nulminrange = False
        if value > self._maximum:
            self._end = self._maximum
        elif value < self._start:
            if self._start == self._minimum:
                self._end = self._minimum
                self._nulminrange = True
            else:
                self._end = self._start
        else:
            self._end = value


    def getEnd(self):
        """
        Get end range value.

        :returns: Value of end range.
        :rtype: int, float
        """
        return self._end


    def setMinimum(self, value):
        """
        Set minimum bar value.

        :param value: Minimum bar value -- default value 0.
        :type value: int, float
        """
        self._minimum = value


    def getMinimum(self):
        """
        Get minimum bar value.

        :returns: Value of minimum bar.
        :rtype: int, float
        """
        return self._minimum


    def setMaximum(self, value):
        """
        Set maximum bar value

        :param value: Maximum bar value -- default value 100.
        :type value: int, float
        """
        self._maximum = value


    def getMaximum(self):
        """
        Get maximum bar value.

        :returns: Value of maximum bar.
        :rtype: int, float
        """
        return self._maximum





class QLevels(QRange):
    """
    .. image:: classes/QLevels.png
        :alt: Diagram
        :align: center
        :height: 550px

    Create a range object with levels informations.

    Example::

        from rangewidgets import QLevels

        rangeobject = QLevels(1,100,20,80,10,30,70,90)
        print('Minimum: %s' % rangeobject.getMinimum())
        print('Minimum alert end: %s' % rangeobject.getMinimumAlert())
        print('Minimum warning end: %s' % rangeobject.getMinimumWarning())
        print('Start: %s' % rangeobject.getStart())
        print('End: %s' % rangeobject.getEnd())
        print('Maximum warning start: %s' % rangeobject.getMaximumWarning())
        print('Maximum alert start: %s' % rangeobject.getMaximumAlert())
        print('Maximum: %s' % rangeobject.getMaximum())

    :param minimum: The minimum bar value.
    :type minimum: int
    :param maximum: The maximum bar value.
    :type maximum: int
    :param start: The start of range.
    :type start: int
    :param end: The end of range.
    :type end: int
    :param alert_min: Level of minimuScalem alert value.
    :type alert_min: int
    :param warning_min: Level of minimum warning value.
    :type warning_min: int
    :param warning_max: Level of maximum warning value.
    :type  warning_max: int
    :param alert_max: Level of maximum alert value.
    :type alert_max: int
    """

    def __init__(self, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max):
        """ Initialize class QLevels. """
        # Initialize QRange
        QRange.__init__(self, minimum, maximum, start, end)

        # Initialisation parameters
        self.setMinimumAlert(alert_min) # Set UI alert min zone show
        self.setMinimumWarning(warning_min) # Set UI warning min zone show
        self.setMaximumWarning(warning_max) # Set UI warning max zone show
        self.setMaximumAlert(alert_max) # Set UI alert max zone showversion


    def setMinimumAlert(self, value):
        """
        Set end minimum alert level value.

        :param value: Minimum end alert level.
        :type value: int, float
        """
        self._alert_zone_min = value


    def getMinimumAlert(self):
        """
        Get end minimum alert level value.

        :returns: Minimum end alert level.
        :rtype: int, float
        """
        return self._alert_zone_min


    def setMinimumWarning(self, value):
        """
        Set end minimum warning level value.

        :param value: Minimum end warning level.
        :type value: int, float
        """
        self._warning_zone_min = value


    def getMinimumWarning(self):
        """
        Get end warning minimum level value.

        :returns: Minimum end level warning.
        :rtype: int, float
        """
        return self._warning_zone_min


    def setMaximumWarning(self, value):
        """
        Set start maximum warning level value.

        :param value: Maximum start warning level.
        :type value: int, float
        """
        self._warning_zone_max = value


    def getMaximumWarning(self):
        """
        Get start maximum warning level value.

        :returns: Maximum start warning level.
        :rtype: int, float
        """
        return self._warning_zone_max


    def setMaximumAlert(self, value):
        """
        Set start maximum alert level value.

        :param value: Maximum start alert level.
        :type value: int, float
        """
        self._alert_zone_max = value


    def getMaximumAlert(self):
        """
        Get start maximum alert level value.

        :returns: Maximum start alert level.
        :rtype: int, float
        """
        return self._alert_zone_max





class QFactoryRange(object):
    """
    Create a general range object.

    Example::

        from rangewidgets import QFactoryRange

        rangeobject = QFactoryRange(1,100,20,80,True,10,30,70,90)

    :param minimum: The minimum bar value.
    :type minimum: int
    :param maximum: The maximum bar value.
    :type maximum: int
    :param start: The start of range.
    :type start: int
    :param end: The end of range.
    :type end: int
    :param islevels: Set levels adding.
    :type  islevels: bool
    :param alert_min: Level of minimum alert value.
    :type alert_min: int
    :param warning_min: Level of minimum warning value.
    :type warning_min: int
    :param warning_max: Level of maximum warning value.
    :type  warning_max: int
    :param alert_max: Level of maximum alert value.
    :type alert_max: int
    :returns: A range with levels=False.
    :rtype: QRange
    :returns: A range with levels informations with levels=True.
    :rtype: QLevels
    """

    def __new__(cls, minimum=1, maximum=100, start=20, end=80, islevels=None, alert_min=None, warning_min=None, warning_max=None, alert_max=None):
        # Select object to return
        if islevels:
            return QLevels(minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max)
        else:
            return QRange(minimum, maximum, start, end)





class Scale(object):
    """
    .. image:: classes/Scale.png
        :alt: Diagram
        :align: center
        :height: 550px

    Create object with linear or logarithm features scales.

    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    """

    def __init__(self, islogscale, logarithmbase=10):
        """ Initialize Scale object """
        # Initialisation parameters
        if islogscale:
            self.setLogarithmScale()
        else:
            self.setLinearScale()
        self.setLinearFactor()
        self.setLogarithmBase(logarithmbase)
        self.setLog0()


    def setLinearScale(self):
        """ Set linear scale step. """
        self._logarithmscale = False


    def setLinearFactor(self, value=1):
        """
        Set linear factor.

        :param value: Linear factor value.
        :type value: int, float
        """
        self._linearfactor = value


    def getLinearFactor(self):
        """
        Get linear factor.

        :returns: Linear factor value.
        :rtype: int, float
        """
        return self._linearfactor

    def setLogarithmScale(self):
        """ Set logarithm scale step. """
        self._logarithmscale = True


    def isLogarithmScale(self):
        """
        Get logarithm step state.

        :returns: State of logarithm scale.
        :rtype: bool
        """
        return self._logarithmscale


    def setLogarithmBase(self, base=10):
        """
        Set logarithm base value.

        :param base: Base value of logrithm -- default value 10.
        :type base: int
        """
        self._logarithmbase = base


    def getLogarithmBase(self):
        """
        Get default logarithm base.

        :returns: Value of base logarithm.
        :rtype: int
        """
        return self._logarithmbase


    def setLog0(self, value=0.1):
        """
        Set default value log for value 0

        :param value: Value of 0.
        :type value: float
        """
        if value != 0.0:
            self._log0 = value
        else:
            self._log0 = 0.1


    def getLog0(self):
        """
        Get default value log for value 0

        :returns: Value of 0 logarithm.
        :rtype: int
        """
        return self._log0


    def scaleValue(self, value):
        """
        Return value scaled.

        :param value: value to scale.
        :type value: int,float
        :returns: Value scaled.
        :rtype: int, float
        """
        if self._logarithmscale:
            if value == 0:
                logvalue = log(self._log0, self._logarithmbase)
            elif value > 0:
                logvalue = log(value, self._logarithmbase)
            else:
                logvalue = -log(-value, self._logarithmbase)
            return logvalue
        else:
            return value * self._linearfactor





class QScale(QRange, Scale):
    """
    .. image:: classes/QScale.png
        :alt: Diagram
        :align: center
        :height: 500px

    Create a scaled range object.

    Example::

        from rangewidgets import QSale

        scaledobject = QScale(1,100,20,80,True,2)

    :param minimum: The minimum bar value.
    :type minimum: int
    :param maximum: The maximum bar value.
    :type maximum: int
    :param start: The start of range.
    :type start: int
    :param end: The end of range.
    :type end: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    """

    def __init__(self, minimum, maximum, start, end, islogscale, logarithmbase=10):
        """ Initialize class Scale. """
        # Initialize QRange
        QRange.__init__(self, minimum, maximum, start, end)
        # Initialize Scale
        Scale.__init__(self, islogscale, logarithmbase)


    def getScaledValueMinimum(self):
        """
        Return minimum bar value scaled.

        :returns: Value scaled of minimum bar.
        :rtype: int, float
        """
        if self._logarithmscale:
            # Set value minimum progress bar
            if self.getMinimum() == 0:
                logvalue = log(self._log0, self._logarithmbase)
            elif self.getMinimum() > 0:
                logvalue = log(self.getMinimum(), self._logarithmbase)
            else:
                logvalue = -log(-self.getMinimum(), self._logarithmbase)
            return logvalue
        else:
            return self.getMinimum() * self._linearfactor


    def getScaledValueMaximum(self):
        """
        Return maximum bar value scaled.

        :returns: Value scaled of maximum bar.
        :rtype: int, float
        """
        if self._logarithmscale:
            # Set value maximum progress bar
            if self.getMaximum() == 0:
                logvalue = log(self._log0, self._logarithmbase)
            elif self.getMaximum() > 0 :
                logvalue = log(self.getMaximum(), self._logarithmbase)
            else:
                logvalue = -log(-self.getMaximum(), self._logarithmbase)
            return logvalue
        else:
            return self.getMaximum() * self._linearfactor


    def getScaledWidthBar(self):
        """
        Return width bar value scaled.

        :returns: Value scaled of width bar.
        :rtype: int, float
        """
        return self.getScaledValueMaximum() - self.getScaledValueMinimum()


    def getScaledValueStart(self):
        """
        Return start range valulogvalueende scaled.

        :returns: Value scaled of start range.
        :rtype: int, float
        """
        if self._logarithmscale:
            #Set value start range
            if self.getStart() == 0:
                logvalue = log(self._log0, self._logarithmbase)
            elif self.getStart() > 0 :
                logvalue = log(self.getStart(), self._logarithmbase)
            else:
                logvalue = -log(-self.getStart(), self._logarithmbase)
            return logvalue
        else:
            return self.getStart() * self._linearfactor


    def getScaledValueEnd(self):
        """
        Return end range value scaled.

        :returns: Value scaled of end range.
        :rtype: int, float
        """
        if self._logarithmscale:
            # Set value end range
            if self.getEnd() == 0:
                logvalue = log(self._log0, self._logarithmbase)
            elif self.getEnd() > 0 :
                logvalue = log(self.getEnd(), self._logarithmbase)
            else:
                logvalue = -log(-self.getEnd(), self._logarithmbase)
            return logvalue
        else:
            return self.getEnd() * self._linearfactor
    
    
    def getScaledWidthRange(self):
        """
        Return width range value scaled.

        :returns: Value scaled of width range.
        :rtype: int, float
        """
        return self.getScaledValueEnd() - self.getScaledValueStart()
        




class QScaleLevels(QLevels, Scale):
    """
    .. image:: classes/QScaleLevels.png
        :alt: Diagram
        :align: center
        :height: 380px

    Create a scaled range object with levels informations.

    Example::

        from rangewidgets import QSaleLevels

        scaledobject = QScaleLevels(1,100,20,80,10,30,70,90,True,2)

    :param minimum: The minimum bar value.
    :type minimum: int
    :param maximum: The maximum bar value.
    :type maximum: int
    :param start: The start of range.
    :type start: int
    :param end: The end of range.
    :type end: int
    :param alert_min: Level of minimum alert value.
    :type alert_min: int
    :param warning_min: Level of minimum warning value.
    :type warning_min: int
    :param warning_max: Level of maximum warning value.
    :type  warning_max: int
    :param alert_max: Level of maximum alert value.
    :type alert_max: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    """

    def __init__(self, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase=10):
        """ Initialize class QScaleLevels """
        # Initialize QLevels
        QLevels.__init__(self, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max)
        # Initialize Scale
        Scale.__init__(self, islogscale, logarithmbase)


    def getScaledValueMinimum(self):
        """
        Return minimum bar value scaled.

        :returns: Value scaled of minimum bar.
        :rtype: int, float
        """
        if self._logarithmscale:
            # Set value minimum progress bar
            if self.getMinimum() == 0:
                logvalue = log(self._log0, self._logarithmbase)
            elif self.getMinimum() > 0:
                logvalue = log(self.getMinimum(), self._logarithmbase)
            else:
                logvalue = -log(-self.getMinimum(), self._logarithmbase)
            return logvalue
        else:
            return self.getMinimum() * self._linearfactor


    def getScaledValueMaximum(self):
        """
        Return maximum bar value scaled.

        :returns: Value scaled of maximum bar.
        :rtype: int, float
        """
        if self._logarithmscale:
            # Set value maximum progress bar
            if self.getMaximum() == 0:
                logvalue = log(self._log0, self._logarithmbase)
            elif self.getMaximum() > 0 :
                logvalue = log(self.getMaximum(), self._logarithmbase)
            else:
                logvalue = -log(-self.getMaximum(), self._logarithmbase)
            return logvalue
        else:
            return self.getMaximum() * self._linearfactor


    def getScaledWidthBar(self):
        """
        Return width bar value scaled.

        :returns: Value scaled of width bar.
        :rtype: int, float
        """
        return self.getScaledValueMaximum() - self.getScaledValueMinimum()


    def getScaledValueStart(self):
        """
        Return start range valulogvalueende scaled.

        :returns: Value scaled of start range.
        :rtype: int, float
        """
        if self._logarithmscale:
            #Set value start range
            if self.getStart() == 0:
                logvalue = log(self._log0, self._logarithmbase)
            elif self.getStart() > 0 :
                logvalue = log(self.getStart(), self._logarithmbase)
            else:
                logvalue = -log(-self.getStart(), self._logarithmbase)
            return logvalue
        else:
            return self.getStart() * self._linearfactor


    def getScaledValueEnd(self):
        """
        Return end range value scaled.

        :returns: Value scaled of end range.
        :rtype: int, float
        """
        if self._logarithmscale:
            # Set value end range
            if self.getEnd() == 0:
                logvalue = log(self._log0, self._logarithmbase)
            elif self.getEnd() > 0 :
                logvalue = log(self.getEnd(), self._logarithmbase)
            else:
                logvalue = -log(-self.getEnd(), self._logarithmbase)
            return logvalue
        else:
            return self.getEnd() * self._linearfactor
    
    
    def getScaledWidthRange(self):
        """
        Return width range value scaled.

        :returns: Value scaled of width range.
        :rtype: int, float
        """
        return self.getScaledValueEnd() - self.getScaledValueStart()


    def getScaledValueMinimumAlert(self):
        """
        Return minimum alert value scaled.

        :returns:Value scaled of minimum alert.
        :rtype: int, float
        """
        if not self.getMinimumAlert() is None:
            if self._logarithmscale:
                if self.getMinimumAlert() == 0:
                    logalert_min = log(1, self._logarithmbase)
                elif self.getMinimumAlert() > 0:
                    logalert_min = log(self.getMinimumAlert(), self._logarithmbase)
                elif self.getMinimumAlert() < 0:
                    logalert_min = -log(-self.getMinimumAlert(), self._logarithmbase)
                return logalert_min
            else:
                return self.getMinimumAlert() * self._linearfactor
        else:
            return None


    def getScaledValueMinimumWarning(self):
        """
        Return minimum warning value scaled.

        :returns: Value scaled of minimum warning.
        :rtype: int, float
        """
        if not self.getMinimumWarning() is None:
            if self._logarithmscale:
                if self.getMinimumWarning() == 0:
                    logwarning_min = log(1, self._logarithmbase)
                elif self.getMinimumWarning() > 0:
                    logwarning_min = log(self.getMinimumWarning(), self._logarithmbase)
                elif self.getMinimumWarning() < 0:
                    logwarning_min = -log(-self.getMinimumWarning(), self._logarithmbase)
                return logwarning_min
            else:
                return self.getMinimumWarning() * self._linearfactor
        else:
            return None


    def getScaledValueMaximumWarning(self):
        """
        Return maximum warning value scaled.

        :returns: Value scaled of maximum warning.
        :rtype: int, float
        """
        if not self.getMaximumWarning() is None:
            if self._logarithmscale:
                if self.getMaximumWarning() == 0:
                    logwarning_max =  log(1, self._logarithmbase)
                if self.getMaximumWarning() > 0:
                    logwarning_max =  log(self.getMaximumWarning(), self._logarithmbase)
                if self.getMaximumWarning() < 0:
                    logwarning_max =  -log(-self.getMaximumWarning(), self._logarithmbase)
                return logwarning_max
            else:
                return self.getMaximumWarning() * self._linearfactor
        else:
            return None


    def getScaledValueMaximumAlert(self):
        """
        Return maximum alert value scaled.

        :returns: Value scaled of maximum alert.
        :rtype: int, float
        """
        if not self.getMaximumAlert() is None:
            if self._logarithmscale:
                if self.getMaximumAlert() == 0:
                    logalert_max = log(1, self._logarithmbase)
                if self.getMaximumAlert() > 0:
                    logalert_max = log(self.getMaximumAlert(), self._logarithmbase)
                if self.getMaximumAlert() < 0:
                    logalert_max = -log(-self.getMaximumAlert(), self._logarithmbase)
                return logalert_max
            else:
                return self.getMaximumAlert() * self._linearfactor
        else:
            return None





class QFactoryScale(object):
    """
    Create a generic scaled range.

    Example::

        from rangewidgets import QSaleLevels

        scaledobject = QScaleLevels(1,100,20,80,True,2,True,10,30,70,90)

    :param minimum: The minimum bar value.
    :type minimum: int
    :param maximum: The maximum bar value.
    :type maximum: int
    :param start: The start of range.
    :type start: int
    :param end: The end of range.
    :type end: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    :param levels: Choose levels adding.
    :type  levels: bool
    :param alert_min: Level of minimum alert value.
    :type alert_min: int
    :param warning_min: Level of minimum warning value.
    :type warning_min: int
    :param warning_max: Level of maximum warning value.
    :type  warning_max: int
    :param alert_max: Level of maximum alert value.
    :type alert_max: int
    :returns: A scaled range if levels=False.
    :rtype: QScale or QScaleLevels
    :returns: A scaled range with levels informations if levels=True.
    :rtype: QScaleLevels
    """

    def __new__(cls, minimum=1, maximum=100, start=20, end=80, islogscale=False, logarithmbase=10, levels=None, alert_min=None, warning_min=None, warning_max=None, alert_max=None):
        if levels:
            return QScaleLevels(minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)
        else:
            return QScale(minimum, maximum, start, end, islogscale, logarithmbase)





class BarRange(object):
    """
    .. image:: classes/BarRange.png
        :alt: Diagram
        :align: center
        :height: 550px

    Bar range features objet.

    :param mindraw: value of minimal draw widget.
    :type mindraw: int
    :param maxdraw: value of maximal draw widget.
    :type maxdraw: int
    """

    def __init__(self, mindraw, maxdraw):
        """ Initialize class QProgress. """
        # Initialisation parameters
        self._minimum_draw_plage = mindraw
        self._maximum_draw_plage = maxdraw
        self._offset_minimum_bar = 0
        self._offset_maximum_bar = 0
        self._showoffset = None
        self._show_range_draw = True


    def showOffset(self):
        """ Show offset set """
        self._showoffset = True


    def hideOffset(self):
        """ Hide offset set """
        self._showoffset = False


    def isOffsetShow(self):
        """
        Get state show offset bar into plage.

        :Return: State of show offset bar.
        :rtype: bool
        """
        return self._showoffset


    def showRange(self):
        """ Show range draw """
        self._show_range_draw = True


    def hideRange(self):
        """ Hide range draw """
        self._show_range_draw = False


    def isRangeShow(self):
        """
        Get state show Range draw.

        :Return: State of show draw range.
        :rtype: bool
        """
        return self._show_range_draw


    def setOffsetStartBar(self, offset=None):
        """
        Set start spacer for range progress bar.

        :param offset: value of start spacer for range progress bar.
        :type offset: int
        """
        self._offset_minimum_bar = offset


    def getOffsetStartBar(self):
        """
        Set start spacer from range progress bar.

        :returns: Value of begining spacer bar.
        :rtype: int
        """
        return self._offset_minimum_bar


    def getDrawValueOffsetStartBar(self):
        """
        Get real offset min draw value.

        :returns: Value of real draw begining spacer bar.
        :rtype: int
        """
        if self._showoffset:
            if self._offset_minimum_bar and self._offset_minimum_bar>0:
                offsetmin = self._offset_minimum_bar
            else:
                offsetmin = 0
        else:
            offsetmin = 0
        return offsetmin


    def setOffsetEndBar(self, offset=None):
        """
        Set end spacer for range progress bar.

        :param offset: value of end spacer for range progress bar.
        :type offset: int
        """
        self._offset_maximum_bar = offset


    def getOffsetEndBar(self):
        """
        Get end spacer from range progress bar.

        :returns: Value of ending spacer bar.
        :rtype: int
        """
        return self._offset_maximum_bar


    def getDrawValueOffsetEndBar(self):
        """
        Get real offset max draw value.

        :returns: Value of real draw ending spacer bar.
        :rtype: int
        """
        if self._showoffset:
            if self._offset_maximum_bar and self._offset_maximum_bar>0:
                offsetmax = self._offset_maximum_bar
            else:
                offsetmax = 0
        else:
            offsetmax = 0
        return offsetmax


    def setDrawValueMinimumPlage(self, value):
        """
        Set value of minimum plage draw.

        :paran value: Value of minimum draw.
        :type value: int
        """
        self._minimum_draw_plage = value


    def getDrawValueMinimumPlage(self):
        """
        Get value of minimimum value bar draw.

        :returns: Pixel value of Minimum index plage draw.
        :rtype: int
        """
        return self._minimum_draw_plage


    def getDrawValueStartBar(self):
        """
        Value of start draw bar.

        :returns: Pixel value of start bar.
        :rtype: int
        """
        if self._showoffset:
            return self.getDrawValueMinimumPlage() + self.getDrawValueOffsetStartBar()
        else:
            return self.getDrawValueMinimumPlage()


    def setDrawValueMaximumPlage(self, value):
        """
        Set value of maximum index draw plage.

        :paran value: Value of maximum draw plage.
        :type value: int
        """
        self._maximum_draw_plage = value


    def getDrawValueMaximumPlage(self):
        """
        Get value of maximum index draw plage.

        :returns: Pixel value of maximum index draw plage.
        :rtype: int
        """
        return self._maximum_draw_plage


    def getDrawValueEndBar(self):
        """
        Value of maximum bar draw.

        :returns: Pixel value of maximum bar.
        :rtype: int
        """
        if self._showoffset:
            return self.getDrawValueMaximumPlage() - self.getDrawValueOffsetEndBar()
        else:
            return self.getDrawValueMaximumPlage()


    def getDrawWidthPlage(self):
        """
        Value of pixel draw width plage.

        :returns: Pixel value of width draw plage.
        :rtype: int
        """
        return self.getDrawValueMaximumPlage() - self.getDrawValueMinimumPlage()


    def getDrawWidthBar(self):
        """
        Value of pixel bar width.
        (Need minimum and maximum plage set)

        :returns: Pixel value of width draw bar.
        :rtype: int
        """
        return self.getDrawValueEndBar() - self.getDrawValueStartBar()


    def getDrawValueStartRange(self):
        """
        Value of start all draw range.

        :returns: Real value of start all draw range.
        :rtype: int
        """
        minvalue = self.getMinimum()
        if minvalue is None:
            return None
        startvalue = self.getStart()
        if startvalue is None:
            return None
        endvalue = self.getEnd()
        if endvalue is None:
            return None
        maxvalue = self.getMaximum()
        if maxvalue is None:
            return None

        if not self.isRangeShow() or startvalue > endvalue:
            return None
        else:
            return int(self.getDrawValue(startvalue))


    def getDrawValueEndRange(self):
        """
        Value of end all draw range.

        :returns: Real value of end all draw range.
        :rtype: int
        """
        minvalue = self.getMinimum()
        if minvalue is None:
            return None
        startvalue = self.getStart()
        if startvalue is None:
            return None
        endvalue = self.getEnd()
        if endvalue is None:
            return None
        maxvalue = self.getMaximum()
        if maxvalue is None:
            return None

        if not self.isRangeShow() or startvalue > endvalue:
            return None
        else:
            return int(self.getDrawValue(endvalue))


    def getDrawWidthRange(self):
        """
        Value pixel of width all draw range.

        :returns: Value width of all draw range.
        :rtype: int
        """
        startrange = self.getDrawValueStartRange()
        endrange = self.getDrawValueEndRange()
        if startrange is None or endrange is None:
            return None
        else:
            return endrange - startrange

    def getDrawValue(self, value):
        """
        Get draw value.

        :param value: value to scale draw range.
        :type value: int, float
        :returns: Real draw value.
        :rtype: int, float
        """
        return (self.scaleValue(value) - self.getScaledValueMinimum()) / self.getScaledWidthBar() * self.getDrawWidthBar() + self.getDrawValueStartBar()





class QProgress(QScale, BarRange):
    """
    .. image:: classes/QProgress.png
        :alt: Diagram
        :align: center
        :height: 480px

    Create a progress range object

    Example::

        from rangewidgets import QProgress

        progressobject = QProgress(0, 400,-100,100,-50,50,True,2)

    :param mindraw: The minimum draw widget value.
    :type mindraw: int
    :param maxdraw: The maximum draw widget value.
    :type maxdraw: int
    :param minimum: The minimum plage value.
    :type minimum: int
    :param maximum: The maximum plage value.
    :type maximum: int
    :param start: The start of range.
    :type start: int
    :param end: The end of range.
    :type end: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    """

    def __init__(self, mindraw, maxdraw, minimum, maximum, start, end, islogscale, logarithmbase=10):
        """ Initialize class QProgress. """
        # Initialize QScale
        QScale.__init__(self, minimum, maximum, start, end, islogscale, logarithmbase)
        # Initialize BarRange
        BarRange.__init__(self, mindraw, maxdraw)


    def updateProgress(self, mindraw, maxdraw):
        """
        Update values of progress ranges.

        :param mindraw: value of minimal draw widget.
        :type mindraw: int
        :param maxdraw: value of maximal draw widget.
        :type maxdraw: int
        """
        self.setDrawValueMinimumPlage(mindraw)
        self.setDrawValueMaximumPlage(maxdraw)

        if self.getDrawValueEndRange() < self.getDrawValueStartRange():
            self.hideRange()
        else:
            self.showRange()





class QProgressLevels(QScaleLevels, BarRange):
    """
    .. image:: classes/QProgressLevels.png
        :alt: Diagram
        :align: center
        :height: 850px

    Create a progress range object with levels values.

    Example::

        from rangewidgets import QProgressLevels

        progressobject = QProgressLevels(0, 400,-100,100,-50,50,-90,-70,70,90,True,2)

    :param mindraw: The minimum draw widget value.
    :type mindraw: int
    :param maxdraw: The maximum draw widget value.
    :type maxdraw: int
    :param minimum: The minimum plage value.
    :type minimum: int
    :param maximum: The maximum plage value.
    :type maximum: int
    :param start: The start of range.
    :type start: int
    :param end: The end of range.
    :type end: int
    :param alert_min: Level of minimum alert value.
    :type alert_min: int
    :param warning_min: Level of minimum warning value.
    :type warning_min: int
    :param warning_max: Level of maximum warning value.
    :type  warning_max: int
    :param alert_max: Level of maximum alert value.
    :type alert_max: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    """

    def __init__(self, mindraw, maxdraw, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase):
        """ Initialize class QProgressLevels. """
        # Initialize QScaleLevels
        QScaleLevels.__init__(self, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)
        # Initialize BarRange
        BarRange.__init__(self, mindraw, maxdraw)

        # Initialize state show levels
        self.showLevels()



    def updateProgress(self, mindraw, maxdraw):
        """
        Update values of progress ranges and levels.

        :param mindraw: value of minimal draw widget.
        :type mindraw: int
        :param maxdraw: value of maximal draw widget.
        :type maxdraw: int
        """
        self.setDrawValueMinimumPlage(mindraw)
        self.setDrawValueMaximumPlage(maxdraw)


    def showLevels(self):
        """
        Show levels bar indicators
        """
        self.showWarnings()
        self.showAlerts()
        self._setlevelsshow = True


    def hideLevels(self):
        """
        Hide levels bar indicators
        """
        self.hideWarnings()
        self.hideAlerts()
        self._setlevelsshow = False


    def isLevelsShow(self):
        """
        State of levels show.

        :returns: State levels show from progress bar.
        :rtype: bool
        """
        return self._setlevelsshow


    def showWarnings(self):
        """
        Show warnings levels
        """
        self.showMinimumWarning()
        self.showMaximumWarning()


    def hideWarnings(self):
        """
        Hide warnings levels
        """
        self.hideMinimumWarning()
        self.hideMaximumWarning()


    def isWarningsShow(self):
        """
        State warnings levels show
        """
        if self.isMinimumWarningShow() and self.isMaximumWarningShow():
            return True
        return False


    def showAlerts(self):
        """
        Show alerts levels
        """
        self.showMinimumAlert()
        self.showMaximumAlert()


    def hideAlerts(self):
        """
        Hide alerts levels
        """
        self.hideMinimumAlert()
        self.hideMaximumAlert()


    def isAlertsShow(self):
        """
        State alerts levels show
        """
        if self.isMinimumAlertShow() and self.isMaximumAlertShow():
            return True
        return False


    def showMinimumsLevels(self):
        """
        Show minimums levels
        """
        self.showMinimumWarning()
        self.showMinimumAlert()


    def hideMinimumsLevels(self):
        """
        Hide minimums levels
        """
        self.hideMinimumWarning()
        self.hideMinimumAlert()


    def isMinimumsLevelsShow(self):
        """
        State minimum levels show
        """
        if self.isMinimumAlertShow() and self.isMinimumWarningShow():
            return True
        return False


    def showMaximumsLevels(self):
        """
        Show maximums levels
        """
        self.showMaximumWarning()
        self.showMaximumAlert()


    def hideMaximumsLevels(self):
        """
        Hide maximums levels
        """
        self.hideMaximumWarning()
        self.hideMaximumAlert()


    def isMaximumsLevelsShow(self):
        """
        State maximum levels show
        """
        if self.isMaxnimumAlertDrawShow() and self.isMaximumWarningShow():
            return True
        return False


    def showMinimumAlert(self):
        """
        Show minimum alert
        """
        self._alert_minimum_show = True


    def hideMinimumAlert(self):
        """
        Hide minimum alert
        """
        self._alert_minimum_show = False


    def isMinimumAlertShow(self):
        """
        Set of minimum alert show

        :returns: Set of Minimum alert show
        :rtype: bool
        """
        return self._alert_minimum_show


    def isMinimumAlertActive(self):
        """
        State of minimum alert show

        :returns: State of Minimum alert show
        :rtype: bool
        """
        if not self.getMinimumAlert() is None:
            startscale = self.getScaledValueStart()
            if startscale is None:
                return False
            alertminscale = self.getScaledValueMinimumAlert()
            if startscale <= alertminscale:
                return True
        return False


    def showMinimumWarning(self):
        """
        Show minimum warning level
        """
        self._warning_minimum_show = True


    def hideMinimumWarning(self):
        """
        Hide minimum warning level
        """
        self._warning_minimum_show = False


    def isMinimumWarningShow(self):
        """
        Set of minimum warning show

        :returns: Set of Minimum warning show
        :rtype: bool
        """
        return self._warning_minimum_show


    def isMinimumWarningActive(self):
        """
        State of minimum warning show

        :returns: State of Minimum warning show
        :rtype: bool
        """
        if not self.getMinimumWarning() is None:
            startscale = self.getScaledValueStart()
            if startscale is None:
                return False
            warningminstart = self.getScaledValueMinimumWarning()
            if startscale < warningminstart:
                if self.isMinimumAlertShow() and self.isMinimumAlertActive():
                    endscale = self.getScaledValueEnd()
                    if endscale is None:
                        return False
                    alertminstart = self.getScaledValueMinimumAlert()
                    if endscale > alertminstart:
                        return True
                else:
                    return True
        return False

    def showMaximumWarning(self):
        """
        Show maximum warning levela
        """
        self._warning_maximum_show = True


    def hideMaximumWarning(self):
        """
        Hide maximum warning level
        """
        self._warning_maximum_show = False


    def isMaximumWarningShow(self):
        """
        Set of maximum warning show

        :returns: Set of Maximum warning show
        :rtype: bool
        """
        return self._warning_maximum_show


    def isMaximumWarningActive(self):
        """
        State of maximum warning show

        :returns: State of Maximum warning show
        :rtype: bool
        """
        if not self.getMaximumWarning() is None:
            endscale = self.getScaledValueEnd()
            if endscale is None:
                return False
            warningmaxstart = self.getScaledValueMaximumWarning()
            if endscale > warningmaxstart:
                if self.isMaximumAlertShow() and self.isMaximumAlertActive():
                    startscale = self.getScaledValueStart()
                    if startscale is None:
                        return False
                    alertmaxstart = self.getScaledValueMaximumAlert()
                    if startscale < alertmaxstart:
                        return True
                else:
                    return True
        return False

    def showMaximumAlert(self):
        """
        Show maximum alert
        """
        self._alert_maximum_show = True


    def hideMaximumAlert(self):
        """
        Hide maximum alert
        """
        self._alert_maximum_show = False


    def isMaximumAlertShow(self):
        """
        Set of maximum alert show

        :returns: Set of Maximum alert show
        :rtype: bool
        """
        return self._alert_maximum_show


    def isMaximumAlertActive(self):
        """
        State of maximum alert show

        :returns: State of Maximum alert show
        :rtype: bool
        """
        if not self.getMaximumAlert() is None:
            endscale = self.getScaledValueEnd()
            if endscale is None:
                return False
            alertmaxscale = self.getScaledValueMaximumAlert()
            if endscale >= alertmaxscale:
                return True
        return False

    def getDrawValueStartNormal(self):
        """
        Value of start normal draw range.

        :returns: Real value of start normal draw range.
        :rtype: int
        """
        minvalue = self.getMinimum()
        if minvalue is None:
            return None
        startvalue = self.getStart()
        if startvalue is None:
            return None
        endvalue = self.getEnd()
        if endvalue is None:
            return None
        maxvalue = self.getMaximum()
        if maxvalue is None:
            return None

        if not self.isRangeShow() or startvalue > endvalue:
            return None
        else:
            warningminvalue = self.getMinimumWarning()
            if (not warningminvalue is None) and startvalue > warningminvalue:
                if self.isMaximumWarningShow():
                    if startvalue >= self.getMaximumWarning():
                        return None
                if self.isMaximumAlertShow():
                    if startvalue >= self.getMaximumAlert():
                        return None
                if startvalue >= maxvalue:
                    return int(self.getDrawValue(maxvalue))
            else:
                alertminvalue = self.getMinimumAlert()
                if not (alertminvalue is None):
                    if startvalue > alertminvalue:
                        if (not warningminvalue is None) and self.isMinimumWarningShow():
                            if endvalue > warningminvalue:
                                return int(self.getDrawValue(warningminvalue))
                            return None
                    else:
                        if (not warningminvalue is None) and self.isMinimumWarningShow():
                            if endvalue > warningminvalue:
                                return int(self.getDrawValue(warningminvalue))
                            return None
                        if self.isMinimumAlertShow():
                            if endvalue > alertminvalue:
                                return int(self.getDrawValue(alertminvalue))
                            return None
                else:
                    if (not warningminvalue is None) and startvalue < warningminvalue:
                        return int(self.getDrawValue(warningminvalue))
            return int(self.getDrawValue(startvalue))


    def getDrawValueEndNormal(self):
        """
        Value of end normal draw range.

        :returns: Real value of end normal draw range.
        :rtype: int
        """
        minvalue = self.getMinimum()
        if minvalue is None:
            return None
        startvalue = self.getStart()
        if startvalue is None:
            return None
        endvalue = self.getEnd()
        if endvalue is None:
            return None
        maxvalue = self.getScaledValueMaximum()
        if maxvalue is None:
            return None

        if not self.isRangeShow() or startvalue > endvalue:
            return None
        else:
            warningmaxvalue = self.getMaximumWarning()
            if (not warningmaxvalue is None) and endvalue < warningmaxvalue:
                if self.isMinimumWarningShow():
                    if endvalue <= self.getMinimumWarning():
                        return None
                if self.isMinimumAlertShow():
                    if endvalue <= self.getMinimumAlert():
                        return None
                if endvalue <= minvalue:
                    return int(self.getDrawValue(minvalue))
            else:
                alertmaxvalue = self.getMaximumAlert()
                if not alertmaxvalue is None:
                    if endvalue < alertmaxvalue:
                        if self.isMaximumWarningShow():
                            if startvalue < warningmaxvalue:
                                return int(self.getDrawValue(warningmaxvalue))
                            return None
                    else:
                        if  (not warningmaxvalue is None) and self.isMaximumWarningShow():
                            if startvalue < warningmaxvalue:
                                return int(self.getDrawValue(warningmaxvalue))
                            return None
                        if  self.isMaximumAlertShow():
                            if startvalue < alertmaxvalue:
                                return int(self.getDrawValue(alertmaxvalue))
                            return None
                else:
                    if (not warningmaxvalue is None) and endvalue > warningmaxvalue:
                        return int(self.getDrawValue(warningmaxvalue))
            return int(self.getDrawValue(endvalue))


    def getDrawWidthNormal(self):
        """
        Value pixel of normal draw range.

        :returns: Value width of normal draw range.
        :rtype: int
        """
        startrange = self.getDrawValueStartNormal()
        endrange = self.getDrawValueEndNormal()
        if startrange is None or endrange is None:
            return None
        else:
            return endrange - startrange


    def getDrawValueStartMinimumAlert(self):
        """
        Value pixel of start minimum alert draw range.

        :returns: Real value of start minimum alert draw range.
        :rtype: int
        """
        if self.isMinimumAlertShow():
            if self.isMinimumAlertActive():
                startvalue = self.getStart()
                minimimum = self.getMinimum()
                if minimimum is None:
                    return None
                if startvalue > minimimum:
                    return int(self.getDrawValue(startvalue))
                else:
                    return int(self.getDrawValue(minimimum))
        return None


    def getDrawWidthMinimumAlert(self):
        """
        Value pixel of width minimum alert draw range.

        :returns: Real value of width minimum alert draw range.
        :rtype: int
        """
        alertminstart = self.getDrawValueStartMinimumAlert()
        if not alertminstart is None:
            alertminvalue = self.getMinimumAlert()
            if alertminvalue is None:
                return None
            endvalue = self.getEnd()
            if endvalue is None:
                return None
            if endvalue < alertminvalue:
                minimum = self.getScaledValueMinimum()
                if endvalue > minimum:
                    return int(self.getDrawValue(endvalue) - alertminstart)
                else:
                    return int(self.getDrawValue(minimum) - alertminstart)
            else:
                return int(self.getDrawValue(alertminvalue) - alertminstart)
        return None


    def getDrawValueStartMinimumWarning(self):
        """
        Value pixel of start draw minimum warning draw range.

        :returns: Real value of start minimum warning draw range.
        :rtype: int
        """
        if self.isMinimumWarningShow():
            if self.isMinimumWarningActive():
                if self.isMinimumAlertShow():
                    if self.isMinimumAlertActive():
                        return int(self.getDrawValue(self.getMinimumAlert()))
                return int(self.getDrawValue(self.getStart()))
        return None


    def getDrawWidthMinimumWarning(self):
        """
        Value pixel of width minimum warning draw range.

        :returns: Real value of minimum warning width draw range.
        :rtype: int
        """
        warningminstart = self.getDrawValueStartMinimumWarning()
        if not warningminstart is None:
            endvalue = self.getEnd()
            warningmin = self.getMinimumWarning()
            if endvalue > warningmin:
                return int(self.getDrawValue(warningmin) - warningminstart)
            else:
                return int(self.getDrawValue(endvalue) - warningminstart)
        return None


    def getDrawValueStartMaximumWarning(self):
        """
        Value pixel of start maximum warning draw range.

        :returns: Real value of start maximum warning draw range.
        :rtype: int
        """
        if self.isMaximumWarningShow():
            if self.isMaximumWarningActive():
                startvalue = self.getStart()
                warningmax = self.getMaximumWarning()
                if startvalue < warningmax:
                    return int(self.getDrawValue(warningmax))
                else:
                    return int(self.getDrawValue(startvalue))
        return None


    def getDrawWidthMaximumWarning(self):
        """
        Value pixel of width maximum warning draw range.

        :returns: Real value of maximum warning width draw range.
        :rtype: int
        """
        warningmaxstart = self.getDrawValueStartMaximumWarning()
        if not warningmaxstart is None:
            if not self.getDrawValueStartMaximumAlert() is None:
                return int(self.getDrawValue(self.getMaximumAlert()) - warningmaxstart)
            return int(self.getDrawValue(self.getEnd()) - warningmaxstart)
        return None


    def getDrawValueStartMaximumAlert(self):
        """
        Value pixel of start maximum alert draw range.

        :returns: Real value of start maximum alert draw range.
        :rtype: int
        """
        if self.isMaximumAlertShow():
            if self.isMaximumAlertActive():
                maximimum = self.getMaximum()
                alertmax = self.getMaximumAlert()
                startvalue = self.getStart()
                if startvalue > alertmax:
                    if startvalue < maximimum:
                        return int(self.getDrawValue(startvalue))
                    else:
                        return int(self.getDrawValue(maximimum))
                else:
                    return int(self.getDrawValue(alertmax))
        return None


    def getDrawWidthMaximumAlert(self):
        """
        Value pixel of width maximum alert draw range.

        :returns: int -- initiatial default value None.
        """
        alertmaxstart = self.getDrawValueStartMaximumAlert()
        if not alertmaxstart is None:
            endvalue = self.getEnd()
            maximum = self.getMaximum()
            if endvalue < maximum:
                return int(self.getDrawValue(endvalue) - alertmaxstart)
            else:
                return int(self.getDrawValue(maximum) - alertmaxstart)
        return None





class QFactoryProgress():
    """
    Create a generic progress range.

    Example::

        from rangewidgets import QFactoryProgress

        progressobject = QFactoryProgress(0, 400,-100,100,-50,50,True,2,True,-90,-70,70,90)

    :param mindraw: The minimum draw widget value.
    :type mindraw: int
    :param maxdraw: The maximum draw widget value.
    :type maxdraw: int
    :param minimum: The minimum plage value.
    :type minimum: int
    :param maximum: The maximum plage value.
    :type maximum: int
    :param start: The start of range.
    :type start: int
    :param end: The end of range.
    :type end: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    :param levels: Choose levels adding.
    :type  levels: bool
    :param alert_min: Level of minimum alert value.
    :type alert_min: int
    :param warning_min: Level of minimum warning value.
    :type warning_min: int
    :param warning_max: Level of maximum warning value.
    :type  warning_max: int
    :param alert_max: Level of maximum alert value.
    :type alert_max: int
    :returns: A range progress if levels=False.
    :rtype: QProgress
    :returns: A range progress with levels informations if levels=True.
    :rtype: QProgressLevels
    """

    def __new__(cls, mindraw=0, maxdraw=400, minimum=1, maximum=100, start=20, end=80, islogscale=False, logarithmbase=10, levels=None, alert_min=None, warning_min=None, warning_max=None, alert_max=None):
        if levels:
            return QProgressLevels(mindraw, maxdraw, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)
        else:
            return QProgress(mindraw, maxdraw, minimum, maximum, start, end, islogscale, logarithmbase)





class TextsBar():
    """
    .. image:: classes/TextsBar.png
        :alt: Diagram
        :align: center
        :height: 600px

    Texts extremums bar features object.
    """

    def __init__(self):
        """ Initialize class DrawTextsExtremums. """
        self.setMinimumText()
        self.setMaximumText()
        self.setMinimumTextColor()
        self.setMaximumTextColor()
        self.setMinimumLabelPositionCenter()
        self.setMaximumLabelPositionCenter()
        self.setMinimumLabelReadingReverse()
        self.setMaximumLabelReadingReverse()
        self.setExtremumTextsFont()


    def showExtremumsTexts(self):
        """
        .. image:: pictures/QRangeProgressBar_HideRangeTexts.png
            :alt: Show extremum text with range text example
            :align: center
            :width: 600px
            :height: 70px

        Show text of maximum/minimum progress bar.
        """
        self._showextremumstexts = True


    def hideExtremumsTexts(self):
        """
        .. image:: pictures/QRangeProgressBar_HideRangeExtremums.png
            :alt: hide extremum text example width hideRangeTexts()
            :align: center
            :width: 600px
            :height: 70px

        Hide text of maximum/minimum progress bar.

        .. image:: pictures/QRangeProgressBar_HideExtremumsTexts.png
            :alt: Hide extremum text example
            :align: center
            :width: 600px
            :height: 70px
            
        or with showRangeTexts():
        """
        self._showextremumstexts = False


    def isExtremumsTextsShow(self):
        """
        :returns: State maximum/minimum labels show from progress bar.
        :rtype: bool
        """
        return self._showextremumstexts


    def setMinimumText(self, text=None):
        """
        Set fixed text for minimum progress bar.

        :param text: Text of minimum progress bar.
        :type text: str
        """
        self._textmin = text


    def getMinimumText(self):
        """
        :returns: Get fixed text of minimum progress bar.
        :rtype: string
        """
        return self._textmin


    def setMaximumText(self, text=None):
        """
        Set fixed text for minimum progress bar.

        :param text: Text of maximum progress bar.
        :type text: str
        """
        self._textmax = text


    def getMaximumText(self):
        """
        :returns: Get fixed text of minimum progress bar.
        :rtype: string
        """
        return self._textmax


    def setMinimumLabelPositionTop(self):
        """
        .. image:: pictures/QRangeProgressBar_LabelMinPositionTop.png
            :alt: Set label minimum progress to top position
            :align: center
            :height: 100px

        Set label minimum progress bar position top.
        """
        self._textminposition = 'TOP'


    def setMaximumLabelPositionTop(self):
        """
        .. image:: pictures/QRangeProgressBar_LabelMaxPositionTop.png
            :alt: Set label maximum progress to top position
            :align: center
            :height: 100px

        Set label maximum progress bar position top.
        """
        self._textmaxposition = 'TOP'


    def setMinimumLabelPositionCenter(self):
        """
        .. image:: pictures/QRangeProgressBar_LabelMinPositionCenter.png
            :alt: Set label minimum progress to center position
            :align: center
            :height: 100px

        Set label minimum progress bar position center.
        """
        self._textminposition = 'CENTER'

    def setMaximumLabelPositionCenter(self):
        """
        .. image:: pictures/QRangeProgressBar_LabelMaxPositionCenter.png
            :alt: Set label maximum progress to center position
            :align: center
            :height: 100px

        Set label maximum progress bar position center.
        """
        self._textmaxposition = 'CENTER'


    def setMinimumLabelPositionBottom(self):
        """
        .. image:: pictures/QRangeProgressBar_LabelMinPositionBottom.png
            :alt: Set label minimum progress to bottom position
            :align: center
            :height: 100px

        Set label minimum progress bar position bottom.
        """
        self._textminposition = 'BOTTOM'


    def setMaximumLabelPositionBottom(self):
        """
        .. image:: pictures/QRangeProgressBar_LabelMaxPositionBottom.png
            :alt: Set label maximum progress to bottom position
            :align: center
            :height: 100px

        Set label maximum progress bar position bottom.
        """
        self._textmaxposition = 'BOTTOM'


    def setMinimumLabelReadingReverse(self, revert=False):
        """
        .. image:: pictures/QRangeProgressBar_LabelMinReadingReverse.png
            :alt: Reverse reading direction of label minimum
            :align: center
            :height: 80px

        Reverse the reading direction of label minimum.

        :param revert: state of the inversion of the reading direction.
        :type revert: bool
        """
        self._textminreadingreverse = revert


    def setMaximumLabelReadingReverse(self, revert=False):
        """
        .. image:: pictures/QRangeProgressBar_LabelMaxReadingReverse.png
            :alt: Reverse reading direction of label maximum
            :align: center
            :height: 80px

        Reverse the reading direction of label maximum.

        :param revert: state of the inversion of the reading direction.
        :type revert: bool
        """
        self._textmaxreadingreverse = revert


    def isMinimumLabelReadingReverse(self):
        """
        .. image:: pictures/QRangeProgressBar_LabelMinReadingReverse.png
            :alt: Get boolean reading direction of label minimum
            :align: center
            :height: 80px

        :returns: State of reverse reading direction for label minimum.
        :rtype: bool
        """
        return self._textminreadingreverse


    def isMaximumLabelReadingReverse(self):
        """
        .. image:: pictures/QRangeProgressBar_LabelMaxReadingReverse.png
            :alt: Get boolean state direction of label maximum
            :align: center
            :height: 80px

        :returns: State of reverse reading direction for label maximum.
        :rtype: bool
        """
        return self._textmaxreadingreverse


    def setExtremumTextsFont(self, font=QFont('Serif', 15, QFont.Light)):
        """
        Set text extremum font.

        :param font: font object of minimum and maximum text.
        :type font: QFont
        """
        self._extremumfont = font
        self._minimumfont = QFont(font)
        self._maximumfont = QFont(font)


    def getExtremumTextsFont(self):
        """
        :returns: Get text extremum font.
        :rtype: QFont
        """
        return self._extremumfont


    def setExtremumTextsFontSize(self, size=10):
        """
        Set text extremum font size.

        :param size: size of minimum and maximum text.
        :type size: int
        """
        self._extremumfont.setPointSize(size)
        self._minimumfont.setPointSize(size)
        self._maximumfont.setPointSize(size)


    def getExtremumTextsFontSize(self):
        """
        :returns: Get text extremum font size.
        :rtype: int
        """
        return self._extremumfont.pointSize()


    def setMinimumTextColor(self, color=QColor(255, 184, 184)):
        """
        .. image:: pictures/QRangeProgressBar_ColorTextMinimum.png
            :alt: set minimum color text
            :align: center

        Set color Text minimum progress bar.

        :param color: color object of minimum progress bar text.
        :type color: QColor
        """
        self._colortextmin = color


    def setMaximumTextColor(self, color=QColor(184, 184, 255)):
        """
        .. image:: pictures/QRangeProgressBar_ColorTextMaximum.png
            :alt: set maximum color text
            :align: center

        Set color Text maximum progress bar.

        :param color: color object of maximum progress bar text.
        :type color: QColor
        """
        self._colortextmax = color


    def getMinimumTextColor(self):
        """
        .. image:: pictures/QRangeProgressBar_ColorTextMinimum.png
            :alt: set minimum color text
            :align: center

        :returns: Get color Text minimum plage.
        :rtype: QColor
        """
        return self._colortextmin


    def getMaximumTextColor(self):
        """
        .. image:: pictures/QRangeProgressBar_ColorTextMaximum.png
            :alt: get maximum color text
            :align: center

        :returns: Get color Text maximum plage.
        :rtype: QColor
        """
        return self._colortextmax





class TextsRange():
    """
    .. image:: classes/TextsRange.png
        :alt: Diagram
        :align: center
        :height: 900px

    Texts range features object.
    """

    def __init__(self):
        """ Initialize class DrawTextsRange. """
        self.setStartText()
        self.setEndText()
        self.setRangeTextInside()
        self.setStartTextSpacer()
        self.setEndTextSpacer()
        self.setStartTextColor()
        self.setEndTextColor()
        self.setStartLabelPositionMiddle()
        self.setEndLabelPositionMiddle()
        self.setRangeMinimumTextsSpacer()
        self.setRangeTextsFont()
        self.setRangeTextsScale()


    def showRangeTexts(self):
        """
        .. image:: pictures/QRangeProgressBar_HideExtremumsTexts.png
            :alt: Show range text example
            :align: center
            :width: 600px
            :height: 70px

        Show text of range.
        """
        self._showrangetext = True


    def hideRangeTexts(self):
        """
        .. image:: pictures/QRangeProgressBar_HideRangeExtremums.png
            :alt: hide range text example with hideExtremumsTexts()
            :align: center
            :width: 600px
            :height: 70px

        Hide text of range.

        .. image:: pictures/QRangeProgressBar_HideRangeTexts.png
            :alt: Hide range text example
            :align: center
            :width: 600px
            :height: 70px

        or with showExtremumsTexts():
        """
        self._showrangetext = False


    def isRangeTextsShow(self):
        """
        :returns: State of range texts show.
        :rtype: bool
        """
        return self._showrangetext


    def setRangeTextInside(self):
        """ Set range text inside position. """
        self._range_text_inside = True


    def isRangeTextsInside(self):
        """
        :returns: Range text is position inside.
        :rtype: bool
        """
        return self._range_text_inside


    def setRangeTextsOutside(self):
        """ Set range text inside position. """
        self._range_text_inside = False


    def isRangeTextsOutside(self):
        """
        :returns: Range text is position outside.
        :rtype: bool
        """
        return not self._range_text_inside


    def setStartText(self, text=None):
        """
        Set fixed text for start range progress.

        :param text: Text of start range progress
        :type text: str
        """
        self._textstart = text


    def setEndText(self, text=None):
        """
        Set fixed text for end range progress.

        :param text: Text of end range progress
        :type text: str
        """
        self._textend = text


    def getStartText(self):
        """
        :returns: Get fixed text of start range progress.
        :rtype: string
        """
        return self._textstart


    def getEndText(self):
        """
        :returns: Get fixed text of end range progress.
        :rtype: string
        """
        return self._textend


    def setStartLabelPositionTop(self):
        """
        Set start label to top position.
        """
        self._startlabelposition = 'TOP'


    def setEndLabelPositionTop(self):
        """
        Set end label to top position.
        """
        self._endlabelposition = 'TOP'


    def setStartLabelPositionMiddleTop(self):
        """
        Set start label to middle top position.
        """
        self._startlabelposition = 'MIDDLE-TOP'


    def setEndLabelPositionMiddleTop(self):
        """
        Set end label to middle top position.
        """
        self._endlabelposition = 'MIDDLE-TOP'


    def setStartLabelPositionMiddle(self):
        """
        Set start label to middle position.
        """
        self._startlabelposition = 'MIDDLE'


    def setEndLabelPositionMiddle(self):
        """
        Set end label to middle position.
        """
        self._endlabelposition = 'MIDDLE'


    def setStartLabelPositionMiddleBottom(self):
        """
        Set start label to middle bottom position.
        """
        self._startlabelposition = 'MIDDLE-BOTTOM'


    def setEndLabelPositionMiddleBottom(self):
        """
        Set end label to middle bottom position.
        """
        self._endlabelposition = 'MIDDLE-BOTTOM'


    def setStartLabelPositionBottom(self):
        """
        Set start label to bottom position.
        """
        self._startlabelposition = 'BOTTOM'


    def setEndLabelPositionBottom(self):
        """
        Set end label to bottom position.
        """
        self._endlabelposition = 'BOTTOM'


    def getStartLabelPosition(self):
        """
        Get start label position.

        :returns: Start label position.
        :rtype: string
        """
        return self._startlabelposition


    def getEndLabelPosition(self):
        """
        Get end label position.

        :returns: End label position.
        :rtype: string
        """
        return self._endlabelposition


    def setStartTextSpacer(self, value=1):
        """
        Set spacer between start label and start range bar.

        :param value: Value of spacer.
        :type value: int
        """
        self._text_spacer_start = value


    def setEndTextSpacer(self, value=1):
        """
        Set spacer between end label and end range bar.

        :param value: Value of spacer.
        :type value: int
        """
        self._text_spacer_end = value


    def getStartTextSpacer(self):
        """
        :returns: Space between start label and start range bar.
        :rtype: int
        """
        return self._text_spacer_start


    def getEndTextSpacer(self):
        """
        :returns: Space between end label and end range bar.
        :rtype: int
        """
        return self._text_spacer_end


    def setStartTextColor(self, color=QColor(0, 100, 0)):
        """
        .. image:: pictures/QRangeProgressBar_ColorTextStart.png
            :alt: set color text start of range
            :align: center
            :height: 95px

        Set color Text start range.

        :param color: color object of start range text.
        :type color: QColor
        """
        self._colortextstart = color


    def setEndTextColor(self, color=QColor(100, 0, 0)):
        """
        .. image:: pictures/QRangeProgressBar_ColorTextEnd.png
            :alt: set color text end of range
            :align: center
            :height: 95px

        Set color Text end range.

        :param color: color object of end range text.
        :type color: QColor
        """
        self._colortextend = color


    def getStartTextColor(self):
        """
        .. image:: pictures/QRangeProgressBar_ColorTextStart.png
            :alt: get color text start of range
            :align: center
            :height: 95px

        Get color Text start range.
        """
        return self._colortextstart


    def getEndTextColor(self):
        """
        .. image:: pictures/QRangeProgressBar_ColorTextEnd.png
            :alt: get color text end of range
            :align: center
            :height: 95px

        Get color Text end range.
        """
        return self._colortextend


    def setRangeMinimumTextsSpacer(self, value=1):
        """
        Set spacer between labels into range bar.

        :param value: Value of spacer.
        :type value: int
        """
        self._min_range_texts_spacer = value


    def getRangeMinimumTextsSpacer(self):
        """
        :returns: Space between labels into range bar.
        :rtype: int
        """
        return self._min_range_texts_spacer


    def setRangeTextsFont(self, font=QFont('Serif', 10, QFont.Bold)):
        """
        Set text range font.

        :param font: font object of start and end range text
        :type font: QFont
        """
        self._rangefont = font


    def getRangeTextsFont(self):
        """
        :returns: Range text font.
        :rtype: Qfont
        """
        return self._rangefont


    def setRangeTextsFontSize(self, size=10):
        """
        Set size range text font.

        :param size: size of start and end range text font.
        :type size: int
        """
        self._rangefont.setPointSize(size)


    def getRangeTextsFontSize(self):
        """
        :returns: Text range font size.
        :rtype: int
        """
        self._rangefont.pointSize()


    def setRangeTextsScale(self, scale=0.4):
        """
        Set scale of range text.

        :param scale: scale of range text. 1.0 ≥ scale ≥ 0.01.
        :type scale: float
        """
        if scale > 1.0:
            scale = 1.0
        if scale <= 0.01:
            scale = 0.01
        self._scalerangetext = scale


    def getRangeTextsScale(self):
        """
        :returns: Scale range text (between 0.0 to 1.0).
        :rtype: float
        """
        return self._scalerangetext





class DrawTexts(TextsBar, TextsRange):
    """
    .. image:: classes/DrawTexts.png
        :alt: Diagram
        :align: center
        :height: 900px

    Draw texts object.
    """

    def __init__(self):
        """ Initialize class DrawText. """
        # Initialize BarRange
        TextsBar.__init__(self)
        # Initialize BarRange
        TextsRange.__init__(self)

        # State progress bar properties default
        self._showtext = False
        self.saveminimumfontsize = None
        self.savemaximumfontsize = None

        # Initialize properties
        self.showExtremumsTexts()
        self.showRangeTexts()
        self.showTexts()


    def showTexts(self):
        """
        .. image:: pictures/QRangeProgressBar.png
            :alt: Show text example
            :align: center
            :width: 600px
            :height: 60px

        Show text of progress bar.
        """
        self._showtext = True


    def hideTexts(self):
        """
        .. image:: pictures/QRangeProgressBar_LevelValues.png
            :alt: Hide text example
            :align: center
            :width: 600px
            :height: 60px

        Hide text of progress bar.
        """
        self._showtext = False


    def isTextsShow(self):
        """
        :returns: State text show from progress bar.
        :rtype: bool
        """
        return self._showtext


    def calculateMinimumLabelPosition(self, qp, height):
        """
        Caculate the position of minimum label.

        :param qp: object of bar draw.
        :type qp: QPainter
        :param height: height of widget.
        :type height: int
        :returns: property labelminxposition: horizontal position of minimum label.
        :rtype: int
        :returns: property labelminyposition: vertical position of minimum label.
        :rtype: int
        :returns: property textmin: Text of minimum label.
        :rtype: string
        """
        qp.setFont(self._minimumfont)
        metrics = qp.fontMetrics()
        fh = metrics.height()
        # Set horizontal position minimum label
        # Min label position
        if self._offset_minimum_bar:
            if ((self._offset_minimum_bar - self._extremumfont.pointSize() + 1)/2 - 2) < 0:
                if not self.saveminimumfontsize:
                    self.saveminimumfontsize = self._minimumfont.pointSize()
                self._minimumfont.setPointSize(self._offset_minimum_bar-2)
                qp.setFont(self._minimumfont)
                metrics = qp.fontMetrics()
                fh = metrics.height()
            else:
                if self.saveminimumfontsize:
                    self._extremumfont.setPointSize(self.saveminimumfontsize)
                    self.saveminimumfontsize = None
        else:
            self._offset_minimum_bar = self.getExtremumTextsFontSize()+2
        labelminxposition = (self._offset_minimum_bar)/2 - (2*int(self._textminreadingreverse) -1) * self._minimumfont.pointSize()/2 -1
        # Set vertical position minimum label
        if self._textmin:
            textmin = str(self._textmin)
            fw = metrics.width(textmin)
        else:
            textmin = str(self.getMinimum())
            fw = metrics.width(textmin)
        # Set text color
        pen = QPen(self._colortextmin, 1, Qt.SolidLine)
        qp.setPen(pen)
        # Set position label minimum
        if self._textminposition == 'TOP':
            labelminyposition = int(not(self._textminreadingreverse))*fw
        elif self._textminposition == 'CENTER':
            labelminyposition = (height- int(self._textminreadingreverse)*fw)/2 + int(not(self._textminreadingreverse))*fw/2
        elif self._textminposition == 'BOTTOM':
            labelminyposition = height - int(self._textminreadingreverse)*fw
        return labelminxposition, labelminyposition, textmin


    def calculateMaximumLabelPosition(self, qp, height):
        """
        Caculate the position of maximum label.

        :param qp: object of bar draw.
        :type qp: QPainter
        :param height: height of widget.
        :type height: int
        :returns: property labelmaxxposition: horizontal position of maximum label.
        :rtype: int
        :returns: property labelmaxyposition: vertical position of maximum label.
        :rtype: int
        :returns: property textmax: Text of maximum label.
        :rtype: string
        """
        qp.setFont(self._maximumfont)
        metrics = qp.fontMetrics()
        fh = metrics.height()
        # Set horizontal position maxnimum label
        # Max label position
        if self._offset_maximum_bar:
            if ((self._offset_maximum_bar - self._extremumfont.pointSize() + 1)/2 - 2) < 0:
                if not self.savemaximumfontsize:
                    self.savemaximumfontsize = self._maximumfont.pointSize()
                self._maximumfont.setPointSize(self._offset_maximum_bar-2)
                qp.setFont(self._maximumfont)
                metrics = qp.fontMetrics()
                fh = metrics.height()
            else:
                if self.savemaximumfontsize:
                    self._extremumfont.setPointSize(self.savemaximumfontsize)
                    self.savemaximumfontsize = None
        else:
            self._offset_maximum_bar = self.getExtremumTextsFontSize()+2
        labelmaxxposition = self.getWidth() - (1-int(self._textmaxreadingreverse))*(self._offset_maximum_bar-fh/2)/2  - int(self._textmaxreadingreverse)*(self._offset_maximum_bar+fh/2)/2 - (2*int(self._textmaxreadingreverse) -1)

        # Set maximum label vertical position
        if self._textmax:
            textmax = str(self._textmax)
            fw = metrics.width(textmax)
        else:
            textmax = str(self.getMaximum())
            fw = metrics.width(textmax)
        fh = metrics.height()
        # Set text color
        pen = QPen(self._colortextmax, 1, Qt.SolidLine) # Color text
        qp.setPen(pen)
        # Set position label maximum
        if self._textmaxposition == 'TOP':
            labelmaxyposition = int(not(self._textmaxreadingreverse))*fw
        elif self._textmaxposition == 'CENTER':
            labelmaxyposition = (height- int(self._textmaxreadingreverse)*fw)/2 + int(not(self._textmaxreadingreverse))*fw/2
        elif self._textmaxposition == 'BOTTOM':
            labelmaxyposition = height - int(self._textmaxreadingreverse)*fw
        return labelmaxxposition, labelmaxyposition, textmax


    def scaleText(self, qp, text, height):
        """
        Scale a text for a box draw

        :param qp: QPainter object drawing.
        :type qp: QPainter
        :param text: text for scale calculate
        :type text: string
        :param height: height of box draw text.
        :type height: int
        :returns: textwidth -- width of text.
        :rtype: int
        :returns: textheight -- heigth of text.
        :rtype: int
        """
        # Calculate scale point size
        font = QFont(self._rangefont)
        font.setBold(True)
        textpointsize = font.pointSize()
        if textpointsize >= 0:
            qp.setFont(font)
            metrics = qp.fontMetrics()
            textwidth = metrics.width(text)
            while height <= metrics.height():
                textpointsize += -1
                if textpointsize < 0:
                    textpointsize += 1
                    break
                font.setPointSizeF(textpointsize)
                qp.setFont(font)
                metrics = qp.fontMetrics()
                textwidth = metrics.width(text)
            while height > metrics.height():
                textpointsize += 1
                font.setPointSizeF(textpointsize)
                qp.setFont(font)
                metrics = qp.fontMetrics()
                textwidth = metrics.width(text)
            textheight = metrics.height()

        return int(textwidth) * 1.23, int(textheight), int(textpointsize)


    def calculateRangeLabelsPositions(self, qp, height):
        """
        Calculate horizontal values of range labels.

        :param qp: QPainter object drawing.
        :type qp: QPainter
        :param height: Height of QPainter drawing.
        :type height: int
        :returns: horizontalpositionstartlabel -- Horizontal position of start label.
        :rtype: int
        :returns: verticalpositionstartlabel -- vertical position of start label.
        :rtype: int
        :returns: pointsizestarttext -- Size of start text.
        :rtype: int
        :returns: horizontalpositionendlabel -- Horizontal position of end label.
        :rtype: int
        :returns: verticalpositionendlabel -- Vertical position of end label.
        :rtype: int
        :returns: pointsizeendtext -- Size of end text.
        :rtype: int
        """

        # Set texts range labels
        if self._textstart:
            labelstart = str(self._textstart)
        else:
            labelstart = str(self.getStart())
        if self._textend:
            labelend = str(self._textend)
        else:
            labelend = str(self.getEnd())

        # Set dimensions labels
        #scaleTextStart = False
        #scaleTextEnd = False

        # Calculate verticals positions range texts
        # Start text
        if self._startlabelposition == 'TOP':
            # Start text dimensions
            (widthstartlabel, heightstartlabel, pointsizestarttext) = self.scaleText(qp, labelstart, height)
            verticalpositionstartlabel = heightstartlabel * 0.775 + 2
        if self._startlabelposition == 'MIDDLE-TOP':
            # Start text dimensions
            (widthstartlabel, heightstartlabel, pointsizestarttext) = self.scaleText(qp, labelstart, height/2)
            verticalpositionstartlabel = (height/2 - heightstartlabel * 0.775)/2 + heightstartlabel * 0.775
        if self._startlabelposition == 'MIDDLE':
            # Start text dimensions
            (widthstartlabel, heightstartlabel, pointsizestarttext) = self.scaleText(qp, labelstart, height)
            verticalpositionstartlabel = (height - heightstartlabel * 0.775)/2 + heightstartlabel * 0.775
        if self._startlabelposition == 'MIDDLE-BOTTOM':
            # Start text dimensions
            (widthstartlabel, heightstartlabel, pointsizestarttext) = self.scaleText(qp, labelstart, height/2)
            verticalpositionstartlabel = height/2 + (height/2 - heightstartlabel * 0.775)/2 + heightstartlabel * 0.775
        if self._startlabelposition == 'BOTTOM':
            # Start text dimensions
            (widthstartlabel, heightstartlabel, pointsizestarttext) = self.scaleText(qp, labelstart, height)
            verticalpositionstartlabel = height-2
        pointsizestarttext = heightstartlabel

        # End text
        if self._endlabelposition == 'TOP':
            # End text dimensions
            (widthendlabel, heightendlabel, pointsizeendtext) = self.scaleText(qp, labelend, height)
            verticalpositionendlabel = heightendlabel * 0.775 + 2
        if self._endlabelposition == 'MIDDLE-TOP':
            (widthendlabel, heightendlabel, pointsizeendtext) = self.scaleText(qp, labelend, height/2)
            verticalpositionendlabel =  (height/2 - heightendlabel * 0.775)/2 +  heightendlabel * 0.775
        if self._endlabelposition == 'MIDDLE':
            (widthendlabel, heightendlabel, pointsizeendtext) = self.scaleText(qp, labelend, height)
            verticalpositionendlabel = (height - heightendlabel * 0.775)/2 + heightendlabel * 0.775
        if self._endlabelposition == 'MIDDLE-BOTTOM':
            (widthendlabel, heightendlabel, pointsizeendtext) = self.scaleText(qp, labelend, height/2)
            verticalpositionendlabel = height/2 + (height/2 - heightendlabel * 0.775)/2 + heightendlabel * 0.775
        if self._endlabelposition == 'BOTTOM':
            (widthendlabel, heightendlabel, pointsizeendtext) = self.scaleText(qp, labelend, height)
            verticalpositionendlabel = height-2
        pointsizeendtext = heightendlabel

        # Calculate horizontals positions range texts
        if self._range_text_inside:
            horizontalpositionstartlabel = self.getDrawValueStartRange() + self.getStartTextSpacer()
            horizontalpositionendlabel = self.getDrawValueEndRange() - self.getEndTextSpacer() - widthendlabel
            if widthstartlabel + widthendlabel + self.getStartTextSpacer() + self.getEndTextSpacer() + self.getRangeMinimumTextsSpacer() > self.getDrawWidthRange():
                # Range width not enough inside space
                if self.getDrawValueStartRange() < widthstartlabel + self.getDrawValueStartBar() + self.getStartTextSpacer():
                    # Start text not enough outside space
                    if self.getDrawValueEndRange() > self.getDrawValueEndBar() - widthendlabel - self.getEndTextSpacer():
                        # End text not enough outside space
                        factor = self.getDrawWidthRange() / (widthstartlabel + widthendlabel + self.getStartTextSpacer() + self.getEndTextSpacer() + self.getRangeMinimumTextsSpacer()) 
                        pointsizestarttext = heightstartlabel * factor
                        if self._startlabelposition == 'TOP':
                            verticalpositionstartlabel = verticalpositionstartlabel*factor
                        if self._startlabelposition ==  'MIDDLE':
                            verticalpositionstartlabel = (height - heightstartlabel * 0.775 * factor)/2 + heightstartlabel * 0.775 * factor
                        if self._startlabelposition ==  'MIDDLE-TOP':
                            verticalpositionstartlabel = (height/2 - heightstartlabel * 0.775 * factor)/2 + heightstartlabel * 0.775 * factor
                        if self._startlabelposition ==  'MIDDLE-BOTTOM':
                            verticalpositionstartlabel = height/2 + (height/2 - heightstartlabel * 0.775 * factor)/2 + heightstartlabel * 0.775 * factor
                        pointsizeendtext = heightendlabel * factor
                        horizontalpositionendlabel = horizontalpositionendlabel + (widthendlabel - widthendlabel*factor)
                        if self._endlabelposition == 'TOP':
                            verticalpositionendlabel = verticalpositionendlabel*factor
                        if self._endlabelposition == 'MIDDLE':
                            verticalpositionendlabel = (height - heightendlabel * 0.775 * factor)/2 + heightendlabel * 0.775 * factor
                        if self._endlabelposition == 'MIDDLE-TOP':
                            verticalpositionendlabel = (height/2 - heightendlabel * 0.775 * factor)/2 +  heightendlabel * 0.775 * factor
                        if self._endlabelposition == 'MIDDLE-BOTTOM':
                            verticalpositionendlabel = height/2 + (height/2 - heightendlabel * 0.775 * factor)/2 + heightendlabel * 0.775 * factor
                    else:
                        # End text enough outside space
                        horizontalpositionendlabel = self.getDrawValueEndRange() + self.getEndTextSpacer()
                        if widthstartlabel + self.getStartTextSpacer() > self.getDrawWidthRange():
                            # Start text not enough inside space 
                            if widthstartlabel + self.getStartTextSpacer() > self.getDrawValueStartRange() - self.getDrawValueStartBar():
                                factor = (self.getDrawValueStartRange() - self.getDrawValueStartBar()) / (widthstartlabel + self.getStartTextSpacer())
                                pointsizestarttext = heightstartlabel * factor
                                horizontalpositionstartlabel = horizontalpositionstartlabel - 2*self.getStartTextSpacer() - widthstartlabel*factor
                                if self._startlabelposition == 'TOP':
                                    verticalpositionstartlabel = verticalpositionstartlabel*factor
                                if self._startlabelposition ==  'MIDDLE':
                                    verticalpositionstartlabel = (height - heightstartlabel * 0.775 * factor)/2 + heightstartlabel * 0.775 * factor
                                if self._startlabelposition ==  'MIDDLE-TOP':
                                    verticalpositionstartlabel = (height/2 - heightstartlabel * 0.775 * factor)/2 + heightstartlabel * 0.775 * factor
                                if self._startlabelposition ==  'MIDDLE-BOTTOM':
                                    verticalpositionstartlabel = height/2 + (height/2 - heightstartlabel * 0.775 * factor)/2 + heightstartlabel * 0.775 * factor
                            else:
                                horizontalpositionstartlabel = horizontalpositionstartlabel - 2*self.getStartTextSpacer() - widthstartlabel

                elif self.getDrawValueEndRange() > self.getDrawValueEndBar() - widthendlabel - self.getEndTextSpacer():
                    # End text not enough outside space
                    horizontalpositionstartlabel = self.getDrawValueStartRange() - self.getStartTextSpacer() - widthstartlabel
                    if widthendlabel + self.getEndTextSpacer() > self.getDrawWidthRange():
                        # End text not enough inside space
                        if widthendlabel + self.getEndTextSpacer() > self.getDrawValueEndBar() - self.getDrawValueEndRange():
                            factor = (self.getDrawValueEndBar() - self.getDrawValueEndRange()) / (widthendlabel + self.getEndTextSpacer())
                            pointsizeendtext =  heightendlabel * factor
                            if self._endlabelposition == 'TOP':
                                verticalpositionendlabel = verticalpositionendlabel*factor
                            if self._endlabelposition == 'MIDDLE':
                                verticalpositionendlabel = (height - heightendlabel * 0.775 * factor)/2 + heightendlabel * 0.775 * factor
                            if self._endlabelposition == 'MIDDLE-TOP':
                                verticalpositionendlabel = (height/2 - heightendlabel * 0.775 * factor)/2 +  heightendlabel * 0.775 * factor
                            if self._endlabelposition == 'MIDDLE-BOTTOM':
                                verticalpositionendlabel = height/2 + (height/2 - heightendlabel * 0.775 * factor)/2 + heightendlabel * 0.775 * factor
                        horizontalpositionendlabel = horizontalpositionendlabel + widthendlabel + 2*self.getEndTextSpacer()
                else:
                    # Outside range texts
                    horizontalpositionstartlabel = self.getDrawValueStartRange() - self.getStartTextSpacer() - widthstartlabel
                    horizontalpositionendlabel = self.getDrawValueEndRange() + self.getEndTextSpacer()
        else:
            # Range text outside
            horizontalpositionstartlabel = self.getDrawValueStartRange() - self.getStartTextSpacer() - widthstartlabel
            horizontalpositionendlabel = self.getDrawValueEndRange() + self.getEndTextSpacer()
            if self.getDrawValueStartRange() < self.getDrawValueStartBar() + self.getStartTextSpacer() + widthstartlabel:
                # Start text not enough outside space
                if self.getDrawValueEndRange() > self.getDrawValueEndBar() - widthendlabel - self.getEndTextSpacer():
                    # End text not enough outside space
                    if self.getDrawWidthRange() >= widthstartlabel + widthendlabel + self.getStartTextSpacer() + self.getEndTextSpacer() + self.getRangeMinimumTextsSpacer():
                        # Range text enough inside space
                        horizontalpositionstartlabel = self.getDrawValueStartRange() + self.getStartTextSpacer()
                        horizontalpositionendlabel = self.getDrawValueEndRange() - self.getEndTextSpacer() - widthendlabel
                    elif widthstartlabel + self.getStartTextSpacer() <= self.getDrawWidthRange():
                        # Start text enough inside space
                        horizontalpositionstartlabel = self.getDrawValueStartRange() + self.getStartTextSpacer()
                        # End label not enough outside space
                        if widthendlabel + self.getEndTextSpacer() >= self.getDrawValueEndBar() - self.getDrawValueEndRange():
                            factor = (self.getDrawValueEndBar() - self.getDrawValueEndRange()) / (widthendlabel + self.getStartTextSpacer())
                            pointsizeendtext =  heightendlabel * factor
                            if self._endlabelposition == 'TOP':
                                verticalpositionendlabel = verticalpositionendlabel*factor
                            if self._endlabelposition == 'MIDDLE':
                                verticalpositionendlabel = (height - heightendlabel * 0.775 * factor)/2 + heightendlabel * 0.775 * factor
                            if self._endlabelposition == 'MIDDLE-TOP':
                                verticalpositionendlabel = (height/2 - heightendlabel * 0.775 * factor)/2 +  heightendlabel * 0.775 * factor
                            if self._endlabelposition == 'MIDDLE-BOTTOM':
                                verticalpositionendlabel = height/2 + (height/2 - heightendlabel * 0.775 * factor)/2 + heightendlabel * 0.775 * factor
                        #else:
                        #    if self._endlabelposition in ('MIDDLE', 'MIDDLE-TOP', 'MIDDLE-BOTTOM'):
                        #        verticalpositionendlabel = verticalpositionendlabel - (heightendlabel - pointsizeendtext)/2
                        #    if self._endlabelposition == 'TOP':
                        #        verticalpositionendlabel = verticalpositionendlabel - (heightendlabel - pointsizeendtext)
                    elif widthendlabel + self.getEndTextSpacer() <= self.getDrawWidthRange():
                        # End text enough inside space
                        horizontalpositionendlabel = self.getDrawValueEndRange() - self.getEndTextSpacer() - widthendlabel
                        factor = self.getDrawWidthRange() / (widthendlabel + self.getEndTextSpacer())
                        pointsizestarttext = heightstartlabel * factor
                        horizontalpositionstartlabel = horizontalpositionstartlabel + (widthstartlabel - widthstartlabel*factor)
                    else:
                        # Range text not enough inside space for all
                        factorstart = (self.getDrawValueStartRange() - self.getDrawValueStartBar()) / (widthstartlabel + self.getStartTextSpacer())
                        pointsizestarttext = heightstartlabel * factorstart
                        factorend = (self.getDrawValueStartBar() - self.getDrawValueEndRange()) / (widthendlabel + self.getEndTextSpacer())
                        pointsizeendtext =  heightendlabel * factorend
                else:
                    # End text enough outside space
                    if self.getDrawWidthRange() >= widthstartlabel + self.getStartTextSpacer():
                        # Start text enough inside space
                        horizontalpositionstartlabel = self.getDrawValueStartRange() + self.getStartTextSpacer()
                    else:
                        # Start text not enough inside space
                        factor = (self.getDrawValueStartRange() - self.getDrawValueStartBar()) / (widthstartlabel + self.getStartTextSpacer())
                        pointsizestarttext = heightstartlabel * factor
                        horizontalpositionstartlabel = horizontalpositionstartlabel + (widthstartlabel - widthstartlabel*factor)
                        if self._startlabelposition == 'TOP':
                            verticalpositionstartlabel = verticalpositionstartlabel*factor
                        if self._startlabelposition ==  'MIDDLE':
                            verticalpositionstartlabel = (height - heightstartlabel * 0.775 * factor)/2 + heightstartlabel * 0.775 * factor
                        if self._startlabelposition ==  'MIDDLE-TOP':
                            verticalpositionstartlabel = (height/2 - heightstartlabel * 0.775 * factor)/2 + heightstartlabel * 0.775 * factor
                        if self._startlabelposition ==  'MIDDLE-BOTTOM':
                            verticalpositionstartlabel = height/2 + (height/2 - heightstartlabel * 0.775 * factor)/2 + heightstartlabel * 0.775 * factor
            else:
                # Range width enough space
                if self.getDrawValueEndRange() > self.getDrawValueEndBar() - widthendlabel - self.getEndTextSpacer():
                    # End text not enough outside space
                    if self.getDrawWidthRange() >= widthendlabel + self.getEndTextSpacer():
                        # End text enough inside space
                        horizontalpositionendlabel = self.getDrawValueEndRange() - self.getEndTextSpacer() - widthendlabel
                    else:
                        # End text not enough inside space
                        factor = (self.getDrawValueEndBar() - self.getDrawValueEndRange()) / (widthendlabel - self.getEndTextSpacer())
                        pointsizeendtext =  heightendlabel * factor
                        if self._endlabelposition == 'TOP':
                            verticalpositionendlabel = verticalpositionendlabel*factor
                        if self._endlabelposition == 'MIDDLE':
                            verticalpositionendlabel = (height - heightendlabel * 0.775 * factor)/2 + heightendlabel * 0.775 * factor
                        if self._endlabelposition == 'MIDDLE-TOP':
                            verticalpositionendlabel = (height/2 - heightendlabel * 0.775 * factor)/2 +  heightendlabel * 0.775 * factor
                        if self._endlabelposition == 'MIDDLE-BOTTOM':
                            verticalpositionendlabel = height/2 + (height/2 - heightendlabel * 0.775 * factor)/2 + heightendlabel * 0.775 * factor


        return labelstart, horizontalpositionstartlabel, verticalpositionstartlabel, pointsizestarttext, labelend, horizontalpositionendlabel, verticalpositionendlabel, pointsizeendtext


    def updateDrawTexts(self, qp, height):
        """
        Draw texts of bar.

        :param qp: object of bar.
        :type qp: QPainter
        :param height: height of widget.
        :type height: int
        """
        if self._showtext:
            # Aspect labels range configure
            if (self.getDrawWidthRange()) / self.getDrawValueEndBar() > 0.2:
                position = -1
            else:
                position = 1

            if self._showextremumstexts:
                # Set minimum position label
                qp.save()
                (labelminxposition, labelminyposition, textmin) = self.calculateMinimumLabelPosition(qp, height)
                # Draw text label minimum
                qp.translate(labelminxposition, labelminyposition)
                qp.rotate((int(self._textminreadingreverse)*2-1)*90)
                qp.drawText(0, 0, textmin)
                qp.restore()

                # Set maximum position label
                qp.save()
                (labelmaxxposition, labelmaxyposition, textmax) = self.calculateMaximumLabelPosition(qp, height)
                # Set show text label maximum
                qp.translate(labelmaxxposition, labelmaxyposition)
                qp.rotate((-1 + int(self._textmaxreadingreverse)*2)*90)
                qp.drawText(0, 0, textmax)
                qp.restore()

            if self._showrangetext:
                # Caculate position label range
                (labelstart, positionhlabelstart, positionvlabelstart, sizetextstart, labelend, positionhlabelend, positionvlabelend, sizetextend) = self.calculateRangeLabelsPositions(qp, height)

                # Set draw label start range progress
                if self.getStart() != self.getMinimum() and self._showtext:
                    pen = QPen(self._colortextstart, 1, Qt.SolidLine) # Color text
                    qp.setPen(pen)
                    font = QFont(self._rangefont)
                    font.setBold(True)
                    font.setPointSize(sizetextstart)
                    qp.setFont(font)
                    qp.drawText(positionhlabelstart, positionvlabelstart, labelstart)

                # Set draw label end range progress
                if self.getEnd() != self.getMaximum() and self._showtext:
                    pen = QPen(self._colortextend, 1, Qt.SolidLine) # Color text
                    qp.setPen(pen)
                    font = QFont(self._rangefont)
                    font.setBold(True)
                    font.setPointSize(sizetextend)
                    qp.setFont(font)
                    qp.drawText(positionhlabelend, positionvlabelend, labelend)





class DrawLevels():
    """
    .. image:: classes/DrawLevels.png
        :alt: Diagram
        :align: center
        :height: 350px

    Levels features object.
    """


    def __init__(self):
        """ Initialize class QFactoryDrawLevels. """

        # Initialize parent properties
        self.setColorSquareWarning()
        self.setColorWarning()
        self.setColorSquareAlert()
        self.setColorAlert()


    def setColorSquareWarning(self, color=QColor(255, 255, 255)):
        """
        .. image:: pictures/QRangeProgressBar_ColorSquareWarning.png
            :alt: set warning level color square
            :align: center
            :height: 80px

        Set color square of warning level.

        :param color: color object of square warning.
        :type color: QColor
        """
        self._colorsquarewarning = color


    def getColorSquareWarning(self):
        """
        .. image:: pictures/QRangeProgressBar_ColorSquareWarning.png
            :alt: get warning level color square
            :align: center
            :height: 80px

        :returns: Get color square of warning level.
        :rtype: QColor
        """
        return self._colorsquarewarning


    def setColorWarning(self, color=QColor(255, 255, 184)):
        """
        .. image:: pictures/QRangeProgressBar_ColorWarning.png
            :alt: set color warning level
            :align: center
            :height: 80px

        Set color of warning level.

        :param color: color object of warning.
        :type color: QColor
        """
        self._colorwarning = color


    def getColorWarning(self):
        """
        .. image:: pictures/QRangeProgressBar_ColorWarning.png
            :alt: get color warning level
            :align: center
            :height: 80px

        :returns: Get color of warning level.
        :rtype: QColor
        """
        return self._colorwarning


    def setColorSquareAlert(self, color=QColor(255, 175, 175)):
        """
        .. image:: pictures/QRangeProgressBar_ColorSquareAlert.png
            :alt: set alert color square
            :align: center
            :height: 80px

        Set color square of alert.

        :param color: color object of square alert.
        :type color: QColor
        """
        self._colorsquarealert = color


    def getColorSquareAlert(self):
        """
        .. image:: pictures/QRangeProgressBar_ColorSquareAlert.png
            :alt: get alert level color square
            :align: center
            :height: 80px

        :returns: Get color square of alert level.
        :rtype: QColor
        """
        return self._colorsquarealert


    def setColorAlert(self, color=QColor(255, 175, 175)):
        """
        .. image:: pictures/QRangeProgressBar_ColorAlert.png
            :alt: set color alert level
            :align: center
            :height: 80px

        Set color of alert level.

        :param color: color object of alert.
        :type color: QColor
        """
        self._coloralert = color


    def getColorAlert(self):
        """
        .. image:: pictures/QRangeProgressBar_ColorAlert.png
            :alt: get color alert level
            :align: center
            :height: 80px

        :returns: Get color of alert level.
        :rtype: QColor
        """
        return self._coloralert


    def updateDrawLevels(self, qp, height):
        """
        Draw Levels of bar.

        :param qp: object of bar.
        :type qp: QPainter
        :param height: height of widget.
        :type height: int
        """
        # Set alert ranges progress
        if self._colorsquarealert:
            qp.setPen(self._colorsquarealert) # Square warning color
        else:
            qp.setPen(Qt.NoPen)
        if self._coloralert:
            qp.setBrush(self._coloralert) # Alert color
        else:
            qp.setBrush(Qt.NoBrush)
        startalertmax = self.getDrawValueStartMaximumAlert()
        widthalertmax = self.getDrawWidthMaximumAlert()
        if not (startalertmax is None or widthalertmax is None):
            qp.drawRect(startalertmax, 0, widthalertmax, height)
        startalertmin = self.getDrawValueStartMinimumAlert()
        widthalertmin = self.getDrawWidthMinimumAlert()
        if not (startalertmin is None or widthalertmin is None):
            qp.drawRect(startalertmin, 0, widthalertmin, height)

        # Set warning ranges progress
        if self._colorsquarewarning:
            qp.setPen(self._colorsquarewarning) # Square warning color
        else:
            qp.setPen(Qt.NoPen)
        if self._colorwarning:
            qp.setBrush(self._colorwarning) # Warning color
        else:
            qp.setBrush(Qt.NoBrush)
        startwarningmax = self.getDrawValueStartMaximumWarning()
        widthwarningmax = self.getDrawWidthMaximumWarning()
        if not (startwarningmax is None or widthwarningmax is None):
            qp.drawRect(startwarningmax, 0, widthwarningmax, height)
        startwarningmin = self.getDrawValueStartMinimumWarning()
        widthwarningmin = self.getDrawWidthMinimumWarning()
        if not (startwarningmin is None or widthwarningmin is None):
            qp.drawRect(startwarningmin, 0, widthwarningmin, height)






class DrawGraduations():
    """
    .. image:: classes/DrawGraduations.png
        :alt: Diagram
        :align: center
        :height: 900px

    Graduations features object.
    """


    def __init__(self):
        """ Initialize class QFactoryDrawGraduations. """
        self._showgraduations = False

        # Initialize parent properties
        self.showGraduations()
        self.setGraduationFont()
        self.setColorTextGraduations()
        self.showGraduations()
        self.setGraduationsPositionCenter()
        self.setLineGraduationLength()
        self.showTextsGraduations()
        self.setListLabelGraduations()
        self.setPositionTextBottomCenterGraduations()


    def showGraduations(self):
        """
        .. image:: pictures/QRangeProgressBar_Graduations_Center_TopText.png
            :alt: Show graduations example
            :align: center
            :width: 600px
            :height: 60px

        Show graduation of progress bar.
        """
        self._showgraduations = True


    def hideGraduations(self):
        """
        .. image:: pictures/QRangeProgressBar.png
            :alt: Hide graduations example
            :align: center
            :width: 600px
            :height: 60px

        Hide graduation of progress bar.
        """
        self._showgraduations = False


    def isGraduationsShow(self):
        """
        :returns: State show graduation of progress bar.
        :rtype: bool
        """
        return self._showgraduations


    def showTextsGraduations(self):
        """
        .. image:: pictures/QRangeProgressBar_TextGraduations.png
            :alt: Show text graduations example
            :align: center
            :width: 600px
            :height: 60px

        Show text graduation of progress bar.
        """
        self._graduatedtextshow = True


    def hideTextsGraduations(self):
        """
        .. image:: pictures/QRangeProgressBar_Graduations_Center.png
            :alt: Hide texts graduations example
            :align: center
            :width: 600px
            :height: 60px

        Hide text graduation of progress bar.
        """
        self._graduatedtextshow = False


    def isGraduationsShowText(self):
        """
        :returns: Show text graduation state.
        :rtype: bool
        """
        return self._graduatedtextshow


    def setListLabelGraduations(self, listlabels=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90]):
        """
        Set list labels graduations.

        :param listlabels: list of real values labels.
        :type listlabels: list
        """
        self._graduationlabel = listlabels


    def getListLabelsGraduations(self):
        """
        :returns: Get list labels graduations.
        :rtype: list
        """
        return self._graduationlabel


    def setGraduationFont(self, font=QFont('Serif', 7, QFont.Light)):
        """
        Set graduation font.

        :param font: font object of graduations texts.
        :type font: QFont
        """
        self._graduationsfont = font #font.setPointSize(7)


    def getGraduationFont(self):
        """
        :returns: Get graduation font.
        :rtype: QFont
        """
        return self._graduationsfont


    def setGraduationFontSize(self, size=7):
        """
        Set graduation font size.

        :param size: size of graduations texts.
        :type size: int
        """
        self._graduationsfont.setPointSize(size)


    def getGraduationFontSize(self):
        """
        :returns: Get graduation font size.
        :rtype: int
        """
        return self._graduationsfont.pointSize()


    def setGraduationsPositionTop(self):
        """
        .. image:: pictures/QRangeProgressBar_Graduations_Top.png
            :alt: Graduations top example
            :align: center
            :width: 600px
            :height: 60px

        Set graduations top.
        """
        self._graduatedposition = 'TOP'


    def setGraduationsPositionCenter(self):
        """
        .. image:: pictures/QRangeProgressBar_Graduations_Center.png
            :alt: Graduations center example
            :align: center
            :width: 600px
            :height: 60px

        Set graduations center.
        """
        self._graduatedposition = 'CENTER'


    def setGraduationsPositionBottom(self):
        """
        .. image:: pictures/QRangeProgressBar_Graduations_Bottom.png
            :alt: Graduations bottom example
            :align: center
            :width: 600px
            :height: 60px

        Set graduations bottom.
        """
        self._graduatedposition = 'BOTTOM'


    def setGraduationsPositionTopBottom(self):
        """
        .. image:: pictures/QRangeProgressBar_Graduations_TopBottom.png
            :alt: Graduations top and bottom example
            :align: center
            :width: 600px
            :height: 60px

        Set graduations top and bottom.
        """
        self._graduatedposition = 'TOP-BOTTOM'


    def setGraduationsPositionExpand(self):
        """
        .. image:: pictures/QRangeProgressBar_Graduations_Expand.png
            :alt: Graduations expand example
            :align: center
            :width: 600px
            :height: 60px

        Set graduations expand.
        """
        self._graduatedposition = 'EXPAND'


    def getGraduationsPosition(self):
        """
        .. image:: pictures/QRangeProgressBar_GraduationsPositions.png
            :alt: get Graduations position
            :align: center
            :width: 600px
            :height: 60px

        :returns: Get graduations position.
        :rtype: string
        """
        return self._graduatedposition


    def setPositionTextTopCenterGraduations(self):
        """
        .. image:: pictures/QRangeProgressBar_Graduations_Center_TopText.png
            :alt: Graduations center and texts top example
            :align: center
            :width: 600px
            :height: 60px

        Set text graduations center to top.
        """
        self._graduatedcentertextshowtop = True


    def setPositionTextBottomCenterGraduations(self):
        """
        .. image:: pictures/QRangeProgressBar_Graduations_Center_BottomText.png
            :alt: Graduations center and texts bottom example
            :align: center
            :width: 600px
            :height: 60px

        Set text graduations center to bottom.
        """
        self._graduatedcentertextshowtop = False


    def getPositionTextCenterGraduations(self):
        """
        .. image:: pictures/QRangeProgressBar_GraduationsCenterTextPosition.png
            :alt: get text position for center graduations
            :align: center
            :width: 600px
            :height: 60px

        :returns: Get text position for center graduations.
        :rtype: string
        """
        if self._graduatedcentertextshowtop:
            return 'TOP'
        else:
            return 'BOTTOM'


    def setLineGraduationLength(self, sizeline=5):
        """
        Set line graduation length.

        :param size: size of graduations lengths.
        :type size: int
        """
        self._linegraduationlength = sizeline


    def getLineGraduationLength(self):
        """
        :returns: Get line graduation length.
        :rtype: int
        """
        return self._linegraduationlength


    def setColorTextGraduations(self, color=QColor(184, 184, 184)):
        """
        .. image:: pictures/QRangeProgressBar_ColorTextGraduations.png
            :alt: set color text graduations
            :align: center
            :width: 600px
            :height: 60px

        Set color Text graduations.

        :param color: color object of graduations texts.
        :type color: QColor
        """
        self._colortextgraduations = color


    def getColorTextGraduations(self):
        """
        .. image:: pictures/QRangeProgressBar_ColorTextGraduations.png
            :alt: get color text graduations
            :align: center
            :width: 600px
            :height: 60px

        :returns: Get color Text graduations
        :rtype: QColor
        """
        return self._colortextgraduations


    def updateDrawGraduations(self, qp, height):
        """
        Draw graduations of bar.

        :param qp: object of bar.
        :type qp: QPainter
        :param height: height of widget.
        :type height: int
        """
        if self._showgraduations:
            # Set font text graduation
            font = self._graduationsfont
            qp.setFont(font)
            metrics = qp.fontMetrics()
            pen = QPen(self._colortextgraduations, 1, Qt.SolidLine) # Color text
            qp.setPen(pen)

            metrics = qp.fontMetrics()
            lenlabelsgraduations = len(self._graduationlabel)

            for i in range(1, lenlabelsgraduations+1):
                value = self._graduationlabel[i-1]
                # For values in
                if value > self.getMinimum() and value < self.getMaximum() and not (value == 0 and self._logarithmscale):
                    # Calculate values
                    i = (self.scaleValue(value) - self.getScaledValueMinimum()) * (self.getDrawValueEndBar() - self.getDrawValueStartBar()) / (self.getScaledValueMaximum() - self.getScaledValueMinimum())

                    fw = metrics.width(str(value))
                    fh = metrics.height()

                    # Draw graduations
                    if self._graduatedposition == 'TOP':
                        qp.drawLine(i + self.getDrawValueOffsetStartBar(), 0, i + self.getDrawValueOffsetStartBar(), self._linegraduationlength)
                        if self._graduatedtextshow:
                            qp.drawText(i-fw/2 + self.getDrawValueOffsetStartBar(), fh +2, str(value))
                    if self._graduatedposition == 'CENTER':
                        qp.drawLine(i + self.getDrawValueOffsetStartBar(), height/2 - self._linegraduationlength/2, i + self.getDrawValueOffsetStartBar(), height/2 + self._linegraduationlength/2)
                        if self._graduatedtextshow:
                            if self._graduatedcentertextshowtop:
                                qp.drawText(i-fw/2 + self.getDrawValueOffsetStartBar(), height/2 - self._linegraduationlength, str(value))
                            else:
                                qp.drawText(i-fw/2 + self.getDrawValueOffsetStartBar(), height/2 + self._linegraduationlength/2 + fh, str(value))
                    if self._graduatedposition == 'BOTTOM':
                        qp.drawLine(i + self.getDrawValueOffsetStartBar(), height, i + self.getDrawValueOffsetStartBar(), height - self._linegraduationlength -1)
                        if self._graduatedtextshow:
                            qp.drawText(i-fw/2 + self.getDrawValueOffsetStartBar(), height - self._linegraduationlength -2, str(value))
                    if self._graduatedposition == 'EXPAND':
                        qp.drawLine(i + self.getDrawValueOffsetStartBar(), 0, i + self.getDrawValueOffsetStartBar(), height)
                        if self._graduatedtextshow:
                            qp.drawText(i-fw/2 + self.getDrawValueOffsetStartBar(), height/2 + fh/4, str(value))
                    if self._graduatedposition == 'TOP-BOTTOM':
                        qp.drawLine(i + self.getDrawValueOffsetStartBar(), 0, i + self.getDrawValueOffsetStartBar(), self._linegraduationlength)
                        qp.drawLine(i + self.getDrawValueOffsetStartBar(), height, i + self.getDrawValueOffsetStartBar(), height - self._linegraduationlength -1)
                        if self._graduatedtextshow:
                            qp.drawText(i-fw/2 + self.getDrawValueOffsetStartBar(), height/2, str(value))





class DrawProgressBar(QWidget):
    """
    Create a draw progress bar.
    """

    def __init__(self):
        """ Initialize class QRangeProgressBar. """
        QWidget.__init__(self)


    def initUI(self):
        """ Initialize QWidget parameters default UI. """
        # Set UI geometry
        self.setMinimumSize(1, 50)
        self.setContentsMargins(0,0,0,0)

        # Set UI style
        self.setColorSquareChunk()
        self.setColorChunk()
        self.setColorSquareRange()
        self.setColorRange()


    def setColorSquareChunk(self, color=QColor(20, 20, 20)):
        """
        .. image:: pictures/QRangeProgressBar_ColorSquareChunk.png
            :alt: set chunk color square
            :align: center

        Set color square of chunk progress bar.

        :param color: color object of square chunk progress bar.
        :type color: QColor
        """
        self._colorsquarechunk = color


    def getColorSquareChunk(self):
        """
        .. image:: pictures/QRangeProgressBar_ColorSquareChunk.png
            :alt: get chunk color square
            :align: center

        Get color square of chunk progress bar.
        """
        return self._colorsquarechunk


    def setColorChunk(self, color=None):
        """
        .. image:: pictures/QRangeProgressBar_ColorChunk.png
            :alt: set color chunk
            :align: center

        Set color of chunk progress bar.

        :param color: color object of chunk progress bar.
        :type color: QColor
        """
        self._colorchunk = color


    def getColorChunk(self):
        """
        .. image:: pictures/QRangeProgressBar_ColorChunk.png
            :alt: get color chunk
            :align: center

        Get color of chunk progress bar.
        """
        return self._colorchunk


    def setColorSquareRange(self, color=QColor(255, 255, 255)):
        """
        .. image:: pictures/QRangeProgressBar_ColorSquareRange.png
            :alt: set range color square
            :align: center

        Set color square of range.

        :param color: color object of square range.
        :type color: QColor
        """
        self._colorsquarerange = color


    def getColorSquareRange(self):
        """
        .. image:: pictures/QRangeProgressBar_ColorSquareRange.png
            :alt: get range color square
            :align: center

        Get color square of range.
        """
        return self._colorsquarerange


    def setColorRange(self, color=QColor(184, 255, 184)):
        """
        .. image:: pictures/QRangeProgressBar_ColorRange.png
            :alt: set color range
            :align: center

        Set color of range.

        :param color: color object of range.
        :type color: QColor
        """
        self._colorrange = color


    def getColorRange(self):
        """
        .. image:: pictures/QRangeProgressBar_ColorRange.png
            :alt: get color range
            :align: center

        Get color of range.
        """
        return self._colorrange


    def getWidth(self):
        """
        :returns: Get widget size width.
        :rtype: int
        """
        return self.size().width()


    def getHeight(self):
        """
        :returns: Get widget size height.
        :rtype: int
        """
        return self.size().height()





class QRangeProgressBar(QProgress, DrawProgressBar):
    """
    .. image:: classes/QRangeProgressBar.png
        :alt: Diagram
        :align: center
        :height: 650px

    Create a range progress bar widget.

    :param minimum: The start of plage values.
    :type minimum: int
    :param maximum: The end of plages value.
    :type maximum: int
    :param start: The start of range progress.
    :type start: int
    :param end: The end of range progress.
    :type end: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    """

    def __init__(self, minimum, maximum, start, end, islogscale, logarithmbase):
        """ Initialize class QRangeProgressBar. """
        DrawProgressBar.__init__(self)
        QProgress.__init__(self, 0, self.getWidth(), minimum, maximum, start, end, islogscale, logarithmbase)

        # Initialisation parameters
        self.initProgress()


    def initProgress(self):
        """ Initialize QWidget parameters default UI. """
        # Set UI options
        self.initUI()
        # Set Progress options
        self.setLinearScale()
        self.setOffsetStartBar()
        self.setOffsetEndBar()


    def updateDrawChunk(self, qp, height):
        """
        Draw chunk bar.

        :param qp: object of bar.
        :type qp: QPainter
        :param height: height of widget.
        :type height: int
        """
        startbar = self.getDrawValueStartBar()
        widthbar = self.getDrawWidthBar()
        if not (startbar is None or widthbar is None):
            if self._colorsquarechunk:
                qp.setPen(self._colorsquarechunk) # Square chunk color
            else:
                qp.setPen(Qt.NoPen)
            if self._colorchunk:
                qp.setBrush(self._colorchunk) # Alert color
            else:
                qp.setBrush(Qt.NoBrush)
                #pen = QPen(Qt.NoPen, 1, Qt.SolidLine)
                #qp.setPen(pen)
                #qp.setBrush(self._colorchunk) # Chunk progress bar color
            qp.drawRect(startbar, 0, widthbar-1, height-1) # Draw chunk progress bar


    def updateDrawSquareChunk(self, qp, height):
        """
        Draw square chunk bar.

        :param qp: object of bar.
        :type qp: QPainter
        :param height: height of widget.
        :type height: int
        """
        startbar = self.getDrawValueStartBar()
        widthbar = self.getDrawWidthBar()
        if not (startbar is None or widthbar is None):
            # Square chunk progress bar color
            if self._colorsquarechunk:
                pen = QPen(self._colorsquarechunk, 1, Qt.SolidLine)
                qp.setPen(pen)
                qp.setBrush(Qt.NoBrush)
                # Draw chunk progress bar
                qp.drawRect(startbar, 0, widthbar-1, height-1)


    def updateDrawRange(self, qp, height):
        """
        Draw chunk bar.

        :param qp: object of bar.
        :type qp: QPainter
        :param height: height of widget.
        :type height: int
        """
        startrange = self.getDrawValueStartRange()
        widthrange = self.getDrawWidthRange()
        if not (startrange is None or widthrange is None):
            if self._colorsquarerange:
                qp.setPen(self._colorsquarerange) # Square range color
            else:
                qp.setPen(Qt.NoPen)
            if self._colorrange:
                qp.setBrush(self._colorrange) # Range color
            else:
                qp.setBrush(Qt.NoBrush)
            # Draw range
            if self.isRangeShow():
                qp.drawRect(startrange, 0, widthrange, height)


    def drawWidget(self, qp):
        """ 
        Draw widget progress bar. 

        .. image:: pictures/QRangeProgressBar.png
            :alt: Diagram
            :align: center
            :height: 580px
        """
        widget_height = self.getHeight()

        if not(self.getMinimum is None or self.getStart is None or self.getEnd is None or self.getMaximum is None):
            # Set values of progress
            self.updateProgress(0, self.getWidth())

            # Set chunk progress bar
            self.updateDrawChunk(qp, widget_height)

            # Set normal range progress color
            self.updateDrawRange(qp, widget_height)

            # Set square chunk progress bar
            self.updateDrawSquareChunk(qp, widget_height)


    def paintEvent(self, e):
        """
        Redraw widget progress bar on event repaint().

        :param e: event of widget.
        :type e: QEvent
        """
        if not(self.getMinimum is None or self.getStart is None or self.getEnd is None or self.getMaximum is None):
            qp = QPainter()
            qp.begin(self)
            self.drawWidget(qp)
            qp.end()





class QRangeLevelsProgressBar(QProgressLevels, DrawProgressBar, DrawLevels):
    """
    .. image:: classes/QRangeLevelsProgressBar.png
        :alt: Diagram
        :align: center
        :width: 700px
        :height: 1000px

    Create a range with levels progress bar widget.

    :param minimum: The start of plage values.
    :type minimum: int
    :param maximum: The end of plages value.
    :type maximum: int
    :param start: The start of range progress.
    :type start: int
    :param end: The end of range progress.
    :type end: int
    :param alert_min: Level of minimum alert value.
    :type alert_min: int
    :param warning_min: Level of minimum warning value.
    :type warning_min: int
    :param warning_max: Level of maximum warning value.
    :type  warning_max: int
    :param alert_max: Level of maximum alert value.
    :type alert_max: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    """

    def __init__(self, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase):
        """ Initialize class QRangeLevelsProgressBar. """
        DrawProgressBar.__init__(self)
        QProgressLevels.__init__(self, 0, self.getWidth(), minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)
        DrawLevels.__init__(self)

        # Initialisation parameters
        self.initProgress()


    def initProgress(self):
        """ Initialize QWidget parameters default UI. """
        # Set UI options
        self.initUI()
        # Set Progress options
        self.setLinearScale()
        self.setOffsetStartBar()
        self.setOffsetEndBar()


    def updateDrawChunk(self, qp, height):
        """
        Draw chunk bar.

        :param qp: object of bar.
        :type qp: QPainter
        :param height: height of widget.
        :type height: int
        """
        startbar = self.getDrawValueStartBar()
        widthbar = self.getDrawWidthBar()
        if not (startbar is None or widthbar is None):
            if self._colorsquarechunk:
                qp.setPen(self._colorsquarechunk) # Square chunk color
            else:
                qp.setPen(Qt.NoPen)
            if self._colorchunk:
                qp.setBrush(self._colorchunk) # Alert color
            else:
                qp.setBrush(Qt.NoBrush)
                #pen = QPen(Qt.NoPen, 1, Qt.SolidLine)
                #qp.setPen(pen)
                #qp.setBrush(self._colorchunk) # Chunk progress bar color
            qp.drawRect(startbar, 0, widthbar-1, height-1) # Draw chunk progress bar


    def updateDrawSquareChunk(self, qp, height):
        """
        Draw square chunk bar.

        :param qp: object of bar.
        :type qp: QPainter
        :param height: height of widget.
        :type height: int
        """
        startbar = self.getDrawValueStartBar()
        widthbar = self.getDrawWidthBar()
        if not (startbar is None or widthbar is None):
            # Square chunk progress bar color
            if self._colorsquarechunk:
                pen = QPen(self._colorsquarechunk, 1, Qt.SolidLine)
                qp.setPen(pen)
                qp.setBrush(Qt.NoBrush)
                # Draw chunk progress bar
                qp.drawRect(startbar, 0, widthbar-1, height-1)


    def updateDrawRange(self, qp, height):
        """
        Draw chunk bar.

        :param qp: object of bar.
        :type qp: QPainter
        :param height: height of widget.
        :type height: int
        """
        startrange = self.getDrawValueStartNormal()
        widthrange = self.getDrawWidthNormal()
        if not (startrange is None or widthrange is None):
            if self._colorsquarerange:
                qp.setPen(self._colorsquarerange) # Square range color
            else:
                qp.setPen(Qt.NoPen)
            if self._colorrange:
                qp.setBrush(self._colorrange) # Range color
            else:
                qp.setBrush(Qt.NoBrush)
            # Draw range
            if self.isRangeShow():
                qp.drawRect(startrange, 0, widthrange, height)


    def drawWidget(self, qp):
        """ 
        Draw widget progress bar. 

        .. image:: pictures/QRangeLevelsProgressBar.png
            :alt: Diagram
            :align: center
            :height: 580px
        """
        widget_height = self.getHeight()

        if not(self.getMinimum is None or self.getStart is None or self.getEnd is None or self.getMaximum is None):
            # Set values of progress
            self.updateProgress(0, self.getWidth())

            # Set chunk progress bar
            self.updateDrawChunk(qp, widget_height)

            if self.isLevelsShow():
                # Set levels of bar
                self.updateDrawLevels(qp, widget_height)

            # Set normal range progress color
            self.updateDrawRange(qp, widget_height)

            # Set square chunk progress bar
            self.updateDrawSquareChunk(qp, widget_height)


    def paintEvent(self, e):
        """
        Redraw widget progress bar on event repaint().

        :param e: event of widget.
        :type e: QEvent
        """
        qp = QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()





class QRangeTextsProgressBar(QRangeProgressBar, DrawTexts):
    """
    .. image:: classes/QRangeTextsProgressBar.png
        :alt: Diagram
        :align: center
        :width: 600px
        :height: 680px

    Create a range with labels progress bar widget.

    :param start: The start of range progress.
    :type start: int
    :param end: The end of range progress.
    :type end: int
    :param minimum: The start of plage values.
    :type minimum: int
    :param maximum: The end of plages value.
    :type maximum: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    """

    def __init__(self, minimum, maximum, start, end, islogscale, logarithmbase):
        """ Initialize class QRangeTextsProgressBar. """
        QRangeProgressBar.__init__(self, minimum, maximum, start, end, islogscale, logarithmbase)
        DrawTexts.__init__(self)


    def drawWidget(self, qp):
        """ 
        Draw widget progress bar. 

        .. image:: pictures/QRangeTextsProgressBar.png
            :alt: Diagram
            :align: center
            :height: 580px
        """
        widget_height = self.getHeight()

        if not(self.getMinimum is None or self.getStart is None or self.getEnd is None or self.getMaximum is None):
            # Set values of progress
            self.updateProgress(0, self.getWidth())

            # Set chunk progress bar
            self.updateDrawChunk(qp, widget_height)

            # Set normal range progress color
            self.updateDrawRange(qp, widget_height)

            # Set square chunk progress bar
            self.updateDrawSquareChunk(qp, widget_height)

            # Set labels of progress bar
            if self.isTextsShow():
                self.updateDrawTexts(qp, widget_height)


    def paintEvent(self, e):
        """
        Redraw widget progress bar on event repaint().

        :param e: event of widget.
        :type e: QEvent
        """
        qp = QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()





class QRangeGraduatedProgressBar(QRangeProgressBar, DrawGraduations):
    """
    .. image:: classes/QRangeGraduatedProgressBar.png
        :alt: Diagram
        :align: center
        :height: 600px

    Create a range with graduations progress bar widget.

    :param start: The start of range progress.
    :type start: int
    :param end: The end of range progress.
    :type end: int
    :param minimum: The start of plage values.
    :type minimum: int
    :param maximum: The end of plages value.
    :type maximum: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    """

    def __init__(self, minimum, maximum, start, end, islogscale, logarithmbase):
        """ Initialize class QRangeGraduatedProgressBar. """
        QRangeProgressBar.__init__(self, minimum, maximum, start, end, islogscale, logarithmbase)
        DrawGraduations.__init__(self)


    def drawWidget(self, qp):
        """ 
        Draw widget progress bar.

        .. image:: pictures/QRangeGraduatedProgressBar.png
            :alt: Diagram
            :align: center
            :height: 580px
        """
        widget_height = self.getHeight()

        if not(self.getMinimum is None or self.getStart is None or self.getEnd is None or self.getMaximum is None):
            # Set values of progress
            self.updateProgress(0, self.getWidth())

            # Set chunk progress bar
            self.updateDrawChunk(qp, widget_height)

            # Set normal range progress color
            self.updateDrawRange(qp, widget_height)

            # Set square chunk progress bar
            self.updateDrawSquareChunk(qp, widget_height)

            # Set graduation progress bar
            if self.isGraduationsShow():
                self.updateDrawGraduations(qp, widget_height)


    def paintEvent(self, e):
        """
        Redraw widget progress bar on event repaint().

        :param e: event of widget.
        :type e: QEvent
        """
        qp = QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()





class QRangeGraduatedTextsProgressBar(QRangeTextsProgressBar, DrawGraduations):
    """
    .. image:: classes/QRangeGraduatedTextsProgressBar.png
        :alt: Diagram
        :align: center
        :height: 550px

    Create a range with graduations progress bar widget.

    :param start: The start of range progress.
    :type start: int
    :param end: The end of range progress.
    :type end: int
    :param minimum: The start of plage values.
    :type minimum: int
    :param maximum: The end of plages value.
    :type maximum: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    """

    def __init__(self, minimum, maximum, start, end, islogscale, logarithmbase):
        """ Initialize class QRangeGraduatedProgressBar. """
        DrawGraduations.__init__(self)
        QRangeTextsProgressBar.__init__(self, minimum, maximum, start, end, islogscale, logarithmbase)


    def drawWidget(self, qp):
        """ 
        Draw widget progress bar. 

        .. image:: pictures/QRangeGraduatedTextsProgressBar.png
            :alt: Diagram
            :align: center
            :height: 580px
        """
        widget_height = self.getHeight()

        if not(self.getMinimum is None or self.getStart is None or self.getEnd is None or self.getMaximum is None):
            # Set values of progress
            self.updateProgress(0, self.getWidth())

            # Set chunk progress bar
            self.updateDrawChunk(qp, widget_height)

            # Set normal range progress color
            self.updateDrawRange(qp, widget_height)

            # Set square chunk progress bar
            self.updateDrawSquareChunk(qp, widget_height)

            # Set graduation progress bar
            if self.isGraduationsShow():
                self.updateDrawGraduations(qp, widget_height)

            # Set labels of progress bar
            if self.isTextsShow():
                self.updateDrawTexts(qp, widget_height)


    def paintEvent(self, e):
        """
        Redraw widget progress bar on event repaint().

        :param e: event of widget.
        :type e: QEvent
        """
        qp = QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()





class QRangeLevelsTextsProgressBar(QRangeLevelsProgressBar, DrawTexts):
    """
    .. image:: classes/QRangeLevelsTextsProgressBar.png
        :alt: Diagram
        :align: center
        :height: 650px

    Create a range with levels progress bar widget.

    :param minimum: The start of plage values.
    :type minimum: int
    :param maximum: The end of plages value.
    :type maximum: int
    :param start: The start of range progress.
    :type start: int
    :param end: The end of range progress.
    :type end: int
    :param alert_min: Level of minimum alert value.
    :type alert_min: int
    :param warning_min: Level of minimum warning value.
    :type warning_min: int
    :param warning_max: Level of maximum warning value.
    :type  warning_max: int
    :param alert_max: Level of maximum alert value.
    :type alert_max: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    """

    def __init__(self, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase):
        """ Initialize class QRangeLevelsTextsProgressBar. """
        DrawTexts.__init__(self)
        QRangeLevelsProgressBar.__init__(self, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)


    def drawWidget(self, qp):
        """ 
        Draw widget progress bar. 

        .. image:: pictures/QRangeLevelsTextsProgressBar.png
            :alt: Diagram
            :align: center
            :height: 580px
        """
        widget_height = self.getHeight()

        if not(self.getMinimum is None or self.getStart is None or self.getEnd is None or self.getMaximum is None):
            # Set values of progress
            self.updateProgress(0, self.getWidth())

            # Set chunk progress bar
            self.updateDrawChunk(qp, widget_height)

            if self.isLevelsShow():
                # Set levels of bar
                self.updateDrawLevels(qp, widget_height)

            # Set normal range progress color
            self.updateDrawRange(qp, widget_height)

            # Set square chunk progress bar
            self.updateDrawSquareChunk(qp, widget_height)

            # Set labels of progress bar
            if self.isTextsShow():
                self.updateDrawTexts(qp, widget_height)


    def paintEvent(self, e):
        """
        Redraw widget progress bar on event repaint().

        :param e: event of widget.
        :type e: QEvent
        """
        qp = QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()





class QRangeLevelsGraduatedProgressBar(QRangeLevelsProgressBar, DrawGraduations):
    """
    .. image:: classes/QRangeLevelsGraduatedProgressBar.png
        :alt: Diagram
        :align: center
        :height: 650px

    Create a range with levels progress bar widget.

    :param minimum: The start of plage values.
    :type minimum: int
    :param maximum: The end of plages value.
    :type maximum: int
    :param start: The start of range progress.
    :type start: int
    :param end: The end of range progress.
    :type end: int
    :param alert_min: Level of minimum alert value.
    :type alert_min: int
    :param warning_min: Level of minimum warning value.
    :type warning_min: int
    :param warning_max: Level of maximum warning value.
    :type  warning_max: int
    :param alert_max: Level of maximum alert value.
    :type alert_max: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    """

    def __init__(self, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase):
        """ Initialize class QRangeLevelsGraduatedProgressBar. """
        DrawGraduations.__init__(self)
        QRangeLevelsProgressBar.__init__(self, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)


    def drawWidget(self, qp):
        """ 
        Draw widget progress bar. 

        .. image:: pictures/QRangeLevelsGraduatedProgressBar.png
            :alt: Diagram
            :align: center
            :height: 580px
        """
        widget_height = self.getHeight()

        if not(self.getMinimum is None or self.getStart is None or self.getEnd is None or self.getMaximum is None):
            # Set values of progress
            self.updateProgress(0, self.getWidth())

            # Set chunk progress bar
            self.updateDrawChunk(qp, widget_height)

            if self.isLevelsShow():
                # Set levels of bar
                self.updateDrawLevels(qp, widget_height)

            # Set normal range progress color
            self.updateDrawRange(qp, widget_height)

            # Set square chunk progress bar
            self.updateDrawSquareChunk(qp, widget_height)

            # Set graduation progress bar
            if self.isGraduationsShow():
                self.updateDrawGraduations(qp, widget_height)


    def paintEvent(self, e):
        """
        Redraw widget progress bar on event repaint().

        :param e: event of widget.
        :type e: QEvent
        """
        qp = QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()





class QRangeLevelsGraduatedTextsProgressBar(QRangeLevelsTextsProgressBar, DrawGraduations):
    """
    .. image:: classes/QRangeLevelsGraduatedTextsProgressBar.png
        :alt: Diagram
        :align: center
        :height: 580px

    Create a range with levels progress bar widget.

    :param minimum: The start of plage values.
    :type minimum: int
    :param maximum: The end of plages value.
    :type maximum: int
    :param start: The start of range progress.
    :type start: int
    :param end: The end of range progress.
    :type end: int
    :param alert_min: Level of minimum alert value.
    :type alert_min: int
    :param warning_min: Level of minimum warning value.
    :type warning_min: int
    :param warning_max: Level of maximum warning value.
    :type  warning_max: int
    :param alert_max: Level of maximum alert value.
    :type alert_max: int
    :param islogscale: Choose logarithm state scale.
    :type islogscale: bool
    :param logarithmbase: Choose logarithm value base.
    :type logarithmbase: int
    """

    def __init__(self, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase):
        """ Initialize class QRangeLevelsGraduatedTextsProgressBar. """
        DrawGraduations.__init__(self)
        QRangeLevelsTextsProgressBar.__init__(self, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)


    def drawWidget(self, qp):
        """ 
        Draw widget progress bar.

        .. image:: pictures/QRangeLevelsGraduatedTextsProgressBar.png
            :alt: Diagram
            :align: center
            :height: 580px
        """
        widget_height = self.getHeight()

        if not(self.getMinimum is None or self.getStart is None or self.getEnd is None or self.getMaximum is None):
            # Set values of progress
            self.updateProgress(0, self.getWidth())

            # Set chunk progress bar
            self.updateDrawChunk(qp, widget_height)

            if self.isLevelsShow():
                # Set levels of bar
                self.updateDrawLevels(qp, widget_height)

            # Set normal range progress color
            self.updateDrawRange(qp, widget_height)

            # Set square chunk progress bar
            self.updateDrawSquareChunk(qp, widget_height)

            # Set graduation progress bar
            if self.isGraduationsShow():
                self.updateDrawGraduations(qp, widget_height)

            # Set labels of progress bar
            if self.isTextsShow():
                self.updateDrawTexts(qp, widget_height)


    def paintEvent(self, e):
        """
        Redraw widget progress bar on event repaint().

        :param e: event of widget.
        :type e: QEvent
        """
        qp = QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()





class QFactoryRangeProgressBar(object):
    """
    .. image:: pictures/QFactoryRangeProgressBar.png
        :alt: Diagram
        :align: center
        :height: 580px

    Create a range progress bar widget.

    :param minimum: The start of plage values.
    :type minimum: int
    :param maximum: The end of plages value.
    :type maximum: int
    :param start: The start of range progress.
    :type start: int
    :param end: The end of range progress.
    :type end: int
    :param islogscale: Choose logarithm state scale -- default False.
    :type islogscale: bool
    :param isaddtexts: Add indicators text for progress bar -- default False.
    :type isaddtexts: bool
    :param isgraduated: Add graduations for progress bar -- default False.
    :type isgraduated: bool
    :param islevels: Add levels for progress bar -- default False.
    :type islevels: bool
    :param alert_min: Level of minimum alert value -- default None.
    :type alert_min: int
    :param warning_min: Level of minimum warning value -- default None.
    :type warning_min: int
    :param warning_max: Level of maximum warning value -- default None.
    :type  warning_max: int
    :param alert_max: Level of maximum alert value -- default None.
    :type alert_max: int
    :param logarithmbase: Choose logarithm value base -- default 10.
    :type logarithmbase: int
    """

    def __new__(self, minimum, maximum, start, end, islogscale=False, isaddtexts=False, isgraduated=False, islevels=False, alert_min=None, warning_min=None, warning_max=None, alert_max=None, logarithmbase=10):
        """ Initialize class QRangeProgressBar. """
        if islevels:
            if isaddtexts:
                if isgraduated:
                    return QRangeLevelsGraduatedTextsProgressBar(minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)
                else:
                    return QRangeLevelsTextsProgressBar(minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)
            else:
                if isgraduated:
                    return QRangeLevelsGraduatedProgressBar(minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)
                else:
                    return QRangeLevelsProgressBar(minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)
        else:
            if isaddtexts:
                if isgraduated:
                    return QRangeGraduatedTextsProgressBar(minimum, maximum, start, end, islogscale, logarithmbase)
                else:
                    return QRangeTextsProgressBar(minimum, maximum, start, end, islogscale, logarithmbase)
            else:
                if isgraduated:
                    return QRangeGraduatedProgressBar(minimum, maximum, start, end, islogscale, logarithmbase)
                else:
                    return QRangeProgressBar(minimum, maximum, start, end, islogscale, logarithmbase)





class QSlideRange(QWidget):
    """
    .. image:: classes/QSlideRange.png
        :alt: Diagram
        :align: center
        :height: 580px

    Create a slide range widget.

    :param minimum: The start of progress bar.
    :type minimum: int
    :param maximum: The end of progress bar.
    :type maximum: int
    :param start: The start of range progress.
    :type start: int
    :param end: The end of range progress.
    :type end: int
    :param islogscale: Choose logarithm state scale -- default False.
    :type islogscale: bool
    :param isaddtexts: Add indicators text for progress bar.
    :type isaddtexts: bool
    :param isgraduated: Show the isgraduated progress bar.
    :type isgraduated: bool
    :param islevels: Add levels for progress bar -- default False.
    :type islevels: bool
    :param alert_min: Level of minimum alert value -- default None.
    :type alert_min: int
    :param warning_min: Level of minimum warning value -- default None.
    :type warning_min: int
    :param warning_max: Level of maximum warning value -- default None.
    :type  warning_max: int
    :param alert_max: Level of maximum alert value -- default None.
    :type alert_max: int
    :param logarithmbase: Choose logarithm value base -- default 10.
    :type logarithmbase: int

        .. image:: pictures/QSlideRange.png
            :alt: Diagram
            :align: center
            :height: 580px
    """

    resized = QtCore.pyqtSignal()
    def __init__(self, minimum, maximum, start, end, islogscale=False, isaddtexts=False, isgraduated=False, islevels=False, alert_min=None, warning_min=None, warning_max=None, alert_max=None, logarithmbase=10):
        """ Initialize class QSlideRange. """
        # Initialize QWidget
        super().__init__()
        
        self._liststepstart = None
        self._liststepend = None
        
        self.initUI(minimum, maximum, start, end, islogscale, isaddtexts, isgraduated, islevels, alert_min, warning_min, warning_max, alert_max, logarithmbase)
        
    def initUI(self, min_progress_bar, max_progress_bar, start_range, end_range, islogscale, addtexts, addgraduated, addlevels, alertmin, warningmin, warningmax, alertmax, logarithmbase):
        """
        .. image:: pictures/QSlideRange.png
            :alt: Diagram
            :align: center
            :height: 580px

        Initialize UI slide range widget

        :param min_progress_bar: The start of progress bar.
        :type min_progress_bar: int
        :param max_progress_bar: The end of progress bar.
        :type max_progress_bar: int
        :param start_range: The start of range progress.
        :type start_range: int
        :param end_range: The end of range progress.
        :type end_range: int
        :param islogscale: Choose logarithm state scale.
        :type islogscale: bool
        :param addtexts: Add indicators text for progress bar.
        :type addtexts: bool
        :param addgraduated: Show the isgraduated progress bar.
        :type addgraduated: bool
        :param addlevels: Add levels for progress bar -- default False.
        :type addlevels: bool
        :param alert_min: Level of minimum alert value -- default None.
        :type alert_min: int
        :param warning_min: Level of minimum warning value -- default None.
        :type warning_min: int
        :param warning_max: Level of maximum warning value -- default None.
        :type  warning_max: int
        :param alert_max: Level of maximum alert value -- default None.
        :type alert_max: int
        :param logarithmbase: Choose logarithm value base -- default 10.
        :type logarithmbase: int
        """
        # Set default range slider parameters
        layoutscreen = QHBoxLayout(self)
        layoutscreen.setContentsMargins(0, 0, 0, 0)
        
        self.rangedisplay = QFactoryRangeProgressBar(min_progress_bar, max_progress_bar, start_range, end_range, islogscale, addtexts, addgraduated, addlevels, alertmin, warningmin, warningmax, alertmax, logarithmbase)
        self.rangedisplay.setOffsetStartBar()
        self.rangedisplay.setOffsetEndBar()
        if addtexts:
            self.rangedisplay.showTexts()
            self.rangedisplay.hideExtremumsTexts()
        if addgraduated:
            self.rangedisplay.setGraduationsPositionExpand()
            self.rangedisplay.showGraduations()
            self.rangedisplay.hideTextsGraduations()

        layoutscreen.addWidget(self.rangedisplay)
        
        # Set slider start
        self.sliderstart = QSlider(Qt.Horizontal, self)
        self.sliderstart.setMinimum(min_progress_bar)
        self.sliderstart.setMaximum(end_range)
        self.sliderstart.setValue(start_range)
        self.sliderstart.setToolTip(str(start_range))

        # Set slider end
        self.sliderend = QSlider(Qt.Horizontal, self)
        self.sliderend.setMinimum(start_range)
        self.sliderend.setMaximum(max_progress_bar)
        self.sliderend.setValue(end_range)
        self.sliderend.setToolTip(str(end_range))

        # Set slider start aspect
        factor = self.geometry().width() / (max_progress_bar - min_progress_bar +1)
        width_start_progress_bar = int(end_range * factor)
        self.sliderstart.setFixedWidth(width_start_progress_bar)
        self.sliderstart.move(0, self.geometry().height() - 2*self.sliderstart.height()/3 +2)
        STYLE_CSS_MIN = """
            QSlider::groove:horizontal {
                border: 0px solid;
                height: 2px;
                margin: 1px O;
                border-radius: 3px;
            }

            QSlider::handle:horizontal {
                background: #ececec;
                border: 1px solid #1abc9c;
                width: 3px;
                margin: -14px 0; /* expand outside the groove */
                border-radius: 1px;
            }

            QSlider::add-page:horizontal {
                background: #1abc9c;
            }

            QSlider::sub-page:horizontal {
                background: darkgray;
            }
            """
        self.sliderstart.setStyleSheet(STYLE_CSS_MIN)

        # Set slider end aspect
        factor = self.geometry().width() / (max_progress_bar - min_progress_bar)
        width_end_progress_bar = int((max_progress_bar - start_range) * factor)
        start_end_range_progress = int((start_range-1) * factor)
        self.sliderend.setFixedWidth(width_end_progress_bar)
        self.sliderend.move(start_end_range_progress, - self.sliderend.height()/3 -2)
        STYLE_CSS_MAX = """
            QSlider::groove:horizontal {
                border: 0px solid;
                height: 2px;
                margin: 1px O;
                border-radius: 3px;
            }

            QSlider::handle:horizontal {
                background: #ececec;
                border: 1px solid #1abc9c;
                width: 3px;
                margin: -14px 0; /* expand outside the groove */
                border-radius: 1px;
            }

            QSlider::add-page:horizontal {
                background: darkgray;
            }

            QSlider::sub-page:horizontal {
                background: #1abc9c;
            }
            """
        self.sliderend.setStyleSheet(STYLE_CSS_MAX)

        # Active actions sliders
        self.sliderstart.valueChanged.connect(self.on_change_value_slider_start_range)
        self.sliderend.valueChanged.connect(self.on_change_value_slider_end_range)

        self.show()

    def on_change_value_slider_start_range(self):
        """ Set start range slide """
        # Set start value
        if self.getListStartStepSlide() is None:
            startvalueslide = self.sliderstart.value()
        else:
            if self.sliderstart.value() > self.sliderend.value():
                for startvalue in self.getListStartStepSlide():
                    if startvalue <= self.sliderend.value():
                        startvalueslide = startvalue
                    else:
                        break
            else:
                if self.sliderstart.value() >= self.getListStartStepSlide()[-1]:
                    startvalueslide = self.getListStartStepSlide()[-1]
                else:
                    for startvalue in self.getListStartStepSlide():
                        if startvalue <= self.sliderstart.value():
                            startvalueslide = startvalue
                        else:
                            break
        #self.sliderstart.setValue(startvalueslide)
        # Set parameters values
        factor = self.geometry().width() / (self.rangedisplay.getMaximum() - self.rangedisplay.getMinimum())
        width_end_progress_bar = int((self.rangedisplay.getMaximum() - startvalueslide) * factor)
        start_end_range_progress = int((startvalueslide-1) * factor)

        self.sliderstart.setToolTip(str(startvalueslide))
        self.sliderend.blockSignals(True)
        self.sliderend.setMinimum(startvalueslide)
        self.sliderend.setFixedWidth(width_end_progress_bar)
        self.sliderend.move(start_end_range_progress, - self.sliderend.height()/3 -2)
        self.sliderend.blockSignals(False)

        self.rangedisplay.setStart(startvalueslide)
        self.rangedisplay.update()
    
    
    def on_change_value_slider_end_range(self):
        """ Set end range slide """
        # Set end value
        if self.getListEndStepSlide() is None:
            endvalueslide = self.sliderend.value()
        else:
            if self.sliderend.value() < self.sliderstart.value():
                for endvalue in self.getListEndStepSlide():
                    if endvalue <= self.sliderend.value():
                        endvalueslide = endvalue
                    else:
                        break
            else:
                if self.sliderend.value() <= self.getListEndStepSlide()[-1]:
                    endvalueslide = self.getListEndStepSlide()[-1]
                else:
                    for endvalue in self.getListEndStepSlide():
                        if endvalue >= self.sliderend.value():
                            endvalueslide = endvalue
                        else:
                            break
        #self.sliderend.setValue(endvalueslide)
        # Set parameters values
        factor = self.geometry().width() / (self.rangedisplay.getMaximum() - self.rangedisplay.getMinimum() +1)
        width_start_progress_bar = int(endvalueslide * factor)

        self.sliderend.setToolTip(str(endvalueslide))
        self.sliderstart.blockSignals(True)
        self.sliderstart.setMaximum(endvalueslide)
        self.sliderstart.setFixedWidth(width_start_progress_bar)
        self.sliderstart.move(0, self.geometry().height() - 2*self.sliderstart.height()/3 +2)
        self.sliderstart.blockSignals(False)

        self.rangedisplay.setEnd(endvalueslide)
        self.rangedisplay.update()


    def setListStartStepSlide(self, liststart):
        """
        Set list of start steps slide
        
        :param liststart: List of step start range.
        :type liststart: list
        """
        if type(liststart) is list:
            # Sort by growing
            liststart.sort()
            self._liststepstart = tuple(liststart)


    def getListStartStepSlide(self):
        """
        Get list of start steps slide
        
        :returns: Start steps slide.
        :rtype: tuple
        """
        return self._liststepstart


    def setListEndStepSlide(self, listend=None):
        """
        Set list of end steps slide
        
        :param listend: List of step end range.
        :type listend: list
        """
        if type(listend) is list:
            # sort by shrinking
            listend.sort(reverse=True)
            self._liststepend = tuple(listend)


    def getListEndStepSlide(self):
        """
        Get list of end steps slide
        
        :returns: End steps slide.
        :rtype: tuple
        """
        return self._liststepend


    def resizeEvent(self, event):
        """ Update widget when resized """
        self.resized.emit()
        # Set parameters values
        factorstart = self.geometry().width() / (self.rangedisplay.getMaximum() - self.rangedisplay.getMinimum() +1)
        width_start_progress_bar = int(self.sliderend.value() * factorstart)
        self.sliderstart.blockSignals(True)
        self.sliderstart.setFixedWidth(width_start_progress_bar)
        self.sliderstart.move(0, self.geometry().height() - 2*self.sliderstart.height()/3 +2)
        self.sliderstart.blockSignals(False)
        factorend = self.geometry().width() / (self.rangedisplay.getMaximum() - self.rangedisplay.getMinimum())
        width_end_progress_bar = int((self.rangedisplay.getMaximum() - self.sliderstart.value()) * factorend)
        start_end_range_progress = int((self.sliderstart.value()-1) * factorend)
        self.sliderend.blockSignals(True)
        self.sliderend.setFixedWidth(width_end_progress_bar)
        self.sliderend.move(start_end_range_progress, - self.sliderend.height()/3 -2)
        self.sliderend.blockSignals(False)
        
        self.rangedisplay.update()

        return super(QSlideRange, self).resizeEvent(event)










if __name__ == '__main__':

    from PyQt5.QtWidgets import QApplication, QVBoxLayout, QHBoxLayout, QFrame, QSizePolicy, QSlider, QLabel, QSpinBox, QCheckBox, QComboBox

    valuemin = -100
    valueminmax = 10
    valuemax = 100
    valuemaxmin = -10
    valuestart = -90
    valueend = 90
    valueoffsetmin = 20
    valueoffsetmax = 20
    valueoffsetspace = 50
    valuealertmin = -70
    valuewarningmin = -50
    valuewarningmax = 50
    valuealertmax = 70
    listgraduations = [-90, -80, -70, -60, -50, -40, -30, -20, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90]

    STYLE_CSS_MIN = """
        QSlider::groove:horizontal {
            border: 0px solid;
            height: 2px;
            margin: 1px O;
            border-radius: 3px;
        }

        QSlider::handle:horizontal {
            background: #ececec;
            border: 1px solid #1abc9c;
            width: 3px;
            /*height: 8px;*/
            margin: -8px 0; /* expand outside the groove */

            border-radius: 1px;
        }

        QSlider::add-page:horizontal {
            background: #1abc9c;
        }

        QSlider::sub-page:horizontal {
            background: darkgray;
        }
        """

    STYLE_CSS_MAX = """
        QSlider::groove:horizontal {
            border: 0px solid;
            height: 2px;
            margin: 1px O;
            border-radius: 3px;
        }

        QSlider::handle:horizontal {
            background: #ececec;
            border: 1px solid #1abc9c;
            width: 3px;
            /*height: 8px;*/
            margin: -8px 0; /* expand outside the groove */
            border-radius: 1px;
        }

        QSlider::add-page:horizontal {
            background: darkgray;
        }

        QSlider::sub-page:horizontal {
            background: #1abc9c;
        }
        """

    def removeRangeWidget():
        """ Remove all range widget """
        

    def objectWidthText():
        """ Set show labels """
        global rangeshow
        if chkbobjecttext.checkState():
            verticallayout.removeWidget(rangeshow)
            rangeshow.setParent(None)
            rangeshow = QFactoryRangeProgressBar(valuemin, valuemax, valuestart, valueend, chkblogarithmscale.checkState(), chkbobjecttext.checkState(), chkbobjectgraduations.checkState(), chkbobjectlevel.checkState())
            rangeshow.showTexts()
            rangeshow.setMinimumLabelPositionCenter()
            rangeshow.setMaximumLabelPositionCenter()
            rangeshow.setMaximumLabelReadingReverse(True)
            if chkblogarithmscale.checkState():
                rangeshow.setLogarithmScale()
            if chkboffsetshow.checkState():
                rangeshow.setOffsetStartBar(valueoffsetmin)
                rangeshow.setOffsetEndBar(valueoffsetmax)
                rangeshow.showOffset()
            if chkbobjectgraduations.checkState():
                rangeshow.setListLabelGraduations(listgraduations)
                rangeshow.showGraduations()
            if chkbobjectlevel.checkState():
                rangeshow.setMinimumAlert(valuealertmin)
                rangeshow.setMinimumWarning(valuewarningmin)
                rangeshow.setMaximumWarning(valuewarningmax)
                rangeshow.setMaximumAlert(valuealertmax)
            verticallayout.insertWidget(2, rangeshow)
            chkblabelsshow.setVisible(True)
            chkblabelsshow.setChecked(True)
        else:
            chkblabelsshow.setChecked(False)
            chkblabelsshow.setVisible(False)
            verticallayout.removeWidget(rangeshow)
            rangeshow.setParent(None)
            rangeshow = QFactoryRangeProgressBar(valuemin, valuemax, valuestart, valueend, chkblogarithmscale.checkState(), chkbobjecttext.checkState(), chkbobjectgraduations.checkState(), chkbobjectlevel.checkState())
            if chkblogarithmscale.checkState():
                rangeshow.setLogarithmScale()
            if chkboffsetshow.checkState():
                rangeshow.setOffsetStartBar(valueoffsetmin)
                rangeshow.setOffsetEndBar(valueoffsetmax)
                rangeshow.showOffset()
            if chkbobjectgraduations.checkState():
                rangeshow.setListLabelGraduations(listgraduations)
                rangeshow.showGraduations()
            if chkbobjectlevel.checkState():
                rangeshow.setMinimumAlert(valuealertmin)
                rangeshow.setMinimumWarning(valuewarningmin)
                rangeshow.setMaximumWarning(valuewarningmax)
                rangeshow.setMaximumAlert(valuealertmax)
            verticallayout.insertWidget(2, rangeshow)


    def objectWidthLevel():
        """ Set show labels """
        global rangeshow
        if chkbobjectlevel.checkState():
            verticallayout.removeWidget(rangeshow)
            rangeshow.setParent(None)
            rangeshow = QFactoryRangeProgressBar(valuemin, valuemax, valuestart, valueend, chkblogarithmscale.checkState(), chkbobjecttext.checkState(), chkbobjectgraduations.checkState(), chkbobjectlevel.checkState())
            rangeshow.setMinimumAlert(valuealertmin)
            rangeshow.setMinimumWarning(valuewarningmin)
            rangeshow.setMaximumWarning(valuewarningmax)
            rangeshow.setMaximumAlert(valuealertmax)
            if chkblogarithmscale.checkState():
                rangeshow.setLogarithmScale()
            if chkboffsetshow.checkState():
                rangeshow.setOffsetStartBar(valueoffsetmin)
                rangeshow.setOffsetEndBar(valueoffsetmax)
                rangeshow.showOffset()
            if chkbobjecttext.checkState(): 
                rangeshow.showTexts()
                rangeshow.setMinimumLabelPositionCenter()
                rangeshow.setMaximumLabelPositionCenter()
                rangeshow.setMaximumLabelReadingReverse(True)
            if chkbobjectgraduations.checkState():
                rangeshow.setListLabelGraduations(listgraduations)
                rangeshow.showGraduations()
            verticallayout.insertWidget(2, rangeshow)
            chkblevelsshow.setVisible(True)
            chkblevelsshow.setChecked(True)
        else:
            chkblevelsshow.setChecked(False)
            chkblevelsshow.setVisible(False)
            verticallayout.removeWidget(rangeshow)
            rangeshow.setParent(None)
            rangeshow = QFactoryRangeProgressBar(valuemin, valuemax, valuestart, valueend, chkblogarithmscale.checkState(), chkbobjecttext.checkState(), chkbobjectgraduations.checkState(), chkbobjectlevel.checkState())
            if chkblogarithmscale.checkState():
                rangeshow.setLogarithmScale()
            if chkboffsetshow.checkState():
                rangeshow.setOffsetStartBar(valueoffsetmin)
                rangeshow.setOffsetEndBar(valueoffsetmax)
                rangeshow.showOffset()
            if chkbobjecttext.checkState(): 
                rangeshow.showTexts()
                rangeshow.setMinimumLabelPositionCenter()
                rangeshow.setMaximumLabelPositionCenter()
                rangeshow.setMaximumLabelReadingReverse(True)
            if chkbobjectgraduations.checkState():
                rangeshow.setListLabelGraduations(listgraduations)
                rangeshow.showGraduations()
            verticallayout.insertWidget(2, rangeshow)


    def objectWidthGraduations():
        """ Set show labels """
        global rangeshow
        if chkbobjectgraduations.checkState():
            verticallayout.removeWidget(rangeshow)
            rangeshow.setParent(None)
            rangeshow = QFactoryRangeProgressBar(valuemin, valuemax, valuestart, valueend, chkblogarithmscale.checkState(), chkbobjecttext.checkState(), chkbobjectgraduations.checkState(), chkbobjectlevel.checkState())
            rangeshow.setListLabelGraduations(listgraduations)
            rangeshow.showGraduations()
            if chkblogarithmscale.checkState():
                rangeshow.setLogarithmScale()
            if chkboffsetshow.checkState():
                rangeshow.setOffsetStartBar(valueoffsetmin)
                rangeshow.setOffsetEndBar(valueoffsetmax)
                rangeshow.showOffset()
            if chkbobjecttext.checkState(): 
                rangeshow.showTexts()
                rangeshow.setMinimumLabelPositionCenter()
                rangeshow.setMaximumLabelPositionCenter()
            if chkbobjectlevel.checkState():
                rangeshow.setMinimumAlert(valuealertmin)
                rangeshow.setMinimumWarning(valuewarningmin)
                rangeshow.setMaximumWarning(valuewarningmax)
                rangeshow.setMaximumAlert(valuealertmax)
            verticallayout.insertWidget(2, rangeshow)
            chkbgraduationsshow.setVisible(True)
            chkbgraduationsshow.setChecked(True)
        else:
            chkbgraduationsshow.setChecked(False)
            chkbgraduationsshow.setVisible(False)
            verticallayout.removeWidget(rangeshow)
            rangeshow.setParent(None)
            rangeshow = QFactoryRangeProgressBar(valuemin, valuemax, valuestart, valueend, chkblogarithmscale.checkState(), chkbobjecttext.checkState(), chkbobjectgraduations.checkState(), chkbobjectlevel.checkState())
            if chkblogarithmscale.checkState():
                rangeshow.setLogarithmScale()
            if chkboffsetshow.checkState():
                rangeshow.setOffsetStartBar(valueoffsetmin)
                rangeshow.setOffsetEndBar(valueoffsetmax)
                rangeshow.showOffset()
            if chkbobjecttext.checkState(): 
                rangeshow.showTexts()
                rangeshow.setMinimumLabelPositionCenter()
                rangeshow.setMaximumLabelPositionCenter()
            if chkbobjectlevel.checkState():
                rangeshow.setMinimumAlert(valuealertmin)
                rangeshow.setMinimumWarning(valuewarningmin)
                rangeshow.setMaximumWarning(valuewarningmax)
                rangeshow.setMaximumAlert(valuealertmax)
            verticallayout.insertWidget(2, rangeshow)


    def realValue(valslide):
        """ Return real value of slider value """
        return valslide + rangeshow.getMinimum()


    def sliderValue(value):
        """ Return slider value of real value """
        return value - rangeshow.getMinimum()


    def on_change_value_sliderend():
        """ Set max value slider start """
        rangeshow.setEnd(realValue(sliderend.value()))
        rangeshow.repaint()
        sliderend.setToolTip(str(realValue(sliderend.value())))
        sliderstart.setMaximum(sliderend.value())
        sliderstart.setFixedWidth(rangeshow.getDrawValueEndRange())


    def on_change_value_sliderstart():
        """ Set min value slider end """
        rangeshow.setStart(realValue(sliderstart.value()))
        rangeshow.repaint()
        sliderstart.setToolTip(str(realValue(sliderstart.value())))
        sliderend.setMinimum(sliderstart.value())
        sliderend.setFixedWidth(rangeshow.getWidth() - rangeshow.getDrawValueStartRange())


    def set_value_min_progress_bar():
        """ Set min value of progress bar """
        if valmin.value() > valmax.value():
            valmax.setValue(valmin.value())

        if valmin.value() > rangeshow.getStart():
            rangeshow.setStart(valmin.value())

        if valmin.value() > rangeshow.getEnd():
            rangeshow.setEnd(valmin.value())

        rangeshow.setMinimum(valmin.value())
        rangeshow.repaint()

        # Set size slides bars
        sliderstart.setFixedWidth(rangeshow.getDrawValueEndRange())
        sliderend.setFixedWidth(rangeshow.getWidth() - rangeshow.getDrawValueStartRange())

        # Get reals values
        progressvaluemin = rangeshow.getMinimum()
        progressvaluestart = rangeshow.getStart()
        progressvalueend = rangeshow.getEnd()
        progressvaluemax = rangeshow.getMaximum()

        # Set news values cursor slides
        sliderstart.setMaximum(progressvalueend - progressvaluemin)
        sliderstart.setValue(sliderValue(progressvaluestart))
        sliderend.setMinimum(sliderValue(progressvaluestart))
        sliderend.setMaximum(progressvaluemax - progressvaluemin)
        sliderend.setValue(sliderValue(progressvalueend))


    def set_value_max_progress_bar():
        """ Set max value of progress bar """
        if valmax.value() < valmin.value():
            valmin.setValue(valmax.value())

        if valmax.value() < rangeshow.getEnd():
            rangeshow.setEnd(valmax.value())

        if valmax.value() < rangeshow.getStart():
            rangeshow.setStart(valmax.value())

        rangeshow.setMaximum(valmax.value())
        rangeshow.repaint()

        # Set size slides bars
        sliderstart.setFixedWidth(rangeshow.getDrawValueEndRange())
        sliderend.setFixedWidth(rangeshow.getWidth() - rangeshow.getDrawValueStartRange())

        # Get reals values
        progressvaluemin = rangeshow.getMinimum()
        progressvaluestart = rangeshow.getStart()
        progressvalueend = rangeshow.getEnd()
        progressvaluemax = rangeshow.getMaximum()

        # Set news values cursor slides
        sliderstart.setMaximum(progressvalueend - progressvaluemin)
        sliderstart.setValue(sliderValue(progressvaluestart))
        sliderend.setMinimum(sliderValue(progressvaluestart))
        sliderend.setMaximum(progressvaluemax - progressvaluemin)
        sliderend.setValue(sliderValue(progressvalueend))


    def showLabels():
        """ Set show labels """
        if chkblabelsshow.checkState():
            rangeshow.showTexts()
            conflbl.setVisible(True)
        else:
            rangeshow.hideTexts()
            conflbl.setVisible(False)


    def showOffset():
        """ Set show labels """
        if chkboffsetshow.checkState():
            rangeshow.showOffset()
        else:
            rangeshow.hideOffset()
        rangeshow.repaint()


    def showRangeLabels():
        """ Set range labels show """
        if chkbrangelabelsshow.checkState():
            rangeshow.showRangeTexts()
            confrangelbl.setVisible(True)
        else:
            rangeshow.hideRangeTexts()
            confrangelbl.setVisible(False)
        rangeshow.repaint()


    def textRangeInside():
        """ Set horizontal position range label """
        if chkbtextinside.checkState():
            rangeshow.setRangeTextInside()
        else:
            rangeshow.setRangeTextsOutside()
        rangeshow.repaint()


    def showExtemumsLabels():
        """ Set extremums labels show """
        if chkbextremumsshow.checkState():
            rangeshow.showExtremumsTexts()
            confextremumlbl.setVisible(True)
        else:
            rangeshow.hideExtremumsTexts()
            confextremumlbl.setVisible(False)
        rangeshow.repaint()


    def setValueMinOffsetBar():
        """ Set value of minimimum offset bar """
        rangeshow.setOffsetStartBar(valoffsetmin.value())
        rangeshow.repaint()


    def setValueMaxOffsetBar():
        """ Set value of maximimum offset bar """
        rangeshow.setOffsetEndBar(valoffsetmax.value())
        rangeshow.repaint()


    def reverseMinimumLabels():
        """ Set reverse label min show """
        if chkbreverseminlabel.checkState():
            rangeshow.setMinimumLabelReadingReverse(True)
        else:
            rangeshow.setMinimumLabelReadingReverse(False)
        rangeshow.repaint()

    def reverseMaximumLabels():
        """ Set reverse label max show """
        if chkbreversemaxlabel.checkState():
            rangeshow.setMaximumLabelReadingReverse(True)
        else:
            rangeshow.setMaximumLabelReadingReverse(False)
        rangeshow.repaint()


    def positionLabelMin():
        """ Set label min position """
        position = dropdownlblminposition.currentText()
        if position == 'Top':
            rangeshow.setMinimumLabelPositionTop()
        if position == 'Center':
            rangeshow.setMinimumLabelPositionCenter()
        if position == 'Bottom':
            rangeshow.setMinimumLabelPositionBottom()
        rangeshow.repaint()


    def positionLabelMax():
        """ Set label max position """
        position = dropdownlblmaxposition.currentText()
        if position == 'Top':
            rangeshow.setMaximumLabelPositionTop()
        if position == 'Center':
            rangeshow.setMaximumLabelPositionCenter()
        if position == 'Bottom':
            rangeshow.setMaximumLabelPositionBottom()
        rangeshow.repaint()


    def positionLabelStart():
        """ Set start label position """
        position = dropdownstartlblposition.currentText()
        if position == 'Top':
            rangeshow.setStartLabelPositionTop()
        if position == 'Middle-Top':
            rangeshow.setStartLabelPositionMiddleTop()
        if position == 'Middle':
            rangeshow.setStartLabelPositionMiddle()
        if position == 'Middle-Bottom':
            rangeshow.setStartLabelPositionMiddleBottom()
        if position == 'Bottom':
            rangeshow.setStartLabelPositionBottom()
        rangeshow.repaint()


    def positionLabelEnd():
        """ Set end label position """
        position = dropdownendlblposition.currentText()
        if position == 'Top':
            rangeshow.setEndLabelPositionTop()
        if position == 'Middle-Top':
            rangeshow.setEndLabelPositionMiddleTop()
        if position == 'Middle':
            rangeshow.setEndLabelPositionMiddle()
        if position == 'Middle-Bottom':
            rangeshow.setEndLabelPositionMiddleBottom()
        if position == 'Bottom':
            rangeshow.setEndLabelPositionBottom()
        rangeshow.repaint()


    def showLevels():
        """ Set show labels """
        if chkblevelsshow.checkState():
            rangeshow.showLevels()
            conflevel.setVisible(True)
        else:
            rangeshow.hideLevels()
            conflevel.setVisible(False)


    def showAlertMax():
        """ Set alert level max """
        if chkbalertmax.checkState():
            rangeshow.showMaximumAlert()
        else:
            rangeshow.hideMaximumAlert()
        rangeshow.repaint()


    def showWarningMax():
        """ Set warning level max """
        if chkbwarningmax.checkState():
            rangeshow.showMaximumWarning()
        else:
            rangeshow.hideMaximumWarning()
        rangeshow.repaint()


    def showWarningMin():
        """ Set warning level min """
        if chkbwarningmin.checkState():
            rangeshow.showMinimumWarning()
        else:
            rangeshow.hideMinimumWarning()
        rangeshow.repaint()


    def showAlertMin():
        """ Set alert level min """
        if chkbalertmin.checkState():
            rangeshow.showMinimumAlert()
        else:
            rangeshow.hideMinimumAlert()
        rangeshow.repaint()


    def showGraduations():
        """ Set show labels """
        if chkbgraduationsshow.checkState():
            rangeshow.showGraduations()
            confgraduations.setVisible(True)
        else:
            rangeshow.hideGraduations()
            confgraduations.setVisible(False)
        #rangeshow.repaint()


    def showTextsGraduations():
        """ Show text graduations """
        if chkbtextgraduation.checkState():
            rangeshow.showTextsGraduations()
            conftextgraduations.setVisible(True)
        else:
            rangeshow.hideTextsGraduations()
            conftextgraduations.setVisible(False)
        rangeshow.repaint()


    def setLogarithmGraduations():
        """ Set logarithm graduations """
        if chkblogarithmscale.checkState():
            rangeshow.setLogarithmScale()
        else:
            rangeshow.setLinearScale()
        rangeshow.repaint()


    def positionGraduations():
        """ Set graduations position """
        position = dropdowngraduationsposition.currentText()
        if position == 'Top':
            rangeshow.setGraduationsPositionTop()
        if position == 'Center':
            rangeshow.setGraduationsPositionCenter()
        if position == 'Bottom':
            rangeshow.setGraduationsPositionBottom()
        if position == 'Top-Bottom':
            rangeshow.setGraduationsPositionTopBottom()
        if position == 'Expand':
            rangeshow.setGraduationsPositionExpand()
        rangeshow.repaint()


    def positionTextGraduations():
        """ Set text graduations position """
        position = dropdowntextgraduationcenterposition.currentText()
        if position == 'Top':
            rangeshow.setPositionTextTopCenterGraduations()
        if position == 'Bottom':
            rangeshow.setPositionTextBottomCenterGraduations()
        rangeshow.repaint()


    def setRangeByStep():
        """ Set split graduation interval calculation by step """
        caculationinterval = 1


    def rangeByPointStep():
        """ Return State of range by point """
        if caculationinterval == 1:
            return True
        else:
            return False


    def setRangeByPoint():
        """ Set split graduation interval calculation by point """
        caculationinterval = 0


    def rangeByPointState():
        """ Return State of range by point """
        if caculationinterval == 0:
            return True
        else:
            return False

    app = QApplication([])
    window = QWidget()
    verticallayout = QVBoxLayout(window)

    # Option create range object
    objectlayout = QHBoxLayout()
    lblobject = QLabel("Progress bar: ")
    chkbobjecttext = QCheckBox('Text', window)
    chkbobjectlevel = QCheckBox('Level', window)
    chkbobjectgraduations = QCheckBox('Graduations', window)
    chkblogarithmscale = QCheckBox('Logarithm scale', window)
    objectlayout.addWidget(lblobject)
    objectlayout.addWidget(chkbobjecttext)
    objectlayout.addWidget(chkbobjectlevel)
    objectlayout.addWidget(chkbobjectgraduations)
    objectlayout.addWidget(chkblogarithmscale)
   
    
    # QRangeProgressBar
    rangeshow = QFactoryRangeProgressBar(valuemin, valuemax, valuestart, valueend, chkblogarithmscale.checkState(), chkbobjecttext.checkState(), chkbobjectgraduations.checkState(), chkbobjectlevel.checkState())
    rangeshow.setOffsetStartBar(valueoffsetmin)
    rangeshow.setOffsetEndBar(valueoffsetmax)
    rangeshow.showOffset()
    if chkbobjecttext.checkState():
        rangeshow.showTexts()
        rangeshow.setMinimumLabelPositionCenter()
        rangeshow.setMaximumLabelPositionCenter()
        rangeshow.setMaximumLabelReadingReverse(True)
    if chkbobjectgraduations.checkState():
        rangeshow.setListLabelGraduations(listgraduations)
        rangeshow.showGraduations()
    if chkbobjectlevel.checkState():
        rangeshow.setMinimumAlert(valuealertmin)
        rangeshow.setMinimumWarning(valuewarningmin)
        rangeshow.setMaximumWarning(valuewarningmax)
        rangeshow.setMaximumAlert(valuealertmax)
        

    # Slider start value range
    sliderstart = QSlider(Qt.Horizontal)
    sliderstart.setMinimum(0)
    sliderstart.setMaximum(valueend - valuemin)
    sliderstart.setValue(valuestart - valuemin)
    sliderstart.setStyleSheet(STYLE_CSS_MIN)
    hboxstart = QHBoxLayout()
    hboxstart.addWidget(sliderstart)
    hboxstart.addStretch()
    hboxstart.setContentsMargins(0, 0, 0, 0)

    # Slider end value range
    sliderend = QSlider(Qt.Horizontal)
    sliderend.setMinimum(valuestart - valuemin)
    sliderend.setMaximum(valuemax - valuemin)
    sliderend.setValue(valueend - valuemin)
    sliderend.setStyleSheet(STYLE_CSS_MAX)
    hboxend = QHBoxLayout()
    hboxend.addStretch()
    hboxend.addWidget(sliderend)
    hboxend.setContentsMargins(0, 0, 0, 0)

    # Spinner value min progress bar
    lblvalmin = QLabel("Minimum value ")
    valmin = QSpinBox()
    valmin.setMinimum(valuemin)
    valmin.setMaximum(valueminmax)
    valmin.setValue(valuemin)
    hboxminimum = QHBoxLayout()
    hboxminimum.addWidget(lblvalmin)
    hboxminimum.addWidget(valmin)
    # Spinner value max progress bar
    lblvalmax = QLabel("Maximum value ")
    valmax = QSpinBox()
    valmax.setMinimum(valuemaxmin)
    valmax.setMaximum(valuemax)
    valmax.setValue(valuemax)
    hboxmaximum = QHBoxLayout()
    hboxmaximum.addWidget(lblvalmax)
    hboxmaximum.addWidget(valmax)
    # HBox set min/max values
    hboxextremum = QHBoxLayout()
    hboxextremum.addLayout(hboxminimum)
    hboxextremum.addLayout(hboxmaximum)

    # Config labels show
    # Spinner offset min
    chkboffsetshow = QCheckBox('Show range offset', window)
    chkboffsetshow.toggle()
    lbloffsetmin = QLabel("Offset min ")
    valoffsetmin = QSpinBox()
    valoffsetmin.setMinimum(0)
    valoffsetmin.setMaximum(valueoffsetspace)
    valoffsetmin.setValue(valueoffsetmin)
    hboxoffsetmin = QHBoxLayout()
    hboxoffsetmin.addWidget(lbloffsetmin)
    hboxoffsetmin.addWidget(valoffsetmin)
    # Spinner offset max
    lbloffsetmax = QLabel("Offset max ")
    valoffsetmax = QSpinBox()
    valoffsetmax.setMinimum(0)
    valoffsetmax.setMaximum(valueoffsetspace)
    valoffsetmax.setValue(valueoffsetmax)
    hboxoffsetmax = QHBoxLayout()
    hboxoffsetmax.addWidget(lbloffsetmax)
    hboxoffsetmax.addWidget(valoffsetmax)
    # HBox config labels
    hboxoffset = QHBoxLayout()
    hboxoffset.addWidget(chkboffsetshow)
    hboxoffset.addLayout(hboxoffsetmin)
    hboxoffset.addLayout(hboxoffsetmax)

    # Widget config labels
    conflbl = QWidget()
    # Labels show properties
    chkbrangelabelsshow = QCheckBox('Show range labels', window)
    chkbrangelabelsshow.toggle()
    vboxconflbl = QVBoxLayout()
    vboxconflbl.addWidget(chkbrangelabelsshow)
    conflbl.setLayout(vboxconflbl)
    # Config range labels
    confrangelbl = QWidget()
    chkbtextinside = QCheckBox('Text inside', window)
    chkbtextinside.toggle()
    # Dropdown label start position
    lblstartpos = QLabel("Position start labels")
    dropdownstartlblposition = QComboBox()
    dropdownstartlblposition.addItem('Top')
    dropdownstartlblposition.addItem('Middle-Top')
    dropdownstartlblposition.addItem('Middle')
    dropdownstartlblposition.addItem('Middle-Bottom')
    dropdownstartlblposition.addItem('Bottom')
    dropdownstartlblposition.setCurrentText('Middle')
    hboxconfstartlbl = QHBoxLayout()
    hboxconfstartlbl.addWidget(lblstartpos)
    hboxconfstartlbl.addWidget(dropdownstartlblposition)
    # Dropdown label end position
    lblendpos = QLabel("Position end labels")
    dropdownendlblposition = QComboBox()
    dropdownendlblposition.addItem('Top')
    dropdownendlblposition.addItem('Middle-Top')
    dropdownendlblposition.addItem('Middle')
    dropdownendlblposition.addItem('Middle-Bottom')
    dropdownendlblposition.addItem('Bottom')
    dropdownendlblposition.setCurrentText('Middle')
    hboxconfendlbl = QHBoxLayout()
    hboxconfendlbl.addWidget(lblendpos)
    hboxconfendlbl.addWidget(dropdownendlblposition)
    # Set configuration range labels
    vboxconfrangelbl = QVBoxLayout()
    vboxconfrangelbl.addWidget(chkbtextinside)
    vboxconfrangelbl.addLayout(hboxconfstartlbl)
    vboxconfrangelbl.addLayout(hboxconfendlbl)
    confrangelbl.setLayout(vboxconfrangelbl)
    vboxconflbl.addWidget(confrangelbl)
    # Config extremums labels
    chkbextremumsshow = QCheckBox('Show extremums labels', window)
    chkbextremumsshow.toggle()
    vboxconflbl.addWidget(chkbextremumsshow)
    # Config extremums labels show
    lblposmin = QLabel("Position label min ")
    dropdownlblminposition = QComboBox()
    dropdownlblminposition.addItem('Top')
    dropdownlblminposition.addItem('Center')
    dropdownlblminposition.addItem('Bottom')
    dropdownlblminposition.setCurrentText('Center')
    hboxposmin = QHBoxLayout()
    hboxposmin.addWidget(lblposmin)
    hboxposmin.addWidget(dropdownlblminposition)
    lblposmax = QLabel("Position label max ")
    dropdownlblmaxposition = QComboBox()
    dropdownlblmaxposition.addItem('Top')
    dropdownlblmaxposition.addItem('Center')
    dropdownlblmaxposition.addItem('Bottom')
    dropdownlblmaxposition.setCurrentText('Center')
    hboxposmax = QHBoxLayout()
    hboxposmax.addWidget(lblposmax)
    hboxposmax.addWidget(dropdownlblmaxposition)
    chkbreverseminlabel = QCheckBox('Revert min label', window)
    chkbreversemaxlabel = QCheckBox('Revert max label', window)
    chkbreversemaxlabel.toggle()
    hboxsetextremumslabels = QHBoxLayout()
    hboxsetextremumslabels.addLayout(hboxposmin)
    hboxsetextremumslabels.addLayout(hboxposmax)
    hboxsetextremumslabels.addWidget(chkbreverseminlabel)
    hboxsetextremumslabels.addWidget(chkbreversemaxlabel)
    # Config extremums labels
    confextremumlbl = QWidget()
    confextremumlbl.setLayout(hboxsetextremumslabels)
    vboxconflbl.addWidget(confextremumlbl)

    # Show levels range informations
    chkbwarningmax = QCheckBox('Show Warning max', window)
    chkbwarningmax.toggle()
    chkbalertmax = QCheckBox('Show Alert max', window)
    chkbalertmax.toggle()
    chkbwarningmin = QCheckBox('Show Warning min', window)
    chkbwarningmin.toggle()
    chkbalertmin = QCheckBox('Show Alert min', window)
    chkbalertmin.toggle()
    hboxlevels = QHBoxLayout()
    hboxlevels.addWidget(chkbalertmin)
    hboxlevels.addWidget(chkbwarningmin)
    hboxlevels.addWidget(chkbwarningmax)
    hboxlevels.addWidget(chkbalertmax)
    conflevel = QWidget()
    conflevel.setLayout(hboxlevels)

    # Show graduations
    chkbtextgraduation = QCheckBox('Show text graduations', window)
    chkbtextgraduation.toggle()
    # Dropdown graduation position
    lblposgraduation = QLabel("Position graduations ")
    dropdowngraduationsposition = QComboBox()
    dropdowngraduationsposition.addItem('Top')
    dropdowngraduationsposition.addItem('Center')
    dropdowngraduationsposition.addItem('Bottom')
    dropdowngraduationsposition.addItem('Top-Bottom')
    dropdowngraduationsposition.addItem('Expand')
    dropdowngraduationsposition.setCurrentText('Center')
    hboxgraduationposition = QHBoxLayout()
    hboxgraduationposition.addWidget(lblposgraduation)
    hboxgraduationposition.addWidget(dropdowngraduationsposition)
    # Dropdown text graduation position
    lbltextgraduationcenterpos = QLabel("Position text graduations ")
    dropdowntextgraduationcenterposition = QComboBox()
    dropdowntextgraduationcenterposition.addItem('Top')
    dropdowntextgraduationcenterposition.addItem('Bottom')
    dropdowntextgraduationcenterposition.setCurrentText('Bottom')
    hboxtextgraduationcenterposition = QHBoxLayout()
    hboxtextgraduationcenterposition.addWidget(lbltextgraduationcenterpos)
    hboxtextgraduationcenterposition.addWidget(dropdowntextgraduationcenterposition)
    conftextgraduations = QWidget()
    conftextgraduations.setLayout(hboxtextgraduationcenterposition)
    hboxgraduations = QHBoxLayout()
    hboxgraduations.addLayout(hboxgraduationposition)
    hboxgraduations.addWidget(chkbtextgraduation)
    hboxgraduations.addWidget(conftextgraduations)
    confgraduations = QWidget()
    confgraduations.setLayout(hboxgraduations)

    # Show Informations
    chkblabelsshow = QCheckBox('Show labels', window)
    #rangeshow.hideTexts()
    conflbl.setVisible(False)
    chkblabelsshow.setVisible(False)
    #chkblabelsshow.toggle()
    chkblevelsshow = QCheckBox('Show levels', window)
    #rangeshow.hideLevels()
    conflevel.setVisible(False)
    chkblevelsshow.setVisible(False)
    #chkblevelsshow.toggle()
    chkbgraduationsshow = QCheckBox('Show graduations', window)
    #rangeshow.hideGraduations()
    confgraduations.setVisible(False)
    chkbgraduationsshow.setVisible(False)
    #chkbgraduationsshow.toggle()

    # Add widgets to test vertical layout
    verticallayout.addLayout(objectlayout)
    verticallayout.addLayout(hboxend)
    verticallayout.addWidget(rangeshow)
    verticallayout.addLayout(hboxstart)
    verticallayout.addLayout(hboxextremum)
    verticallayout.addLayout(hboxoffset)
    verticallayout.addWidget(chkblabelsshow)
    verticallayout.addWidget(conflbl)
    verticallayout.addWidget(chkblevelsshow)
    verticallayout.addWidget(conflevel)
    verticallayout.addWidget(chkbgraduationsshow)
    verticallayout.addWidget(confgraduations)

    window.show()

    # Connect widgets to actions
    chkbobjecttext.stateChanged.connect(objectWidthText)
    chkbobjectlevel.stateChanged.connect(objectWidthLevel)
    chkbobjectgraduations.stateChanged.connect(objectWidthGraduations)
    
    sliderend.valueChanged.connect(on_change_value_sliderend)
    sliderstart.valueChanged.connect(on_change_value_sliderstart)
    valmin.valueChanged.connect(set_value_min_progress_bar)
    valmax.valueChanged.connect(set_value_max_progress_bar)
    chkboffsetshow.stateChanged.connect(showOffset)

    chkblabelsshow.stateChanged.connect(showLabels)
    valoffsetmin.valueChanged.connect(setValueMinOffsetBar)
    valoffsetmax.valueChanged.connect(setValueMaxOffsetBar)
    chkbrangelabelsshow.stateChanged.connect(showRangeLabels)
    chkbtextinside.stateChanged.connect(textRangeInside)
    dropdownstartlblposition.activated.connect(positionLabelStart)
    dropdownendlblposition.activated.connect(positionLabelEnd)
    chkbextremumsshow.stateChanged.connect(showExtemumsLabels)
    chkbreverseminlabel.stateChanged.connect(reverseMinimumLabels)
    chkbreversemaxlabel.stateChanged.connect(reverseMaximumLabels)

    chkblevelsshow.stateChanged.connect(showLevels)
    chkbalertmax.stateChanged.connect(showAlertMax)
    chkbwarningmax.stateChanged.connect(showWarningMax)
    chkbwarningmin.stateChanged.connect(showWarningMin)
    chkbalertmin.stateChanged.connect(showAlertMin)

    chkbgraduationsshow.stateChanged.connect(showGraduations)
    dropdownlblminposition.activated.connect(positionLabelMin)
    dropdownlblmaxposition.activated.connect(positionLabelMax)
    chkbtextgraduation.stateChanged.connect(showTextsGraduations)
    chkblogarithmscale.stateChanged.connect(setLogarithmGraduations)
    dropdowngraduationsposition.activated.connect(positionGraduations)
    dropdowntextgraduationcenterposition.activated.connect(positionTextGraduations)

    # Initialize aspect
    sliderend.setFixedWidth(rangeshow.getWidth() - rangeshow.getDrawValueStartRange())
    sliderstart.setFixedWidth(rangeshow.getDrawValueEndRange())

    app.exec_()
