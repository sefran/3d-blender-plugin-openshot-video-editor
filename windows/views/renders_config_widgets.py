"""
    The ``renders_config_widgets`` module
    -------------------------------------

    :Date: |date|
    :Revision: 1.0
    :Author: Franc SERRES <sefran007@gmail.com> from preview work of Jonathan Thomas <jonathan@openshot.org>
    :Description: Module Views Configurations for OpenShot Animated 3D Title plugin
    :Info: See <https://framagit.org/sefran/range-progress-bar> for more recent info.

    Use it to import 3D Title render animation widgets configurations features.

    :class:`CreateWidgetsParametersRenderPlugin`
    
    :class:`BlenderEnginesWidget`
    
    :class:`BooleanWidget`
    
    :class:`BooleanGroupWidget`
    
    :class:`ColorWidget`
    
    :class:`DropdownWidget`
    
    :class:`FileWidget`
    
    :class:`FontWidget`
    
    :class:`GroupWidget`
    
    :class:`LabelWidget`
    
    :class:`MovieWidget`
    
    :class:`MultilineWidget`
    
    :class:`PictureWidget`
    
    :class:`RangeFramesWidget`
    
    :class:`SoundWidget`
    
    :class:`SpinnerDoubleWidget`
    
    :class:`SpinnerIntWidget`
    
    :class:`SpinnerWidget`
    
    :class:`TextWidget`
    
    :class:`OpenFileConfig`
    
    :class:`OpenFontFile`
    
    :class:`OpenMovieFile`
    
    :class:`OpenSoundFile`
    
"""
import os
import uuid
import shutil
import subprocess
import codecs
import functools

from PyQt5.QtCore import QSize, Qt, QEvent, QObject, QThread, pyqtSlot, pyqtSignal, QMetaObject, Q_ARG, QFileInfo, QDir, QStandardPaths, QUrl, pyqtSignal
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtMultimedia import *
from PyQt5.QtMultimediaWidgets import *

from classes import info, ui_util
from classes.logger import log
from classes.app import get_app

from windows.views.rangewidgets import QFactoryRangeProgressBar




class CreateWidgetsParametersRenderPlugin(QObject):
    ''' 
    .. image:: classes/CreateWidgetsParametersRenderPlugin.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class create Widgets from render config

    :param id_task: identifiant of task render
    :type id_task: TaskPluginView
    :param list_running_plugins: 
    :type list_running_plugins:
    :param render_version: 
    :type render_version:
    :param settingsContainer: Widget to put all params widgets to configure the render plugin
    :type settingsContainer: QWidget
    :param title3DWindow: Windows of Animated 3D Title plugins
    :type title3DWindow: RendersPluginsViews
    
    Create type of widget:
        
        * rangeframes (min, start, end, max, stepsstart, stepsend)
        * blender_engines (blender, cycle, eevee, povray, game)
        * boolean (on_active_show, on_active_hide, on_inactive_show, on_inactive_hide)
        * text
        * multiline
        * dropdown (sort: lineal/reverse/label_lineal/label_reverse)
        * spinner (min, max, step, data_type: int/float)
        * color (return dict {'rgb', 'rgba', 'cmyk', 'hsv', 'hsl'})
        * file
        * font
        * picture
        * sound
        * movie
        * group (type: static)
        * booleangroup (type: selectable/on/off/enabled/disabled/active/inactive, state, on_active_show, on_active_hide, on_inactive_show, on_inactive_hide)
    '''
    def __init__(self, id_task, list_running_plugins, render_version, settingsContainer, title3DWindow):
        """Initialize class CreateWidgetsParametersRenderPlugin"""
        super(CreateWidgetsParametersRenderPlugin, self).__init__()

        # Get render plugin parameters
        self.title3DWindow = title3DWindow
        self.settingsContainer = settingsContainer
        self.render_version = render_version
        self.configuration_plugin = list_running_plugins[id_task]['configuration']
        self.parameters_plugin = list_running_plugins[id_task]['parameters']
        self.params = self.parameters_plugin.active_params
        self.nameWidgetEngine = None
        
        # Translate text
        _ = get_app()._tr

        setwidgetengine = True
        listgroupsaddwidgetengine = []
        
        log.info("CreateWidgetsParametersRenderPlugin-> " + _("Begin creation config widgets"))
        # Loop through params
        for param in self.params:
            widget = None
            label = None
            if self.params[param]['create_widget']:
                # Create Label
                label = LabelWidget(self.params[param], self.title3DWindow)
                #log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget label: %s") % self.params[param]['label'])

                if self.params[param]['type'] == "rangeframes":
                    # Set default preview picture
                    self.title3DWindow.sliderPreview_valueChanged(self.params[param]['value'])
                    widget = RangeFramesWidget(self.params[param], self.title3DWindow)
                    #log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget frame: %s") % self.params[param]['label'])

                elif self.params[param]['type'] == 'blender_engines':
                    widget = BlenderEnginesWidget(self.params[param], self.render_version, self.title3DWindow, self.params)
                    # Values for correct view for widget after this widget into xml
                    self.nameWidgetEngine = param
                    #log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget blender_engines: %s") % self.params[param]['label'])

                elif self.params[param]['type'] == 'text':
                    widget = TextWidget(self.params[param], self.title3DWindow)
                    #log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget text: %s") % self.params[param]['label'])

                elif self.params[param]['type'] == 'multiline':
                    widget = MultilineWidget(self.params[param], self.title3DWindow)
                    #log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget multiline: %s") % self.params[param]['label'])

                elif self.params[param]['type'] == 'boolean':
                    widget = BooleanWidget(self.params[param], self.title3DWindow, self.params)
                    #log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget boolean: %s") % self.params[param]['label'])

                elif self.params[param]['type'] == 'dropdown':
                    widget = DropdownWidget(self.params[param], self.title3DWindow)
                    #log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget dropdown: %s") % self.params[param]['label'])

                elif self.params[param]['type'] == 'spinner':
                    widget = SpinnerWidget(self.params[param], self.title3DWindow)
                    #log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget spinner: %s") % self.params[param]['label'])

                elif self.params[param]['type'] == 'color':
                    widget = ColorWidget(self.params[param], self.title3DWindow)
                    #log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget color: %s") % self.params[param]['label'])

                elif self.params[param]['type'] == 'file':
                    widget = FileWidget(self.params[param], self.title3DWindow)
                    #log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget file: %s") % self.params[param]['label'])

                elif self.params[param]['type'] == 'font':
                    widget = FontWidget(self.params[param], self.title3DWindow)
                    #log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget font: %s") % self.params[param]['label'])

                elif self.params[param]['type'] == 'picture':
                    widget = PictureWidget(self.params[param], self.title3DWindow)
                    #log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget picture: %s") % self.params[param]['label'])

                elif self.params[param]['type'] == 'sound':
                    widget = SoundWidget(self.params[param], self.title3DWindow)
                    log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget sound: %s") % self.params[param]['label'])

                elif self.params[param]['type'] == 'movie':
                    widget = MovieWidget(self.params[param], self.title3DWindow)
                    log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget movie: %s") % self.params[param]['label'])

                if not widget is None:
                    self.params[param]['widget'] = widget
                    self.params[param]['label_widget'] = label
            elif self.params[param]['type'] == "rangeframes":
                # Set default preview picture
                self.title3DWindow.sliderPreview_valueChanged(self.params[param]['value'])

            # Add Label and Widget to the form
            if (widget and label):
                #define widget group
                if 'group' in self.params[param] and self.params[param]['group']:
                    #log.info("CreateWidgetsParametersRenderPlugin-> " + _("Widget option group: %s") % (self.params[param]['group']))
                    # Preview creation of group widget exist
                    if self.params[param]['group']['widget']:
                        # Get group widgets
                        groupArea = self.params[param]['group']['widget']
                    else:
                        # Create group widgets
                        groupArea = GroupWidget(self.params[param]['group'], self.title3DWindow, self.parameters_plugin.groups_defines, self.params)
                        # Add widget engine name if created
                        if not setwidgetengine:
                            groupArea.nameWidgetEngine = self.nameWidgetEngine
                        self.params[param]['group']['widget'] = groupArea
                        # Set group label
                        label_groupArea = LabelWidget(self.params[param]['group'], self.title3DWindow)
                        self.params[param]['group']['label_widget'] = label_groupArea
                        # group is selectable ?
                        if self.params[param]['group']['type'] == 'selectable':
                            select_widget = BooleanGroupWidget(self.params[param]['group'], self.title3DWindow)
                            # Check widget
                            if self.params[param]['group']['state']:
                                select_widget.setChecked(True)
                            else:
                                select_widget.setChecked(False)
                            # Add widget selectable to group
                            groupArea.addWidget(select_widget)
                        # Set group with windows
                        self.settingsContainer.layout().addRow(label_groupArea, groupArea)
                    # Add widget engine name into all widget group created
                    if setwidgetengine:
                        if self.nameWidgetEngine is None:
                            # Add widget into list
                            if not groupArea in listgroupsaddwidgetengine:
                                listgroupsaddwidgetengine.append(groupArea)
                        else:
                            # Define alls priliminary widget created width real widget engine name
                            groupArea.nameWidgetEngine = self.nameWidgetEngine
                            for group in listgroupsaddwidgetengine:
                                group.nameWidgetEngine = self.nameWidgetEngine
                            setwidgetengine = False
                            
                    # Add widget to list of widget group
                    if self.parameters_plugin.groups_defines[self.params[param]['group']['name']] is None:
                        self.parameters_plugin.groups_defines[self.params[param]['group']['name']] = {self.params[param]['name']: {'params': self.params[param], 'widget': widget}}
                    else:
                        self.parameters_plugin.groups_defines[self.params[param]['group']['name']].update({self.params[param]['name']: {'params': self.params[param], 'widget': widget}})
                    # Add label widget
                    groupArea.addWidget(label)
                    groupArea.addWidget(widget)
                    # Hide initial show widget when type = selectable and state = False
                    if self.params[param]['group']['type'] == 'selectable':
                        groupArea.group_changed(self.params[param]['group'], 0)
                else:
                    # create widget with label
                    self.settingsContainer.layout().addRow(label, widget)
            elif (label):
                # Add pure label
                self.settingsContainer.layout().addRow(label)
        # End for
        # Update dependant render view states widget
        if self.nameWidgetEngine:
            self.params[self.nameWidgetEngine]['widget'].update_widget(self.params[self.nameWidgetEngine])


class LabelWidget(QLabel):
    """
    .. image:: classes/LabelWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class plugin configuration Widget label

    .. image:: pictures/WidgetLabel.png
        :alt: Look like
        :align: center
        :width: 500px

    """
    def __init__(self, param, title3DWindow):
        """ Initialize class LabelWidget """
        # Initialize QLabel
        super(LabelWidget, self).__init__()

        self.title3DWindow = title3DWindow

        # Translate text
        _ = get_app()._tr

        #log.info("LabelWidget-> " + _("Create widget label %s") % param)

        self.setText(_(param['label']))
        self.setToolTip(_(param['tooltip']))



class BooleanWidget(QCheckBox):
    """
    .. image:: classes/BooleanWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget boolean

    .. image:: pictures/WidgetBoolean1.png
        :alt: Look like off
        :align: center
        :width: 500px

    .. image:: pictures/WidgetBoolean2.png
        :alt: Look like on
        :align: center
        :width: 500px
        
    Conditional view widgets

    .. image:: pictures/BooleanSwitchConditionnalWidgetVisible1.png
        :alt: Look like on
        :align: center
        :width: 500px

    .. image:: pictures/BooleanSwitchConditionnalWidgetVisible2.png
        :alt: Look like on
        :align: center
        :width: 500px

    """
    def __init__(self, param, title3DWindow, params):
        """ Initialize class BooleanWidget """
        # Initialize QCheckBox
        super(BooleanWidget, self).__init__()

        self.title3DWindow = title3DWindow
        self.params = params

        # Translate text
        _ = get_app()._tr

        #log.info("BooleanWidget-> " + _("Create widget boolean switch %s") % param)

        openshootpathimages = os.path.join(info.PATH, "images")
        self.setStyleSheet("""
            QCheckBox {
                spacing: 5px;
                }
            QCheckBox::indicator {
                width: 100px; height: 40px;
                }
            QCheckBox::indicator:unchecked {
                image: url(%s/checkbox_unchecked.png);
                }
            QCheckBox::indicator:unchecked:hover {
                image: url(%s/checkbox_unchecked_hover.png);
                }
            QCheckBox::indicator:unchecked:pressed {
                image: url(%s/checkbox_unchecked_pressed.png);
                }
            QCheckBox::indicator:checked {
                image: url(%s/checkbox_checked.png);
                }
            QCheckBox::indicator:checked:hover {
                image: url(%s/checkbox_checked_hover.png);
                }
            QCheckBox::indicator:checked:pressed {
                image: url(%s/checkbox_checked_pressed.png);
                }
            QCheckBox::indicator:indeterminate:hover {
                image: url(%s/checkbox_indeterminate_hover.png);
                }
            QCheckBox::indicator:indeterminate:pressed {
                image: url(%s/checkbox_indeterminate_pressed.png);
                }
                """ % (openshootpathimages, openshootpathimages, openshootpathimages, openshootpathimages, openshootpathimages, openshootpathimages, openshootpathimages, openshootpathimages))

        # Set state checkbox
        self.setChecked(param['value'])
        
        # Update state show dependant widget
        self.update_widget(param)

        # Action on change state
        self.stateChanged.connect(functools.partial(self.boolean_changed, self, param))#

        if not hasattr(self.title3DWindow, 'boolean_changed'):
            self.title3DWindow.boolean_changed = self.boolean_changed

    def boolean_changed(self, widget, param, index):
        """ Change boolean state """
        # Translate text
        _ = get_app()._tr

        #log.info('boolean_changed')
        value = widget.isChecked()
        param['value'] = value
        self.update_widget(param)
        log.info('boolean_changed-> Widget  %s %s' % (param["name"], str(value)))
    
    def update_widget(self, param):
        """ Update visibility state from dependants widgets """
        if param['value']:
            if not param['on_active_show'] is None:
                for widget in param['on_active_show']:
                    if 'widget' in self.params[widget]:
                        self.params[widget]['label_widget'].show()
                        self.params[widget]['widget'].show()
            if not param['on_active_hide'] is None:
                for widget in param['on_active_hide']:
                    if 'widget' in self.params[widget]:
                        self.params[widget]['label_widget'].hide()
                        self.params[widget]['widget'].hide()
        else:
            if not param['on_inactive_show'] is None:
                for widget in param['on_inactive_show']:
                    if 'widget' in self.params[widget]:
                        self.params[widget]['label_widget'].show()
                        self.params[widget]['widget'].show()
            if not param['on_inactive_hide'] is None:
                for widget in param['on_inactive_hide']:
                    if 'widget' in self.params[widget]:
                        self.params[widget]['label_widget'].hide()
                        self.params[widget]['widget'].hide()
        


class GroupWidget(QVBoxLayout):
    """
    .. image:: classes/GroupWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget group

    .. image:: pictures/GroupWidgetFeature.png
        :alt: Look like
        :align: center
        :width: 500px

    """
    def __init__(self, group, title3DWindow, listgroups, params):
        """ Initialize class GroupWidget """
        # Initialize QVBoxLayout
        super(GroupWidget, self).__init__()

        self.title3DWindow = title3DWindow
        self.groups_defines = listgroups
        self.nameWidgetEngine = None
        self.params = params

        # Translate text
        _ = get_app()._tr

        # Update state show/hide dependant widgets
        self.update_widget(group)

        if not hasattr(self.title3DWindow, 'group_changed'):
            self.title3DWindow.group_changed = self.group_changed

    def group_changed(self, group, index):
        """ Change group state """
        # Translate text
        _ = get_app()._tr

        #State of boolean group
        value = group['widget'].itemAt(0).widget().isChecked()
        
        #Widgets gesture
        list_update_booleans_widgets = []
        for widget in self.groups_defines[group['name']].keys():
            if value:
                # Show widgets from group
                self.groups_defines[group['name']][widget]['params']['label_widget'].show()
                self.groups_defines[group['name']][widget]['widget'].show()
                if self.groups_defines[group['name']][widget]['params']['type'] == 'boolean':
                    # Add update state show for boolean features ('on_active_show', 'on_active_hide, 'on_inactive_show' and 'on_inactive_hide')
                    list_update_booleans_widgets.append({'widget': self.groups_defines[group['name']][widget]['widget'], 'params': self.groups_defines[group['name']][widget]['params']})
                group['state'] = True
            else:
                self.groups_defines[group['name']][widget]['params']['label_widget'].hide()
                self.groups_defines[group['name']][widget]['widget'].hide()
                group['state'] = False

        # update state show/hide dependant widgets ('on_active_show', 'on_active_hide, 'on_inactive_show' and 'on_inactive_hide')
        self.update_widget(group)
        
        # update state show for widgets booleans childrens group ('on_active_show', 'on_active_hide, 'on_inactive_show' and 'on_inactive_hide')
        for boolean_widget in list_update_booleans_widgets:
            boolean_widget['widget'].update_widget(boolean_widget['params'])
        log.info('group_changed-> %s %s' % (group['name'], str(value)))
    
    def update_widget(self, group):
        """ Update visibility state from dependants widgets """
        if group['type'] != 'static':
            if group['state']:
                if 'on_active_show' in group:
                    for widget in group['on_active_show']:
                        if 'widget' in self.params[widget]:
                            self.params[widget]['label_widget'].show()
                            self.params[widget]['widget'].show()
                if 'on_active_hide' in group:
                    for widget in group['on_active_hide']:
                        if 'widget' in self.params[widget]:
                            self.params[widget]['label_widget'].hide()
                            self.params[widget]['widget'].hide()
                # Update blender engine case into group show/hide widget
                if self.nameWidgetEngine:
                    self.params[self.nameWidgetEngine]['widget'].update_widget(self.params[self.nameWidgetEngine])
            else:
                if 'on_inactive_show' in group:
                    for widget in group['on_inactive_show']:
                        if 'widget' in self.params[widget]:
                            self.params[widget]['label_widget'].show()
                            self.params[widget]['widget'].show()
                if 'on_inactive_hide' in group:
                    for widget in group['on_inactive_hide']:
                        if 'widget' in self.params[widget]:
                            self.params[widget]['label_widget'].hide()
                            self.params[widget]['widget'].hide()


class BooleanGroupWidget(QCheckBox):
    """
    .. image:: classes/BooleanGroupWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget boolean switch of group widgets

    .. image:: pictures/BooleanGroupWidgetFeature1.png
        :alt: Look like off
        :align: center
        :width: 500px

    .. image:: pictures/BooleanGroupWidgetFeature2.png
        :alt: Look like on
        :align: center
        :width: 500px

    """
    def __init__(self, group, title3DWindow):
        """ Initialize class BooleanGroupWidget """
        # Initialize QCheckBox
        super(BooleanGroupWidget, self).__init__()

        self.title3DWindow = title3DWindow

        # Translate text
        _ = get_app()._tr

        #log.info("BooleanGroupWidget-> " + _("Create widget switch group %s") % group)

        openshootpathimages = os.path.join(info.PATH, "images")
        self.setStyleSheet("""
            QCheckBox {
                spacing: 5px;
                }
            QCheckBox::indicator {
                width: 50px; height: 20px;
                }
            QCheckBox::indicator:unchecked {
                image: url(%s/checkbox_unchecked.png);
                }
            QCheckBox::indicator:unchecked:hover {
                image: url(%s/checkbox_unchecked_hover.png);
                }
            QCheckBox::indicator:unchecked:pressed {
                image: url(%s/checkbox_unchecked_pressed.png);
                }
            QCheckBox::indicator:checked {
                image: url(%s/checkbox_checked.png);
                }
            QCheckBox::indicator:checked:hover {
                image: url(%s/checkbox_checked_hover.png);
                }
            QCheckBox::indicator:checked:pressed {
                image: url(%s/checkbox_checked_pressed.png);
                }
            QCheckBox::indicator:indeterminate:hover {
                image: url(%s/checkbox_indeterminate_hover.png);
                }
            QCheckBox::indicator:indeterminate:pressed {
                image: url(%s/checkbox_indeterminate_pressed.png);
                }
                """ % (openshootpathimages, openshootpathimages, openshootpathimages, openshootpathimages, openshootpathimages, openshootpathimages, openshootpathimages, openshootpathimages))

        # Set state checkbox
        self.setChecked(group['state'])

        # Add action group
        self.stateChanged.connect(functools.partial(group['widget'].group_changed, group)) # voir objet groupe



class BlenderEnginesWidget(QComboBox):
    """
    .. image:: classes/BlenderEnginesWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget Blender render

    """
    def __init__(self, param, blender_version, title3DWindow, params):
        """ Initialize class BlenderEnginesWidget """
        # create dropdown
        super(BlenderEnginesWidget, self).__init__()

        self.title3DWindow = title3DWindow
        self.params = params

        # Translate text
        _ = get_app()._tr

        log.info("BlenderEnginesWidget-> " + _("Create widget engine selection %s") % param)


        # Add values
        blender_renders = param['values'] #('BLENDER', 'CYCLES', 'EEVEE', 'POVRAY', 'GAME')
        for box_index in range(0,len(blender_renders)):
            # add dropdown item
            self.addItem(blender_renders[box_index].capitalize(), blender_renders[box_index])
            # select dropdown (if default)
            if param['value'] == blender_renders[box_index]:
                self.setCurrentIndex(box_index)

        self.currentIndexChanged.connect(functools.partial(self.render_index_changed, self, param))

        self.update_widget(param)

        if not hasattr(self.title3DWindow, 'render_index_changed'):
            self.title3DWindow.render_index_changed = self.render_index_changed
        
    def render_index_changed(self, widget, param, index):
        """ List of renders index change """
        # Translate text
        _ = get_app()._tr

        log.info('render_index_changed')
        value = widget.itemData(index)
        param['value'] = value
        self.update_widget(param)
        log.info('render_index_changed-> Widget %s %s' % (param["name"], value))
    
    def update_widget(self, param):
        """ Update visibility state from dependants widgets """
        if 'on_select_show' in param:
            # Selected value into show case view
            if param['value'] in param['on_select_show']:
                # Get all widgets names to show
                for widget in param['on_select_show'][param['value']]:
                    if widget in self.params:
                        # Widget is into group ?
                        if 'group' in self.params[widget]:
                            # Group is static or showing ?
                            if self.params[widget]['group']['state'] is None or self.params[widget]['group']['state'] == True:
                                # Show widget
                                if 'widget' in self.params[widget]:
                                    self.params[widget]['widget'].show()
                                    self.params[widget]['label_widget'].show()
                        elif 'widget' in self.params[widget]:
                            self.params[widget]['widget'].show()
                            self.params[widget]['label_widget'].show()
                    else:
                        log.info('update_widget-> Warning do not have widget «%s» for «%s» choice' % (widget, param['value']))

        if 'on_select_hide' in param:
            # Selected value into showhide case view
            if param['value'] in param['on_select_hide']:
                # Get all widgets names to hide
                for widget in param['on_select_hide'][param['value']]:
                    if widget in self.params:
                        # Widget is into group ?
                        if 'group' in self.params[widget]:
                            # Group is static or showing ?
                            if self.params[widget]['group']['state'] is None or self.params[widget]['group']['state'] == True:
                                # Hide widget
                                if 'widget' in self.params[widget]:
                                    self.params[widget]['widget'].hide()
                                    self.params[widget]['label_widget'].hide()
                        elif 'widget' in self.params[widget]:
                            self.params[widget]['widget'].hide()
                            self.params[widget]['label_widget'].hide()
                    else:
                        log.info('update_widget-> Warning do not have widget «%s» for «%s» choice' % (widget, param['value']))



class RangeFramesWidget(QWidget):
    """
    .. image:: classes/RangeFramesWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget frame

    .. image:: pictures/RangeFrameWidget.png
        :alt: Look like
        :align: center
        :width: 500px

    """
    resized = pyqtSignal()
    def __init__(self, param, title3DWindow):
        """ Initialize class RangeFramesWidget """
        # Initialize QWidget
        super(RangeFramesWidget, self).__init__()

        self.title3DWindow = title3DWindow
        self._liststepstart = None
        self._liststepend = None
        
        # Translate text
        _ = get_app()._tr

        #log.info("RangeFramesWidget-> " + _("Create widget frame %s") % param)

        self.setToolTip(param["tooltip"])

        # Set Range display
        layoutscreen = QHBoxLayout(self)
        layoutscreen.setContentsMargins(0,0,0,0)
        self.rangedisplay = QFactoryRangeProgressBar(param['min'], param['max'], param['start'], param['end'], False, True, 'stepsstart' in param or 'stepsend' in param, False, None, None, None, None, 10)
        self.rangedisplay.setOffsetStartBar()
        self.rangedisplay.setOffsetEndBar()
        self.rangedisplay.showTexts()
        self.rangedisplay.hideExtremumsTexts()
        layoutscreen.addWidget(self.rangedisplay)
        
        # Set slider start
        self.sliderstart = QSlider(Qt.Horizontal, self)
        self.sliderstart.setMinimum(param['min'])
        self.sliderstart.setMaximum(param['end'])
        self.sliderstart.setValue(param['start'])
        self.sliderstart.setToolTip(str(param['start']))
        
        # Set slider start aspect
        factor = self.geometry().width() / (param['max'] - param['min'] + 1)
        width_start_progress_bar = int(param['end'] * factor)
        self.sliderstart.setFixedWidth(width_start_progress_bar)
        self.sliderstart.move(0, self.geometry().height() - 2*self.sliderstart.height()/3 +2)
        
        STYLE_CSS_MIN = """
            QSlider::groove:horizontal {
                border: 0px solid;
                height: 2px;
                margin: 1px O;
                border-radius: 3px;
            }

            QSlider::handle:horizontal {
                background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #ececec, stop:1 #1abc9c);
                width: 8px;
                border: 1px solid #1abc9c;
                margin: -14px 0; /* expand outside the groove */
                border-radius: 4px;
            }

            QSlider::add-page:horizontal {
                background: #1abc9c;
            }

            QSlider::sub-page:horizontal {
                background: darkgray;
            }
            """
        self.sliderstart.setStyleSheet(STYLE_CSS_MIN)

        # Set slider end
        self.sliderend = QSlider(Qt.Horizontal, self)
        self.sliderend.setMinimum(param['start'])
        self.sliderend.setMaximum(param['max'])
        self.sliderend.setValue(param['end'])
        self.sliderend.setToolTip(str(param['end']))
        
        # Set slider end aspect
        factor = self.geometry().width() / (param['max'] - param['min'])
        width_end_progress_bar = int((param['max'] - param['start']) * factor)
        start_end_range_progress = int((param['start']-1) * factor)
        self.sliderend.setFixedWidth(width_end_progress_bar)
        self.sliderend.move(start_end_range_progress, - self.sliderend.height()/3 -2)
        STYLE_CSS_MAX = """
            QSlider::groove:horizontal {
                border: 0px solid;
                height: 2px;
                margin: 1px O;
                border-radius: 3px;
            }

            QSlider::handle:horizontal {
                background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #ececec, stop:1 #1abc9c);
                width: 8px;
                border: 1px solid #1abc9c;
                margin: -14px 0; /* expand outside the groove */
                border-radius: 4px;
            }

            QSlider::add-page:horizontal {
                background: darkgray;
            }

            QSlider::sub-page:horizontal {
                background: #1abc9c;
            }
            """
        self.sliderend.setStyleSheet(STYLE_CSS_MAX)

        # Set if step start/end select
        if 'stepsstart' in param or 'stepsend' in param:
            slidelabels = []
            self.rangedisplay.setGraduationsPositionExpand()
            self.rangedisplay.showGraduations()
            self.rangedisplay.setGraduationFontSize(18)
            if 'stepsstart' in param:
                startvalues = list(param['stepsstart'].values())
                slidelabels = startvalues
                self.setListStartStepSlide(startvalues)
            if 'stepsend' in param:
                endvalues = list(param['stepsend'].values())
                slidelabels = slidelabels + endvalues
                self.setListEndStepSlide(endvalues)
            self.rangedisplay.setListLabelGraduations(slidelabels)

        # Active actions sliders
        self.show
        self.sliderstart.valueChanged.connect(functools.partial(self.on_change_value_slider_start_range, param))
        self.sliderend.valueChanged.connect(functools.partial(self.on_change_value_slider_end_range, param))


    def resizeEvent(self, event):
        """ Update widget when resized """
        self.resized.emit()
        # Set parameters values
        factorstart = self.geometry().width() / (self.rangedisplay.getMaximum() - self.rangedisplay.getMinimum() +1)
        width_start_progress_bar = int(self.sliderend.value() * factorstart)
        self.sliderstart.blockSignals(True)
        self.sliderstart.setFixedWidth(width_start_progress_bar)
        self.sliderstart.move(0, self.geometry().height() - 2*self.sliderstart.height()/3 +2)
        self.sliderstart.blockSignals(False)
        factorend = self.geometry().width() / (self.rangedisplay.getMaximum() - self.rangedisplay.getMinimum())
        width_end_progress_bar = int((self.rangedisplay.getMaximum() - self.sliderstart.value()) * factorend)
        start_end_range_progress = int((self.sliderstart.value()-1) * factorend)
        self.sliderend.blockSignals(True)
        self.sliderend.setFixedWidth(width_end_progress_bar)
        self.sliderend.move(start_end_range_progress, - self.sliderend.height()/3 -2)
        self.sliderend.blockSignals(False)
        
        self.rangedisplay.update()

        return super(RangeFramesWidget, self).resizeEvent(event)

    def on_change_value_slider_start_range(self, param, value):
        """ Set start range slide """
        # Translate text
        _ = get_app()._tr

        # Set start value
        if self.getListStartStepSlide() is None:
            startvalueslide = self.sliderstart.value()
        else:
            if self.sliderstart.value() > self.sliderend.value():
                for startvalue in self.getListStartStepSlide():
                    if startvalue <= self.sliderend.value():
                        startvalueslide = startvalue
                    else:
                        break
            else:
                if self.sliderstart.value() >= self.getListStartStepSlide()[-1]:
                    startvalueslide = self.getListStartStepSlide()[-1]
                else:
                    for startvalue in self.getListStartStepSlide():
                        if startvalue <= self.sliderstart.value():
                            startvalueslide = startvalue
                        else:
                            break

        param['start'] = value
        log.info('RangeFramesWidget.on_change_value_slider_start_range-> Widget %s %s' % (param['name'], value)) #         # Set parameters values
        #self.sliderstart.setValue(startvalueslide)

        factor = self.geometry().width() / (self.rangedisplay.getMaximum() - self.rangedisplay.getMinimum())
        width_end_progress_bar = int((self.rangedisplay.getMaximum() - startvalueslide) * factor)
        start_end_range_progress = int((startvalueslide-1) * factor)

        self.sliderstart.setToolTip(str(startvalueslide))
        self.sliderend.blockSignals(True)
        self.sliderend.setMinimum(startvalueslide)
        self.sliderend.setFixedWidth(width_end_progress_bar)
        self.sliderend.move(start_end_range_progress, - self.sliderend.height()/3 -2)
        self.sliderend.blockSignals(False)

        self.rangedisplay.setStart(startvalueslide)
        self.rangedisplay.update()
        self.title3DWindow.setCountPreviewControl(self.sliderstart.value(), self.sliderend.value())
    
    
    def on_change_value_slider_end_range(self, param, value):
        """ Set end range slide """
        # Translate text
        _ = get_app()._tr

        # Set end value
        if self.getListEndStepSlide() is None:
            endvalueslide = self.sliderend.value()
        else:
            if self.sliderend.value() < self.sliderstart.value():
                for endvalue in self.getListEndStepSlide():
                    if endvalue <= self.sliderend.value():
                        endvalueslide = endvalue
                    else:
                        break
            else:
                if self.sliderend.value() <= self.getListEndStepSlide()[-1]:
                    endvalueslide = self.getListEndStepSlide()[-1]
                else:
                    for endvalue in self.getListEndStepSlide():
                        if endvalue >= self.sliderend.value():
                            endvalueslide = endvalue
                        else:
                            break

        param['end'] = value
        log.info('RangeFramesWidget.on_change_value_slider_end_range-> Widget %s %s' % (param['name'], value))
        #self.sliderend.setValue(endvalueslide)

        # Set parameters values
        factor = self.geometry().width() / (self.rangedisplay.getMaximum() - self.rangedisplay.getMinimum() +1)
        width_start_progress_bar = int(endvalueslide * factor)

        self.sliderend.setToolTip(str(endvalueslide))
        self.sliderstart.blockSignals(True)
        self.sliderstart.setMaximum(endvalueslide)
        self.sliderstart.setFixedWidth(width_start_progress_bar)
        self.sliderstart.move(0, self.geometry().height() - 2*self.sliderstart.height()/3 +2)
        self.sliderstart.blockSignals(False)

        self.rangedisplay.setEnd(endvalueslide)
        self.rangedisplay.update()
        self.title3DWindow.setCountPreviewControl(self.sliderstart.value(), self.sliderend.value())


    def setListStartStepSlide(self, liststart):
        """
        Set list of start steps slide
        
        :param liststart: List of step start range.
        :type liststart: list
        """
        if type(liststart) is list:
            # Sort by growing
            liststart.sort()
            self._liststepstart = tuple(liststart)


    def getListStartStepSlide(self):
        """
        Get list of start steps slide
        
        :returns: Start steps slide.
        :rtype: tuple
        """
        return self._liststepstart


    def setListEndStepSlide(self, listend=None):
        """
        Set list of end steps slide
        
        :param listend: List of step end range.
        :type listend: list
        """
        if type(listend) is list:
            # sort by shrinking
            listend.sort(reverse=True)
            self._liststepend = tuple(listend)


    def getListEndStepSlide(self):
        """
        Get list of end steps slide
        
        :returns: End steps slide.
        :rtype: tuple
        """
        return self._liststepend




class TextWidget(QLineEdit):
    """
    .. image:: classes/TextWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget text

    .. image:: pictures/WidgetText.png
        :alt: Look like
        :align: center
        :width: 500px

    """
    def __init__(self, param, title3DWindow):
        """ Initialize class TextWidget """
        # Initialize QLineEdit
        super(TextWidget, self).__init__()

        self.title3DWindow = title3DWindow

        # Translate text
        _ = get_app()._tr

        #log.info("TextWidget-> " + _("Create widget text %s") % param)

        self.setText(_(param['value']))

        self.textChanged.connect(functools.partial(self.text_value_changed, self, param))

        if not hasattr(self.title3DWindow, 'text_value_changed'):
            self.title3DWindow.text_value_changed = self.text_value_changed

    def text_value_changed(self, widget, param, value=None):
        """ Change text """
        # Translate text
        _ = get_app()._tr

        #log.info('text_value_changed')
        try:
            # Attempt to load value from QTextEdit (i.e. multi-line)
            if not value:
                value = widget.toPlainText()
        except Exception:
            pass
        param['value'] = value.replace("\n", "\\n")
        log.info('text_value_changed-> Widget %s %s' % (param['name'], value))  # self.title3DWindow.plugin_manager.list_running_plugins[self.title3DWindow.plugin_manager.id_task]['parameters'].active_params[param['name']]['value']



class MultilineWidget(QTextEdit):
    """
    .. image:: classes/MultilineWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget text multi line

    .. image:: pictures/WidgetMultiline.png
        :alt: Look like
        :align: center
        :width: 500px

    """
    def __init__(self, param, title3DWindow):
        """ Initialize class MultilineWidget """
        # Initialize QTextEdit
        super(MultilineWidget, self).__init__()

        self.title3DWindow = title3DWindow

        # Translate text
        _ = get_app()._tr

        #log.info("MultilineWidget-> " + _("Create widget text multiline %s") % param)

        self.setText(_(param["value"]).replace("\\n", "\n"))
        self.textChanged.connect(functools.partial(self.text_value_changed, self, param))

        if not hasattr(self.title3DWindow, 'text_value_changed'):
            self.title3DWindow.text_value_changed = self.text_value_changed

    def text_value_changed(self, widget, param, value=None):
        """ Change text """
        # Translate text
        _ = get_app()._tr

        #log.info('text_value_changed')
        try:
            # Attempt to load value from QTextEdit (i.e. multi-line)
            if not value:
                value = widget.toPlainText()
        except Exception:
            pass
        param['value'] = value.replace("\n", "\\n")
        log.info('text_value_changed-> Widget %s %s' % (param['name'], value)) # self.title3DWindow.plugin_manager.list_running_plugins[self.title3DWindow.plugin_manager.id_task]['parameters'].active_params[param['name']]['value']



class DropdownWidget(QComboBox):
    """
    .. image:: classes/MultilineWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget dropdown

    .. image:: pictures/WidgetDropdown.png
        :alt: Look like
        :align: center
        :width: 500px

    """
    def __init__(self, param, title3DWindow):
        """ Initialize class DropdownWidget """
        # Initialize QComboBox
        super(DropdownWidget, self).__init__()

        self.title3DWindow = title3DWindow

        # Translate text
        _ = get_app()._tr

        #log.info("DropdownWidget-> " + _("Create widget dropdown %s") % param)

        # get sort option
        if 'sort' in param:
            if param['sort'] == 'lineal':
                list_reverse = False
            elif param['sort'] == 'reverse':
                list_reverse = True
            elif param['sort'] == 'label_lineal':
                list_label_reverse = False
            elif param['sort'] == 'label_reverse':
                list_label_reverse = True

        # Get values
        d = param['values']
        # get type option
        # Add list values
        box_index = 0
        if not('list_reverse' in locals() or 'list_label_reverse' in locals()):
            # without sorted
            for k, v in param['values'].items():
                # add dropdown item
                self.addItem(_(k), v)
                # select dropdown (if default)
                if v == param['value']:
                    self.setCurrentIndex(box_index)
                box_index = box_index + 1
        else:
            # with sorted
            if 'list_label_reverse' in locals():
                # sort by items name
                s = sorted(param['values'].items(), reverse = list_label_reverse)
            else:
                # sort by values items
                s = [(k, d[k]) for k in sorted(d, key=d.get, reverse= list_reverse)]
            for k, v in s:
                # add dropdown item
                self.addItem(_(k), v)
                # select dropdown (if default)
                if v == param['value']:
                    self.setCurrentIndex(box_index)
                box_index = box_index + 1

        if not param['values']:
            self.addItem(_("No items Found"), "")
            self.setEnabled(False)

        self.currentIndexChanged.connect(functools.partial(self.dropdown_index_changed, self, param))

        if not hasattr(self.title3DWindow, 'dropdown_index_changed'):
            self.title3DWindow.dropdown_index_changed = self.dropdown_index_changed

    def dropdown_index_changed(self, widget, param, index):
        """ Change list index select """
        # Translate text
        _ = get_app()._tr

        #log.info('dropdown_index_changed')
        value = widget.itemData(index)
        param['value'] = value
        log.info('dropdown_index_changed-> Widget %s %s' % (param["name"], str(value)))  # self.title3DWindow.plugin_manager.list_running_plugins[self.title3DWindow.plugin_manager.id_task]['parameters'].active_params[param['name']]['value']



class SpinnerIntWidget(QSpinBox):
    """
    .. image:: classes/SpinnerIntWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget spinner int

    .. image:: pictures/WidgetSpinnerFloat.png
        :alt: Look like
        :align: center
        :width: 500px

    """
    def __init__(self, param, title3DWindow):
        """ Initialize class SpinnerIntWidget """
        # Initialize QSpinBox
        super(SpinnerIntWidget, self).__init__()

        self.title3DWindow = title3DWindow

        # Translate text
        _ = get_app()._tr

        #log.info("SpinnerIntWidget-> " + _("Create widget integer spinner %s") % param)

        self.setMinimum(int(param["min"]))
        self.setMaximum(int(param["max"]))
        self.setValue(param['value'])
        if 'step' in param:
            self.setSingleStep(int(param["step"]))
        else:
            self.setSingleStep(1)
        self.setToolTip(param["tooltip"])

        self.valueChanged.connect(functools.partial(self.spinner_value_changed, param))

        if not hasattr(self.title3DWindow, 'spinner_value_changed'):
            self.title3DWindow.spinner_value_changed = self.spinner_value_changed

    def spinner_value_changed(self, param, value):
        """ Change value spinner """
        # Translate text
        _ = get_app()._tr

        #log.info('spinner_value_changed')
        param['value'] = value
        log.info('spinner_value_changed-> Widget %s %s' % (param['name'], value))



class SpinnerDoubleWidget(QDoubleSpinBox):
    """
    .. image:: classes/SpinnerDoubleWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget spinner double

    .. image:: pictures/WidgetSpinnerFloat.png
        :alt: Look like
        :align: center
        :width: 500px

    """
    def __init__(self, param, title3DWindow):
        """ Initialize class SpinnerDoubleWidget """
        # Initialize SpinnerDoubleWidget
        super(SpinnerDoubleWidget, self).__init__()

        self.title3DWindow = title3DWindow

        # Translate text
        _ = get_app()._tr

        #log.info("SpinnerDoubleWidget-> " + _("Create widget double spinner %s") % param)

        self.setMinimum(float(param['min']))
        self.setMaximum(float(param['max']))
        self.setValue(param['value'])
        if 'step' in param:
            self.setSingleStep(float(param['step']))
        else:
            self.setSingleStep(1.0)
        self.setToolTip(param['label'])

        self.valueChanged.connect(functools.partial(self.spinner_value_changed, param))

        if not hasattr(self.title3DWindow, 'spinner_value_changed'):
            self.title3DWindow.spinner_value_changed = self.spinner_value_changed

    def spinner_value_changed(self, param, value):
        """ Change value spinner """
        # Translate text
        _ = get_app()._tr

        #log.info('spinner_value_changed')
        param['value'] = value
        log.info('spinner_value_changed-> Widget %s %s' % (param['name'], value))



class SpinnerWidget(QObject):
    """
    .. image:: classes/SpinnerWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget spinner

    .. image:: pictures/WidgetSpinnerFloat.png
        :alt: Look like
        :align: center
        :width: 500px

    """
    def __new__(cls, param, title3DWindow):
        """ Do with create class SpinnerWidget """
        if 'data_type' in param:
            if param['data_type'] == 'int':
                # create int spinner
                return SpinnerIntWidget(param, title3DWindow)
            elif param['data_type'] == 'float':
                # create float spinner
                return SpinnerDoubleWidget(param, title3DWindow)
        else:
            # create int spinner
            return SpinnerIntWidget(param, title3DWindow)



class ColorWidget(QPushButton):
    """
    .. image:: classes/ColorWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget color

    .. image:: pictures/WidgetColor1.png
        :alt: Look like
        :align: center
        :width: 500px

    .. image:: pictures/WidgetColor2.png
        :alt: Look like window
        :align: center
        :width: 500px

    """
    def __init__(self, param, title3DWindow):
        """ Initialize class ColorWidget """
        # Initialize QPushButton
        super(ColorWidget, self).__init__()

        self.title3DWindow = title3DWindow

        # Translate text
        _ = get_app()._tr

        #log.info("ColorWidget-> " + _("Create widget color %s") % param)

        self.setText("")
        defaultColor = QColor()
        defaultColor.setRgbF(param['value']['rgba'][0], param['value']['rgba'][1], param['value']['rgba'][2], param['value']['rgba'][3])
        self.setStyleSheet("background-color: {}".format(defaultColor.name()))

        self.clicked.connect(functools.partial(self.color_button_clicked, self, param))

        if not hasattr(self.title3DWindow, 'color_button_clicked'):
            self.title3DWindow.color_button_clicked = self.color_button_clicked

    def color_button_clicked(self, widget, param, index):
        """ Change color selected """
        # Translate text
        _ = get_app()._tr

        #log.info('color_button_clicked-> Widget ' + _('Animation param being changed: %s') % param['name'])
        color_value = param['value']
        currentColor = QColor()
        currentColor.setRgbF(color_value['rgba'][0], color_value['rgba'][1], color_value['rgba'][2], color_value['rgba'][3])
        newColor = QColorDialog.getColor(currentColor)
        if newColor.isValid():
            widget.setStyleSheet("background-color: {}".format(newColor.name()))
            param['value'] = {
                    'rgb': [newColor.redF(), newColor.greenF(), newColor.blueF()],
                    'rgba': [newColor.redF(), newColor.greenF(), newColor.blueF(), newColor.alphaF()],
                    'cmyk': [newColor.cyanF(), newColor.magentaF(), newColor.yellowF(), newColor.blackF()],
                    'hsv': [newColor.hsvHueF(), newColor.hsvSaturationF(), newColor.valueF()],
                    'hsl': [newColor.hslHueF(), newColor.hslSaturationF(), newColor.lightnessF()],
                    }
            log.info('color_button_clicked-> %s and set color %s' % (param['name'], newColor.name()))



class FileWidget(QListWidget):
    """
    .. image:: classes/FileWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget file

    .. image:: pictures/WidgetChooseFile.png
        :alt: Look like
        :align: center
        :width: 500px

    .. image:: pictures/WidgetChooseFileWindow.png
        :alt: Look like window
        :align: center
        :width: 500px

    """
    def __init__(self, param, title3DWindow):
        """ Initialize class FileWidget """
        # Initialize QListWidget
        super(FileWidget, self).__init__()

        self.title3DWindow = title3DWindow

        # Translate text
        _ = get_app()._tr

        #log.info("FileWidget-> " + _("Create widget file select %s") % param)

        if param['value']:
            (dirName, fileName) = os.path.split(param['value'])
            # add icon of picture to item
            item = QListWidgetItem(fileName)
            # add value to tooltip QListWidget
            self.setToolTip(param['value'])
        elif param['values'] == []:
            item = QListWidgetItem(_("No File Selected"))
        else:
            (dirName, fileName) = os.path.split(param['values'][0])
            # add icon of picture to item
            item = QListWidgetItem(fileName)
            # add value to tooltip QListWidget
            self.setToolTip(param['values'][0])
        self.insertItem(0, item)
        self.setMinimumHeight(self.sizeHintForRow(0))

        # Action QListWidget
        self.itemClicked.connect(functools.partial(self.file_index_Activated, self, param))

        if not hasattr(self.title3DWindow, 'file_index_Activated'):
            self.title3DWindow.file_index_Activated = self.file_index_Activated

    def file_index_Activated(self, widget, param):
        """ Change file selected """
        # Translate text
        _ = get_app()._tr

        #log.info('file_index_Activated')
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        titlefiletitle3DWindow = _("Open file")
        extensionfiletitle3DWindow = _("All Files (*)")
        getfilename, filterselect = QFileDialog.getOpenFileName(None, titlefiletitle3DWindow, "", extensionfiletitle3DWindow, options=options)
        if getfilename:
            # History file
            if not getfilename in param["values"]:
                # Add new file path for widget
                param["values"].append(getfilename)
            (dirName, fileName) = os.path.split(getfilename)
            (fileBaseName, fileExtension) = os.path.splitext(fileName)
            # set path to file for blender
            param['value'] = getfilename
            widget.clear()
            item = QListWidgetItem(fileName)
            widget.insertItem(0, item)
            widget.setToolTip(getfilename)
            # Add system info of action
        log.info('FileWidget.file_index_Activated-> Widget %s ' % param + _("Selected file: %s") % getfilename)




class FontWidget(QListWidget):
    """
    .. image:: classes/FontWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget font

    .. image:: pictures/FontSelectWidget.png
        :alt: Look like
        :align: center
        :width: 500px

    .. image:: pictures/FontSelectWidgetWindowChooseFont.png
        :alt: Look like window
        :align: center
        :width: 500px

    """
    def __init__(self, param, title3DWindow):
        """ Initialize class FontWidget """
        # Initialize QListWidget
        super(FontWidget, self).__init__()

        self.title3DWindow = title3DWindow

        # Translate text
        _ = get_app()._tr

        #log.info("FontWidget-> " + _("Create widget font selector %s") % param)

        if param['value']:
            (dirName, fileName) = os.path.split(param['value'])
            # add icon of picture to item
            item = QListWidgetItem(fileName)
            # add value to tooltip QListWidget
            self.setToolTip(param['value'])
        elif param['values'] == []:
            item = QListWidgetItem(_("No File Selected"))
        else:
            (dirName, fileName) = os.path.split(param['values'][0])
            # add icon of picture to item
            item = QListWidgetItem(fileName)
            # add value to tooltip QListWidget
            self.setToolTip(param['values'][0])
        self.insertItem(0, item)
        self.setMinimumHeight(self.sizeHintForRow(0))

        self.itemClicked.connect(functools.partial(self.font_index_Activated, self, param))

        if not hasattr(self.title3DWindow, 'font_index_Activated'):
            self.title3DWindow.font_index_Activated = self.font_index_Activated

    def font_index_Activated(self, widget, param):
        """ Load new movie file """
        # Translate text
        _ = get_app()._tr

        font_file = OpenFontFile(param, self, self.title3DWindow).getFileFont()
        if font_file:
            # set path to file for blender
            param['value'] = font_file
            (dirName, fileName) = os.path.split(font_file)
            # Add file name for list widget
            self.clear()
            item = QListWidgetItem(fileName)
            self.insertItem(0, item)
            # Add path with filename to tooltip widget
            self.setToolTip(font_file)
            #self.setFixedSize(500, 150)
        log.info('FileWidget.font_index_Activated-> ' + _('%s with font %s') % (param['name'], font_file))



class PictureWidget(QListWidget):
    """
    .. image:: classes/PictureWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget picture

    .. image:: pictures/WidgetChoosePicture.png
        :alt: Look like
        :align: center
        :width: 500px

    .. image:: pictures/WidgetChoosePictureWindow.png
        :alt: Look like window
        :align: center
        :width: 500px

    """
    def __init__(self, param, title3DWindow):
        """ Initialize class PictureWidget """
        # Initialize QListWidget
        super(PictureWidget, self).__init__()

        self.title3DWindow = title3DWindow

        # Translate text
        _ = get_app()._tr

        #log.info("PictureWidget-> " + _("Create widget picture selector %s") % param)

        self.setViewMode(QListView.IconMode)
        if param['value']:
            (dirName, fileName) = os.path.split(param['value'])
            # add icon of picture to item
            icon = QIcon()
            icon.addPixmap(QPixmap(param['value']), QIcon.Normal, QIcon.Off)
            item = QListWidgetItem()
            item.setIcon(icon)
            self.setIconSize(QSize(140, 140))
            # add value to tooltip QListWidget
            self.setToolTip(param['value'])
        elif param['values'] == []:
            icon = QIcon()
            icon.addPixmap(QPixmap('/usr/lib/python3.7/site-packages/openshot_qt/images/no_image_available.png'), QIcon.Normal, QIcon.Off)
            item = QListWidgetItem()
            item.setIcon(icon)
            self.setIconSize(QSize(140, 140))
            # add value to tooltip QListWidget
            self.setToolTip(_("No File Selected"))
        else:
            (dirName, fileName) = os.path.split(param['values'][0])
            # add icon of picture to item
            icon = QIcon()
            icon.addPixmap(QPixmap(param['values'][0]), QIcon.Normal, QIcon.Off)
            item = QListWidgetItem()
            item.setIcon(icon)
            self.setIconSize(QSize(140, 140))
            # add value to tooltip QListWidget
            self.setToolTip(param['values'][0])
        self.insertItem(0, item)
        self.setMinimumHeight(self.sizeHintForRow(0))

        self.itemClicked.connect(functools.partial(self.picture_index_Activated, self, param))

        if not hasattr(self.title3DWindow, 'picture_index_Activated'):
            self.title3DWindow.picture_index_Activated = self.picture_index_Activated

    def picture_index_Activated(self, widget, param):
        """ Change selected picture """
        # Translate text
        _ = get_app()._tr

        #log.info('picture_index_Activated')
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        titlefiletitle3DWindow = _("Open image file")
        extensionfiletitle3DWindow = _("PNG File (*.PNG *.png);;Jpeg File (*.JPEG *.jpeg *.JPG *.jpg *.JPE *.jpe *.JFIF *.jfif);;GIF File (*.GIF *.gif);;Bitmap Files (*.BMP *.bmp *.DIB *.dib);;TIFF File (*.TIFF *.tiff *.TIF *.tif);;ICO File (*.ICO *.ico);;All Files (*)")
        getfilename, filterselect = QFileDialog.getOpenFileName(None, titlefiletitle3DWindow, "", extensionfiletitle3DWindow, options=options)
        if getfilename:
            (dirName, fileName) = os.path.split(getfilename)
            (fileBaseName, fileExtension) = os.path.splitext(fileName)
            if fileExtension.lower() not in (".svg"):
                # set path to file for blender
                param['value'] = getfilename
                # Delete old storage file
                param['values'] = {}
                # Add new file path for openshot
                param['values'][fileName] = getfilename
                # Add file path for widget
                widget.clear()
                item = QListWidgetItem()
                icon = QIcon()
                icon.addPixmap(QPixmap(getfilename), QIcon.Normal, QIcon.On)
                item.setIcon(icon)
                widget.insertItem(0, item)
                widget.setToolTip(getfilename)
                widget.setIconSize(QSize(140, 140))
                # Add system info of action
        log.info("picture_index_Activated-> Widget %s " % param['name'] + _("Selected file: %s") % getfilename)



class SoundWidget(QListWidget):
    """
    .. image:: classes/SoundWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget sound

    .. image:: pictures/WidgetSound1.png
        :alt: Look like
        :align: center
        :width: 500px

    .. image:: pictures/WidgetSound2.png
        :alt: Look like window
        :align: center
        :width: 500px

    """
    def __init__(self, param, title3DWindow):
        """ Initialize class SoundWidget """
        # Initialize QListWidget
        super(SoundWidget, self).__init__()

        self.title3DWindow = title3DWindow

        # Translate text
        _ = get_app()._tr

        log.info("SoundWidget-> " + _("Create widget sound solector %s") % param)

        if param['value']:
            (dirName, fileName) = os.path.split(param['value'])
            # add icon of picture to item
            item = QListWidgetItem(fileName)
            # add value to tooltip QListWidget
            self.setToolTip(param['value'])
        elif param['values'] == []:
            item = QListWidgetItem(_("No File Selected"))
        else:
            (dirName, fileName) = os.path.split(param['values'][0])
            # add icon of picture to item
            item = QListWidgetItem(fileName)
            # add value to tooltip QListWidget
            self.setToolTip(param['values'][0])
        self.insertItem(0, item)
        self.setMinimumHeight(self.sizeHintForRow(0))

        self.itemClicked.connect(functools.partial(self.sound_index_Activated, self, param))

        if not hasattr(self.title3DWindow, 'sound_index_Activated'):
            self.title3DWindow.sound_index_Activated = self.sound_index_Activated

    def sound_index_Activated(self, widget, param):
        """ Load new sound file """
        # Translate text
        _ = get_app()._tr

        sound_file = OpenSoundFile(param, self, self.title3DWindow).getFileSound()
        if sound_file:
            # set path to file for blender
            param['value'] = sound_file
            (dirName, fileName) = os.path.split(sound_file)
            # Add file name for list widget
            self.clear()
            item = QListWidgetItem(fileName)
            self.insertItem(0, item)
            # Add path with filename to tooltip widget
            self.setToolTip(sound_file)



class MovieWidget(QListWidget):
    """
    .. image:: classes/MovieWidget.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class Widget movie

    .. image:: pictures/WidgetMovie1.png
        :alt: Look like
        :align: center
        :width: 500px

    .. image:: pictures/WidgetMovie2.png
        :alt: Look like window
        :align: center
        :width: 500px

    """
    def __init__(self, param, title3DWindow):
        """ Initialize class MovieWidget """
        # Initialize QListWidget
        super(MovieWidget, self).__init__()

        self.title3DWindow = title3DWindow

        # Translate text
        _ = get_app()._tr

        log.info("MovieWidget-> " + _("Create widget movie selector %s") % param)

        if param['value']:
            (dirName, fileName) = os.path.split(param['value'])
            # add icon of picture to item
            item = QListWidgetItem(fileName)
            # add value to tooltip QListWidget
            self.setToolTip(param['value'])
        elif param['values'] == []:
            item = QListWidgetItem(_("No File Selected"))
        else:
            (dirName, fileName) = os.path.split(param['values'][0])
            # add icon of picture to item
            item = QListWidgetItem(fileName)
            # add value to tooltip QListWidget
            self.setToolTip(param['values'][0])
        self.insertItem(0, item)
        self.setMinimumHeight(self.sizeHintForRow(0))

        self.itemClicked.connect(functools.partial(self.movie_index_Activated, self, param))

        if not hasattr(self.title3DWindow, 'movie_index_Activated'):
            self.title3DWindow.movie_index_Activated = self.movie_index_Activated

    def movie_index_Activated(self, widget, param):
        """ Load new movie file """
        # Translate text
        _ = get_app()._tr

        movie_file = OpenMovieFile(param, self, self.title3DWindow).getFileMovie()
        if movie_file:
            # set path to file for blender
            param['value'] = movie_file
            (dirName, fileName) = os.path.split(movie_file)
            # Add file name for list widget
            self.clear()
            item = QListWidgetItem(fileName)
            self.insertItem(0, item)
            # Add path with filename to tooltip widget
            self.setToolTip(movie_file)



class OpenFileConfig(QObject):
    """
    .. image:: classes/OpenFileConfig.png
        :alt: Diagram
        :align: center
        :width: 500px

    Test real present files from blender plugin xml configs
    """
    def __init__(self, parentWindow, parameters):
        super(OpenFileConfig, self).__init__(parentWindow)
        self.parentWindow = parentWindow
        self.parameters = parameters
        self.originpath = QDir.currentPath()
        self.homepath = QDir.homePath()
        self.temppath = QDir.tempPath()
        self.rootpath = QDir.rootPath()
        self.openshootpath = info.PATH
        self.realfileslist = []

        # Translate text
        _ = get_app()._tr

        log.info('OpenFileConfig-> ' + _("Try to get file %s from default set config") % self.parameters["default"])

    def getRealsFiles(self):
        """Return list of reals files from blender plugin xml parameters"""
        # Translate text
        _ = get_app()._tr

        #Test file definition into default parameter
        if self.parameters["default"]:
            log.info('OpenFileConfig.getRealsFiles-> ' + _("Try to get file %s from default set config") % self.parameters["default"])
            if self.parameters["default"][0] == ':':
                openshootpathblender = os.path.join(info.PATH, "blender")
                if self.parameters["default"][1] == os.path.sep:
                    self.parameters["default"] = os.path.join(openshootpathblender, self.parameters["default"][2:])
                else:
                    self.parameters["default"] = os.path.join(openshootpathblender, self.parameters["default"][1:])
            try:
                with open(os.path.expanduser(self.parameters["default"])): pass
            except IOError:
                log.info('Warning: OpenFileConfig.getRealsFiles-> ' + _("Default File %s Not Found") % self.parameters["default"])
            else:
                self.parameters["default"] = os.path.expanduser(self.parameters["default"])
                self.realfileslist.append(self.parameters["default"])
                log.info('OpenFileConfig.getRealsFiles-> ' + _("File loaded %s") % self.parameters["default"])
        #Test file definition into values parameter
        if self.parameters["values"].items() and self.parameters["values"] != {'': ''}:
            log.info('OpenFileConfig.getRealsFiles-> ' + _("Try to get a file from values configured list %s") % str(self.parameters["values"]))
            flag = True
            for choosefile in self.parameters["values"]:
                if choosefile[0] == ':':
                    openshootpathblender = os.path.join(info.PATH, "blender")
                    if choosefile[1] == os.path.sep:
                        choosefile = os.path.join(openshootpathblender, choosefile[2:])
                    else:
                        choosefile = os.path.join(openshootpathblender, choosefile[1:])
                try:
                    with open(os.path.expanduser(choosefile)): pass
                except IOError:
                    continue
                else:
                    if os.path.expanduser(choosefile) not in self.realfileslist:
                        choosefile = os.path.expanduser(choosefile)
                        self.realfileslist.append(choosefile)
                        flag = False
                        log.info('OpenFileConfig.getRealsFiles-> ' + _("File loaded %s") % choosefile)
                    else:
                        log.info('Warning: OpenFileConfig.getRealsFiles-> ' + _("File %s already in list") % choosefile)
                    #break
            if flag:
                log.info('Warning: OpenFileConfig.getRealsFiles-> ' + _("No Files found from configured list of files"))
        if self.realfileslist == []:
            log.info('OpenFileConfig.getRealsFiles-> ' + _("No File detected from config"))
        return self.realfileslist



class OpenFontFile(QDialog):
    """
    .. image:: classes/OpenFontFile.png
        :alt: Diagram
        :align: center
        :width: 500px

    Open font file widget with test player
    """
    def __init__(self, param, widget, title3DWindow):
        # finitialize QDialog class
        super(OpenFontFile, self).__init__()

        self.param = param
        self._widget = widget
        self.title3DWindow = title3DWindow
        self.urlFont = None
        self.selectFont = None
        self.default_list_fonts = {}
        self.defaultFontName = None
        self.defaultFontPath = None
        self.defaultFontid = None

        # Translate text
        _ = get_app()._tr

        # Initialize title3DWindow
        self.initUI()

        # Initialize data base system list fonts
        self.dbFont = QFontDatabase()

        # Get font setting for default file font
        if self._widget.toolTip() == '' :
            self.defaultFontPath = QDir.homePath()
            self.initialPathFile = False
            # Update widgets view
            self.updateFontsList()
        else:
            # Define default path
            self.defaultFontPath = self._widget.toolTip()
            # Set default font name
            self.defaultFontName = self._widget.currentItem().text()
            # Define title to default font name
            self.setWindowTitle(self.defaultFontName)
            # Flag for default file path font
            self.initialPathFile = True
            # Install font into system and get id for system font
            self.defaultFontid = self.dbFont.addApplicationFont(self.defaultFontPath)
            self.default_list_fonts.update({ self.defaultFontid: { 'names': self.dbFont.applicationFontFamilies(self.defaultFontid), 'file_name': self.defaultFontName, 'file_path': self.defaultFontPath, 'url': None }})

            # Update widgets view
            self.updateFontsList({ self.defaultFontid: self.default_list_fonts[self.defaultFontid] })

    def initUI(self):
        """Add title3DWindow UI"""
        # Translate text
        _ = get_app()._tr

        # Path to ui title3DWindow
        self.ui_path = os.path.join(info.PATH, 'windows', 'ui', 'font-choose.ui')

        # Load UI from designer
        ui_util.load_ui(self, self.ui_path)

        # Init UI
        ui_util.init_ui(self)

        # Set Window title
        self.setWindowTitle(_('Choose a font file'))

        # Connect button Ok to chooseFont function
        self.buttonBoxActions.button(QDialogButtonBox.Ok).clicked.connect(self.chooseFont)

        # Connect button Open font file to function openFontFile
        self.btn_open_file_font.clicked.connect(self.openFontFile)

        # Connect button Import system fonts to function immportSystemFonts
        self.btn_import_system_fonts.clicked.connect(self.importSystemFonts)

        # Connect list fonts choose to method chooseFontFromList
        self.list_fonts.itemClicked.connect(self.chooseFontFromList)

        # Connect list fonts names to method previewFont
        self.list_file_fonts_names.itemClicked.connect(self.previewFont)

        # Connect style font choose to method stylePreviewFont
        #self.list_style_font.itemClicked.connect(self.previewFont)

        #self.setWindowTitle(self.title)
        #self.setGeometry(self.left, self.top, self.width, self.height)

        # Show font choose title3DWindow
        self.show()


    def updateFontsList(self, default=None):
        """ Update list of fonts and default font """
        # Translate text
        _ = get_app()._tr

        # clear list widget
        self.list_fonts.clear()
        self.list_file_fonts_names.clear()

        if default:
            log.info('OpenFontFile.updateFontsList-> ' + _("default font: %s") % default)
            # set default id font
            self.defaultFontid = list(default.keys())[0]
            # Set url font selected
            self.urlFont = default[self.defaultFontid]['url']
            # Set default file for next open
            self.defaultFontPath = default[self.defaultFontid]['file_path']
            # Set default font name
            self.defaultFontName = default[self.defaultFontid]['file_name']
            # Set title title3DWindow to file name
            self.setWindowTitle(default[self.defaultFontid]['file_name'])
            #self.setFixedSize(500, 150)

        for fontinlist in self.default_list_fonts:
            # Add name file to list
            item = QListWidgetItem(self.default_list_fonts[fontinlist]['file_name'])
            # Add file path to tooltip item
            item.setToolTip(self.default_list_fonts[fontinlist]['file_path'])
            # Add id system font
            item.id_system = fontinlist
            # Add item to list
            self.list_fonts.addItem(item)
            # Select if default value
            if default and fontinlist == list(default.keys())[0]:
                self.list_fonts.setCurrentItem(item)
                self.list_file_fonts_names.addItems(self.dbFont.applicationFontFamilies(fontinlist))

            # Create default font from path self.defaultFontPath
            #defaultFont = QFont()
            # Create default family font
            #defaultFont.setFamily(self.dbFont.applicationFontFamilies(self.defaultFontid)[0])


    def chooseFontFromList(self, item):
        """ Font file choose from list """
        # Translate text
        _ = get_app()._tr

        print('---------> item = %s, text = %s, id = %s' % (item, item.text(), item.id_system))
        self.list_file_fonts_names.clear()
        self.list_file_fonts_names.addItems(self.dbFont.applicationFontFamilies(item.id_system))


    def previewFont(self, fontname):
        """ Font file choose from list """
        # Translate text
        _ = get_app()._tr

        print('---------> font = %s' % (fontname))
        #self.list_file_fonts_names.addItems(self.dbFont.applicationFontFamilies(item.id_system))


    def stylePreviewFont(self, fontstyle):
        """ Font file choose from list """
        # Translate text
        _ = get_app()._tr

        print('---------> style = %s' % (fontstyle))
        #self.list_file_fonts_names.addItems(self.dbFont.applicationFontFamilies(item.id_system))


    def closeEvent(self, event):
        """ Exit from close title3DWindow choose font"""
        # Translate text
        _ = get_app()._tr

        # getFileFont method return None
        self.ok = None

        #Stop the player when exit
        event.accept()


    def reject(self):
        """ Exit from button cancel choose font title3DWindow """
        # Translate text
        _ = get_app()._tr

        # getFileFont method return None
        self.ok = None

        log.info("OpenFontFile.reject-> " + _("Cancel button"))

        #Cancel title3DWindow
        super(OpenFontFile, self).reject()


    def chooseFont(self):
        """Return selected file font"""
        # Translate text
        _ = get_app()._tr

        if self.urlFont:
            #(dirName, fileName) = os.path.split(self.urlFont.fileName())
            log.info('OpenFontFile.chooseFont-> ' + _("Selected font file: %s") % self.urlFont.fileName())
            self.selectFont = self.urlFont.path()
        else:
            log.info('OpenFontFile.chooseFont-> ' + _("No File selected"))
            self.selectFont = None

        self.ok = None

        #exit OpenFontFile Window
        self.close()


    def openFontFile(self):
        """ Open file font """
        # Translate text
        _ = get_app()._tr

        # Open file font
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        titlefiletitle3DWindow = _("Open font file")
        extensionfiletitle3DWindow = _("Font Files (*.ttf *.TTF *.otf *.OTF)")
        font, filterselect = QFileDialog.getOpenFileName(self, titlefiletitle3DWindow, self.defaultFontPath, extensionfiletitle3DWindow, options=options)
        # File font statement
        if font != '':
            log.info('OpenFontFile.openFontFile-> ' + _("Open file font: %s") % font)
            #TODO test if allrady into list
            fontnotexist = True
            for itemfont in self.default_list_fonts:
                # Path font already into list selected fonts ?
                if font == self.default_list_fonts[itemfont]['file_path']:
                    fontnotexist = False
                    # Update default font to selected font
                    self.updateFontsList({ itemfont: self.default_list_fonts[itemfont] })
            if fontnotexist:
                # Install font into system and get id for system font
                Fontid = self.dbFont.addApplicationFont(QUrl.fromLocalFile(font).path())
                if Fontid > 0:
                    font = { Fontid: { 'names': self.dbFont.applicationFontFamilies(Fontid), 'file_name': QUrl.fromLocalFile(font).fileName(), 'file_path': QUrl.fromLocalFile(font).path(), 'url': QUrl.fromLocalFile(font) }}
                    # Add font to list fonts
                    self.default_list_fonts.update(font)
                    # Install into widget list
                    self.updateFontsList(font)
                else:
                    log.info('OpenFontFile.openFontFile-> ' + _("Wrong file font: %s, error: %s") % (font, Fontid))


    def importSystemFonts(self):
        """ Import fonts files from system """
        # Translate text
        _ = get_app()._tr

        log.info('OpenFontFile.importSystemFonts-> ' + _("Get list system fonts"))

    def getFileFont(self):
        """ Get font file selected """
        # Translate text
        _ = get_app()._tr

        while (not hasattr(self, 'ok')):
            qApp.processEvents()

        log.info('OpenFontFile.getFileFont-> %s' % self.selectFont)
        return self.selectFont

class OpenSoundFile(QMainWindow):
    """
    .. image:: classes/OpenSoundFile.png
        :alt: Diagram
        :align: center
        :width: 500px

    Open sound file widget with test player
    """
    def __init__(self, param, widget, title3DWindow):
        super(OpenSoundFile, self).__init__()
         # Translate text
        _ = get_app()._tr

        self.param = param
        self._widget = widget
        self.title3DWindow = title3DWindow
        self.urlSong = None
        self.selectSound = None

        # Add player widgets song controls
        self.player = QMediaPlayer()
        self.playlist = QMediaPlaylist()
        self.playBtn = QPushButton()
        self.stopBtn = QPushButton()
        self.eraseBtn = QPushButton()
        self.positionSlider = QSlider(Qt.Horizontal)
        self.playsoundframe = QFrame()
        self.volumeslider = QSlider(Qt.Vertical)
        # Add buttons song choose controls
        self.chooseBtn = QPushButton(_('OK'))
        self.cancelBtn = QPushButton(_('Cancel'))
        self.title = _('Choose a sound file')
        self.defaultPlay = True
        # Get file song preview selected for default folder looking for
        if self._widget.toolTip() == '' :
            self.defaultSoundPath = QDir.homePath()
            self.initialPathFile = False
        else:
            self.defaultSoundPath = self._widget.toolTip()
            self.initialPathFile = True
        self.left = 300
        self.top = 300
        self.width = 500
        self.height = 70
        self.color = 0  # 0- toggle to dark 1- toggle to light
        self.userAction = -1  # 0- stopped, 1- playing 2-paused
        self.initUI()


    def initUI(self):
        """Add menu bar and defaults sets to QMainWindow object"""
        # Translate text
        _ = get_app()._tr

        # Add file menu
        menubar = self.menuBar()
        filemenu = menubar.addMenu(self.style().standardIcon(QStyle.SP_DirIcon), _('File'))

        fileAct = QAction(self.style().standardIcon(QStyle.SP_DirOpenIcon), _('&Open File'), self)

        fileAct.setShortcut(_('Ctrl+O'))

        fileAct.setStatusTip(_('Open sound file'))

        filemenu.addAction(fileAct)

        fileAct.triggered.connect(self.openFile)

        self.addControls()

        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.show()


    def addControls(self):
        """Add widgets to QMainWindow object"""
        # Translate text
        _ = get_app()._tr

        wid = QWidget(self)
        self.setCentralWidget(wid)
        # Add song position controls
        self.positionSlider.setRange(0, 0)
        self.positionSlider.setEnabled(False)
        # Add song volume controls
        self.volumeslider.setFocusPolicy(Qt.NoFocus)
        self.volumeslider.setValue(100)
        self.playBtn.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.stopBtn.setIcon(self.style().standardIcon(QStyle.SP_MediaStop))
        self.eraseBtn.setIcon(self.style().standardIcon(QStyle.SP_TrashIcon))
        # Add layouts
        #centralWidget
        soundArea = QHBoxLayout()
        #Sound panel
        panelSoundArea = QVBoxLayout()
        #Choose actions
        chooseLayout = QHBoxLayout()
        #Player sound
        playlistCtrlLayout = QHBoxLayout()
        playsoundlayout = QHBoxLayout()
        self.playsoundframe.setLayout(playlistCtrlLayout)
        # Add play sound buttons to song controls layout
        playsoundlayout.addWidget(self.playBtn)
        playsoundlayout.addWidget(self.stopBtn)
        # Add buttons to playlist controls layout
        playlistCtrlLayout.addLayout(playsoundlayout)
        playlistCtrlLayout.addWidget(self.positionSlider)
        playlistCtrlLayout.addWidget(self.eraseBtn)
        # Add button to choose action layout
        chooseLayout.addWidget(self.chooseBtn)
        chooseLayout.addWidget(self.cancelBtn)
        # Add to layouts to panelSoundArea
        panelSoundArea.addWidget(self.playsoundframe)
        panelSoundArea.addLayout(chooseLayout)
        # Add to horizontal layout
        soundArea.addLayout(panelSoundArea)
        soundArea.addWidget(self.volumeslider)
        wid.setLayout(soundArea)
        # Set initial widget state
        if self.initialPathFile:
            # Get song selected
            self.urlSong = QUrl.fromLocalFile(self.defaultSoundPath)
            # Set default file for player
            self.playlist.clear()
            self.playlist.addMedia(QMediaContent(self.urlSong))
            self.player.setPlaylist(self.playlist)
            self.playBtn.setEnabled(True)
            self.stopBtn.setEnabled(True)
            self.eraseBtn.setEnabled(True)
            self.volumeslider.show()
            self.playsoundframe.show()
            self.setFixedSize(500, 150)
        else:
            self.playBtn.setEnabled(False)
            self.stopBtn.setEnabled(False)
            self.eraseBtn.setEnabled(False)
            self.volumeslider.hide()
            self.playsoundframe.hide()
        # Connect each signal to their appropriate function
        self.playBtn.clicked.connect(self.playhandler)
        self.stopBtn.clicked.connect(self.stophandler)
        self.eraseBtn.clicked.connect(self.clearhandler)

        self.player.stateChanged.connect(self.playStateChanged)
        self.player.positionChanged.connect(self.positionChanged)
        self.player.durationChanged.connect(self.durationChanged)
        self.player.error.connect(self.handleError)

        self.positionSlider.sliderMoved.connect(self.setPosition)
        self.volumeslider.valueChanged[int].connect(self.changeVolume)

        self.chooseBtn.clicked.connect(self.chooseSong)
        self.cancelBtn.clicked.connect(self.close)

        self.statusBar()
        self.playlist.currentMediaChanged.connect(self.songChanged)


    def openFile(self):
        """Open file song"""
        # Translate text
        _ = get_app()._tr

        # Open file song
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        titlefiletitle3DWindow = _("Open song file")
        extensionfiletitle3DWindow = _("Sound Files (*.mp3 *.ogg *.wav *.m4a)")
        song, filterselect = QFileDialog.getOpenFileName(self, titlefiletitle3DWindow, self.defaultSoundPath, extensionfiletitle3DWindow, options=options)
        # File song statement
        if song != '':
            # Get song selected
            self.urlSong = QUrl.fromLocalFile(song)
            # Set default file for next open
            self.defaultSoundPath = self.urlSong.path()
            self.playlist.clear()
            self.playlist.addMedia(QMediaContent(self.urlSong))
            self.player.setPlaylist(self.playlist)
            self.playBtn.setEnabled(True)
            self.eraseBtn.setEnabled(True)
            self.positionSlider.setEnabled(True)
            self.volumeslider.show()
            self.playsoundframe.show()
            if self.defaultPlay:
                self.player.play()
                self.stopBtn.setEnabled(True)
            self.userAction = 1
            self.setFixedSize(500, 150)
            log.info('OpenSoundFile.openFile-> ' + _("Selected file: %s") % song)


    def playhandler(self):
        """Play or pause music if playing"""
        # Translate text
        _ = get_app()._tr

        if self.playlist.mediaCount() == 0:
            self.openFile()
        elif self.playlist.mediaCount() != 0:
            self.stopBtn.setEnabled(True)
            if self.player.state() == QMediaPlayer.PlayingState:
                self.player.pause()
                self.userAction = 2
                self.setWindowTitle(self.urlSong.fileName())
                self.statusBar().showMessage(_("Pause"))
            else:
                self.player.play()
                self.userAction = 1
                self.setWindowTitle(self.urlSong.fileName())
                self.statusBar().showMessage(_("Play"))


    def stophandler(self):
        """Stop play music"""
        # Translate text
        _ = get_app()._tr

        self.userAction = 0
        self.player.stop()
        self.stopBtn.setEnabled(False)
        self.setWindowTitle(self.urlSong.fileName())
        self.statusBar().showMessage(_("Stopped"))


    def clearhandler(self):
        """Stop play music"""
        # Translate text
        _ = get_app()._tr

        self.userAction = 0
        self.player.stop()
        self.setWindowTitle(self.urlSong.fileName())
        self.playlist.clear()
        self.player.setPlaylist(self.playlist)
        self.playBtn.setEnabled(False)
        self.positionSlider.setEnabled(False)
        self.volumeslider.hide()
        self.playsoundframe.hide()
        self.urlSong = None
        self.setFixedSize(500, 100)
        self.statusBar().showMessage(_("Cleared"))


    def changeVolume(self, value):
        """Set volume player from slider volume"""
        self.player.setVolume(value)


    def songChanged(self, media):
        """Set select song, and display into status bar to OpenSoundFile widget"""
        if not media.isNull():
            self.urlSong = media.canonicalUrl()
            self.statusBar().showMessage(self.urlSong.fileName())


    def setPosition(self, position):
        """Set position song play from slider"""
        self.player.setPosition(position)


    def playStateChanged(self, state):
        """Set state of play song : play or pause"""
        if self.player.state() == QMediaPlayer.PlayingState:
            self.playBtn.setIcon(self.style().standardIcon(QStyle.SP_MediaPause))
        else:
            self.playBtn.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))


    def positionChanged(self, position):
        """Set position slider from song position play"""
        self.positionSlider.setValue(position)


    def durationChanged(self, duration):
        """Update range from media duration song"""
        self.positionSlider.setRange(0, duration)


    def chooseSong(self):
        """Return selected file song"""
        # Translate text
        _ = get_app()._tr

        if self.urlSong:
            log.info("OpenSoundFile.chooseSang-> " + _("Selected sound file: %s") % self.urlSong.fileName())
            self.selectSound = self.urlSong.path()
        else:
            log.info("OpenSoundFile.chooseSang-> " + _("No File selected"))


        # getFileSound method return None
        self.ok = None

        #exit OpenSoundFile Window
        self.close()


    def closeEvent(self, event):
        #Stop the player when exit
        self.player.stop()

        # getFileSound method return None
        self.ok = None

        event.accept()


    def handleError(self):
        # Translate text
        _ = get_app()._tr

        #self.playBtn.hide()
        self.playBtn.setEnabled(False)
        self.stopBtn.setEnabled(False)
        self.playmovieframe.hide()
        self.volumeslider.hide()
        self.setFixedSize(500, 100)
        self.statusBar().showMessage(_("Error: ") + self.player.errorString())


    def getFileSound(self):
        """ Get sound file from selected sound with GUI"""
        # Translate text
        _ = get_app()._tr

        while (not hasattr(self, 'ok')):
            qApp.processEvents()

        log.info('OpenSoundFile.getFileSound-> %s' % self.urlSong)
        return self.selectSound



class OpenMovieFile(QMainWindow):
    """
    .. image:: classes/OpenMovieFile.png
        :alt: Diagram
        :align: center
        :width: 500px

    Open movie file widget with test player
    """
    def __init__(self, param, widget, title3DWindow):
        super(OpenMovieFile, self).__init__()
        # Translate text
        _ = get_app()._tr

        self.param = param
        self._widget = widget
        self.title3DWindow = title3DWindow
        self.urlMovie = None
        self.selectMovie = None

        #centralWidgetself.playmovieframe
        self.movieArea = QHBoxLayout()
        # Add player widgets movie controls
        self.player = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        self.playlist = QMediaPlaylist()
        self.playBtn = QPushButton()
        self.stopBtn = QPushButton()
        self.eraseBtn = QPushButton()
        self.positionSlider = QSlider(Qt.Horizontal)
        self.playmovieframe = QFrame()
        self.volumeslider = QSlider(Qt.Vertical)
        # Add buttons movie choose controls
        self.chooseBtn = QPushButton(_('OK'))
        self.cancelBtn = QPushButton(_('Cancel'))
        self.title = _('Choose a movie file')
        self.defaultPlay = True
        # Get file movie preview selected for default folder looking for
        if self._widget.toolTip() == '' :
            self.defaultMoviePath = QDir.homePath()
            self.initialPathFile = False
        else:
            self.defaultMoviePath = self._widget.toolTip()
            self.initialPathFile = True
        self.left = 300
        self.top = 300
        self.width = 500
        self.height = 100
        self.color = 0  # 0- toggle to dark 1- toggle to light
        self.userAction = -1  # 0- stopped, 1- playing 2-paused
        self.initUI()

    def initUI(self):
        """Add menu bar and defaults sets to QMainWindow object"""
        # Translate text
        _ = get_app()._tr

        # Add file menu
        menubar = self.menuBar()
        filemenu = menubar.addMenu(self.style().standardIcon(QStyle.SP_DirIcon), _('File'))

        fileAct = QAction(self.style().standardIcon(QStyle.SP_DirOpenIcon), _('&Open File'), self)

        fileAct.setShortcut(_('Ctrl+O'))

        fileAct.setStatusTip(_('Open movie file'))

        filemenu.addAction(fileAct)

        fileAct.triggered.connect(self.openFile)

        self.addControls()

        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.show()

    def addControls(self):
        """Add widgets to QMainWindow object"""
        # Translate text
        _ = get_app()._tr

        wid = QWidget(self)
        videoWidget = QVideoWidget()
        self.setCentralWidget(wid)
        # Add movie position controls
        self.positionSlider.setRange(0, 0)
        self.positionSlider.setEnabled(False)
        # Add movie volume controls
        self.volumeslider.setFocusPolicy(Qt.NoFocus)
        self.volumeslider.setValue(100)
        self.playBtn.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.stopBtn.setIcon(self.style().standardIcon(QStyle.SP_MediaStop))
        self.eraseBtn.setIcon(self.style().standardIcon(QStyle.SP_TrashIcon))
        # Add layouts
        # Movie panel
        panelMovieArea = QVBoxLayout()
        #Choose actions
        chooseLayout = QHBoxLayout()
        #Player movie
        playvideoLayout = QVBoxLayout()
        playlistCtrlLayout = QHBoxLayout()
        playmovielayout = QHBoxLayout()
        # Add widgets to playmovieframe
        playvideoLayout.addWidget(videoWidget)
        playvideoLayout.addLayout(playlistCtrlLayout)
        self.playmovieframe.setLayout(playvideoLayout)
        # Add play movie buttons to movie controls layout
        playmovielayout.addWidget(self.playBtn)
        playmovielayout.addWidget(self.stopBtn)
        # Add buttons to playlist controls layout
        playlistCtrlLayout.addLayout(playmovielayout)
        playlistCtrlLayout.addWidget(self.positionSlider)
        playlistCtrlLayout.addWidget(self.eraseBtn)
        # Add button to choose action layout
        chooseLayout.addWidget(self.chooseBtn)
        chooseLayout.addWidget(self.cancelBtn)
        # Add to layouts to panelMovieArea
        self.playmovieframe.show()
        panelMovieArea.addWidget(self.playmovieframe)
        panelMovieArea.addLayout(chooseLayout)
        # Add to horizontal layout
        self.movieArea.addLayout(panelMovieArea)
        self.movieArea.addWidget(self.volumeslider)
        wid.setLayout(self.movieArea)
        # Set initial widget state
        if self.initialPathFile:
            # Get movie selected
            self.urlMovie = QUrl.fromLocalFile(self.defaultMoviePath)
            # Set default file for player
            self.playlist.clear()
            self.playlist.addMedia(QMediaContent(self.urlMovie))
            self.player.setPlaylist(self.playlist)
            self.playBtn.setEnabled(True)
            self.stopBtn.setEnabled(True)
            self.eraseBtn.setEnabled(True)
            self.volumeslider.show()
            self.playmovieframe.show()
            self.setFixedSize(1024, 780)
        else:
            self.playBtn.setEnabled(False)
            self.stopBtn.setEnabled(False)
            self.eraseBtn.setEnabled(False)
            self.volumeslider.hide()
            self.playmovieframe.hide()
        # Connect each signal to their appropriate function
        self.playBtn.clicked.connect(self.playhandler)
        self.stopBtn.clicked.connect(self.stophandler)
        self.eraseBtn.clicked.connect(self.clearhandler)

        self.player.setVideoOutput(videoWidget)

        self.player.stateChanged.connect(self.playStateChanged)
        self.player.positionChanged.connect(self.positionChanged)
        self.player.durationChanged.connect(self.durationChanged)
        self.player.error.connect(self.handleError)

        self.positionSlider.sliderMoved.connect(self.setPosition)
        self.volumeslider.valueChanged[int].connect(self.changeVolume)

        self.chooseBtn.clicked.connect(self.chooseMovie)
        self.cancelBtn.clicked.connect(self.close)

        self.statusBar()
        self.playlist.currentMediaChanged.connect(self.movieChanged)

    def openFile(self):
        """Open file movie"""
        # Translate text
        _ = get_app()._tr

        # Open file movie
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        titlefiletitle3DWindow = _("Open movie file")
        extensionfiletitle3DWindow = _("Movies Files (*.avi *.mpeg *.mpg *.ts *.vob *.mp4 *.mov *.moov *.qt *.qtvr *.webm *.flv *.mkv *.ogm *.ogg *.ogv *.mjpeg *.mjpg *.divx *.avf *.dv)")
        movie, filterselect = QFileDialog.getOpenFileName(self, titlefiletitle3DWindow, self.defaultMoviePath, extensionfiletitle3DWindow, options=options)
        # File movie statement
        if movie != '':
            # Get movie selected
            self.urlMovie = QUrl.fromLocalFile(movie)
            # Set default file for next open
            self.defaultMoviePath = self.urlMovie.path()
            self.playlist.clear()
            self.playlist.addMedia(QMediaContent(self.urlMovie))
            self.player.setPlaylist(self.playlist)
            self.playBtn.setEnabled(True)
            self.eraseBtn.setEnabled(True)
            self.positionSlider.setEnabled(True)
            self.volumeslider.show()
            self.playmovieframe.show()
            if self.defaultPlay:
                self.player.play()
                self.stopBtn.setEnabled(True)
            self.userAction = 1
            self.setFixedSize(1024, 780)
            log.info('OpenMovieFile.openFile-> ' + _("Selected file: %s") % movie)

    def playhandler(self):
        """Play or pause movie if playing"""
        # Translate text
        _ = get_app()._tr

        if self.playlist.mediaCount() == 0:
            self.openFile()
        elif self.playlist.mediaCount() != 0:
            self.stopBtn.setEnabled(True)
            if self.player.state() == QMediaPlayer.PlayingState:
                self.player.pause()
                self.userAction = 2
                self.setWindowTitle(self.urlMovie.fileName())
                self.statusBar().showMessage(_("Pause"))
            else:
                self.player.play()
                self.userAction = 1
                self.setWindowTitle(self.urlMovie.fileName())
                self.statusBar().showMessage(_("Play"))

    def stophandler(self):
        """Stop play movie"""
        # Translate text
        _ = get_app()._tr

        self.userAction = 0
        self.player.stop()
        self.stopBtn.setEnabled(False)
        self.setWindowTitle(self.urlMovie.fileName())
        self.statusBar().showMessage(_("Stopped"))

    def clearhandler(self):
        """Stop play movie"""
        # Translate text
        _ = get_app()._tr

        self.userAction = 0
        self.player.stop()
        self.setWindowTitle(self.urlMovie.fileName())
        self.playlist.clear()
        self.player.setPlaylist(self.playlist)
        self.playBtn.setEnabled(False)
        self.positionSlider.setEnabled(False)
        self.volumeslider.hide()
        self.playmovieframe.hide()
        self.urlMovie = None
        self.setFixedSize(500, 100)
        self.statusBar().showMessage(_("Cleared"))

    def changeVolume(self, value):
        """Set volume player from slider volume"""
        self.player.setVolume(value)

    def movieChanged(self, media):
        """Set select movie, and display into status bar to OpenMovieFile widget"""
        if not media.isNull():
            self.urlMovie = media.canonicalUrl()
            self.statusBar().showMessage(self.urlMovie.fileName())

    def setPosition(self, position):
        """Set position movie play from slider"""
        self.player.setPosition(position)

    def playStateChanged(self, state):
        """Set state of play movie : play or pause"""
        if self.player.state() == QMediaPlayer.PlayingState:
            self.playBtn.setIcon(self.style().standardIcon(QStyle.SP_MediaPause))
        else:
            self.playBtn.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))

    def positionChanged(self, position):
        """Set position slider from movie position play"""
        self.positionSlider.setValue(position)

    def durationChanged(self, duration):
        """Update range from media duration movie"""
        self.positionSlider.setRange(0, duration)

    def chooseMovie(self):
        """Return selected file movie"""
        # Translate text
        _ = get_app()._tr

        if self.urlMovie:
            log.info("OpenMovieFile.chooseMovie-> " + _("Selected movie file: %s") % self.urlMovie.fileName())
            self.selectMovie = self.urlMovie.path()
        else:
            log.info("OpenMovieFile.chooseMovie-> " + _("No File selected"))

        # getFileMovie method return None
        self.ok = None

        #exit OpenMovieFile Window
        self.close()

    def closeEvent(self, event):
        #Stop the player when exit
        self.player.stop()

        # getFileModie method return None
        self.ok = None

        event.accept()

    def handleError(self):
        # Translate text
        _ = get_app()._tr

        #self.playBtn.hide()
        self.playBtn.setEnabled(False)
        self.stopBtn.setEnabled(False)
        self.playmovieframe.hide()
        self.volumeslider.hide()
        self.setFixedSize(500, 100)
        self.statusBar().showMessage(_("Error: ") + self.player.errorString())

    def getFileMovie(self):
        """ Get movie file from selected movie with GUI"""
        # Translate text
        _ = get_app()._tr

        while (not hasattr(self, 'ok')):
            qApp.processEvents()

        log.info('OpenMovieFile.getFileMovie-> %s' % self.urlMovie)
        return self.selectMovie

