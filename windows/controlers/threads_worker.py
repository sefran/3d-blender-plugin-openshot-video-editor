"""
    The ``threads_worker`` module
    -----------------------------

    :Date: |date|
    :Revision: 1.0
    :Author: Franc SERRES <sefran007@gmail.com> from preview work of Jonathan Thomas <jonathan@openshot.org>
    :Description: Module Controler for threads OpenShot Animated 3D plugins
    :Info: See <https://framagit.org/sefran/3d-blender-plugin-openshot-video-editor> for more recent info.

    Use it to import threads worker features.
    
    :class:`ThreadsManager`
    
    :class:`Worker`
    
    :class:`WorkerSignals`
    
"""

import subprocess, signal
import traceback, sys

from PyQt5.QtCore import QObject, QThreadPool, QRunnable, pyqtSlot, pyqtSignal

from classes import info
from classes.logger import log
from classes.app import get_app




class ThreadsManager(QObject):
    """
    .. image:: classes/ThreadsManager.png
        :alt: Diagram
        :align: center
        :width: 500px

    Class for gesture threaded render tasks

    :ivar terminate_process: Window of Animated 3D title plugins
    :vartype terminate_process: list[]
    :ivar currentworker: active threaded features to pull
    :vartype terminate_process: Worker
    
    """

    def __init__(self, *args, **kwargs):
        """Initialise ThreadsManager class"""
        super(ThreadsManager, self).__init__()

        # Translate text
        _ = get_app()._tr

        # Init variables
        self._param_call = None
        self._operator = None
        self._progress_function = None
        self.list_system_process_renders = {} # list of actives system render ({TaskPluginView: subprocess.Popen()})

        # Create thread pool worker list
        self._threads = []

        # Initialise Call of kill render Blender plugin
        self.terminate_process = [] #[{'State': False, 'object': None }]

        self._threadpool = QThreadPool()


        # Set max execute render threads
        log.info('ThreadsManager-> ' + _("Multithreading with maximum %d threads") % self._threadpool.maxThreadCount())
        self.currentworker = None


    def execute_function(self, execute_this_function, id_task=None, params_function=None, filter_function=None, func_operator=None, progress_function=None, manage_process_render_function=None, manage_errors_function=None):
        """
        Add new threaded function to pool and execute. Return current worker thread

        :param execute_this_function: Function to execute into thread
        :type execute_this_function: function
        :param id_task: identifiant of task render
        :type id_task: TaskPluginView
        :param params_function: identifiant of task render
        :type params_function: dict
        :param filter_function: Function for intermediate traitment
        :type filter_function: function
        :param func_operator: Function for result traitment
        :type func_operator: function
        :param progress_function: Function for progress traitment
        :type progress_function: function
        :param manage_process_render_function: Function for manage execution
        :type manage_process_render_function: function
        :param manage_errors_function: Function for manage errors
        :type manage_errors_function: function
        """

        # Translate text
        _ = get_app()._tr

        log.info("ThreadsManager.execute_function-> " + _("call «%s»") % execute_this_function.__name__)
        self.currentworker = Worker(execute_this_function, id_task, params_function, filter_function) # Function and argrun functionrun funcprogress are passed to the worker

        if func_operator:
            self.currentworker.signals.call_operator.connect(func_operator)
        if progress_function:
            self.currentworker.signals.process_progress.connect(progress_function)
        if manage_process_render_function:
            self.currentworker.signals.manage_process.connect(manage_process_render_function)
        if manage_errors_function:
            self.currentworker.signals.manage_errors.connect(manage_errors_function)
        self.currentworker.signals.result.connect(self.thread_unpool)
        self.currentworker.signals.finished.connect(self.thread_complete)
        #self.currentworker.signals.error.connect()
        self.currentworker.signals.process_thread.connect(self.thread_pool)
        self.currentworker.signals.terminate_process.connect(self.terminate_render_plugin)

        # Execute thread
        self._threadpool.start(self.currentworker)

        # Number of threads
        log.info("ThreadsManager.execute_function-> " + _("actives threads %s") % self._threadpool.activeThreadCount())

        return self.currentworker


    def thread_pool(self, id_thread):
        """ 
        Thread render pool 
        
        :param id_thread: Identifiant of thread
        :type id_thread:
        """
        # Translate text
        _ = get_app()._tr

        #log.info("thread_pool-> " + _("Pool"))
        self._threads.append(id_thread)
        log.info("ThreadsManager.thread_pool-> " + _("Thread %s poolled into %s") % (id_thread, self._threads))


    def thread_unpool(self, result):
        """ 
        Thread render unpool 
        
        :param result: Result of thread traitment
        :type result:
        """
        # Translate text
        _ = get_app()._tr

        self._threads.remove(result[0])
        log.info("ThreadsManager.thread_unpool-> " + _("Thread %s unpoolled from %s") % (result[0], self._threads))


    def thread_complete(self):
        """ Threads state gesture """
        # Translate text
        _ = get_app()._tr

        #if self._worker == []:
        #    self._worker = None
        log.info("ThreadsManager.thread_complete-> " + _("THREAD COMPLETE!"))


    def get_pool(self):
        """ Return thread pool object """
        # Translate text
        _ = get_app()._tr

        return self._threadpool


    def get_list_thread_pool(self):
        """ get list of polled threads """
        # Translate text
        _ = get_app()._tr

        return self._threads


    def get_current_worker(self):
        """ get current worker executed """
        # Translate text
        _ = get_app()._tr

        return self.currentworker


    def terminate_render_plugin(self, param_object=None):
        """ 
        Kill active render plugin 
        
        :param param_object: 
        :type param_object:
        """
        # Translate text
        _ = get_app()._tr

        log.info("ThreadsManager.terminate_render_plugin-> " + _("terminate process asked"))
        self.terminate_process.append({'State': True, 'object': param_object })

        
    def kill_all_process(self):
        """End of all active render process"""
        # Translate text
        _ = get_app()._tr

        log.info("ThreadsManager.kill_all_process-> " + _("terminate all system render threads"))
        for thread_render in self.list_system_process_renders.keys():
            self.kill_process(thread_render)
        
        log.info("ThreadsManager.kill_all_process-> " + _("terminate all process plugin threads"))
        for process in self.get_list_thread_pool():
            try:
                process.stop()
            except:
                log.info("ThreadsManager.kill_all_process-> " + _("Process inactive %s") % process)
            
    
    def execute_process(self, id_task):
        """
        Run process render execution
        
        :param id_task: identifiant of task render
        :type id_task: TaskPluginView
        """
        # Translate text
        _ = get_app()._tr

        log.info("ThreadsManager.run_process-> " + _("Execute process %s") % self.list_system_process_renders[id_task])
        self.list_system_process_renders[id_task].send_signal(signal.SIGCONT)

        
    def suspend_process(self, id_task):
        """
        Suspend process render execution
        
        :param id_task: identifiant of task render
        :type id_task: TaskPluginView
        """
        # Translate text
        _ = get_app()._tr

        log.info("ThreadsManager.suspend_process-> " + _("Suspend process %s") % self.list_system_process_renders[id_task])
        self.list_system_process_renders[id_task].send_signal(signal.SIGSTOP)

        
    def terminate_process(self, id_task):
        """
        Terminate process render execution
        
        :param id_task: identifiant of task render
        :type id_task: TaskPluginView
        """
        # Translate text
        _ = get_app()._tr

        log.info("ThreadsManager.terminate_process-> " + _("Terminate process %s") % self.list_system_process_renders[id_task])
        self.list_system_process_renders[id_task].terminate()

        
    def kill_process(self, id_task):
        """
        Kill process render execution
        
        :param id_task: identifiant of task render
        :type id_task: TaskPluginView
        """
        # Translate text
        _ = get_app()._tr

        log.info("ThreadsManager.kill_process-> " + _("Kill process %s") % self.list_system_process_renders[id_task])
        self.list_system_process_renders[id_task].kill()
    
    
    def adding_process_render_manager(self, id_task, id_process):
        """
        Adding subprocess Popen process render gesture
        
        :param id_task: identifiant of task render
        :type id_task: TaskPluginView
        :param id_process: system render process gesture
        :type id_process: subprocess.Popen
        """
        # Translate text
        _ = get_app()._tr
        
        self.list_system_process_renders.update({id_task: id_process})
    
    
    def close(self):
        """Close trhead manager"""
         # Translate text
        _ = get_app()._tr
        
        self.deleteLater()


    def __del__(self):
        """Delete method object"""
         # Translate text
        _ = get_app()._tr

        log.info("ThreadsManager-> " + _("Del Animated 3D Title thread manager"))




class WorkerSignals(QObject):
    """
    .. image:: classes/WorkerSignals.png
        :alt: Diagram
        :align: center
        :width: 500px

    Defines the signals available from a running worker thread.

    :ivar call_operator: Send object of operator function
    :vartype call_operator: pyqtSignal
    :ivar process_progress: Send object of progress function
    :vartype process_progress: pyqtSignal
    :ivar manage_progress: Send progress to function
    :vartype manage_progress: pyqtSignal
    :ivar manage_errors: Send error to function
    :vartype manage_errors: pyqtSignal
    :ivar result: Data returned from processing, anything
    :vartype result: pyqtSignal
    :ivar finished: No data end of thread fuction
    :vartype finished: pyqtSignal
    :ivar error: (exctype, value, traceback.format_exc())
    :vartype error: pyqtSignal
    :ivar process_thread: Send object of thread function
    :vartype process_thread: pyqtSignal
    :ivar terminate_process: Send object of end thread
    :vartype terminate_process: pyqtSignal

    """
    
    call_operator = pyqtSignal(object, object)
    process_progress = pyqtSignal(tuple)
    manage_process = pyqtSignal(object, object)
    manage_errors = pyqtSignal(object, str)
    result = pyqtSignal(object)
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    process_thread = pyqtSignal(object)
    terminate_process = pyqtSignal(object)




class Worker(QRunnable):
    """
    .. image:: classes/Worker.png
        :alt: Diagram
        :align: center
        :width: 500px

    Worker thread

    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

    :param callback: The function callback to run on this worker thread. Supplied args and
                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    """

    def __init__(self, fonction, *args, **kwargs):
        """Initialize Worker class"""
        super(Worker, self).__init__()

        # Store constructor arguments (re-used for processing)
        self.fn = fonction
        self._args = args
        self._kwargs = kwargs
        self.signals = WorkerSignals()
        
        # Add the callback to our kwargs
        self._kwargs['call_operator'] = self.signals.call_operator
        self._kwargs['id_callback_function'] = self.signals.process_thread
        self._kwargs['terminate_function'] = self.signals.terminate_process
        self._kwargs['progress_function'] = self.signals.process_progress
        self._kwargs['manage_process_render_function'] = self.signals.manage_process
        self._kwargs['manage_errors_function'] = self.signals.manage_errors

    @pyqtSlot()
    def run(self):
        """ Initialise the runner function with passed args, kwargs. """

        # Retrieve args/kwargs here; and fire processing using them
        try:
            result = self.fn(*self._args, **self._kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)  # Return the result of the processing fonction
        finally:
            self.signals.finished.emit()  # Done
