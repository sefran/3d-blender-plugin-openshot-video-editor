"""
    The ``blender_worker`` module
    -----------------------------

    :Date: |date|
    :Revision: 1.0
    :Author: Franc SERRES <sefran007@gmail.com> from preview work of Jonathan Thomas <jonathan@openshot.org>
    :Description: Module Controler for OpenShot Animated 3D Title plugins
    :Info: See <https://framagit.org/sefran/3d-blender-plugin-openshot-video-editor> for more recent info.

    Use it to import blender render worker features.
    
    :class:`QBlenderEvent`
    
    :class:`BlenderRequest`
    
    :func: `get_blender_render`

    :func: `filter_blender_version`

"""

import sys
import platform
import re
import subprocess

from PyQt5.QtCore import QObject, QEvent, QThread, QFileInfo

from classes import info, settings
from classes.logger import log
from classes.app import get_app




class QBlenderEvent(QEvent):
    """ Class Event, a custom Blender which can safely be sent from the Blender thread to the Qt thread (to communicate) """
    def __init__(self, id, data=None, *args):
        """ Initialize QBlenderEvent class """
        # Invoke parent init
        QEvent.__init__(self, id)
        self.data = data
        self.id = id




class BlenderRequest(QObject):
    """
    .. image:: classes/BlenderRequest.png
        :alt: Diagram
        :align: center
        :width: 500px

    BlenderRequest

    Inherits from QObject for request blender command
    
    """
    def __init__(self, *args, **kwargs):
        """Initialise BlenderRequest class"""
        super(BlenderRequest, self).__init__()

        # Store constructor arguments (re-used for processing)result
        self._args = args
        self._kwargs = kwargs

        # set variables
        self.process = None

        # Get settings Openshot-qt Blender's render'
        bsetting = settings.get_settings()
        self.blender_exec_path = bsetting.get('blender_command')

        # Init commands
        self.command_blender = [self.blender_exec_path] + args[0].split(' ')


    def start_render(self):
        """ Start Blender command """
        # Translate text
        _ = get_app()._tr
        
        command = str(self.command_blender)[1:-1].replace('\'', '').replace(',', '')
        
        cmd = 'where' if platform.system() == 'Windows' else 'which'
        okcommand = subprocess.call([cmd, self.command_blender[0]])
        if okcommand == 0:
            startupinfo = None
            if sys.platform == 'win32':
                startupinfo = subprocess.STARTUPINFO()
                startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
            self.process = subprocess.Popen(self.command_blender, stdout=subprocess.PIPE, stderr=subprocess.PIPE, startupinfo=startupinfo)
            log.info("BlenderRequest.start-> " + _("Start Blender with pid %d and command '%s'") % (self.process.pid, command))
            return True
        else:
            log.info("BlenderRequest.start-> " + _("Command %s not found") % self.command_blender[0])
            return False


def get_blender_render(id_task, params_function, filter_function, call_operator, id_callback_function, terminate_function, progress_function, manage_process_render_function, manage_errors_function):
    """
    Get Blender version into threaded task

    :param id_task: identifiant of task render
    :type id_task: TaskPluginView
    :param params_function: identifiant of task render
    :type params_function: dict
    :param filter_function: Function to filter result call function traitment
    :type filter_function: function
    :param call_operator: Function to call for traitment
    :type call_operator: function
    :param id_callback_function: Function call for id process gesture
    :type id_callback_function: function
    :param terminate_function: Function call at the end of render
    :type terminate_function: function
    :param progress_fonction: Function to call for process progress gesture
    :type progress_fonction: function
    :param manage_process_render_function: Function to manage render process
    :type manage_process_render_function: function
    :param manage_errors_function: Function to manage errors
    :type manage_errors_function: function
    """
        # Translate text
    _ = get_app()._tr

    log.info("get_blender_render-> " + _("Get Blender render"))
    # Get id thread from execute
    thread_object = QThread.currentThread()

    # Send id thread to pool
    id_callback_function.emit(thread_object)

    # Create command Blender request version
    blender_request = BlenderRequest(params_function)

    # Start request Blender
    if blender_request.start_render():
        # Manage blender render
        manage_process_render_function.emit(id_task, blender_request.process)

        # Look info in Blender output
        while blender_request.process.poll() is None:
            param_object = blender_request.process.stdout.readline()
            #log.info("Animated3DTitleManager.get_blender_render-> " + _("Blender result line %s") % param_object)
            if param_object:
                # render_worker.py:ThreadsManager->call_operator=func_operator
                call_operator.emit(id_task, filter_function(param_object.splitlines()[0].decode("utf-8"), id_task, params_function))

        return (thread_object, _("Done."))
    else:
        manage_errors_function.emit(id_task, _("Command %s not found.") % settings.get_settings().get('blender_command'))
        return (thread_object, _("Operation Aborted."))




def filter_blender_version(lineresult, id_task, params_function):
    """
        filter version of Blender from lineresult

    :param lineresult: identifiant of task render
    :type lineresult: string
    :param id_task: identifiant of task render
    :type id_task: TaskPluginView
    :param params_function: identifiant of task render
    :type params_function: dict

    :retruns: string with blender version
    :rtype: string
    """

    # Translate text
    _ = get_app()._tr

    version_search = re.compile(r"Blender (.*?) ")
    version = version_search.findall(lineresult)
    if version:
        #log.info("filter_blender_version-> " + _("result blender version filter %s") % lineresult)
        return float(version[0])
    else:
        return None


def filter_blender_render(lineresult, id_task, params_function):
    """
    Filter render of Blender from lineresult

    :param lineresult: Blender output result line
    :type lineresult: string
    :param id_task: identifiant of task render plugin
    :type id_task: TaskPluginView
    :param params_function: identifiant of task render
    :type params_function: dict
    
    """
    # Translate text
    _ = get_app()._tr

    # Init regex expression used to determine Blender's render
    # get data frames
    blender_frame_expression = re.compile(r"^\w+:([0-9]*) ")
    blender_part_expression = re.compile(r"([0-9]*)-([0-9]*)$")
    blender_tracing_expression = re.compile(r"([0-9]*)\/([0-9]*)$")
    blender_saved_expression_picture = re.compile(r"" + QFileInfo(params_function.split(' ')[3]).path() + "/\w+.png")
    
    # Filter line
    result = {}
    if 'Blender quit' in lineresult:
        return {'BlenderQuit' : None }
    if 'Blender ' + str(id_task.animated_3d_title_manager.blender_version) in lineresult:
        return {'StartBlender' : lineresult}
    if 'Read blend' in lineresult:
        return {'Render' : lineresult}
    if 'Compositing' in lineresult:
        return {'Compositing' : lineresult}
    if "error" in lineresult:
        log.info("filter_blender_render-> " + _("Blender output with error: %s") % lineresult)
        result.update({'error' : lineresult})
    if "not found" in lineresult:
        log.info("filter_blender_render-> " + _("Blender output with missing something: %s") % lineresult)
        result.update({'missing' : lineresult})
    if '|' in lineresult:
        output_frame = blender_frame_expression.search(lineresult)
        if output_frame:
            values_frame = output_frame[0].strip().split(':')
            result = {'frame' : int(values_frame[1])}
            # Frame gesture
            if int(values_frame[1]) != id_task.animated_3d_title_manager.plugins_manager.current_frame:
                id_task.animated_3d_title_manager.plugins_manager.current_frame = int(values_frame[1])
                # Resets variables
                id_task.animated_3d_title_manager.plugins_manager.max_parts = None
                previewprogress = 0
                log.info("filter_blender_render-> " + _("Blender generate picture: %s") % id_task.animated_3d_title_manager.plugins_manager.current_frame)
            # Get render part of picture
            output_part = blender_part_expression.search(lineresult)
            if output_part:
                # Set progress result
                progressvalues = output_part[0].strip().split('-')
            else:
                # get tracing part picture
                tracing_part = blender_tracing_expression.search(lineresult)
                # Does it have path tracing generated ?
                if tracing_part:
                    # Set progress result
                    progressvalues = tracing_part[0].strip().split('/')
            # Does it have picture info generated ?
            if output_part or tracing_part:
                # Set part result
                current_part = int(progressvalues[0])
                if id_task.animated_3d_title_manager.plugins_manager.max_parts == None:
                    id_task.animated_3d_title_manager.plugins_manager.max_parts = int(progressvalues[1])
                if not current_part in id_task.animated_3d_title_manager.plugins_manager.countparts:
                    id_task.animated_3d_title_manager.plugins_manager.countparts.append(current_part)
                progress = {'part': len(id_task.animated_3d_title_manager.plugins_manager.countparts)}
                progress.update({'percent': int(len(id_task.animated_3d_title_manager.plugins_manager.countparts)*100/id_task.animated_3d_title_manager.plugins_manager.max_parts)})
                progress.update({'max_parts': int(progressvalues[1])})
                result.update({'progress': progress})
            return result
    
    file_render_saved = blender_saved_expression_picture.findall(lineresult)
    if file_render_saved:
        log.info("filter_blender_render-> "+ _("Image %s detected from Blender") % file_render_saved[0])
        return {'image': file_render_saved[0]}
    else:
            #log.info("filter_blender_render-> "+ "skip %s" % lineresult)
            return None
