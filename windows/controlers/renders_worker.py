"""
    The ``renders_worker`` module
    -----------------------------

    :Date: |date|
    :Revision: 1.0
    :Author: Franc SERRES <sefran007@gmail.com> from preview work of Jonathan Thomas <jonathan@openshot.org>
    :Description: Module Controler for OpenShot Animated 3D Title plugins
    :Info: See <https://framagit.org/sefran/3d-blender-plugin-openshot-video-editor> for more recent info.

    Use it to import 3D Title render animation worker features.
    
    :class:`ManageTaskViewPlugin`
    
    :class:`PluginsManager`
    
"""

import os
import shutil
import codecs
import re

from PyQt5.QtCore import QObject

from functools import partial

from classes import info
from classes.logger import log
from classes.app import get_app

from windows.controlers.blender_worker import *
from windows.models.renders_model import ConfigurationPluginModel, ParametersPluginModel
from windows.views.renders_config_widgets import CreateWidgetsParametersRenderPlugin



class PluginsManager(QObject):
    """
    .. image:: classes/PluginsManager.png
        :alt: Diagram
        :align: center
        :width: 500px

    This class create object management for plugins 3D animations.

    :param animated_3d_title_manager: Manager Animated 3D Title plugins
    :type animated_3d_title_manager: QObject
    :param thread_manager: Gesture of threaded renders
    :type thread_manager: ThreadsManager
    :ivar title3DWindow: Window of Animated 3D title plugins
    :vartype title3DWindow: RendersPluginsViews
    :ivar active_plugin: Set plugin activated
    :vartype active_plugin: TaskPluginView
    :ivar list_running_plugins: List of actives plugins
    :vartype list_running_plugins: list[TaskPluginView]
    :ivar current_configuration_plugin_model: Configuration of active plugin
    :vartype current_configuration_plugin_model: ConfigurationPluginModel
    :ivar current_parameters_plugin_model: Parameters of active plugin
    :vartype current_parameters_plugin_model: ParametersPluginModel
    :ivar current_frame: Frame selected for preview
    :vartype current_frame: int
    :ivar max_parts: Parts of render
    :vartype max_parts: int
    :ivar previewprogress: Value of last progress
    :vartype previewprogress: int
    
    """
    def __init__(self, animated_3d_title_manager, thread_manager):
        """Initialise PluginsManager class"""
        super(PluginsManager, self).__init__()

        # Translate text
        _ = get_app()._tr

        self.animated_3d_title_manager = animated_3d_title_manager
        self.edit_plugin = None
        self.list_running_plugins = {}
        self.current_configuration_plugin_model = None
        self.current_parameters_plugin_model = None
        self.current_frame = None
        self.max_parts = None
        self.countparts = None


    def createPlugin(self, name):
        """ 
        Create new render gesture plugin animation. This is connected with button 'New' from object view.TaskPluginView 
        
        :param name: Name of 3D Title plugin selected
        :type name:
        
        """
        # Translate text
        _ = get_app()._tr

        log.info('PluginsManager.createPlugin-> ' + _("Create task gesture plugin view for %s") % name.data())
        # Create new task control render plugin view and get identifiant
        id_task = self.animated_3d_title_manager.title3DWindow.addTaskManagerToPlugin(name)
        
        log.info('PluginsManager.createPlugin-> ' + _('Add task control render plugin object %s to list plugins controls') % id_task)
        # Add task control render plugin object to list plugins controls
        self.list_running_plugins.update({id_task: {}})

        log.info('PluginsManager.createPlugin-> ' + _("Create configuration gesture plugin"))
        # Get default config details from selected render plugin
        self.list_running_plugins[id_task].update({'configuration': ConfigurationPluginModel(name, self.animated_3d_title_manager.title3DWindow.list_plugins_model)})
        self.current_configuration_plugin_model = self.list_running_plugins[id_task]['configuration']

        log.info('PluginsManager.createPlugin-> ' + _("Create parameters gesture plugin"))
        # Get params for selected render plugin
        if 'BLENDER' in self.current_configuration_plugin_model.animation['render'].keys():
            self.list_running_plugins[id_task].update({'parameters': ParametersPluginModel(self.list_running_plugins[id_task]['configuration'], self.animated_3d_title_manager.blender_version)})
        
        self.activePlugin(id_task)
        

    def deletePlugin(self, id_task):
        """ 
        Delete render gesture plugin animation. This is connected with button 'Trash' from object view.TaskPluginView
        
        :param id_task: identifiant of task render plugin
        :type id_task: TaskPluginView
        
        """
        # Translate text
        _ = get_app()._tr

        log.info('PluginsManager.deletePlugin-> ' + _('Delete render plugin process %s') % id_task.name_task.data())

        # Delete widget from widget_status
        id_task.widget_parent.removeWidgetsFromConfigurationPluginView()

        # Remove render plugin temp folder
        self.remove_folder_plugin(id_task)
        
        # Delete from plugin list
        self.list_running_plugins.pop(id_task, None)
        
        # Delete from active plugin configuration
        self.delete_active_plugin_configuration(id_task)

        # Delete object
        del id_task


    def activePlugin(self, id_task):
        """ 
        Active configuration identifed by id_task render plugin animation. This is connected with button 'Configure' from object view.TaskPluginView
        
        :param id_task: identifiant of task render plugin
        :type id_task: TaskPluginView
        
        """
        # Translate text
        _ = get_app()._tr

        log.info('PluginsManager.activePlugin-> ' + _('Active render plugin process %s') % id_task)

        # Inactivate active plugin
        if self.edit_plugin:
            self.inactiveConfigurationPlugin(self.edit_plugin)

        self.edit_plugin = id_task
        
        # Active plugin configuration gesture
        if self.edit_plugin.state == 'render':
            self.edit_plugin.state_renderedit()
        else:
            if self.edit_plugin.state == 'waitconf':
                self.edit_plugin.state_edit()
            #if self.edit_plugin.state == 'renderedit':

            # Set current plugin configuration
            self.current_configuration_plugin_model = self.list_running_plugins[self.edit_plugin]['configuration']
            
            # Set current selected plugin from list plugins animation configuration
            self.animated_3d_title_manager.title3DWindow.selected_template = self.current_configuration_plugin_model.animation.get("service")

            # Set current plugin parameters
            log.info('PluginsManager.activePlugin-> ' + _("Create parameters gesture plugin"))
            self.current_parameters_plugin_model = self.list_running_plugins[self.edit_plugin]['parameters']

            # Create control configuration plugin from current render plugin selected
            log.info('PluginsManager.activePlugin-> ' + _("Create widgets gesture plugin"))
            self.animated_3d_title_manager.title3DWindow.createControlConfigurationPlugin(self.edit_plugin, self.list_running_plugins)



    def renderPlugin(self, id_task):
        """ 
        Render selected configuration of task plugin. This is connected with button 'Render' from object view.TaskPluginView
        
        :param id_task: identifiant of task render plugin
        :type id_task: TaskPluginView
        
        """
        # Translate text
        _ = get_app()._tr

        if self.animated_3d_title_manager.title3DWindow.preview_unprotected:
            # Protect render preview
            self.animated_3d_title_manager.title3DWindow.disable_interface_preview()
            
            log.info('PluginsManager.renderPlugin-> ' + _('Start render plugin process %s') % id_task)
            id_task.state_render()

            # Get render parameters
            renderconfig = self.list_running_plugins[id_task]['configuration'].animation.get("render")

            # Do render
            renderlist = list(renderconfig.keys())
            if len(renderlist) == 1:
                if renderlist[0] == 'BLENDER':
                    # Get service plugin parameters
                    blend_file_path = renderconfig['BLENDER']['service']['render_file_path']
                    source_script = renderconfig['BLENDER']['service']['script_file_path']
                    path_to_target_script = renderconfig['BLENDER']['service']['target_path']
                    target_script =  renderconfig['BLENDER']['service']['target_file_script_path']
                    
                    # Create target folder if needful
                    self.generateFolder(path_to_target_script)
                    
                    # Copy the .py script associated with this template to the Openshot 'blender' temp folder. This will allow
                    # OpenShot to inject the user-entered params into the Python script
                    shutil.copy(source_script, target_script)
                    
                    # Open new temp .py file, and inject the user parameters
                    self.inject_params(target_script, self.list_running_plugins[id_task]['configuration'].get_project_params(preview=False), self.list_running_plugins[id_task]['parameters'].getConfigurationParameters())
                    
                    # Get render animation
                    log.info('PluginsManager.renderPlugin-> ' + _("Generate render plugin"))
                    params_render = '-b ' + blend_file_path + ' -P ' + target_script + ' -a'
                    self.animated_3d_title_manager.thread_manager.execute_function(get_blender_render, id_task=id_task, params_function=params_render, filter_function=filter_blender_render, func_operator=self.get_render_plugin, manage_process_render_function=self.animated_3d_title_manager.thread_manager.adding_process_render_manager)
            else:
                print('********* Only one render gesture for now **********')

            # Unprotect render preview
            self.animated_3d_title_manager.title3DWindow.enable_interface_preview()


    def controlRenderPlugin(self, id_task):
        """
        Control execution of render plugin. This is connected with button 'Execute' from object view.TaskPluginView

        :param id_task: identifiant of task render plugin
        :type id_task: TaskPluginView
        :param tooltip: Text of button execute/pause render
        :type tooltip: str
        """
        # Translate text
        _ = get_app()._tr

        if id_task.btn_execute.toolTip() == _('Execute'):
            log.info('PluginsManager.controlRenderPlugin-> ' + _("Pause render"))
            id_task.state_pause_render()
            self.animated_3d_title_manager.thread_manager.suspend_process(id_task)
        elif id_task.btn_execute.toolTip() == _('Pause'):
            log.info('PluginsManager.controlRenderPlugin-> ' + _("Execute render"))
            id_task.state_execute_render()
            self.animated_3d_title_manager.thread_manager.execute_process(id_task)


    def inactiveConfigurationPlugin(self, id_task):
        """ 
        Inactive configuration identifed by id_task render plugin animation 

        :param id_task: identifiant of task render plugin
        :type id_task: TaskPluginView
        
        """
        # Translate text
        _ = get_app()._tr

        log.info('PluginsManager.inactiveConfigurationPlugin-> ' + _('Inactive render plugin process %s') % id_task)
        
        # Delete all configuration widgets plugin reference into configuration plugin
        
        self.deleteWidgetsFromParametersPlugin(self.list_running_plugins[id_task]['parameters'])

        # Remove models
        self.current_configuration_plugin_model = None
        self.current_parameters_plugin_model = None

        if hasattr(id_task, 'state'):
            if id_task.state == 'edit':
                id_task.state_waitconf()
            if id_task.state == 'renderedit':
                id_task.state_render()

        if self.edit_plugin == id_task:
            self.edit_plugin = None

        # Clear all widgets plugin from interface
        self.animated_3d_title_manager.title3DWindow.removeWidgetsFromConfigurationPluginView()


    def deleteWidgetsFromParametersPlugin(self, parameters):
        """ Delete widgets from configuration plugin to Configuration view """
        # Translate text
        _ = get_app()._tr

        log.info("PluginsManager.deleteWidgetsFromParametersPlugin-> Delete all widgets parameters plugin from configuration")
        
        # Remove widgets from parameters
        for widgetname in parameters.active_params.keys():
            #print('========== %s ==========' % parameters.active_params[widgetname])
            # Remove widgets from parameter
            if 'widget' in parameters.active_params[widgetname].keys():
                parameters.active_params[widgetname].pop('widget')
                parameters.active_params[widgetname].pop('label_widget')
            # Remove group widget if exist
            #print('++++++++++ %s ++++++++++' % parameters.active_params[widgetname])
            if parameters.active_params[widgetname]['group']:
                parameters.active_params[widgetname]['group']['widget'] = False
                if 'label_widget' in parameters.active_params[widgetname]['group'].keys():
                    parameters.active_params[widgetname]['group'].pop('label_widget')
            print('********** %s **********' % parameters.active_params[widgetname])
        print('////////// %s //////////' % parameters.groups_defines)
        

    def remove_folder_plugin(self, id_task):
        """Remove temp folder of render plugin"""
        # Translate text
        _ = get_app()._tr

        # Clear temp folder plugin
        path_plugin = self.list_running_plugins[id_task]['parameters'].getConfigurationParameters()['output_path']
        log.info("RendersPluginsViews.remove_folder_plugin-> " + _("Clear temp folder plugin: %s") % path_plugin)
        # Remove folder
        if os.path.exists(path_plugin):
            shutil.rmtree(path_plugin)


    def delete_active_plugin_configuration(self, id_task):
        """Remove active plugin parameters configuration from plugins manager"""
        # Translate text
        _ = get_app()._tr

        # Delete task from active plugin configuration
        if self.edit_plugin == id_task:
            self.edit_plugin = None
            # Remove models
            self.current_configuration_plugin_model = None
            self.current_parameters_plugin_model = None
            if self.list_running_plugins == {}:
                self.animated_3d_title_manager.title3DWindow.hideConfigPluginWidget()
            else:
                self.edit_plugin = self.list_running_plugins[list(self.list_running_plugins.keys())[0]]
                self.activePlugin(self.edit_plugin)


    def previewPlugin(self, id_task, frame):
        """ 
        Preview selected configuration of task plugin 
        
        :param id_task: identifiant of task render plugin
        :type id_task: TaskPluginView
        :param frame: Frame to preview
        :type frame: int
        
        """
        # Translate text
        _ = get_app()._tr

        if self.animated_3d_title_manager.title3DWindow.preview_unprotected:
            # Protect render preview
            self.animated_3d_title_manager.title3DWindow.disable_interface_preview()
            
            # Start render
            id_task.state_render()

            # Get render parameters
            renderconfig = self.list_running_plugins[id_task]['configuration'].animation.get("render")

            # Do preview
            renderlist = list(renderconfig.keys())
            if len(renderlist) == 1:
                if renderlist[0] == 'BLENDER':
                    # Get service plugin parameters
                    blend_file_path = renderconfig['BLENDER']['service']['render_file_path']
                    source_script = renderconfig['BLENDER']['service']['script_file_path']
                    path_to_target_script = renderconfig['BLENDER']['service']['target_path']
                    target_script =  renderconfig['BLENDER']['service']['target_file_script_path']
                    
                    # Create target folder if needful
                    self.generateFolder(path_to_target_script)
                    
                    # Copy the .py script associated with this template to the Openshot 'blender' temp folder. This will allow
                    # OpenShot to inject the user-entered params into the Python script
                    shutil.copy(source_script, target_script)
                    
                    # Open new temp .py file, and inject the user parameters
                    self.inject_params(target_script, self.list_running_plugins[id_task]['configuration'].get_project_params(preview=True), self.list_running_plugins[id_task]['parameters'].getConfigurationParameters(frame))
                    
                    # Get picture preview
                    log.info('PluginsManager.previewPlugin-> ' + _("Get preview picture"))
                    params_render = '-b ' + blend_file_path + ' -P ' + target_script + ' -a'
                    self.animated_3d_title_manager.thread_manager.execute_function(get_blender_render, id_task=id_task, params_function=params_render, filter_function=filter_blender_render, func_operator=self.get_picture_preview, manage_process_render_function=self.animated_3d_title_manager.thread_manager.adding_process_render_manager)
            else:
                print('********* Only one render gesture for now **********')

            # End render
            if self.edit_plugin == id_task: 
                id_task.state_edit()
            else:
                id_task.state_waitconf()
            
            # Unprotect render preview
            self.animated_3d_title_manager.title3DWindow.enable_interface_preview()


    def get_picture_preview(self, id_task, blenderout):
        """ 
        Get picture preview from blender render 
        
        :param id_task: identifiant of task render plugin
        :type id_task: TaskPluginView
        :param blenderout: Blender output picture path
        :type blenderout: string
        
        """
        # Translate text
        _ = get_app()._tr

        if not (blenderout is {} or blenderout is None):
            # Set start render blender
            if 'StartBlender' in blenderout.keys():
                self.animated_3d_title_manager.title3DWindow.setProgessRenderControl(id_task, style=self.animated_3d_title_manager.title3DWindow.style_render_config_progressbar, busy=True)
                id_task.state_renderedit()
                
            if 'Render' in blenderout.keys():
                self.animated_3d_title_manager.title3DWindow.setProgessRenderControl(id_task, style=self.animated_3d_title_manager.title3DWindow.style_render_scene_progressbar, busy=True)
                self.countparts = []
                
            if 'Compositing' in blenderout.keys():
                self.animated_3d_title_manager.title3DWindow.setProgessRenderControl(id_task, style=self.animated_3d_title_manager.title3DWindow.style_composing_scene_progressbar, busy=True)
                self.countparts = []
                
            # Set name
            if 'frame' in blenderout.keys() and id_task == self.edit_plugin:
                #log.info("PluginsManager.get_picture_preview-> " + "Set Frame %s" % blenderout['frame'])
                parameters_plugin = self.list_running_plugins[id_task]['parameters'].getConfigurationParameters()
                #parameters_plugin['current_frame'] = blenderout['frame']
                self.animated_3d_title_manager.title3DWindow.setLabelPreviewControl(blenderout['frame'])
                self.animated_3d_title_manager.title3DWindow.setCountPreviewControl(parameters_plugin['start_frame'], parameters_plugin['end_frame'])
            
            # Set state render
            if 'progress' in blenderout.keys():
                #log.info("PluginsManager.get_picture_preview-> " + "Set progress %s" % blenderout['progress'])
                #self.animated_3d_title_manager.
                self.animated_3d_title_manager.title3DWindow.setProgessRenderControl(id_task, activepart=blenderout['progress']['part'], maxparts=blenderout['progress']['max_parts'])
            
            # Get blender render image
            if 'image' in blenderout.keys():
                log.info("PluginsManager.get_picture_preview-> " + _("Set Blender image result %s") % blenderout['image'])
                if self.edit_plugin == id_task:
                    id_task.state_edit()
                else:
                    id_task.state_render()
                
                if id_task == self.edit_plugin:
                    self.animated_3d_title_manager.title3DWindow.setPreviewPicture(blenderout['image'])
            
            # End of Blender
            if 'BlenderQuit' in blenderout.keys():
                log.info("PluginsManager.get_picture_preview-> " + _("Blender end render"))
                if self.edit_plugin == id_task:
                    id_task.state_edit()
                else:
                    id_task.state_render()


    def get_render_plugin(self, id_task, blenderout):
        """ 
        Get render from blender render 
        
        :param id_task: identifiant of task render plugin
        :type id_task: TaskPluginView
        :param blenderout: Blender output picture path
        :type blenderout: string
        
        """
        # Translate text
        _ = get_app()._tr

        if not (blenderout is {} or blenderout is None):
            # Set start render blender
            if 'StartBlender' in blenderout.keys():
                log.info("PluginsManager.get_picture_preview-> " + "Start Blender render")
                self.animated_3d_title_manager.title3DWindow.setProgessRenderControl(id_task, style=self.animated_3d_title_manager.title3DWindow.style_render_config_progressbar, busy=True)
                id_task.state_renderedit()
                
            # Set name
            if 'frame' in blenderout.keys():
                #log.info("PluginsManager.get_picture_preview-> " + "Set Frame %s" % blenderout['frame'])
                parameters_plugin = self.list_running_plugins[id_task]['parameters'].getConfigurationParameters()
                value = (blenderout['frame']-parameters_plugin['start_frame'])/(parameters_plugin['end_frame']-parameters_plugin['start_frame']) * 100
                self.animated_3d_title_manager.title3DWindow.setProgessRenderControl(id_task, percent=value)
            
            # Get blender render image
            if 'image' in blenderout.keys():
                log.info("PluginsManager.get_picture_preview-> " + _("Set Blender image result %s") % blenderout['image'])
                if self.edit_plugin == id_task:
                    id_task.state_edit()
                else:
                    id_task.state_render()
                
                if id_task == self.edit_plugin:
                    self.animated_3d_title_manager.title3DWindow.setPreviewPicture(blenderout['image'])

            # End of Blender
            if 'BlenderQuit' in blenderout.keys():
                log.info("PluginsManager.get_picture_preview-> " + _("Blender end render"))
                parameters_plugin = self.list_running_plugins[id_task]['parameters'].getConfigurationParameters()
                # Get video file
                video_file = [os.path.join(parameters_plugin['output_path'], f) for f in os.listdir(parameters_plugin['output_path']) if f.endswith('.mkv')][0]
                log.info("PluginsManager.get_render_plugin-> " + _("Get Blender render result %s") % video_file)
                self.animated_3d_title_manager.add_file(video_file)
                # End render
                # Delete widget from widget_status
                id_task.widget_parent.removeWidgetsFromConfigurationPluginView()

                # Delete from plugin list task render
                self.list_running_plugins.pop(id_task, None)
                
                # Delete from active plugin configuration
                self.delete_active_plugin_configuration(id_task)

                # Delete task object
                del id_task


    def generateFolder(self, pathfolder):
        '''
        Create directory if not exist

        :param pathfolder: Create folder if no exist
        :type pathfolder: string
        
        '''
        # Translate text
        _ = get_app()._tr

        if not os.path.exists(pathfolder):
            os.mkdir(pathfolder)


    def inject_params(self, targetfilescriptpath, params_openshot, user_params_plugin):
        """ 
        Inject parameters of plugin into targetfilescript 
        
        :param targetfilescriptpath: path of python file script plugin to generate
        :type targetfilescriptpath: string
        :param params_openshot: OpenShot plugins parameters
        :type params_openshot: dict
        :param user_params_plugin: Plugin parameters
        :type user_params_plugin: dict
        
        """
        # Translate text
        _ = get_app()._tr

        # if a frame is passed in, we are in preview mode.
        #if frame == 0:
        #    frame = 1
        # Text to inject
        user_params = "\n#BEGIN INJECT OPENSHOT PARAMS\n"
        for k, v in params_openshot.items():
            if type(v) == int or type(v) == float or type(v) == list or type(v) == bool or type(v) == dict:
                user_params += "params['{}'] = {}\n".format(k, v)
            if type(v) == str:
                user_params += "params['{}'] = '{}'\n".format(k, v.replace("'", r"\'").replace("\\", "\\\\"))
        user_params += "#END INJECTING OPENSHOT PARAMS\n"
        user_params += "#BEGIN INJECTING CONFIG PLUGIN PARAMS\n"
        for k, v in user_params_plugin.items():
            if type(v) == int or type(v) == float or type(v) == list or type(v) == bool or type(v) == dict:
                user_params += "params['{}'] = {}\n".format(k, v)
            if type(v) == str:
                user_params += "params['{}'] = '{}'\n".format(k, v.replace("'", r"\'"))
        user_params += "#END INJECTING CONFIG PLUGIN PARAMS\n"
        
        # Open new temp .py file, and inject the user parameters
        with open(targetfilescriptpath, 'r') as f:
            script_body = f.read()

        # modify script variable
        script_body = script_body.replace("#INJECT_PARAMS_HERE", user_params)

        # Write update script
        with codecs.open(targetfilescriptpath, "w", encoding="UTF-8") as f:
            f.write(script_body)
            log.info('PluginsManager.inject_params-> ' + _('Write render parameters into %s') % targetfilescriptpath)
    
    
    def close(self):
        """Close plugin manager"""
         # Translate text
        _ = get_app()._tr
        
        log.info("PluginsManager.close-> " + _("Close plugins manager"))
        #self.deleteLater()

    
    def __del__(self):
        """Delete method object"""
         # Translate text
        _ = get_app()._tr

        log.info("PluginsManager-> " + _("Del Animated 3D Title plugins manager"))




class ManageTaskViewPlugin(QObject):
    """
    .. image:: classes/ManageTaskViewPlugin.png
        :alt: Diagram
        :align: center
        :width: 500px

    Widget parameters of a render plugin

    id_task : The selected plugin render identifiant
    
    list_running_plugins : List of running plugins
    
    widget_plugin_config
    
    title3DWindow
    
    """
    
    def __init__(self, id_task, list_running_plugins, widget_plugin_config, title3DWindow):
        """ Initialize class ManageTaskViewPlugin """
        # Initialize QObject
        super(ManageTaskViewPlugin, self).__init__()

        # Translate text
        _ = get_app()._tr

        self.title3DWindow = title3DWindow
        self.animated_3d_title_manager = self.title3DWindow.animated_3d_title_manager
        self.plugins_manager = self.animated_3d_title_manager.plugins_manager
        self.settingsContainer = self.title3DWindow.settingsContainer #QWidget
        
        # Set default render for Blender
        if "BLENDER" in list_running_plugins[id_task]['configuration'].animation['render'].keys():
            self.render_version = self.animated_3d_title_manager.blender_version
        else:
            self.render_version = None

        self.id_task = id_task
        
        self.list_running_plugins = list_running_plugins
        self.configuration_plugin = list_running_plugins[id_task]['configuration']
        self.parameters_plugin = list_running_plugins[id_task]['parameters']
        self.configurationparameters_plugin = self.parameters_plugin.getConfigurationParameters()
        
        self.widget_plugin_config = widget_plugin_config
        self._widgets_config = None


    def activeWidgets(self):
        """ Install widgets config plugin into config view  """
        # Translate text
        _ = get_app()._tr

        log.info("ManageTaskViewPlugin.activeWidgets-> " + _("Active widgets into settingsContainer scrollArea view"))
        # Set interface previews parameters
        self.title3DWindow.sliderPreview.setMinimum(int(self.configurationparameters_plugin['start_frame']))
        self.title3DWindow.sliderPreview.setMaximum(int(self.configurationparameters_plugin['end_frame']))
        
        # Set default frame preview
        self.title3DWindow.sliderPreview.setValue(int(self.configurationparameters_plugin['current_frame']))

        # Enable interface preview actions
        self.title3DWindow.btnRefresh.clicked.connect(partial(self.title3DWindow.btnRefresh_clicked))
        self.title3DWindow.sliderPreview.valueChanged.connect(partial(self.title3DWindow.sliderPreview_valueChanged))

        # active configuration interface
        self.title3DWindow.showConfigPluginWidget()

        print('***** D *****')
        self.plugins_manager.previewPlugin(self.id_task, self.title3DWindow.sliderPreview.value())


    def inactiveWidgets(self):
        """ Install widgets config plugin into config view  """
        # Translate text
        _ = get_app()._tr

        log.info("ManageTaskViewPlugin.inactiveWidgets-> " + _("Inactive widgets into settingsContainer scrollArea view"))
        # Disable inteface preview actions
        self.title3DWindow.btnRefresh.clicked.disconnect()
        self.title3DWindow.sliderPreview.valueChanged.disconnect()


    def createWidgets(self):
        """ Install widgets config plugin into config view  """
        # Translate text
        _ = get_app()._tr

        #  Inactive interface actions (btnRefresh and sliderPreview)
        self.inactiveWidgets()
        
        log.info("ManageTaskViewPlugin.createWidgets-> " + _("Create widgets configuration plugin into settingsContainer scrollArea view"))
        # Create configurations widgets plugin
        CreateWidgetsParametersRenderPlugin(self.id_task, self.list_running_plugins, self.render_version, self.settingsContainer, self.title3DWindow)

        # Active interface
        print('***** A *****')
        self.activeWidgets()


    def deleteWidgets(self):
        """ Install widgets config plugin into config view  """
        # Translate text
        _ = get_app()._tr

        log.info("ManageTaskViewPlugin.deleteWidgets-> " + _("Delete widgets from settingsContainer scrollArea view"))
        self.inactiveWidgets()
