"""
    The ``animated_title`` module
    -----------------------------

    .. |date| date::

    :Date: |date|
    :Revision: 1.0
    :Author: Franc SERRES <sefran007@gmail.com> from preview work of Jonathan Thomas <jonathan@openshot.org>
    :Description: Module Manager for OpenShot Animated 3D Title plugins
    :Info: See <https://framagit.org/sefran/3d-blender-plugin-openshot-video-editor> for more recent info.

    Use it to import 3D Title render animation manager features.

    :class:`Animated3DTitleManager`
"""
import sys
import os
import time
import uuid
import threading
import subprocess
import math
import traceback

from functools import partial
from PyQt5.QtCore import *
from PyQt5.QtCore import QObject, QThread, pyqtSlot #, QTimer
from PyQt5.QtGui import QIcon, QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import *
from PyQt5 import uic
import openshot  # Python module for libopenshot (required video editing module installed separately)

from classes import info, ui_util, settings, qt_types, updates
from classes.app import get_app
from classes.logger import log
from classes.query import File
from classes.metrics import *

from windows.controlers.threads_worker import ThreadsManager
from windows.controlers.renders_worker import PluginsManager
from windows.controlers.blender_worker import *
from windows.views.renders_plugins_views import RendersPluginsViews

try:
    import json
except ImportError:
    import simplejson as json


class Animated3DTitleManager(QObject):
    """
    .. image:: classes/Animated3DTitleManager.png
        :alt: Diagram
        :align: center
        :width: 500px

    Create a manager for 3D Title render animation.

    :ivar thread_manager: Gesture of threaded renders
    :vartype thread_manager: ThreadsManager
    :ivar plugins_manager: Gesture of plugins
    :vartype plugins_manager: PluginsManager
    :ivar title3DWindow: Window of Animated 3D title plugins
    :vartype title3DWindow: RendersPluginsViews

    """


    def __init__(self, *args, **kwargs):
        """Initialize Animated3DTitleManager class"""
        super(Animated3DTitleManager, self).__init__(*args, **kwargs)

        # Translate text
        _ = get_app()._tr

        self.blender_version = None
        # Track metrics
        #track_metric_screen("animated-title-screen")

        log.info("Animated3DTitleManager-> " + _("Add threads manager"))
        # Add threads gesture to Blender plugins window
        self.thread_manager = ThreadsManager() # render_worker.py

        log.info("Animated3DTitleManager-> " + _("Add Blender Plugins manager"))
        # Add plugins manager
        self.plugins_manager = PluginsManager(self, self.thread_manager) # render_worker.py

        log.info("Animated3DTitleManager.set_blender_version-> " + _("Add Animated 3D titles window view"))
        self.title3DWindow = RendersPluginsViews(self) # renders_plugins_views.py
        
        # Get blender version
        log.info("Animated3DTitleManager-> " + _("Get Blender version"))
        self.thread_manager.execute_function(get_blender_render, id_task=None, params_function="-v", filter_function=filter_blender_version, func_operator=self.set_blender_version, manage_errors_function=self.error_set_version)
        

    def error_set_version(self, id_task, error):
        """
         Return errors of get version with `Animated3DTitleManager`.error_set_version.
         
         Show error dialog box

        :param id_task: identifiant of task render plugin
        :type id_task: TaskPluginView
        :param error: error message
        :type version: string
        """

        # Translate text
        _ = get_app()._tr

        log.info("Animated3DTitleManager.error_set_version-> " + _("Error: %s") % error)
        # Send Blender error message output
        self.error_with_blender(None, error)
        #self.close()


    def set_blender_version(self, id_task, version):
        """
         Set version of Blender with `Animated3DTitleManager`.set_blender_version

        :param id_task: identifiant of task render plugin
        :type id_task: TaskPluginView
        :param version: Version of Blender
        :type version: string

        :retruns: version into self.blender_version
        :rtype: string
        """

        # Translate text
        _ = get_app()._tr

        # Init regex expression used to determine Blender's version
        if not version is None:
            log.info("Animated3DTitleManager.set_blender_version-> " + _("Version render %s") % version)
            self.blender_version = version
            self.get_plugins()


    def get_plugins(self):
        """Get list of 3D plugins"""

        # Translate text
        _ = get_app()._tr
        
        log.info("Animated3DTitleManager.get_plugins-> " + _("Get list 3D Plugins plugins"))
        self.title3DWindow.get_3d_plugins_list()
        
    
    def show(self):
        """Show Animation 3D titles window"""

        # Translate text
        _ = get_app()._tr

        self.title3DWindow.show()
        log.info("Animated3DTitleManager.show-> " + _("Show Animated 3D Title window"))

    def close(self):
        """End of Animated3DTitleManager"""

        # Translate text
        _ = get_app()._tr

        log.info("Animated3DTitleManager.close-> " + _("Stop manage Animated 3D Title"))
        self.thread_manager.kill_all_process()
        #if hasattr(self.plugins_manager, 'close'):
        #    self.thread_manager.close()
        #    self.plugins_manager.close()
        #    self.deleteLater()
        
    def add_file(self, filepath):
        """ Add an animation to the project file tree """
        # Translation
        _ = get_app()._tr

        path, filename = os.path.split(filepath)

        # Add file into project
        # Check for this path in our existing project data
        file = File.get(path=filepath)

        # If this file is already found, exit
        if file:
            return

        # Get the JSON for the clip's internal reader
        try:
            # Open image sequence in FFmpegReader
            reader = openshot.FFmpegReader(filepath)
            reader.Open()

            # Serialize JSON for the reader
            file_data = json.loads(reader.Json())

            # Set media type
            file_data["media_type"] = "video"

            # Save new file to the project data
            file = File()
            file.data = file_data
            file.save()
            return True

        except:
            # Handle exception
            msg = QMessageBox()
            msg.setText(_("{} is not a valid video, audio, or image file.".format(filename)))
            msg.exec_()
            return False


    def error_with_blender(self, version=None, command_output=None):
        """Show a friendly error message regarding the blender executable or version"""
        # Translation
        _ = get_app()._tr

        s = settings.get_settings()
        
        # Error message
        version_message = ""
        if version:
            version_message = _("\n\nVersion Detected:\n{}").format(version)
        
        if command_output:
            version_message = _("\n\nError Output:\n{}").format(command_output)
        
        # Handle exception
        msg = QMessageBox()
        msg.setText(_(
            "Blender, the free open source 3D content creation suite is required for this action (http://www.blender.org).\n\nPlease check the preferences in OpenShot and be sure the Blender executable is correct.  This setting should be the path of the 'blender' executable on your computer.  Also, please be sure that it is pointing to Blender version {} or greater.\n\nBlender Path:\n{}{}").format(
            version, s.get("blender_command"), version_message))
        msg.exec_()
