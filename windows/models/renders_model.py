"""
    The ``renders_model`` module
    ----------------------------

    :Date: |date|
    :Revision: 1.0
    :Author: Franc SERRES <sefran007@gmail.com> from preview work of Jonathan Thomas <jonathan@openshot.org>
    :Description: Module Datas Models for OpenShot Animated 3D Title plugins
    :Info: See <https://framagit.org/sefran/range-progress-bar> for more recent info.

    Use it to import 3D Title render animation models features.
    
    :class:`ConfigurationPluginModel`
    
    :class:`ListPluginsModel`
    
    :class:`ParametersPluginModel`
    
"""
import os
import uuid
import xml.dom.minidom as xml
import json

from PyQt5.QtCore import QObject, Qt
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QIcon, QColor
from PyQt5.QtWidgets import QMessageBox

import openshot  # Python module for libopenshot (required video editing module installed separately)

from classes import info
from classes.logger import log
from classes.app import get_app



class ListPluginsModel():
    """
    .. image:: classes/ListPluginsModel.png
        :alt: Diagram
        :align: center
        :width: 500px

    This class create a model object list of render plugins for animations 3D titles window.

    :param model: model treeview.
    :type model: QStandardItemModel
    :param model_paths: paths to files render plugins configuration items.
    :type model_paths: dict
    """
    def __init__(self, blender_version):
        """ Initialize variables """
        # Create standard model
        self.app = get_app()
        self.blender_version = blender_version
        self.model = QStandardItemModel()
        self.model.setColumnCount(4)
        self.model_paths = {}

        # Translate text
        _ = get_app()._tr

        log.info("ListPluginsModel-> " + _("Object created."))


    def update_model(self, clear=True):
        '''
        Upgrade list of render plugins
        '''
        # Translate text
        _ = get_app()._tr

        log.info("ListPluginsModel.update_model-> " + _("Load effects model render plugin list."))

        # Clear all items
        if clear:
            self.model_paths = {}
            self.model.clear()

        # Add Headers
        self.model.setHorizontalHeaderLabels([_("Thumb"), _("Name")])

        # get a list of files in the OpenShot /effects directory
        effects_dir = os.path.join(info.PATH, "blender")
        icons_dir = os.path.join(effects_dir, "icons")

        for file in os.listdir(effects_dir):
            if os.path.isfile(os.path.join(effects_dir, file)) and ".xml" in file:
                # Split path
                path = os.path.join(effects_dir, file)
                (fileBaseName, fileExtension) = os.path.splitext(path)

                # load xml effect file
                xmldoc = xml.parse(path)

                # Get all xml attributes title, description, icon, category, service, render and plugintype
                #Get xml attributes title
                title = xmldoc.getElementsByTagName("title")[0].childNodes[0].data
                
                #Get xml attributes description
                description = xmldoc.getElementsByTagName("description")[0].childNodes[0].data
                
                #Get xml attributes icon
                icon_name = xmldoc.getElementsByTagName("icon")[0].childNodes[0].data
                icon_path = os.path.join(icons_dir, icon_name)
                
                #Get xml attributes category
                category = xmldoc.getElementsByTagName("category")[0].childNodes[0].data
                
                #Get xml attributes plugintype
                plugintype = xmldoc.getElementsByTagName("plugintype")[0].childNodes[0].data
                
                # Get xml render features plugin
                xml_render =  xmldoc.getElementsByTagName("render")
                for render_xml in xml_render:
                    # Get name of render
                    render_name = render_xml.attributes['name'].value.upper()

                    # Get plugin render parameters
                    if render_xml.getElementsByTagName("sup"):
                        render_version_sup = render_xml.getElementsByTagName("sup")[0].childNodes[0].data
                    else:
                        render_version_sup = None

                    if render_xml.getElementsByTagName("min"):
                        render_version_min = render_xml.getElementsByTagName("min")[0].childNodes[0].data
                    else:
                        render_version_min = None

                    if render_xml.getElementsByTagName("max"):
                        render_version_max = render_xml.getElementsByTagName("max")[0].childNodes[0].data
                    else:
                        render_version_max = None

                    if render_xml.getElementsByTagName("inf"):
                        render_version_inf = render_xml.getElementsByTagName("inf")[0].childNodes[0].data
                    else:
                        render_version_inf = None
                    #Get engine if exist
                    xmlengines = render_xml.getElementsByTagName("engine")
                    engine = []
                    if xmlengines:
                        for xmlengine in xmlengines:
                            engine.append(xmlengine.childNodes[0].data.upper())

                    #Get xml attributes service
                    render_file = render_xml.getElementsByTagName("service")[0].childNodes[0].data
                    service = {"render_file_name" : render_file}

                addplugin = False
                if render_name == 'BLENDER':
                    addplugin = True    
                    if not render_version_inf is None:
                        if self.blender_version >= float(render_version_inf):
                            addplugin = False
                            log.info("ListPluginsModel.update_model-> " + _("Remove plugin %s from list because plugin need Blender version < %s.") % (title, render_version_inf))
                    if not render_version_max is None:
                        if self.blender_version > float(render_version_max):
                            addplugin = False
                            log.info("ListPluginsModel.update_model-> " + _("Remove plugin %s from list because plugin need maximum Blender version %s.") % (title, render_version_max))
                    if not render_version_sup is None:
                        if self.blender_version <= float(render_version_sup):
                            addplugin = False
                            log.info("ListPluginsModel.update_model-> " + _("Remove plugin %s from list because plugin need Blender version > %s.") % (title, render_version_sup))
                    if not render_version_min is None:
                        if self.blender_version < float(render_version_min):
                            addplugin = False
                            log.info("ListPluginsModel.update_model-> " + _("Remove plugin %s from list because plugin need minimum Blender version %s.") % (title, render_version_min))
                
                    # Parameter render and service to plugin
                    if addplugin:
                        services_path = os.path.join(info.PATH, 'blender')
                        service.update({'services_path': services_path})
                        render_path = os.path.join(services_path, "blend")
                        service.update({'render_path': render_path})
                        render_file_path = os.path.join(render_path, render_file)
                        service.update({'render_file_path': render_file_path})
                        scripts_path = os.path.join(services_path, "scripts")
                        service.update({'script_path': scripts_path})
                        script_file = render_file.replace('.blend', '.py')
                        script_file_path = os.path.join(scripts_path, script_file)
                        service.update({'script_file_path': script_file_path})
                        unique_folder_name = str(uuid.uuid1())
                        target_path = os.path.join(info.BLENDER_PATH, unique_folder_name)
                        service.update({'target_path': target_path})
                        target_file_script_path =  os.path.join(target_path, script_file)
                        service.update({'target_file_script_path': target_file_script_path})
                        render = {render_name: {"sup": render_version_sup, "min": render_version_min, "max": render_version_max, "inf": render_version_inf, "engine": engine, "service": service}}
                        # End get all xml attributes title, description, icon, category, plugintype and render 

                if addplugin:
                    # Generate thumbnail for file (if needed)
                    thumb_path = os.path.join(info.CACHE_PATH, icon_name)

                    # Check if thumb exists
                    if not os.path.exists(thumb_path):

                        try:
                            # Reload this reader
                            clip = openshot.Clip(icon_path)
                            reader = clip.Reader()

                            # Open reader
                            reader.Open()

                            # Determine scale of thumbnail
                            scale = 95.0 / reader.info.width

                            # Save thumbnail
                            reader.GetFrame(0).Save(thumb_path, scale)
                            reader.Close()

                        except:
                            # Handle exception
                            msg = QMessageBox()
                            msg.setText(_("{} is not a valid image file.".format(icon_path)))
                            msg.exec_()
                            continue
                    
                    # Create row width colons thumbnail, name, path, service, render, plugin type
                    row = []

                    # Append thumbnail
                    col = QStandardItem()
                    col.setIcon(QIcon(thumb_path))
                    col.setText(_(title))
                    col.setToolTip(_(title))
                    col.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsUserCheckable)
                    row.append(col)

                    # Append Name
                    col = QStandardItem("Name")
                    col.setData(_(title), Qt.DisplayRole)
                    col.setText(_(title))
                    col.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsUserCheckable)
                    row.append(col)

                    # Append Path
                    col = QStandardItem("Path")
                    col.setData(path, Qt.DisplayRole)
                    col.setText(path)
                    col.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsUserCheckable)
                    row.append(col)

                    # Append Render
                    col = QStandardItem("Render")
                    col.setData(render, Qt.DisplayRole)
                    col.setText(json.dumps(render))
                    col.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsUserCheckable)
                    row.append(col)

                    # Append Plugin type
                    col = QStandardItem("PluginType")
                    col.setData(plugintype, Qt.DisplayRole)
                    col.setText(json.dumps(plugintype))
                    col.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsUserCheckable)
                    row.append(col)

                    # Append ROW to MODEL (if does not already exist in model)
                    if not path in self.model_paths:
                        self.model.appendRow(row)
                        self.model_paths[path] = path

                    # Process events in QT (to keep the interface responsive)
                    self.app.processEvents()




class ConfigurationPluginModel(QObject):
    """
    .. image:: classes/ConfigurationPluginModel.png
        :alt: Diagram
        :align: center
        :width: 500px

    Model object of configuration animation settings and properties from a file xml render plugin

    Parameters:

        QModelIndex item form list of render plugins

        ListPluginsModel data list of render plugins

    properties:
        dict ConfigurationPluginModel.animation :

        dict ConfigurationPluginModel.openshot_params :
    """
    def __init__(self, *args, **kwargs):
        """ Initialize class ConfigurationPluginModel """
        super(ConfigurationPluginModel, self).__init__()

        # Store constructor arguments (re-used for processing)
        self._args = args
        self._kwargs = kwargs

        # Get render plugin model object
        self.plugin = self._args[0]
        self.bmodel = self._args[1]

        # Init properties
        self.animation = None
        self.openshot_params = None
        self.default_plugin_param = None
        self.itemrow = None
        self.animation_title = None
        self.xml_path = None
        self.service = None
        self.class_plugin = None
        self.unique_folder_name = None

        # Translate text
        _ = get_app()._tr

        # Get text plugin self.plugin.data()
        if self.plugin.row() == -1:
            log.info("ConfigurationPluginModel-> " + _("None render plugin selected"))
        else:
            # Get text plugin
            log.info("ConfigurationPluginModel-> " + _("Get config render plugin \"%s\"") % self.plugin.data())
            # Get all selected rows items
            self.itemrow = self.bmodel.model.itemFromIndex(self.plugin).row()
            self.animation_title = self.bmodel.model.item(self.itemrow, 1).text()
            self.xml_path = self.bmodel.model.item(self.itemrow, 2).text()
            self.render = eval(self.bmodel.model.item(self.itemrow, 3).text().replace(": null,", ": None,"))
            self.plugintype = self.bmodel.model.item(self.itemrow, 4).text()

            # Get list of system params
            self.animation = {"title": self.animation_title, "path": self.xml_path, "render": self.render, "plugintype": self.plugintype, "params": []}
            self.get_defaults_plugin_params()

            # Get list of default openshot render params
            self.get_openshot_params()

            #log.info("ConfigurationPluginModel-> " + _('Parameters plugins : %s') % self.animation)


    def get_xml_default_params(self, file_path):
        """ 
        Read comfig xml file render plugin and store datas into ConfigurationPluginModel.animation["params"] and groups into ConfigurationPluginModel.animation["groups"]
        
        The plugin file must have this structure :
        
        .. code-block:: XML

            <?xml version="1.0" encoding="UTF-8" standalone="no"?>
            <!DOCTYPE openshot-effect>

            <effect>
                <title translatable="True">User text plugin</title>
                <description translatable="True">Tooltip text plugin</description>
                <icon>icon_plugin.png</icon>
                <category>Video</category>
                <plugintype>Type animation plugin</plugintype>
                <render name="blender">
                    <service>name_blender_file_plugin.blend</service>
                    <engine>blender</engine>
                    <engine>cycles</engine>
                    <engine>eevee</engine>
                    <max>2.79</max>
                </render>

                <groups>
                    <group name="group_1" title="Group 1" type="static"  description="Tooltip group 1"></group>
                    <group name="group_2" title="Group 2" type="off"  description="Tooltip group 2"></group>
                    <group name="group_3" title="Group 3" type="on"  description="Tooltip group 3"></group>
                </groups>

                <param name="widget_text" type="text" title="Text" description="Tooltip text">
                    <default>Default text value</default>
                </param>

                <param name="widget_multiline" type="multiline" title="multiline" description="Tooltip multiline">
                    <default>
                    Default value
                    Of my multiline text
                    </default>
                </param>

                <param name="widget_dropdown" type="dropdown" title="Text dropdown" description="Tooltip dropdown" group="group_1">
                    <values>
                        <value name="One" num="1"/>
                        <value name="Two" num="2"/>
                        <value name="Three" num="3"/>
                    </values>
                    <default>2</default>
                </param>

                <param name="widget_spinner" type="spinner" title="Text spinner" description="Tooltip spinner" data_type="float" group="group_1">
                    <min>0.0</min>
                    <max>10.0</max>
                    <step>0.1</step>
                    <default>0.5</default>
                </param>

                <param name="widget_boolean" type="boolean" title="Text boolean" description="Tooltip boolean" on_active_show="widget_colorr" on_active_hide="widget_font" on_inactive_show="widget_font" on_inactive_hide="widget_color" group="group_2">
                    <default>off</default>
                </param>

                <param name="widget_color" type="color" title="Text Color" description="Tooltip color" group="group_2">
                    <default>#B20000</default>
                </param>

                <param name="widget_font" type="font" title="Text font" description="Tooltip font" group="group_2">
                    <values>
                        <value name="Leander" num=":fonts/Leander.ttf"/>
                    </values>
                    <default>Leander.ttf</default>
                </param>

                <param name="widget_file" type="file" title="Text file" description="Tooltip file" group="group_3">
                    <values>
                        <value name="File 1" num=":blend/myfile.blend"/>
                    </values>
                    <default>myfile.blend</default>
                </param>

                <param name="widget_picture" type="picture" title="Text picture" description="Tooltip picture" group="group_3">
                    <values>
                        <value name="Picture 1" num=":pictures/mypict.png"/>
                    </values>
                    <default>mypict.png</default>
                </param>

                <param name="widget_sound" type="sound" title="Text sound" description="Tooltip sound" group="group_3">
                    <values>
                        <value name="Picture 1" num=":sound/mysound.ogg"/>
                    </values>
                    <default>mysound.ogg</default>
                </param>

                <param name="widget_movie" type="movie" title="Text movie" description="Tooltip movie" group="group_3">
                    <values>
                        <value name="Picture 1" num=":videos/myvideo.avi"/>
                    </values>
                    <default>myvideo.avi</default>
                </param>

                <param name="widget_range_frames" type="rangeframes" title="Text range frames" description="Tooltip range frames" group="group_3">
                    <min>1</min>
                    <max>200</max>
                    <start>50</start>
                    <end>150</end>
                    <step>1</step>
                    <steps>
                        <start name="0" num="0"/>
                        <start name="50" num="50"/>
                        <start name="80" num="80"/>
                        <start name="150" num="130"/>
                        <end name="80" num="80"/>
                        <end name="150" num="150"/>
                        <end name="180" num="180"/>
                        <end name="200" num="200"/>
                    </steps>
                </param>

                <param name="widget_blender_engines" type="blender_engines" title="Text blender_engines" description="Tooltip blender_engines" group="group_3">
                    <default>BLENDER</default>
                </param>

            </effect>

        """

        if file_path:
            # load xml effect file
            xmldoc = xml.parse(file_path)

            # Get groups plugin
            xml_groups =  xmldoc.getElementsByTagName('groups')
            if xml_groups:
                self.animation.update({'groups': {}})
                xml_group = xml_groups[0].getElementsByTagName('group')
                for group in xml_group:
                    group_items = {}

                    # Get details of group
                    # Get name of group
                    if group.attributes['name']:
                        group_name = group.attributes['name'].value

                        # Get label of group
                        if group.attributes['title']:
                            group_items['label'] = group.attributes['title'].value

                        # Get tooltip of group
                        if group.attributes['description']:
                            group_items['tooltip'] = group.attributes['description'].value

                        # Get type of group
                        if group.attributes['type']:
                            group_items['type'] = group.attributes['type'].value
                        
                        # Get on_active_show
                        if group.hasAttribute('on_active_show'):
                            if group.attributes['on_active_show']:
                                group_items['on_active_show'] = group.attributes['on_active_show'].value.split(",")

                        # Get on_active_hide
                        if group.hasAttribute('on_active_hide'):
                            if group.attributes['on_active_hide']:
                                group_items['on_active_hide'] = group.attributes['on_active_hide'].value.split(",")

                        # Get on_inactive_show
                        if group.hasAttribute('on_inactive_show'):
                            if group.attributes['on_inactive_show']:
                                group_items['on_inactive_show'] = group.attributes['on_inactive_show'].value.split(",")

                        # Get on_inactive_hide
                        if group.hasAttribute('on_inactive_hide'):
                            if group.attributes['on_inactive_hide']:
                                group_items['on_inactive_hide'] = group.attributes['on_inactive_hide'].value.split(",")

                        # Append group object to list of groups
                        self.animation['groups'].update({group_name : group_items})
                #log.info("ConfigurationPluginModel.get_xml_default_params-> " + _("xml_groups: %s" % str(self.animation['groups'])))

            # Get list of plugin params
            xml_params = xmldoc.getElementsByTagName('param')

            self.default_plugin_param = []
            # Loop through params
            for param in xml_params:
                param_items = {}

                # Get details of param
                # Get name of param
                if param.attributes['name']:
                    param_items['name'] = param.attributes['name'].value

                # Get label of param
                if param.attributes['title']:
                    param_items['title'] = param.attributes['title'].value

                # Get tooltip of param
                if param.attributes['description']:
                    param_items['description'] = param.attributes['description'].value

                # Get type of param
                if param.attributes['type']:
                    param_items['type'] = param.attributes['type'].value

                # Get visibility of param
                if 'visibility' in param.attributes:
                    param_items['visibility'] = param.attributes['visibility'].value

                # Get visibility widgets data
                if 'on_active_show' in param.attributes:
                    param_items['on_active_show'] = param.attributes['on_active_show'].value.split(",")

                # Get visibility widgets data
                if 'on_active_hide' in param.attributes:
                    param_items['on_active_hide'] = param.attributes['on_active_hide'].value.split(",")

                # Get visibility widgets data
                if 'on_inactive_show' in param.attributes:
                    param_items['on_inactive_show'] = param.attributes['on_inactive_show'].value.split(",")

                # Get visibility widgets data
                if 'on_inactive_hide' in param.attributes:
                    param_items['on_inactive_hide'] = param.attributes['on_inactive_hide'].value.split(",")

                # Get visibility widgets data on select type
                if 'on_select_show' in param.attributes:
                    visibilityvalues = {}
                    enginesattibutes = param.attributes['on_select_show'].value.split(";")
                    for engineattributes in enginesattibutes:
                        element = engineattributes.split(':')
                        cle = element[0].strip().upper()
                        data = element[1].strip().split(',')
                        visibilityvalues[cle] = data
                    param_items['on_select_show'] = visibilityvalues

                # Get visibility widgets data on select type
                if 'on_select_hide' in param.attributes:
                    visibilityvalues = {}
                    enginesattibutes = param.attributes['on_select_hide'].value.split(";")
                    for engineattributes in enginesattibutes:
                        element = engineattributes.split(':')
                        cle = element[0].strip().upper()
                        data = element[1].strip().split(',')
                        visibilityvalues[cle] = data
                    param_items['on_select_hide'] = visibilityvalues

                # Get data type of param
                if 'data_type' in param.attributes:
                    param_items['data_type'] = param.attributes['data_type'].value

                # Get group of param
                if 'group' in param.attributes:
                    param_items['group'] = param.attributes['group'].value

                # Get sort type of data
                if 'sort' in param.attributes:
                    param_items['sort'] = param.attributes['sort'].value

                # Get tags configurations properties
                if param.getElementsByTagName('min'):
                    param_items['min'] = param.getElementsByTagName('min')[0].childNodes[0].data

                if param.getElementsByTagName('max'):
                    param_items['max'] = param.getElementsByTagName('max')[0].childNodes[0].data

                if param.getElementsByTagName('start'):
                    param_items['start'] = param.getElementsByTagName('start')[0].childNodes[0].data

                if param.getElementsByTagName('end'):
                    param_items['end'] = param.getElementsByTagName('end')[0].childNodes[0].data

                if param.getElementsByTagName('step'):
                    param_items['step'] = param.getElementsByTagName('step')[0].childNodes[0].data

                if param.getElementsByTagName('steps'):
                    stepsstart_values = param.getElementsByTagName('steps')[0].getElementsByTagName('start')
                    param_items['stepsstart'] = {}
                    for start_value in stepsstart_values:
                        # Get list of values
                        name = ''
                        num = ''
                        if start_value.attributes['name']:
                            name = start_value.attributes['name'].value

                        if start_value.attributes['num']:
                            num = start_value.attributes['num'].value
                        # add to stepsstart
                        param_items['stepsstart'].update({name: num})
                    stepsend_values = param.getElementsByTagName('steps')[0].getElementsByTagName('end')
                    param_items['stepsend'] = {}
                    for end_value in stepsend_values:
                        # Get list of values
                        name = ''
                        num = ''
                        if end_value.attributes['name']:
                            name = end_value.attributes['name'].value

                        if end_value.attributes['num']:
                            num = end_value.attributes['num'].value
                        # add to stepsend
                        param_items['stepsend'].update({name: num})

                if param.getElementsByTagName('digits'):
                    param_items['digits'] = param.getElementsByTagName('digits')[0].childNodes[0].data

                if param.getElementsByTagName('default'):
                    if param.getElementsByTagName('default')[0].childNodes:
                        param_items['default'] = param.getElementsByTagName('default')[0].childNodes[0].data
                    else:
                        param_items['default'] = ''

                if param.getElementsByTagName('value'):
                    param_items['values'] = {}
                    values = param.getElementsByTagName('value')
                    for value in values:
                        # Get list of values
                        name = ''
                        num = ''

                        if value.attributes['name']:
                            name = value.attributes['name'].value

                        if value.attributes['num']:
                            num = value.attributes['num'].value

                        # add to parameter
                        param_items['values'][name] = num
                    
                # Append param object to animation
                self.animation['params'].append(param_items)

                # Append param object to default_plugin_param
                self.default_plugin_param.append(param_items)

            # Return animation parameters
            return self.default_plugin_param
            #log.info("ConfigurationPluginModel.get_xml_default_params-> " + _("parameters: %s" % self.default_plugin_param)))
        else:
            return None


    def get_openshot_params(self, is_preview=True):
        """ Assign a dictionary of project related settings, needed by the render python script. Return dict and store datas into ConfigurationPluginModel.openshot_params """
        # Translate text
        _ = get_app()._tr

        app = get_app()
        project = app.project
        # Init default Openshot blender params
        self.openshot_params = {}

        # Append on some project settings
        self.openshot_params['fps'] = project.get(['fps'])['num']
        self.openshot_params['resolution_x'] = project.get(['width'])
        self.openshot_params['resolution_y'] = project.get(['height'])

        if is_preview:
            # Set preview render defaults parameters
            self.openshot_params['animation'] = False
            if self.openshot_params['resolution_x'] < 1024:
                # Low resolution
                self.openshot_params['resolution_percentage'] = 50
            else:
                # Hight resolution
                self.openshot_params['file_format'] = 'PNG'
        else:
            # Set render defaults parameters
            self.openshot_params['animation'] = True
            self.openshot_params['resolution_percentage'] = 100
            self.openshot_params['file_format'] = 'FFMPEG'
            #openshot_params['quality'] = 100

        log.info(_("ConfigurationPluginModel.get_openshot_params-> %s") % self.openshot_params)
        # return the dictionary
        return self.openshot_params


    def get_defaults_plugin_params(self):
        """ 
        Get default parameters plugin 
        
        Return dict of values xml file.
        
        The plugin file must have this structure :
            
        .. code-block:: XML

            <?xml version="1.0" encoding="UTF-8" standalone="no"?>
            <!DOCTYPE openshot-effect>

            <effect>
                <title translatable="True">User text plugin</title>
                <description translatable="True">Tooltip text plugin</description>
                <icon>icon_plugin.png</icon>
                <category>Video</category>
                <plugintype>Type animation plugin</plugintype>
                <render name="blender">
                    <service>name_blender_file_plugin.blend</service>
                    <engine>blender</engine>
                    <engine>cycles</engine>
                    <engine>eevee</engine>
                    <max>2.79</max>
                </render>

                <groups>
                    <group name="group_1" title="Group 1" type="static"  description="Tooltip group 1"></group>
                    <group name="group_2" title="Group 2" type="off"  description="Tooltip group 2"></group>
                    <group name="group_3" title="Group 3" type="on"  description="Tooltip group 3"></group>
                </groups>

                <param name="widget_text" type="text" title="Text" description="Tooltip text">
                    <default>Default text value</default>
                </param>

                <param name="widget_multiline" type="multiline" title="multiline" description="Tooltip multiline">
                    <default>
                    Default value
                    Of my multiline text
                    </default>
                </param>

                <param name="widget_dropdown" type="dropdown" title="Text dropdown" description="Tooltip dropdown" group="group_1">
                    <values>
                        <value name="One" num="1"/>
                        <value name="Two" num="2"/>
                        <value name="Three" num="3"/>
                    </values>
                    <default>2</default>
                </param>

                <param name="widget_spinner" type="spinner" title="Text spinner" description="Tooltip spinner" data_type="float" group="group_1">
                    <min>0.0</min>
                    <max>10.0</max>
                    <step>0.1</step>
                    <default>0.5</default>
                </param>

                <param name="widget_boolean" type="boolean" title="Text boolean" description="Tooltip boolean" on_active_show="widget_colorr" on_active_hide="widget_font" on_inactive_show="widget_font" on_inactive_hide="widget_color" group="group_2">
                    <default>off</default>
                </param>

                <param name="widget_color" type="color" title="Text Color" description="Tooltip color" group="group_2">
                    <default>#B20000</default>
                </param>

                <param name="widget_font" type="font" title="Text font" description="Tooltip font" group="group_2">
                    <values>
                        <value name="Leander" num=":fonts/Leander.ttf"/>
                    </values>
                    <default>Leander.ttf</default>
                </param>

                <param name="widget_file" type="file" title="Text file" description="Tooltip file" group="group_3">
                    <values>
                        <value name="File 1" num=":blend/myfile.blend"/>
                    </values>
                    <default>myfile.blend</default>
                </param>

                <param name="widget_picture" type="picture" title="Text picture" description="Tooltip picture" group="group_3">
                    <values>
                        <value name="Picture 1" num=":pictures/mypict.png"/>
                    </values>
                    <default>mypict.png</default>
                </param>

                <param name="widget_sound" type="sound" title="Text sound" description="Tooltip sound" group="group_3">
                    <values>
                        <value name="Picture 1" num=":sound/mysound.ogg"/>
                    </values>
                    <default>mysound.ogg</default>
                </param>

                <param name="widget_movie" type="movie" title="Text movie" description="Tooltip movie" group="group_3">
                    <values>
                        <value name="Picture 1" num=":videos/myvideo.avi"/>
                    </values>
                    <default>myvideo.avi</default>
                </param>

                <param name="widget_range_frames" type="rangeframes" title="Text range frames" description="Tooltip range frames" group="group_3">
                    <min>1</min>
                    <max>200</max>
                    <start>50</start>
                    <end>150</end>
                    <step>1</step>
                    <steps>
                        <start name="0" num="0"/>
                        <start name="50" num="50"/>
                        <start name="80" num="80"/>
                        <start name="150" num="130"/>
                        <end name="80" num="80"/>
                        <end name="150" num="150"/>
                        <end name="180" num="180"/>
                        <end name="200" num="200"/>
                    </steps>
                </param>

                <param name="widget_blender_engines" type="blender_engines" title="Text blender_engines" description="Tooltip blender_engines" group="group_3">
                    <default>BLENDER</default>
                </param>

            </effect>

        """
        # Translate text
        _ = get_app()._tr

        return self.get_xml_default_params(self.xml_path)

    
    def get_project_params(self, preview):
        """ Return a dictionary of project related settings, needed by the render script """
        project_params = {}

        # Append on some project settings
        project_params["resolution_x"] = get_app().project.get(["width"])
        project_params["resolution_y"] = get_app().project.get(["height"])
        project_params["horizon_color"] = (0.57, 0.57, 0.57)

        if preview:
            project_params["animation"] = False
            project_params["quality"] = 50
            if project_params["resolution_x"] < 1024:
                project_params["resolution_percentage"] = 50
            else:
                project_params["resolution_percentage"] = 30
            project_params["file_format"] = "PNG"
            # preview mode - use offwhite background (i.e. horizon color)
            project_params["color_mode"] = "RGB"
            project_params["alpha_mode"] = "SKY"
        else:    
            project_params["animation"] = True
            project_params["fps"] = get_app().project.get(["fps"])['num']
            project_params["quality"] = 100
            project_params["resolution_percentage"] = 20 #100
            project_params["file_format"] = "FFMPEG"
            # render mode - transparent background
            project_params["color_mode"] = "RGB"
            project_params["alpha_mode"] = "TRANSPARENT"

        log.info('PluginsManager.get_project_params-> ' + 'OpenShot render parameters %s' % project_params)
        return project_params




class ParametersPluginModel(QObject):
    """
    .. image:: classes/ParametersPluginModel.png
        :alt: Diagram
        :align: center
        :width: 500px

    Model of parameters for a render plugin

     configuration_plugin : configuration of render plugin
     
     render_version : version of compatible render application

    """
    def __init__(self, configuration_plugin, render_version):
        """ Initialize class ParametersPluginModel """
        # Initialize QObject
        super(ParametersPluginModel, self).__init__()

        # Translate text
        _ = get_app()._tr

        # Init variables
        self.plugin_config = configuration_plugin
        self._render_version = render_version
        self.active_params = {}
        self.groups_config = None
        self.groups_defines = None
        # Add hide_on_value, show_on_value, hide_on_active, show_on_active ?

        log.info("ParametersPluginModel-> " + _("Generate Parameters from plugin %s") % self.plugin_config.animation['title'])

        # Assign groups of widget
        if 'groups' in self.plugin_config.animation:
            self.groups_config = self.plugin_config.animation.get('groups')
            # Create empty dict with key name of groups for get widget group objects
            self.groups_defines = dict.fromkeys(self.groups_config.keys(), None)

        # Loop through params
        for param in self.plugin_config.animation.get('params',[]):
            #define group
            if 'group' in param:
                #log.info("ParametersPluginModel-> " + _("Param option group: %s") % (param['group']))
                name_group = param['group']
                # Test if group define
                if name_group in self.groups_config:
                    # Name group
                    if not 'name' in self.groups_config[name_group] or self.groups_config[name_group]['name'] != name_group:
                        self.groups_config[name_group].update({ 'name': name_group })

                    # Label group
                    if not 'label' in self.groups_config[name_group]:
                        self.groups_config[name_group].update({ 'label': None })

                    # Tooltip group
                    if not 'tooltip' in self.groups_config[name_group]:
                        self.groups_config[name_group].update({ 'tooltip': None })

                    # Type or group
                    if 'type' in self.groups_config[name_group] and self.groups_config[name_group]['type'] in ('on', 'off', 'enabled', 'disabled', 'active', 'inactive', 'selectable'):
                        if self.groups_config[name_group]['type'] in ('off', 'disabled', 'inactive'):
                            self.groups_config[name_group]['type'] = 'selectable'
                            self.groups_config[name_group].update({ 'state': False })
                        elif self.groups_config[name_group]['type'] != 'selectable':
                            self.groups_config[name_group]['type'] = 'selectable'
                            self.groups_config[name_group].update({ 'state': True })
                    else:
                        self.groups_config[name_group].update({ 'type': 'static' })
                        self.groups_config[name_group].update({ 'state': None })

                    # Widget variable
                    self.groups_config[name_group].update({ 'widget': False })

                else:
                    # Add to group configuration
                    self.groups_config.update({ name_group : {} })
                    self.groups_config[name_group].update({ 'name': name_group, 'label': None, 'type': 'static', 'state': None, 'widget': False })
                group_param = self.groups_config[name_group]
            else:
                group_param = None

            # Is Hidden Param?
            if 'visibility' in param and param['visibility'] == 'hidden':
                create_widget = False
            else:
                create_widget = True

            if param['type'] == 'boolean':
                if 'default' in param:
                    # add value to dictionary
                    if param['default'].strip().lower() == 'true':
                        value = True
                    elif param['default'].strip().lower() == 'on':
                        value = True
                    elif param['default'].strip().lower() == 'false':
                        value = False
                    elif param['default'].strip().lower() == 'off':
                        value = False
                    else:
                        try:
                            value = bool(int(param['default']))
                        except Exception:
                            value = False
                else:
                    # Default value
                    value = True
                
                if 'on_active_show' in param:
                    active_show = param['on_active_show']
                else:
                    active_show = None
                if 'on_active_hide' in param:
                    active_hide = param['on_active_hide']
                else:
                    active_hide = None
                if 'on_inactive_show' in param:
                    inactive_show = param['on_inactive_show']
                else:
                    inactive_show = None
                if 'on_inactive_hide' in param:
                    inactive_hide = param['on_inactive_hide']
                else:
                    inactive_hide = None

                # add values to dictionary
                self.active_params[param['name']] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': value, 'create_widget': create_widget, 'type': 'boolean', 'group': group_param , 'on_active_show': active_show, 'on_active_hide': active_hide, 'on_inactive_show': inactive_show, 'on_inactive_hide': inactive_hide}
                #log.info("ParametersPluginModel-> " + _("Param boolean %s: %s") % (param['name'], self.active_params[param['name']]))

            elif param['type'] == 'text':
                value = ""
                if 'default' in param:
                    value = _(param['default'])
                else:
                    value = None
                #log.info("ParametersPluginModel-> " + _("Param text %s: %s") % (param['name'], value))

                # add values to dictionary
                self.active_params[param['name']] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': value, 'create_widget': create_widget, 'type': 'text', 'group': group_param  }

            elif param['type'] == 'multiline':
                value = ""
                if 'default' in param:
                    value = _(param['default'])
                else:
                    value = None
                #log.info("ParametersPluginModel-> " + _("Param multiline %s: %s") % (param['name'], value))

                # add values to dictionary
                self.active_params[param['name']] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': value, 'create_widget': create_widget, 'type': 'multiline', 'group': group_param  }

            elif param['type'] == 'rangeframes':
                try:
                    min_value = int(param['min'])
                    max_value = int(param['max'])
                    if 'start' in param:
                        start_value = int(param['start'])
                    else:
                        start_value = int(param['min'])
                    if 'end' in param:
                        end_value = int(param['end'])
                    else:
                        end_value = int(param['max'])
                    if 'step' in param:
                        step_value = int(param['step'])
                    else:
                        step_value = 1
                    if 'default' in param and param['default'].strip().isnumeric():
                        current_frame = int(param['default'])
                    else:
                        current_frame = int((int(param['end']) - int(param['start']))/2)
                except Exception as E:
                    create_widget = False
                    default_value = None
                    min_value = None
                    max_value = None
                    step_value = None
                    start_value = None
                    end_value = None
                    log.info("ParametersPluginModel-> " + _("Error: %s for tag frame from xml file plugin") % E)

                if 'stepsstart' in param or 'stepsend' in param:
                        # convert to integer list
                        stepsstart_values = {k:int(v) for k,v in param['stepsstart'].items()}
                        log.info("ParametersPluginModel-> " + _("Param frame_list %s: %s") % (param['name'], stepsstart_values))
                        min_steps_values = stepsstart_values
                        stepsend_values = {k:int(v) for k,v in param['stepsend'].items()}
                        log.info("ParametersPluginModel-> " + _("Param frame_list %s: %s") % (param['name'], stepsend_values))
                        max_steps_values = stepsend_values
                        # add values to dictionary
                        self.active_params[param['name']] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': current_frame, 'min': min_value, 'max': max_value, 'start': start_value, 'end': end_value, 'stepsstart': min_steps_values, 'stepsend': max_steps_values, 'create_widget': create_widget, 'type': 'rangeframes', 'group': group_param  }
                else:
                        # add values to dictionary
                        self.active_params[param["name"]] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': current_frame, 'min': min_value, 'max': max_value, 'start': start_value, 'end': end_value, 'step': step_value, 'create_widget': create_widget, 'type': 'rangeframes', 'group': group_param  }

            elif param['type'] == 'spinner':
                # Get data type variable
                if 'data_type' in param :
                    data_type = param['data_type']
                else:
                    data_type = None

                # Initialize spinner values
                default_value = None
                if data_type == 'float':
                    if 'default' in param:
                        default_value = float(param['default'])
                    min_value = float(param['min'])
                    max_value = float(param['max'])
                    if 'step' in param:
                        step_value = float(param['step'])
                    else:
                        step_value = 1.0
                elif data_type == 'int':
                    if 'default' in param:
                        default_value = int(param['default'])
                    min_value = int(param['min'])
                    max_value = int(param['max'])
                    if 'step' in param:
                        step_value = int(param['step'])
                    else:
                        step_value = 1
                else:
                    if 'default' in param:
                        default_value = param['default']
                    min_value = param['min']
                    max_value = param['max']
                    if 'step' in param:
                        step_value = int(param['step'])
                    else:
                        step_value = 1

                #log.info("ParametersPluginModel-> " + _("Param spinner %s: %s, min: %s, max: %s, step: %s, type %s") % (param['name'], default_value, min_value, max_value, step_value, data_type))
                # add values to dictionary
                self.active_params[param['name']] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': default_value, 'min': min_value, 'max': max_value, 'step': step_value, 'create_widget': create_widget, 'type': 'spinner', 'data_type': data_type, 'group': group_param  }

            elif param['type'] == 'dropdown':
                # Get data type variable
                if 'data_type' in param :
                    data_type = param['data_type']
                else:
                    data_type = None

                # Set sort option
                if 'sort' in param:
                    sort_value = param['sort']
                else:
                    sort_value = None

                # Define dropdown values
                if data_type == 'float':
                    if 'default' in param:
                        default_value = float(param['default'])
                    else:
                        value = None
                    # convert list values to floats
                    values = {k:float(v) for k,v in param['values'].items()}
                elif data_type == 'int':
                    if 'default' in param:
                        default_value = int(param['default'])
                    else:
                        value = None
                    # convert list values to ints
                    values = {k:int(v) for k,v in param['values'].items()}
                else:
                    data_type == 'string'
                    if 'default' in param:
                        default_value = param['default']
                    else:
                        value = None
                    values = param['values']

                #log.info("ParametersPluginModel-> " + _("Param dropdown %s: %s, default %s, type: %s, sort: %s") % (param['name'], values, default_value, data_type, sort_value))
                # add values to dictionary
                self.active_params[param['name']] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': default_value, 'values': values, 'sort': sort_value, 'create_widget': create_widget, 'type': 'dropdown', 'data_type': data_type, 'group': group_param  }

            elif param['type'] == 'color':
                if 'default' in param:
                    color = QColor(param['default'])
                else:
                    color = QColor('FFFFFF')
                value = {
                    'rgb': [color.redF(), color.greenF(), color.blueF()],
                    'rgba': [color.redF(), color.greenF(), color.blueF(), color.alphaF()],
                    'cmyk': [color.cyanF(), color.magentaF(), color.yellowF(), color.blackF()],
                    'hsv': [color.hsvHueF(), color.hsvSaturationF(), color.valueF()],
                    'hsl': [color.hslHueF(), color.hslSaturationF(), color.lightnessF()],
                    }

                #log.info("ParametersPluginModel-> " + _("Param color %s: %s") % (param['name'], value))
                # add values to dictionary
                self.active_params[param['name']] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': value, 'create_widget': create_widget, 'type': 'color', 'group': group_param  }

            elif param['type'] == 'blender_engines':
                # Get render parameters
                values = None
                if 'values' in param:
                    if param['values'] != {}:
                        values = param['values']
                if values is None:
                    if self._render_version < 2.8:
                        values = ('BLENDER', 'CYCLES')
                    else:
                        values = ('BLENDER', 'CYCLES', 'EEVEE')

                if 'default' in param:
                    param['default'] = param['default'].upper()
                    if param['default'] in ('BLENDER', 'CYCLES', 'EEVEE'):
                        if param['default'] == 'EEVEE' and self._render_version < 2.8:
                            log.info("ParametersPluginModel-> " + _("Blender version not compatible with default EEVEE choise"))
                            default_value = 'BLENDER'
                        else:
                            default_value = param['default']
                    else:
                        default_value = 'BLENDER'
                else:
                    default_value = 'BLENDER'

                #log.info("ParametersPluginModel-> " + _("Param render %s: %s, default: %s") % (param['name'], values, default_value))
                # add values to dictionary
                self.active_params[param['name']] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': default_value, 'values': values, 'create_widget': create_widget, 'type': 'blender_engines', 'group': group_param  }
                
                # add options visibility show on select engine
                if 'on_select_show' in param:
                    self.active_params[param['name']].update({'on_select_show': param['on_select_show']})

                # add options visibility hide on select engine
                if 'on_select_hide' in param:
                    self.active_params[param['name']].update({'on_select_hide': param['on_select_hide']})

            elif param['type'] == 'file':
                # set default file from config pluqin
                values, value = self.getRealsFiles(param)
                if not value and values != []:
                    value = values[0]

                #log.info("ParametersPluginModel-> " + _("Param file %s: %s default: %s") % (param['name'], values, value))
                # add values to dictionary
                self.active_params[param['name']] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': value, 'values': values, 'create_widget': create_widget, 'type': 'file', 'group': group_param  }

            elif param['type'] == 'font':
                # set default file from config pluqin
                values, value = self.getRealsFiles(param)
                if not value and values != []:
                    value = values[0]

                #log.info("ParametersPluginModel-> " + _("Param font %s: %s default: %s") % (param['name'], values, value))
                # add values to dictionary
                self.active_params[param['name']] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': value, 'values': values, 'create_widget': create_widget, 'type': 'font', 'group': group_param  }

            elif param['type'] == 'picture':
                # set default file from config pluqin
                values, value = self.getRealsFiles(param)
                if not value and values != []:
                    value = values[0]

                #log.info("ParametersPluginModel-> " + _("Param picture %s: %s default: %s") % (param['name'], values, value))
                # add values to dictionary
                self.active_params[param['name']] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': value, 'values': values, 'create_widget': create_widget, 'type': 'picture', 'group': group_param  }

            elif param['type'] == 'sound':
                # set default file from config pluqin
                values, value = self.getRealsFiles(param)
                if not value and values != []:
                    value = values[0]

                #log.info("ParametersPluginModel-> " + _("Param sound %s: %s default: %s") % (param['name'], values, value))
                # add values to dictionary
                self.active_params[param['name']] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': value, 'values': values, 'create_widget': create_widget, 'type': 'sound', 'group': group_param  }

            elif param['type'] == 'movie':
                # set default file from config pluqin
                values, value = self.getRealsFiles(param)
                if not value and values != []:
                    value = values[0]

                #log.info("ParametersPluginModel-> " + _("Param movie %s: %s default: %s") % (param['name'], values, value))
                # add values to dictionary
                self.active_params[param['name']] = { 'name': param['name'], 'label': param['title'], 'tooltip': param['description'], 'value': value, 'values': values, 'create_widget': create_widget, 'type': 'movie', 'group': group_param  }


    def getRealsFiles(self, parameters):
        """ Return list of reals files from blender plugin config """
        # Translate text
        _ = get_app()._tr

        #Test file definition into default parameter
        defaultfile = None
        if parameters['default']:
            log.info("ParametersPluginModel.getRealsFiles-> " + _("Try to get file %s from default set config") % parameters['default'])
            if parameters['default'][0] == ':':
                openshootpathblender = os.path.join(info.PATH, "blender")
                if parameters['default'][1] == os.path.sep:
                    parameters['default'] = os.path.join(openshootpathblender, parameters['default'][2:])
                else:
                    parameters['default'] = os.path.join(openshootpathblender, parameters['default'][1:])
            try:
                with open(os.path.expanduser(parameters['default'])): pass
            except IOError:
                log.info('Warning: ParametersPluginModel.getRealsFiles-> ' + _("Default File %s Not Found") % parameters['default'])
            else:
                parameters['default'] = os.path.expanduser(parameters['default'])
                defaultfile = parameters['default']
                log.info("ParametersPluginModel.getRealsFiles-> " + _("File loaded %s") % parameters['default'])
        #Test file definition into values parameter
        realfileslist = []
        if parameters['values'].items() and parameters['values'] != {'': ''}:
            log.info("ParametersPluginModel.getRealsFiles-> " + _("Try to get a file from values configured list %s") % str(parameters['values']))
            flag = True
            for font in parameters['values'].keys():
                openshootpathblender = os.path.join(info.PATH, "blender")
                if parameters['values'][font][0] == ':':
                    if parameters['values'][font][1] == os.path.sep:
                        choosefile = os.path.join(openshootpathblender, parameters['values'][font][2:])
                    else:
                        choosefile = os.path.join(openshootpathblender, parameters['values'][font][1:])
                else:
                    choosefile = os.path.join(openshootpathblender, parameters['values'][font][1:])
                try:
                    with open(os.path.expanduser(choosefile)): pass
                except IOError:
                    continue
                else:
                    if os.path.expanduser(choosefile) not in realfileslist:
                        choosefile = os.path.expanduser(choosefile)
                        realfileslist.append(choosefile)
                        flag = False
                        log.info("ParametersPluginModel.getRealsFiles-> " + _("File loaded %s") % choosefile)
                    else:
                        log.info("ParametersPluginModel.getRealsFiles-> " + _("Warning: file %s already in list") % choosefile)
                    #break
            if flag:
                log.info("ParametersPluginModel.getRealsFiles-> " + _("Warning: No Files found from configured list of files"))
        if realfileslist == []:
            log.info("ParametersPluginModel.getRealsFiles-> " + _("No File detected from config %s") % parameters)
        return realfileslist, defaultfile
    
    
    def getConfigurationParameters(self, current_frame=None):
        """ Get dict from active configuration parameters plugin """
        configuration_parameters = {}
        for k, v in self.active_params.items():
            if k != "range_frames":
                configuration_parameters[k] = v["value"]
            else:
                configuration_parameters["start_frame"] = v['start']
                configuration_parameters["end_frame"] = v['end']
                if "value" in configuration_parameters:
                    configuration_parameters["current_frame"] = v["value"]
                else:
                    configuration_parameters["current_frame"] = int((v['end'] - v['end'])/2)
                configuration_parameters["output_path"] = self.plugin_config.animation.get("render")['BLENDER']['service']['target_path'] + '/'
                configuration_parameters["render_version"] = self._render_version
            if not v["group"] is None:
                if not v["group"]["state"] is None:
                    if not v["group"]["name"] in configuration_parameters:
                        configuration_parameters[v["group"]["name"]] = v["group"]["state"]
        if not current_frame is None:
            configuration_parameters["current_frame"] = current_frame
        return configuration_parameters
