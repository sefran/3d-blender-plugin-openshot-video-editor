#!/bin/bash

cd ./source/classes

echo Generate Class AnimatedWindowManager
pyreverse -mn -A -S -f PUB_ONLY -o png -c Animated3DTitleManager ../../../windows/animated_title.py
echo Generate Class RendersPluginsViews
pyreverse -mn -A -S -f PUB_ONLY -o png -c RendersPluginsViews ../../../windows/views/renders_plugins_views.py 
echo Generate Class ManagerPanelPluginsView
pyreverse -mn -A -S -f PUB_ONLY -o png -c ManagerPanelPluginsView ../../../windows/views/renders_plugins_views.py 
echo Generate Class TaskManagerPluginView
pyreverse -mn -A -S -f PUB_ONLY -o png -c TaskManagerPluginView ../../../windows/views/renders_plugins_views.py 
echo Generate Class ListPluginsView
pyreverse -mn -A -S -f PUB_ONLY -o png -c ListPluginsView ../../../windows/views/renders_plugins_views.py 
#echo Generate Class ConfigView
#pyreverse -mn -A -S -f PUB_ONLY -o png -c ConfigView ../../../windows/views/renders_plugins_views.py 
echo Generate Class ThreadsManager
pyreverse -mn -A -S -f PUB_ONLY -o png -c ThreadsManager ../../../windows/controlers/threads_worker.py 
echo Generate Class WorkerSignals
pyreverse -mn -A -S -f PUB_ONLY -o png -c WorkerSignals ../../../windows/controlers/threads_worker.py 
echo Generate Class Worker
pyreverse -mn -A -S -f PUB_ONLY -o png -c Worker ../../../windows/controlers/threads_worker.py
echo Generate Class PluginsManager
pyreverse -mn -A -S -f PUB_ONLY -o png -c PluginsManager ../../../windows/controlers/renders_worker.py 
echo Generate Class ManageTaskViewPlugin
pyreverse -mn -A -S -f PUB_ONLY -o png -c ManageTaskViewPlugin ../../../windows/controlers/renders_worker.py 
echo Generate Class QBlenderEvent
pyreverse -mn -A -S -f PUB_ONLY -o png -c QBlenderEvent ../../../windows/controlers/blender_worker.py 
echo Generate Class BlenderRequest
pyreverse -mn -A -S -f PUB_ONLY -o png -c BlenderRequest ../../../windows/controlers/blender_worker.py 
echo Generate Class ListPluginsModel
pyreverse -mn -A -S -f PUB_ONLY -o png -c ListPluginsModel ../../../windows/models/renders_model.py 
echo Generate Class ParametersPluginModel
pyreverse -mn -A -S -f PUB_ONLY -o png -c ParametersPluginModel ../../../windows/models/renders_model.py 
echo Generate Class ConfigurationPluginModel
pyreverse -mn -A -S -f PUB_ONLY -o png -c ConfigurationPluginModel ../../../windows/models/renders_model.py 
echo Generate Class CreateWidgetsParametersRenderPlugin
pyreverse -mn -A -S -f PUB_ONLY -o png -c CreateWidgetsParametersRenderPlugin ../../../windows/views/renders_config_widgets.py 
echo Generate Class LabelWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c LabelWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class GroupWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c GroupWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class BooleanWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c BooleanWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class BooleanGroupWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c BooleanGroupWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class SpinnerIntWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c SpinnerIntWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class SpinnerDoubleWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c SpinnerDoubleWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class SpinnerWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c SpinnerWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class RangeFramesWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c RangeFramesWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class TextWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c TextWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class MultilineWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c MultilineWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class DropdownWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c DropdownWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class ColorWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c ColorWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class FileWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c FileWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class FontWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c FontWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class PictureWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c PictureWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class SoundWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c SoundWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class MovieWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c MovieWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class BlenderEnginesWidget
pyreverse -mn -A -S -f PUB_ONLY -o png -c BlenderEnginesWidget ../../../windows/views/renders_config_widgets.py 
echo Generate Class OpenFileConfig
pyreverse -mn -A -S -f PUB_ONLY -o png -c OpenFileConfig ../../../windows/views/renders_config_widgets.py 
echo Generate Class OpenFontFile
pyreverse -mn -A -S -f PUB_ONLY -o png -c OpenFontFile ../../../windows/views/renders_config_widgets.py 
echo Generate Class Op-f PUB_ONLY enSoundFile
pyreverse -mn -A -S -f PUB_ONLY -o png -c OpenSoundFile ../../../windows/views/renders_config_widgets.py 
echo Generate Class OpenMovieFile
pyreverse -mn -A -S -f PUB_ONLY -o png -c OpenMovieFile ../../../windows/views/renders_config_widgets.py 

cd ../../

 
