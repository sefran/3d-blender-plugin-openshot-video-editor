\selectlanguage *{english}
\contentsline {chapter}{\numberline {1}Minimal 3D Title Animation plugin}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}Blender plugin version gesture}{11}{chapter.2}%
\contentsline {chapter}{\numberline {3}Blender plugin engine gesture}{15}{chapter.3}%
\contentsline {chapter}{\numberline {4}Blender plugin frames gesture}{17}{chapter.4}%
\contentsline {chapter}{\numberline {5}Blender plugin texts features}{19}{chapter.5}%
\contentsline {chapter}{\numberline {6}Blender plugin booleans features}{21}{chapter.6}%
\contentsline {chapter}{\numberline {7}Blender plugin Widget dropdown}{23}{chapter.7}%
\contentsline {chapter}{\numberline {8}Blender plugin Widget spinner}{25}{chapter.8}%
\contentsline {chapter}{\numberline {9}Blender plugin Widget color}{27}{chapter.9}%
\contentsline {chapter}{\numberline {10}Blender plugin Widget file}{29}{chapter.10}%
\contentsline {chapter}{\numberline {11}Blender plugin Widget font}{31}{chapter.11}%
\contentsline {chapter}{\numberline {12}Blender plugin Widget picture}{33}{chapter.12}%
\contentsline {chapter}{\numberline {13}Blender plugin Widget sound}{35}{chapter.13}%
\contentsline {chapter}{\numberline {14}Blender plugin Widget movie}{37}{chapter.14}%
\contentsline {chapter}{\numberline {15}Python scripts gestures Animated 3D plugin}{39}{chapter.15}%
\contentsline {section}{\numberline {15.1}Debuggin features}{39}{section.15.1}%
\contentsline {section}{\numberline {15.2}Blender features}{41}{section.15.2}%
\contentsline {chapter}{\numberline {16}Modules OpenShot Animated 3D Title plugins}{43}{chapter.16}%
\contentsline {section}{\numberline {16.1}The \sphinxstyleliteralintitle {\sphinxupquote {animated\_title}} module}{43}{section.16.1}%
\contentsline {section}{\numberline {16.2}The \sphinxstyleliteralintitle {\sphinxupquote {renders\_worker}} module}{43}{section.16.2}%
\contentsline {section}{\numberline {16.3}The \sphinxstyleliteralintitle {\sphinxupquote {renders\_model}} module}{44}{section.16.3}%
\contentsline {section}{\numberline {16.4}The \sphinxstyleliteralintitle {\sphinxupquote {renders\_plugins\_views}} module}{44}{section.16.4}%
\contentsline {section}{\numberline {16.5}The \sphinxstyleliteralintitle {\sphinxupquote {renders\_config\_widgets}} module}{45}{section.16.5}%
\contentsline {chapter}{\numberline {17}Classes OpenShot Animated 3D Title plugins}{47}{chapter.17}%
\contentsline {section}{\numberline {17.1}Class Manager Animated 3D Title plugins}{47}{section.17.1}%
\contentsline {section}{\numberline {17.2}Classes Controlers Animated 3D Title plugins}{48}{section.17.2}%
\contentsline {section}{\numberline {17.3}Classes Datas Models 3D Animated Title plugins}{58}{section.17.3}%
\contentsline {section}{\numberline {17.4}Classes Views Window Animated 3D Title plugins}{66}{section.17.4}%
\contentsline {section}{\numberline {17.5}Classes Views Configurations Animated 3D Title plugin}{71}{section.17.5}%
\contentsline {chapter}{Python Module Index}{91}{section*.188}%
\contentsline {chapter}{Index}{93}{section*.189}%
