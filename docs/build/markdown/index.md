<!-- Openshot Render 3D Title Gesture documentation master file, created by
sphinx-quickstart on Wed Aug 28 17:15:10 2019.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive. -->
Welcome to Openshot Render 3D Title Gesture’s documentation!

> 

> ![image](pictures/OpenShotBlender3DTitlePluginsGesture.png)

# How to create Animated 3D plugin

## Minimal 3D Title Animation plugin

> Minimal xml file to put into path_to_openshot_directory/openshot_folder/blender

> ```
> <?xml version="1.0" encoding="UTF-8" standalone="no"?>
> <!DOCTYPE openshot-effect>
> <effect>
>     <title translatable="True">User text plugin</title>
>     <description translatable="True">Tooltip text plugin</description>
>     <icon>icon_plugin.png</icon>
>     <category>Video</category>
>     <plugintype>Type animation plugin</plugintype>
>     <render name="blender">
>         <service>name_blender_file_plugin.blend</service>
>         <engine>blender</engine>
>     </render>
>     <param name="widget_blender_engines" type="blender_engines" title="Text blender_engines" description="Tooltip blender_engines" visibility="hidden">
>         <default>BLENDER</default>
>     </param>
>     <param name="widget_range_frames" type="rangeframes" title="Blender Animation Range Frames" description="Tooltip rangeframes" visibility="hidden">
>         <min>1</min>
>         <max>200</max>
>         <start>1</start>
>         <end>200</end>
>         <step>1</step>
>         <default>100</default>
>     </param>
> </effect>
> ```

> Minimal python script file to path_to_openshot_directory/openshot_folder/blender/scripts

> ```
> #       OpenShot Video Editor is a program that creates, modifies, and edits video files.
> #   Copyright (C) 2009  Jonathan Thomas
> #
> #       This file is part of OpenShot Video Editor (http://launchpad.net/openshot/).
> #
> #       OpenShot Video Editor is free software: you can redistribute it and/or modify
> #       it under the terms of the GNU General Public License as published by
> #       the Free Software Foundation, either version 3 of the License, or
> #       (at your option) any later version.
> #
> #       OpenShot Video Editor is distributed in the hope that it will be useful,
> #       but WITHOUT ANY WARRANTY; without even the implied warranty of
> #       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> #       GNU General Public License for more details.
> #
> #       You should have received a copy of the GNU General Public License
> #       along with OpenShot Video Editor.  If not, see <http://www.gnu.org/licenses/>.
> ```

> ```
> # Debug Info:
> # ./blender -b test.blend -P demo.py
> # -b = background mode
> # -P = run a Python script within the context of the project file
> ```

> ```
> ###########################################################
> #####                Get python library               #####
> # Import Blender's python API.  This only works when the script is being
> # run from the context of Blender.  Blender contains it's own version of Python
> # with this library pre-installed.
> import bpy, colorsys, os, sys
> from math import pi, radians
> #####           End Import Get python library         #####
> ###########################################################
> ```

> ```
> ###########################################################
> #####            Get python Blender library           #####
> directory = os.path.dirname(bpy.data.filepath)[:-5] + 'scripts/openshot'
> if not directory in sys.path:
>     sys.path.append(directory)
> from debug import gest_blender_error
> #####          End Get python Blender library         #####
> ###########################################################
> ```

> ```
> ###########################################################
> #####            Openshot parameters plugin           #####
> # Init all of the variables needed by this script.
> params = {}
> # Specific Blender python script parameters
> # put yours specifics variables Blender script like params['My param'] = Value
> # Because Blender executes
> # this script, OpenShot will inject a dictionary of the required parameters
> # before this script is executed.
> #INJECT_PARAMS_HERE
> # The remainder of this script will modify the current Blender .blend project
> # file, and adjust the settings.  The .blend file is specified in the XML file
> # that defines this template in OpenShot.
> #####          End Openshot parameters plugin         #####
> ###########################################################
> ```

> ```
> ###########################################################
> #####              Blender errors gesture             #####
> # interpretor errors code gesture
> def excepthook(type, value, traceback):
>     sys.exit(gest_blender_error(params, sys.exc_info(), 'Error script code', params["output_path"] + "debug.txt"))
> sys.excepthook = excepthook
> #####            End Blender errors gesture           #####
> ###########################################################
> ```

> ```
> # Init render engine
> if params["scene_render"] == 'CYCLES':
>     bpy.context.scene.render.engine = 'CYCLES'
> elif params["scene_render"] == 'BLENDER_GAME':
>     bpy.context.scene.render.engine = 'BLENDER_GAME'
> elif params["scene_render"] == 'POVRAY_RENDER':
>     bpy.context.scene.render.engine = 'POVRAY_RENDER'
> elif params["scene_render"] == 'EEVEE':
>     bpy.context.scene.render.engine = 'EEVEE'
> else: #if params["scene_render"] == 'BLENDER'
>     if bpy.app.version < (2, 80, 0):
>         bpy.context.scene.render.engine = 'BLENDER_RENDER'
>     else:
>         bpy.context.scene.render.engine = 'BLENDER_WORKBENCH'
> ```

> ```
> ###########################################################
> #####      Define Blender python script functions     #####
> flag_error = True
> try:
>     flag_error = False
> except Exception as E:
>     gest_blender_error(params, sys.exc_info(), 'Error define blender python functions', params["output_path"] + "debug.txt", E)
> #####    End Define Blender python script functions   #####
> ###########################################################
> ```

> ```
> ###########################################################
> #####             Start create scene plugin           #####
> #print(str(params), file=open(os.path.join(os.path.expanduser("~"), "debug.txt"), "a"))
> if not flag_error:
>     #-----------------------------------------------------#
>     #----                  Clean Scene                ----#
>     flag_error = True
>     try:
>         flag_error = False
>         pass
>     except Exception as E:
>         gest_blender_error(params, sys.exc_info(), 'Error clean scene', params["output_path"] + "debug.txt", E)
>     #----                End Clean Scene              ----#
>     #-----------------------------------------------------#
> if not flag_error:
>     #-----------------------------------------------------#
>     #----                  3D Modeling                ----#
>     flag_error = True
>     try:
>        # Get font object
>         flag_error = False
>     except Exception as E:
>         gest_blender_error(params, sys.exc_info(), 'Error 3d modeling', params["output_path"] + "debug.txt", E)
>     #----                End 3D Modeling              ----#
>     #-----------------------------------------------------#
> if not flag_error:
>     #-----------------------------------------------------#
>     #----                  UV Mapping                 ----#
>     flag_error = True
>     try:
>         flag_error = False
>     except Exception as E:
>         gest_blender_error(params, sys.exc_info(), 'Error uv mapping', params["output_path"] + "debug.txt", E)
>     #----                End UV Mapping               ----#
>     #-----------------------------------------------------#
> if not flag_error:
>     #-----------------------------------------------------#
>     #----             Texturing and Shaders           ----#
>     flag_error = True
>     try:
>         flag_error = False
>     except Exception as E:
>         gest_blender_error(params, sys.exc_info(), 'Error texturing and shading', params["output_path"] + "debug.txt", E)
>     #----           End Texturing and Shaders         ----#
>     #-----------------------------------------------------#
> if not flag_error:
>     #-----------------------------------------------------#
>     #----                    Rigging                  ----#
>     flag_error = True
>     try:
>         flag_error = False
>     except Exception as E:
>         gest_blender_error(params, sys.exc_info(), 'Error rigging', params["output_path"] + "debug.txt", E)
>     #----                  End Rigging                ----#
>     #-----------------------------------------------------#
> if not flag_error:
>     #-----------------------------------------------------#
>     #----                   Animation                 ----#
>     flag_error = True
>     try:
>         flag_error = False
>     except Exception as E:
>         gest_blender_error(params, sys.exc_info(), 'Error animation', params["output_path"] + "debug.txt", E)
>     #----                 End Animation               ----#
>     #-----------------------------------------------------#
> if not flag_error:
>     #-----------------------------------------------------#
>     #----                   Lighting                  ----#
>     flag_error = True
>     try:
>         flag_error = False
>     except Exception as E:
>         gest_blender_error(params, sys.exc_info(), 'Error lightning', params["output_path"] + "debug.txt", E)
>     #----                 End Lighting                ----#
>     #-----------------------------------------------------#
> if not flag_error:
>     #-----------------------------------------------------#
>     #----                Camera Setting               ----#
>     flag_error = True
>     try:
>         flag_error = False
>     except Exception as E:
>         gest_blender_error(params, sys.exc_info(), 'Error camera setting', params["output_path"] + "debug.txt", E)
>     #----              End Camera Setting             ----#
>     #-----------------------------------------------------#
> if not flag_error:
>     #-----------------------------------------------------#
>     #----                  Renderring                 ----#
>     flag_error = True
>     try:
>         flag_error = False
>     except Exception as E:
>         gest_blender_error(params, sys.exc_info(), 'Error set renderring', params["output_path"] + "debug.txt", E)
>     #----                End Renderring               ----#
>     #-----------------------------------------------------#
> if not flag_error:
>     #-----------------------------------------------------#
>     #----           Compositing & Special VFX         ----#
>     flag_error = True
>     try:
>         flag_error = False
>     except Exception as E:
>         gest_blender_error(params, sys.exc_info(), 'Error compositing ans special vfx', params["output_path"] + "debug.txt", E)
>     #----         End Compositing & Special VFX       ----#
>     #-----------------------------------------------------#
> if not flag_error:
>     #-----------------------------------------------------#
>     #----                Music and Foley              ----#
>     flag_error = True
>     try:
>         flag_error = False
>     except Exception as E:
>         gest_blender_error(params, sys.exc_info(), 'Error music and foley', params["output_path"] + "debug.txt", E)
>     #----              End Music and Foley            ----#
>     #-----------------------------------------------------#
> #####              End create scene plugin            #####
> ###########################################################
> ```

> ```
> ##############################################################################
> #####                      Editing and Final output                      #####
> # Set the render options.  It is important that these are set
> # to the same values as the current OpenShot project.  These
> # params are automatically set by OpenShot
> # Render the current animation to the params["output_path"] folder
> bpy.context.scene.render.resolution_x = params["resolution_x"]
> bpy.context.scene.render.resolution_y = params["resolution_y"]
> bpy.context.scene.render.resolution_percentage = params["resolution_percentage"]
> #bpy.context.scene.render.quality = params["quality"]
> if flag_error or not params["animation"] :
>     # Render the current animation to the params["output_path"] folder
>     bpy.context.scene.render.filepath = params["output_path"] + str(params["current_frame"])
>     if flag_error:
>         params["current_frame"] = params["start_frame"]
>         try:
>             bpy.context.scene.render.file_format = 'PNG'
>         except:
>             bpy.context.scene.render.image_settings.file_format = 'PNG'
>     bpy.data.scenes[0].frame_current = params["current_frame"]
>     bpy.data.scenes[0].frame_start = params["current_frame"]
>     bpy.data.scenes[0].frame_end = params["current_frame"]
>     bpy.ops.render.render(animation=params["animation"], write_still=True)
> else:
>     # Render the current animation to the params["output_path"] folder
>     bpy.context.scene.render.filepath = params["output_path"]
>     bpy.context.scene.render.fps = params["fps"]
>     bpy.data.scenes[0].frame_current = params['current_frame']
>     bpy.data.scenes[0].frame_start = params['start_frame']
>     bpy.data.scenes[0].frame_end = params['end_frame']
>     # Animation Speed (use Blender's time remapping to slow or speed up animation)
>     #bpy.context.scene.render.frame_map_new = bpy.context.scene.render.frame_map_old * int(params["animation_speed"])
>     bpy.ops.render.render(animation=params["animation"])
> #####                    End Editing and Final output                    #####
> ##############################################################################
> ```

## Blender plugin version gesture

> Valide plugin for Blender version ≥ 2.8

> ```
> <render name="blender">
>     <service>name_blender_file_plugin.blend</service>
>     <engine>blender</engine>
>     <min>2.8</min>
> </render>
> ```

> Valide plugin for Blender version ≤ 2.79

> ```
> <render name="blender">
>     <service>name_blender_file_plugin.blend</service>
>     <engine>blender</engine>
>     <max>2.79</max>
> </render>
> ```

> Valide plugin for Blender version > 2.79

> ```
> <render name="blender">
>     <service>name_blender_file_plugin.blend</service>
>     <engine>blender</engine>
>     <sup>2.79</sup>
> </render>
> ```

> Valide plugin for Blender version < 2.8

> ```
> <render name="blender">
>     <service>name_blender_file_plugin.blend</service>
>     <engine>blender</engine>
>     <inf>2.8</inf>
> </render>
> ```

> Multi validation plugin for Blender version < 2.8 and > 2.7

> ```
> <render name="blender">
>     <service>name_blender_file_plugin.blend</service>
>     <engine>blender</engine>
>     <sup>2.7</sup>
>     <inf>2.8</inf>
> </render>
> ```

> Sample for plugin support Blender less than 2.8 version.



> ![image](pictures/ListPluginsBlender2.7Compatibles.png)

> Sample for plugins support Blender 2.8 version.



> ![image](pictures/ListPluginsBlender2.8Compatibles.png)

## Blender plugin engine gesture

> Set list of blender engines choose

> ```
> <render name="blender">
>     <service>name_blender_file_plugin.blend</service>
>     <engine>blender</engine>
>     <engine>cycles</engine>
>     <engine>eevee</engine>
> </render>
> ```

> Widget select engine render

> ```
> <param name="widget_blender_engine_set" type="blender_engines" title="Blender engine" description="Choose Blender_engine">
>     <default>CYCLES</default>
> </param>
> ```



> ![image](pictures/WidgetBlenderEngineSelect.png)

> Features of hide/show widgets select case render

> ```
> <param name="widget_blender_engine_set" type="blender_engines" title="Blender engine" description="Choose Blender_engine" on_select_show="blender:widget_1; cycles:widget_2; eevee:widget_2, widget_3" on_select_hide="blender:widget_2, widget_3; cycles:widget_1, widget_3; eevee:widget_1">
>     <default>CYCLES</default>
> </param>
> ```

## Blender plugin frames gesture

> Widget select frames

> ```
> <param name="widget_range_frames_set" type="rangeframes" title="Range Frames" description="Choose a range frames">
>     <min>1</min>
>     <max>200</max>
>     <start>34</start>
>     <end>163</end>
>     <step>1</step>
>     <default>50</default>
> </param>
> ```



> ![image](pictures/RangeFrameWidget.png)

> Widget select frames by steps into a group ‘group_render’

> ```
> <groups>
>     <group name="group_render" title="Render" type="static" description="Select render options"></group>
> </groups>
> <param name="widget_range_frames_set" type="rangeframes" title="Range Frames" description="Choose a range frames" group="group_render">
>     <min>1</min>
>     <max>240</max>
>     <start>50</start>
>     <end>200</end>
>     <step>1</step>
>     <steps>
>         <start name="0" num="0"/>
>         <start name="50" num="50"/>
>         <start name="80" num="80"/>
>         <start name="150" num="150"/>
>         <end name="80" num="80"/>
>         <end name="200" num="200"/>
>         <end name="220" num="220"/>
>         <end name="240" num="240"/>
>     </steps>
>     <default>100</default>
> </param>
> ```



> ![image](pictures/RangeFrameWidgetByStep.png)

## Blender plugin texts features

> Widget Text

> ```
> <param name="widget_text" type="text" title="Text label" description="Tooltip text">
>     <default>DefaultText</default>
> </param>
> ```



> ![image](pictures/WidgetText.png)

> Widget Text Multiline

> ```
> <param name="widget_multiline_text" type="multiline" title="Multiline text label" description="Tooltip multiline text">
>     <default>Default
>     Multiline
>     Text
>     </default>
> </param>
> ```



> ![image](pictures/WidgetMultiline.png)

## Blender plugin booleans features

> Widget Boolean

> ```
> <param name="widget_boolean" type="boolean" title="Boolean Label" description="Tooltip boolean">
>     <default>off</default>
> </param>
> ```



> ![image](pictures/WidgetBoolean1.png)



> ![image](pictures/WidgetBoolean2.png)

> Widget Group boolean

> ```
> <groups>
>     <group name="group_boolean" title="Group boolean" type="off"  description="Tooltipn group boolean"></group>
> </groups>
> ```



> ![image](pictures/BooleanGroupWidgetFeature1.png)



> ![image](pictures/BooleanGroupWidgetFeature2.png)

> Conditionnal boolean choice

> ```
> <param name="widget_boolean" type="boolean" title="Label boolean" description="Tooltip boolean" on_active_show="widget2" on_active_hide="widget1" on_inactive_show="widget1" on_inactive_hide="widget2">
>     <default>off</default>
> </param>
> <param name="widget1" type="any_type_off_widget" title="Label of widget1" description="Tooltip of widget1">
>     <values>
>         <value name="" num=""/>
>     </values>
>     <default></default>
> </param>
> <param name="widget2" type="any_type_off_widget" title="Label of widget2" description="Tooltip of widget2">
>     <values>
>         <value name="" num=""/>
>     </values>
>     <default></default>
> </param>
> ```



> ![image](pictures/BooleanSwitchConditionnalWidgetVisible1.png)



> ![image](pictures/BooleanSwitchConditionnalWidgetVisible2.png)

## Blender plugin Widget dropdown

> Define a dropdown list of selectables items

> ```
> <param name="widget_dropdown" type="dropdown" title="Text dropdown" description="Tooltip dropdown">
>     <values>
>         <value name="Label item 1" num="item1"/>
>         <value name="Label item 2" num="item2"/>
>         <value name="Label item 3" num="item3"/>
>     </values>
>     <default>item2</default>
> </param>
> ```



> ![image](pictures/WidgetDropdown.png)

## Blender plugin Widget spinner

> Choose a numerical spinner value.

> Example for integers

> ```
> <param name="widget_spiner" type="spinner" title="Label spinner" description="Tooltip spinner" data_type="int">
>     <min>0</min>
>     <max>10</max>
>     <step>1</step>
>     <default>5</default>
> </param>
> ```

> or for float

> ```
> <param name="widget_spiner" type="spinner" title="Label spinner" description="Tooltip spinner" data_type="float">
>     <min>0.0</min>
>     <max>10.0</max>
>     <step>0.1</step>
>     <default>0.5</default>
> </param>
> ```



> ![image](pictures/WidgetSpinnerFloat.png)

## Blender plugin Widget color

> Choose a color

> ```
> <param name="widget_color" type="color" title="Label color" description="Tooltip color">
>     <default>#B20000</default>
> </param>
> ```



> ![image](pictures/WidgetColor1.png)



> ![image](pictures/WidgetColor2.png)

## Blender plugin Widget file

> Choose a file

> ```
> <param name="widget_file" type="file" title="Label file" description="Tooltip file">
>     <values>
>         <value name="my_file1.txt" num=":folder/my_file.txt"/>
>         <value name="my_file1.txt" num="/static_path/my_file.txt"/>
>     </values>
>     <default>my_file1.txt</default>
> </param>
> ```

> ‘:’ character for path to blender plugins Openshot folder



> ![image](pictures/WidgetChooseFile.png)



> ![image](pictures/WidgetChooseFileWindow.png)

## Blender plugin Widget font

> Choose a file font

> ```
> <param name="widget_font" type="font" title="Lanel font" description="Tooltip font">
>     <values>
>         <value name="Leander.ttf" num=":fonts/Leander.ttf"/>
>     </values>
>     <default>Leander.ttf</default>
> </param>
> ```

> ‘:’ character for path to blender plugins Openshot folder



> ![image](pictures/FontSelectWidget.png)



> ![image](pictures/FontSelectWidgetWindowChooseFont.png)

## Blender plugin Widget picture

> Choose a file picture

> ```
> <param name="widget_picture" type="picture" title="Label picture" description="Tooltip picture">
>     <values>
>         <value name="My-picture.png" num=":pictures/My-picture.png"/>
>     </values>
>     <default>My-picture.png</default>
> </param>
> ```

> ‘:’ character for path to blender plugins Openshot folder



> ![image](pictures/WidgetChoosePicture.png)



> ![image](pictures/WidgetChoosePictureWindow.png)

## Blender plugin Widget sound

> Choose a file sound

> ```
> <param name="widget_sound" type="sound" title="Label sound" description="Tooltip sound">
>     <values>
>         <value name="My-sound.ogg" num=":sounds/My-sound.png"/>
>     </values>
>     <default>My-sound.png</default>
> </param>
> ```

> ‘:’ character for path to blender plugins Openshot folder



> ![image](pictures/WidgetSound1.png)



> ![image](pictures/WidgetSound2.png)

## Blender plugin Widget movie

> Choose a file movie

> ```
> <param name="widget_movie" type="movie" title="Label movie" description="Tooltip movie">
>     <values>
>         <value name="My-movie.mpeg" num=":movies/My-movie.mpeg"/>
>     </values>
>     <default>My-movie.mpeg</default>
> </param>
> ```

> ‘:’ character for path to blender plugins Openshot folder



> ![image](pictures/WidgetMovie1.png)



> ![image](pictures/WidgetMovie2.png)

# Python scripts gestures Animated 3D plugin

## Debuggin features

> ```
> import bpy, platform, textwrap, traceback
> from math import pi, radians
> ```

> ```
> # Clear scene
> def erase_all_forms():
>     """Erase all objects exept camera, lamps and speaker"""
>     if bpy.app.version < (2, 80, 0):
>         bpy.ops.wm.read_factory_settings()
>     objects = bpy.context.scene.objects
>     operation = bpy.ops.object
>     operation.select_all(action='DESELECT')
>     for obj in objects:
>         if bpy.app.version < (2, 80, 0):
>             obj.select = obj.type in ['EMPTY', 'MESH', 'CURVE', 'SURFACE', 'META', 'FONT', 'ARMATURE', 'LATTICE']
>         else:
>             obj.select_set(obj.type in ['EMPTY', 'MESH', 'CURVE', 'SURFACE', 'META', 'FONT', 'ARMATURE', 'LATTICE'])
>     operation.delete(use_global=True)
> ```

> ```
> # Define camera for show error into blender
> def camera_error():
>     """Capture text generated by print_error()"""
>     camera = bpy.data.scenes["Scene"].camera
>     camera.location.x = 10
>     camera.location.y = 0
>     camera.location.z = 0
>     camera.data.angle = 50*(pi/180.0)
>     camera.rotation_euler = (radians(90), 0, radians(90))
> ```

> ```
> # Define light for show error into blender
> def light_error():
>     """Light text generated by print_error()"""
>     lamp = bpy.data.objects["Sun"]
>     lamp.data.energy = 300
>     lamp.location.x = 4.0
>     lamp.location.y = 1.0
>     lamp.location.z = 6.0
> ```

> ```
> # Add debugging text to Blender width error
> def print_error(error_info, text_error):
>     """Print error into generated picture"""
>     erase_all_forms()
>     # Scene set
>     operate = bpy.ops.object
>     if bpy.app.version < (2, 80, 0):
>         operate.camera_add()
>     bpy.context.scene.camera = bpy.data.objects['Camera']
>     if bpy.app.version < (2, 80, 0):
>         operate.lamp_add(type='SUN')
>     else:
>         operate.light_add(type='SUN')
>     light_error()
>     camera_error()
>     if bpy.app.version < (2, 80, 0):
>         bpy.data.worlds[0].horizon_color = [0.9254901960784314, 0.9254901960784314, 0.9254901960784314]
>     # Message error info
>     operate.text_add()
>     obj_text = bpy.data.objects["Text"]
>     text = obj_text.data
>     #exc_type = str(error_info[0])
>     tb = traceback.extract_tb(error_info[2])[-1]
>     message = 'ERROR\n' + platform.dist()[1] + ' ' + platform.architecture()[0] + '\n' + platform.system() + ' ' + platform.release() + '\n' + 'Python ' + platform.python_version() + '\n' + 'Blender ' + bpy.app.version_string + '\nFile: ' + tb[0].split('/')[-1] + ' Line: ' + str(tb[1]) + '\n' + "\n".join(textwrap.wrap(text_error,40))
>     text.body = message
>     text.size=0.5
>     text.align_x = 'CENTER'
>     text.align_y = 'CENTER'
>     operate.origin_set(type='ORIGIN_GEOMETRY')
>     obj_text.location.x = 0.0
>     obj_text.location.y = 0.0
>     obj_text.location.z = 0.0
>     obj_text.rotation_euler = (radians(90), 0, radians(90))
> ```

> ```
> # Blender error gesture
> def gest_blender_error(params, error, text_error, path_error, E=None):
>     """Blender errors gesture"""
>     if E:
>         print_error(error, text_error + ': <' + str(E) + '>')
>     else:
>         print_error(error, text_error)
>     tb = traceback.extract_tb(error[2])[-1]
>     print(text_error, file=open(path_error, "a"))
>     if E:
>         print('File ' + tb[0].split('/')[-1] + ' Line=' + str(tb[1]) + ' :' + str(E), file=open(path_error, "a"))
>     else:
>         print('File ' + tb[0].split('/')[-1] + ' Line=' + str(tb[1]), file=open(path_error, "a"))
>     print(str(params), file=open(path_error, "a"))
> ```

> Example of debug blender render code use

> ```
> directory = os.path.dirname(bpy.data.filepath)[:-5] + 'scripts/openshot'
> if not directory in sys.path:
>     sys.path.append(directory)
> from debug import gest_blender_error
> flag_error = False
> if not flag_error:
>     flag_error = True
>     try:
>         # Your job script
>         flag_error = False
>     except Exception as E:
>         gest_blender_error(params, sys.exc_info(), 'Error clean scene', params["output_path"] + "debug.txt", E)
> ```

> This code generate a picture error Blender information and put a debuggin file into temporry folder pluggin

## Blender features

# Modules OpenShot Animated 3D Title plugins

## The `animated_title` module


* **Date**

    2019-11-07



* **Revision**

    1.0



* **Author**

    Franc SERRES <[sefran007@gmail.com](mailto:sefran007@gmail.com)> from preview work of Jonathan Thomas <[jonathan@openshot.org](mailto:jonathan@openshot.org)>



* **Description**

    Module Manager for OpenShot Animated 3D Title plugins



* **Info**

    See <[https://framagit.org/sefran/3d-blender-plugin-openshot-video-editor](https://framagit.org/sefran/3d-blender-plugin-openshot-video-editor)> for more recent info.


Use it to import 3D Title render animation manager features.

`Animated3DTitleManager`


---

## The `renders_worker` module


* **Date**

    2019-11-07



* **Revision**

    1.0



* **Author**

    Franc SERRES <[sefran007@gmail.com](mailto:sefran007@gmail.com)> from preview work of Jonathan Thomas <[jonathan@openshot.org](mailto:jonathan@openshot.org)>



* **Description**

    Module Controler for OpenShot Animated 3D Title plugins



* **Info**

    See <[https://framagit.org/sefran/3d-blender-plugin-openshot-video-editor](https://framagit.org/sefran/3d-blender-plugin-openshot-video-editor)> for more recent info.


Use it to import 3D Title render animation worker features.

`ManageTaskViewPlugin`

`PluginsManager`


---

## The `renders_model` module


* **Date**

    2019-11-07



* **Revision**

    1.0



* **Author**

    Franc SERRES <[sefran007@gmail.com](mailto:sefran007@gmail.com)> from preview work of Jonathan Thomas <[jonathan@openshot.org](mailto:jonathan@openshot.org)>



* **Description**

    Module Datas Models for OpenShot Animated 3D Title plugins



* **Info**

    See <[https://framagit.org/sefran/range-progress-bar](https://framagit.org/sefran/range-progress-bar)> for more recent info.


Use it to import 3D Title render animation models features.

`ConfigurationPluginModel`

`ListPluginsModel`

`ParametersPluginModel`


---

## The `renders_plugins_views` module


* **Date**

    2019-11-07



* **Revision**

    1.0



* **Author**

    Franc SERRES <[sefran007@gmail.com](mailto:sefran007@gmail.com)> from preview work of Jonathan Thomas <[jonathan@openshot.org](mailto:jonathan@openshot.org)>



* **Description**

    Module Views Window for OpenShot Animated 3D Title plugins



* **Info**

    See <[https://framagit.org/sefran/range-progress-bar](https://framagit.org/sefran/range-progress-bar)> for more recent info.


Use it to import Window 3D Title render animation views features.

`ListPluginsView`

`RendersPluginsViews`

`ManagerPanelPluginsView`

`TaskManagerPluginView`


---

## The `renders_config_widgets` module


* **Date**

    2019-11-07



* **Revision**

    1.0



* **Author**

    Franc SERRES <[sefran007@gmail.com](mailto:sefran007@gmail.com)> from preview work of Jonathan Thomas <[jonathan@openshot.org](mailto:jonathan@openshot.org)>



* **Description**

    Module Views Configurations for OpenShot Animated 3D Title plugin



* **Info**

    See <[https://framagit.org/sefran/range-progress-bar](https://framagit.org/sefran/range-progress-bar)> for more recent info.


Use it to import 3D Title render animation widgets configurations features.

`CreateWidgetsParametersRenderPlugin`

`BlenderEnginesWidget`

`BooleanWidget`

`BooleanGroupWidget`

`ColorWidget`

`DropdownWidget`

`FileWidget`

`FontWidget`

`GroupWidget`

`LabelWidget`

`MovieWidget`

`MultilineWidget`

`PictureWidget`

`RangeFramesWidget`

`SoundWidget`

`SpinnerDoubleWidget`

`SpinnerIntWidget`

`SpinnerWidget`

`TextWidget`

`OpenFileConfig`

`OpenFontFile`

`OpenMovieFile`

`OpenSoundFile`


---

# Classes OpenShot Animated 3D Title plugins

## Class Manager Animated 3D Title plugins


#### class windows.animated_title.Animated3DTitleManager(\*args, \*\*kwargs)


![image](classes/Animated3DTitleManager.png)

Create a manager for 3D Title render animation.


* **Variables**

    * **thread_manager** (*ThreadsManager*) – Gesture of threaded renders

    * **plugins_manager** (*PluginsManager*) – Gesture of plugins

    * **title3DWindow** (*RendersPluginsViews*) – Window of Animated 3D title plugins



#### add_file(filepath)
Add an animation to the project file tree


#### close()
End of Animated3DTitleManager


#### error_set_version(id_task, error)
> Return errors of get version with Animated3DTitleManager.error_set_version.

> Show error dialog box


* **Parameters**

    * **id_task** (*TaskPluginView*) – identifiant of task render plugin

    * **error** – error message



#### error_with_blender(version=None, command_output=None)
Show a friendly error message regarding the blender executable or version


#### get_plugins()
Get list of 3D plugins


#### set_blender_version(id_task, version)
> Set version of Blender with Animated3DTitleManager.set_blender_version


* **Parameters**

    * **id_task** (*TaskPluginView*) – identifiant of task render plugin

    * **version** (*string*) – Version of Blender



* **Retruns**

    version into self.blender_version



* **Return type**

    string



#### show()
Show Animation 3D titles window

## Classes Controlers Animated 3D Title plugins


#### class windows.controlers.threads_worker.ThreadsManager(\*args, \*\*kwargs)


![image](classes/ThreadsManager.png)

Class for gesture threaded render tasks


* **Variables**

    * **terminate_process** (*Worker*) – Window of Animated 3D title plugins

    * **currentworker** – active threaded features to pull



#### adding_process_render_manager(id_task, id_process)
Adding subprocess Popen process render gesture


* **Parameters**

    * **id_task** (*TaskPluginView*) – identifiant of task render

    * **id_process** ([*subprocess.Popen*](https://docs.python.org/3/library/subprocess.html#subprocess.Popen)) – system render process gesture



#### close()
Close trhead manager


#### execute_function(execute_this_function, id_task=None, params_function=None, filter_function=None, func_operator=None, progress_function=None, manage_process_render_function=None, manage_errors_function=None)
Add new threaded function to pool and execute. Return current worker thread


* **Parameters**

    * **execute_this_function** (*function*) – Function to execute into thread

    * **id_task** (*TaskPluginView*) – identifiant of task render

    * **params_function** ([*dict*](https://docs.python.org/3/library/stdtypes.html#dict)) – identifiant of task render

    * **filter_function** (*function*) – Function for intermediate traitment

    * **func_operator** (*function*) – Function for result traitment

    * **progress_function** (*function*) – Function for progress traitment

    * **manage_process_render_function** (*function*) – Function for manage execution

    * **manage_errors_function** (*function*) – Function for manage errors



#### execute_process(id_task)
Run process render execution


* **Parameters**

    **id_task** (*TaskPluginView*) – identifiant of task render



#### get_current_worker()
get current worker executed


#### get_list_thread_pool()
get list of polled threads


#### get_pool()
Return thread pool object


#### kill_all_process()
End of all active render process


#### kill_process(id_task)
Kill process render execution


* **Parameters**

    **id_task** (*TaskPluginView*) – identifiant of task render



#### suspend_process(id_task)
Suspend process render execution


* **Parameters**

    **id_task** (*TaskPluginView*) – identifiant of task render



#### terminate_process(id_task)
Terminate process render execution


* **Parameters**

    **id_task** (*TaskPluginView*) – identifiant of task render



#### terminate_render_plugin(param_object=None)
Kill active render plugin


* **Parameters**

    **param_object** – 



#### thread_complete()
Threads state gesture


#### thread_pool(id_thread)
Thread render pool


* **Parameters**

    **id_thread** – Identifiant of thread



#### thread_unpool(result)
Thread render unpool


* **Parameters**

    **result** – Result of thread traitment



#### class windows.controlers.threads_worker.Worker(fonction, \*args, \*\*kwargs)


![image](classes/Worker.png)

Worker thread

Inherits from QRunnable to handler worker thread setup, signals and wrap-up.


* **Parameters**

    * **callback** (*function*) – The function callback to run on this worker thread. Supplied args and
      kwargs will be passed through to the runner.

    * **args** – Arguments to pass to the callback function

    * **kwargs** – Keywords to pass to the callback function



#### run()
Initialise the runner function with passed args, kwargs.


#### class windows.controlers.threads_worker.WorkerSignals()


![image](classes/WorkerSignals.png)

Defines the signals available from a running worker thread.


* **Variables**

    * **call_operator** (*pyqtSignal*) – Send object of operator function

    * **process_progress** (*pyqtSignal*) – Send object of progress function

    * **manage_progress** (*pyqtSignal*) – Send progress to function

    * **manage_errors** (*pyqtSignal*) – Send error to function

    * **result** (*pyqtSignal*) – Data returned from processing, anything

    * **finished** (*pyqtSignal*) – No data end of thread fuction

    * **error** (*pyqtSignal*) – (exctype, value, traceback.format_exc())

    * **process_thread** (*pyqtSignal*) – Send object of thread function

    * **terminate_process** (*pyqtSignal*) – Send object of end thread



#### class windows.controlers.renders_worker.PluginsManager(animated_3d_title_manager, thread_manager)


![image](classes/PluginsManager.png)

This class create object management for plugins 3D animations.


* **Parameters**

    * **animated_3d_title_manager** (*QObject*) – Manager Animated 3D Title plugins

    * **thread_manager** (*ThreadsManager*) – Gesture of threaded renders



* **Variables**

    * **title3DWindow** (*RendersPluginsViews*) – Window of Animated 3D title plugins

    * **active_plugin** (*TaskPluginView*) – Set plugin activated

    * **list_running_plugins** ([*list*](https://docs.python.org/3/library/stdtypes.html#list)*[**TaskPluginView**]*) – List of actives plugins

    * **current_configuration_plugin_model** (*ConfigurationPluginModel*) – Configuration of active plugin

    * **current_parameters_plugin_model** (*ParametersPluginModel*) – Parameters of active plugin

    * **current_frame** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Frame selected for preview

    * **max_parts** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Parts of render

    * **previewprogress** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Value of last progress



#### activePlugin(id_task)
Active configuration identifed by id_task render plugin animation. This is connected with button ‘Configure’ from object view.TaskPluginView


* **Parameters**

    **id_task** (*TaskPluginView*) – identifiant of task render plugin



#### close()
Close plugin manager


#### controlRenderPlugin(id_task)
Control execution of render plugin. This is connected with button ‘Execute’ from object view.TaskPluginView


* **Parameters**

    * **id_task** (*TaskPluginView*) – identifiant of task render plugin

    * **tooltip** ([*str*](https://docs.python.org/3/library/stdtypes.html#str)) – Text of button execute/pause render



#### createPlugin(name)
Create new render gesture plugin animation. This is connected with button ‘New’ from object view.TaskPluginView


* **Parameters**

    **name** – Name of 3D Title plugin selected



#### deletePlugin(id_task)
Delete render gesture plugin animation. This is connected with button ‘Trash’ from object view.TaskPluginView


* **Parameters**

    **id_task** (*TaskPluginView*) – identifiant of task render plugin



#### deleteWidgetsFromParametersPlugin(parameters)
Delete widgets from configuration plugin to Configuration view


#### delete_active_plugin_configuration(id_task)
Remove active plugin parameters configuration from plugins manager


#### generateFolder(pathfolder)
Create directory if not exist


* **Parameters**

    **pathfolder** (*string*) – Create folder if no exist



#### get_picture_preview(id_task, blenderout)
Get picture preview from blender render


* **Parameters**

    * **id_task** (*TaskPluginView*) – identifiant of task render plugin

    * **blenderout** (*string*) – Blender output picture path



#### get_render_plugin(id_task, blenderout)
Get render from blender render


* **Parameters**

    * **id_task** (*TaskPluginView*) – identifiant of task render plugin

    * **blenderout** (*string*) – Blender output picture path



#### inactiveConfigurationPlugin(id_task)
Inactive configuration identifed by id_task render plugin animation


* **Parameters**

    **id_task** (*TaskPluginView*) – identifiant of task render plugin



#### inject_params(targetfilescriptpath, params_openshot, user_params_plugin)
Inject parameters of plugin into targetfilescript


* **Parameters**

    * **targetfilescriptpath** (*string*) – path of python file script plugin to generate

    * **params_openshot** ([*dict*](https://docs.python.org/3/library/stdtypes.html#dict)) – OpenShot plugins parameters

    * **user_params_plugin** ([*dict*](https://docs.python.org/3/library/stdtypes.html#dict)) – Plugin parameters



#### previewPlugin(id_task, frame)
Preview selected configuration of task plugin


* **Parameters**

    * **id_task** (*TaskPluginView*) – identifiant of task render plugin

    * **frame** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Frame to preview



#### remove_folder_plugin(id_task)
Remove temp folder of render plugin


#### renderPlugin(id_task)
Render selected configuration of task plugin. This is connected with button ‘Render’ from object view.TaskPluginView


* **Parameters**

    **id_task** (*TaskPluginView*) – identifiant of task render plugin



#### class windows.controlers.renders_worker.ManageTaskViewPlugin(id_task, list_running_plugins, widget_plugin_config, title3DWindow)


![image](classes/ManageTaskViewPlugin.png)

Widget parameters of a render plugin

id_task : The selected plugin render identifiant

list_running_plugins : List of running plugins

widget_plugin_config

title3DWindow


#### activeWidgets()
Install widgets config plugin into config view


#### createWidgets()
Install widgets config plugin into config view


#### deleteWidgets()
Install widgets config plugin into config view


#### inactiveWidgets()
Install widgets config plugin into config view


#### class windows.controlers.blender_worker.QBlenderEvent(id, data=None, \*args)
Class Event, a custom Blender which can safely be sent from the Blender thread to the Qt thread (to communicate)


#### class windows.controlers.blender_worker.BlenderRequest(\*args, \*\*kwargs)


![image](classes/BlenderRequest.png)

BlenderRequest

Inherits from QObject for request blender command


#### start_render()
Start Blender command

## Classes Datas Models 3D Animated Title plugins


#### class windows.models.renders_model.ConfigurationPluginModel(\*args, \*\*kwargs)


![image](classes/ConfigurationPluginModel.png)

Model object of configuration animation settings and properties from a file xml render plugin

Parameters:

> QModelIndex item form list of render plugins

> ListPluginsModel data list of render plugins

properties:

    dict ConfigurationPluginModel.animation :

    dict ConfigurationPluginModel.openshot_params :


#### get_defaults_plugin_params()
Get default parameters plugin

Return dict of values xml file.

The plugin file must have this structure :

```
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE openshot-effect>

<effect>
    <title translatable="True">User text plugin</title>
    <description translatable="True">Tooltip text plugin</description>
    <icon>icon_plugin.png</icon>
    <category>Video</category>
    <plugintype>Type animation plugin</plugintype>
    <render name="blender">
        <service>name_blender_file_plugin.blend</service>
        <engine>blender</engine>
        <engine>cycles</engine>
        <engine>eevee</engine>
        <max>2.79</max>
    </render>

    <groups>
        <group name="group_1" title="Group 1" type="static"  description="Tooltip group 1"></group>
        <group name="group_2" title="Group 2" type="off"  description="Tooltip group 2"></group>
        <group name="group_3" title="Group 3" type="on"  description="Tooltip group 3"></group>
    </groups>

    <param name="widget_text" type="text" title="Text" description="Tooltip text">
        <default>Default text value</default>
    </param>

    <param name="widget_multiline" type="multiline" title="multiline" description="Tooltip multiline">
        <default>
        Default value
        Of my multiline text
        </default>
    </param>

    <param name="widget_dropdown" type="dropdown" title="Text dropdown" description="Tooltip dropdown" group="group_1">
        <values>
            <value name="One" num="1"/>
            <value name="Two" num="2"/>
            <value name="Three" num="3"/>
        </values>
        <default>2</default>
    </param>

    <param name="widget_spinner" type="spinner" title="Text spinner" description="Tooltip spinner" data_type="float" group="group_1">
        <min>0.0</min>
        <max>10.0</max>
        <step>0.1</step>
        <default>0.5</default>
    </param>

    <param name="widget_boolean" type="boolean" title="Text boolean" description="Tooltip boolean" on_active_show="widget_colorr" on_active_hide="widget_font" on_inactive_show="widget_font" on_inactive_hide="widget_color" group="group_2">
        <default>off</default>
    </param>

    <param name="widget_color" type="color" title="Text Color" description="Tooltip color" group="group_2">
        <default>#B20000</default>
    </param>

    <param name="widget_font" type="font" title="Text font" description="Tooltip font" group="group_2">
        <values>
            <value name="Leander" num=":fonts/Leander.ttf"/>
        </values>
        <default>Leander.ttf</default>
    </param>

    <param name="widget_file" type="file" title="Text file" description="Tooltip file" group="group_3">
        <values>
            <value name="File 1" num=":blend/myfile.blend"/>
        </values>
        <default>myfile.blend</default>
    </param>

    <param name="widget_picture" type="picture" title="Text picture" description="Tooltip picture" group="group_3">
        <values>
            <value name="Picture 1" num=":pictures/mypict.png"/>
        </values>
        <default>mypict.png</default>
    </param>

    <param name="widget_sound" type="sound" title="Text sound" description="Tooltip sound" group="group_3">
        <values>
            <value name="Picture 1" num=":sound/mysound.ogg"/>
        </values>
        <default>mysound.ogg</default>
    </param>

    <param name="widget_movie" type="movie" title="Text movie" description="Tooltip movie" group="group_3">
        <values>
            <value name="Picture 1" num=":videos/myvideo.avi"/>
        </values>
        <default>myvideo.avi</default>
    </param>

    <param name="widget_range_frames" type="rangeframes" title="Text range frames" description="Tooltip range frames" group="group_3">
        <min>1</min>
        <max>200</max>
        <start>50</start>
        <end>150</end>
        <step>1</step>
        <steps>
            <start name="0" num="0"/>
            <start name="50" num="50"/>
            <start name="80" num="80"/>
            <start name="150" num="130"/>
            <end name="80" num="80"/>
            <end name="150" num="150"/>
            <end name="180" num="180"/>
            <end name="200" num="200"/>
        </steps>
    </param>

    <param name="widget_blender_engines" type="blender_engines" title="Text blender_engines" description="Tooltip blender_engines" group="group_3">
        <default>BLENDER</default>
    </param>

</effect>
```


#### get_openshot_params(is_preview=True)
Assign a dictionary of project related settings, needed by the render python script. Return dict and store datas into ConfigurationPluginModel.openshot_params


#### get_project_params(preview)
Return a dictionary of project related settings, needed by the render script


#### get_xml_default_params(file_path)
Read comfig xml file render plugin and store datas into ConfigurationPluginModel.animation[“params”] and groups into ConfigurationPluginModel.animation[“groups”]

The plugin file must have this structure :

```
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE openshot-effect>

<effect>
    <title translatable="True">User text plugin</title>
    <description translatable="True">Tooltip text plugin</description>
    <icon>icon_plugin.png</icon>
    <category>Video</category>
    <plugintype>Type animation plugin</plugintype>
    <render name="blender">
        <service>name_blender_file_plugin.blend</service>
        <engine>blender</engine>
        <engine>cycles</engine>
        <engine>eevee</engine>
        <max>2.79</max>
    </render>

    <groups>
        <group name="group_1" title="Group 1" type="static"  description="Tooltip group 1"></group>
        <group name="group_2" title="Group 2" type="off"  description="Tooltip group 2"></group>
        <group name="group_3" title="Group 3" type="on"  description="Tooltip group 3"></group>
    </groups>

    <param name="widget_text" type="text" title="Text" description="Tooltip text">
        <default>Default text value</default>
    </param>

    <param name="widget_multiline" type="multiline" title="multiline" description="Tooltip multiline">
        <default>
        Default value
        Of my multiline text
        </default>
    </param>

    <param name="widget_dropdown" type="dropdown" title="Text dropdown" description="Tooltip dropdown" group="group_1">
        <values>
            <value name="One" num="1"/>
            <value name="Two" num="2"/>
            <value name="Three" num="3"/>
        </values>
        <default>2</default>
    </param>

    <param name="widget_spinner" type="spinner" title="Text spinner" description="Tooltip spinner" data_type="float" group="group_1">
        <min>0.0</min>
        <max>10.0</max>
        <step>0.1</step>
        <default>0.5</default>
    </param>

    <param name="widget_boolean" type="boolean" title="Text boolean" description="Tooltip boolean" on_active_show="widget_colorr" on_active_hide="widget_font" on_inactive_show="widget_font" on_inactive_hide="widget_color" group="group_2">
        <default>off</default>
    </param>

    <param name="widget_color" type="color" title="Text Color" description="Tooltip color" group="group_2">
        <default>#B20000</default>
    </param>

    <param name="widget_font" type="font" title="Text font" description="Tooltip font" group="group_2">
        <values>
            <value name="Leander" num=":fonts/Leander.ttf"/>
        </values>
        <default>Leander.ttf</default>
    </param>

    <param name="widget_file" type="file" title="Text file" description="Tooltip file" group="group_3">
        <values>
            <value name="File 1" num=":blend/myfile.blend"/>
        </values>
        <default>myfile.blend</default>
    </param>

    <param name="widget_picture" type="picture" title="Text picture" description="Tooltip picture" group="group_3">
        <values>
            <value name="Picture 1" num=":pictures/mypict.png"/>
        </values>
        <default>mypict.png</default>
    </param>

    <param name="widget_sound" type="sound" title="Text sound" description="Tooltip sound" group="group_3">
        <values>
            <value name="Picture 1" num=":sound/mysound.ogg"/>
        </values>
        <default>mysound.ogg</default>
    </param>

    <param name="widget_movie" type="movie" title="Text movie" description="Tooltip movie" group="group_3">
        <values>
            <value name="Picture 1" num=":videos/myvideo.avi"/>
        </values>
        <default>myvideo.avi</default>
    </param>

    <param name="widget_range_frames" type="rangeframes" title="Text range frames" description="Tooltip range frames" group="group_3">
        <min>1</min>
        <max>200</max>
        <start>50</start>
        <end>150</end>
        <step>1</step>
        <steps>
            <start name="0" num="0"/>
            <start name="50" num="50"/>
            <start name="80" num="80"/>
            <start name="150" num="130"/>
            <end name="80" num="80"/>
            <end name="150" num="150"/>
            <end name="180" num="180"/>
            <end name="200" num="200"/>
        </steps>
    </param>

    <param name="widget_blender_engines" type="blender_engines" title="Text blender_engines" description="Tooltip blender_engines" group="group_3">
        <default>BLENDER</default>
    </param>

</effect>
```


#### class windows.models.renders_model.ListPluginsModel(blender_version)


![image](classes/ListPluginsModel.png)

This class create a model object list of render plugins for animations 3D titles window.


* **Parameters**

    * **model** (*QStandardItemModel*) – model treeview.

    * **model_paths** ([*dict*](https://docs.python.org/3/library/stdtypes.html#dict)) – paths to files render plugins configuration items.



#### update_model(clear=True)
Upgrade list of render plugins


#### class windows.models.renders_model.ParametersPluginModel(configuration_plugin, render_version)


![image](classes/ParametersPluginModel.png)

Model of parameters for a render plugin

> configuration_plugin : configuration of render plugin

> render_version : version of compatible render application


#### getConfigurationParameters(current_frame=None)
Get dict from active configuration parameters plugin


#### getRealsFiles(parameters)
Return list of reals files from blender plugin config

## Classes Views Window Animated 3D Title plugins


#### class windows.views.renders_plugins_views.ListPluginsView(title3DWindow)


![image](classes/ListPluginsView.png)

Widget TreeView List plugins used on the animated title window


#### currentChanged(selected, deselected)
Get new config from blender plugin


#### class windows.views.renders_plugins_views.RendersPluginsViews(animated_3d_title_manager)


![image](classes/RendersPluginsViews.png)

Class create window dialog Animated 3D titles gesture


* **Parameters**

    **animated_3d_title_manager** (*QObject*) – Manager Animated 3D Title plugins



* **Variables**

    * **thread_manager** (*ThreadsManager*) – Gesture of threaded renders

    * **plugins_manager** (*PluginsManager*) – Gesture of plugins

    * **control_plugin** – 

    * **list_plugins_model** – 

    * **selected_template** – 

    * **preview_protected** – 

    * **preview_unprotected** – 

    * **ui_path** – Path to file define ui dialog

    * **btn_cancel** – Cancel windows Animated 3D Title gesture plugins



* **Itype control_plugin**



* **Itype list_plugins_model**



* **Itype selected_template**



* **Itype preview_protected**



* **Itype preview_unprotected**



* **Itype ui_path**

    string



* **Itype btn_cancel**

    QPushButton




![image](pictures/TaskPreviewPluginWidthTextTransFormAnimation.png)


#### activeConfigurationPlugin(control_widget)
Add widgets for configuration plugin to Configuration view


#### btnRefresh_clicked(checked)
Regenerate preview picture


#### createControlConfigurationPlugin(id_task, list_running_plugins)
Create control for active configuration Blender plugin


#### disable_interface_preview()
Disable all controls on interface preview


#### enable_interface_preview()
Enable all controls on interface preview


#### get_3d_plugins_list()
Get list of 3D plugin animated


#### hideConfigPluginWidget()
Hide widget configuration active Blender plugin


#### refresh_list_plugin_view()
Update list of Blender plugins


#### reject()
Exit ask from Cancel button


#### removeControlConfigurationPlugin(control_widget)
Remove control for a configuration plugin


#### setCountPreviewControl(startframe, endframe)
Set label count preview control


#### setLabelPreviewControl(frame)
Set label preview control


#### setPreviewPicture(image_path)
Set picture preview from path file


#### setProgessRenderControl(id_task, percent=None, activepart=None, maxparts=None, style=None, busy=False)
Set value of progress render control


#### showConfigPluginWidget()
Show widget configuration active Blender plugin


#### sliderPreview_sliderReleased()
Get new value of preview slider, and generate preview picture


#### sliderPreview_valueChanged(new_value)
Get new value of preview slider, and generate preview picture


#### class windows.views.renders_plugins_views.ManagerPanelPluginsView(title3DWindow)


![image](classes/ManagerPanelPluginsView.png)

Widget gesture render state Blender plugins view



![image](pictures/MultiRenderPluginsGesture.png)


#### addTaskManagerToPlugin(name)
Add widget task manager for plugin


#### removeWidgetsFromConfigurationPluginView()
Remove configuration widgets into scrollArea from Blender plugins window


#### class windows.views.renders_plugins_views.TaskManagerPluginView(widget_parent, title3DWindow, name)


![image](classes/TaskManagerPluginView.png)

Widget tasks render Blender plugin view



![image](classes/TaskManagerPluginView.png)


#### clear_buttons()
Erase widgets button from QHBoxLayout


#### state_edit()
Show edit state widget


#### state_execute_render()
Show pause render state


#### state_pause_render()
Show pause render state


#### state_render()
Show edit state widget


#### state_renderedit()
Show edit state widget


#### state_waitconf()
Show edit state widget

## Classes Views Configurations Animated 3D Title plugin


#### class windows.views.renders_config_widgets.CreateWidgetsParametersRenderPlugin(id_task, list_running_plugins, render_version, settingsContainer, title3DWindow)


![image](classes/CreateWidgetsParametersRenderPlugin.png)

Class create Widgets from render config


* **Parameters**

    * **id_task** (*TaskPluginView*) – identifiant of task render

    * **list_running_plugins** – 

    * **render_version** – 

    * **settingsContainer** (*QWidget*) – Widget to put all params widgets to configure the render plugin

    * **title3DWindow** (*RendersPluginsViews*) – Windows of Animated 3D Title plugins


Create type of widget:

> * rangeframes (min, start, end, max, stepsstart, stepsend)

> * blender_engines (blender, cycle, eevee, povray, game)

> * boolean (on_active_show, on_active_hide, on_inactive_show, on_inactive_hide)

> * text

> * multiline

> * dropdown (sort: lineal/reverse/label_lineal/label_reverse)

> * spinner (min, max, step, data_type: int/float)

> * color (return dict {‘rgb’, ‘rgba’, ‘cmyk’, ‘hsv’, ‘hsl’})

> * file

> * font

> * picture

> * sound

> * movie

> * group (type: static)

> * booleangroup (type: selectable/on/off/enabled/disabled/active/inactive, state, on_active_show, on_active_hide, on_inactive_show, on_inactive_hide)


#### class windows.views.renders_config_widgets.BlenderEnginesWidget(param, blender_version, title3DWindow, params)


![image](classes/BlenderEnginesWidget.png)

Class Widget Blender render


#### render_index_changed(widget, param, index)
List of renders index change


#### update_widget(param)
Update visibility state from dependants widgets


#### class windows.views.renders_config_widgets.BooleanWidget(param, title3DWindow, params)


![image](classes/BooleanWidget.png)

Class Widget boolean



![image](pictures/WidgetBoolean1.png)



![image](pictures/WidgetBoolean2.png)

Conditional view widgets



![image](pictures/BooleanSwitchConditionnalWidgetVisible1.png)



![image](pictures/BooleanSwitchConditionnalWidgetVisible2.png)


#### boolean_changed(widget, param, index)
Change boolean state


#### update_widget(param)
Update visibility state from dependants widgets


#### class windows.views.renders_config_widgets.BooleanGroupWidget(group, title3DWindow)


![image](classes/BooleanGroupWidget.png)

Class Widget boolean switch of group widgets



![image](pictures/BooleanGroupWidgetFeature1.png)



![image](pictures/BooleanGroupWidgetFeature2.png)


#### class windows.views.renders_config_widgets.ColorWidget(param, title3DWindow)


![image](classes/ColorWidget.png)

Class Widget color



![image](pictures/WidgetColor1.png)



![image](pictures/WidgetColor2.png)


#### color_button_clicked(widget, param, index)
Change color selected


#### class windows.views.renders_config_widgets.DropdownWidget(param, title3DWindow)


![image](classes/MultilineWidget.png)

Class Widget dropdown



![image](pictures/WidgetDropdown.png)


#### dropdown_index_changed(widget, param, index)
Change list index select


#### class windows.views.renders_config_widgets.FileWidget(param, title3DWindow)


![image](classes/FileWidget.png)

Class Widget file



![image](pictures/WidgetChooseFile.png)



![image](pictures/WidgetChooseFileWindow.png)


#### file_index_Activated(widget, param)
Change file selected


#### class windows.views.renders_config_widgets.FontWidget(param, title3DWindow)


![image](classes/FontWidget.png)

Class Widget font



![image](pictures/FontSelectWidget.png)



![image](pictures/FontSelectWidgetWindowChooseFont.png)


#### font_index_Activated(widget, param)
Load new movie file


#### class windows.views.renders_config_widgets.GroupWidget(group, title3DWindow, listgroups, params)


![image](classes/GroupWidget.png)

Class Widget group



![image](pictures/GroupWidgetFeature.png)


#### group_changed(group, index)
Change group state


#### update_widget(group)
Update visibility state from dependants widgets


#### class windows.views.renders_config_widgets.LabelWidget(param, title3DWindow)


![image](classes/LabelWidget.png)

Class plugin configuration Widget label



![image](pictures/WidgetLabel.png)


#### class windows.views.renders_config_widgets.MovieWidget(param, title3DWindow)


![image](classes/MovieWidget.png)

Class Widget movie



![image](pictures/WidgetMovie1.png)



![image](pictures/WidgetMovie2.png)


#### movie_index_Activated(widget, param)
Load new movie file


#### class windows.views.renders_config_widgets.MultilineWidget(param, title3DWindow)


![image](classes/MultilineWidget.png)

Class Widget text multi line



![image](pictures/WidgetMultiline.png)


#### text_value_changed(widget, param, value=None)
Change text


#### class windows.views.renders_config_widgets.PictureWidget(param, title3DWindow)


![image](classes/PictureWidget.png)

Class Widget picture



![image](pictures/WidgetChoosePicture.png)



![image](pictures/WidgetChoosePictureWindow.png)


#### picture_index_Activated(widget, param)
Change selected picture


#### class windows.views.renders_config_widgets.RangeFramesWidget(param, title3DWindow)


![image](classes/RangeFramesWidget.png)

Class Widget frame



![image](pictures/RangeFrameWidget.png)


#### getListEndStepSlide()
Get list of end steps slide


* **Returns**

    End steps slide.



* **Return type**

    [tuple](https://docs.python.org/3/library/stdtypes.html#tuple)



#### getListStartStepSlide()
Get list of start steps slide


* **Returns**

    Start steps slide.



* **Return type**

    [tuple](https://docs.python.org/3/library/stdtypes.html#tuple)



#### on_change_value_slider_end_range(param, value)
Set end range slide


#### on_change_value_slider_start_range(param, value)
Set start range slide


#### resizeEvent(event)
Update widget when resized


#### setListEndStepSlide(listend=None)
Set list of end steps slide


* **Parameters**

    **listend** ([*list*](https://docs.python.org/3/library/stdtypes.html#list)) – List of step end range.



#### setListStartStepSlide(liststart)
Set list of start steps slide


* **Parameters**

    **liststart** ([*list*](https://docs.python.org/3/library/stdtypes.html#list)) – List of step start range.



#### class windows.views.renders_config_widgets.SoundWidget(param, title3DWindow)


![image](classes/SoundWidget.png)

Class Widget sound



![image](pictures/WidgetSound1.png)



![image](pictures/WidgetSound2.png)


#### sound_index_Activated(widget, param)
Load new sound file


#### class windows.views.renders_config_widgets.SpinnerDoubleWidget(param, title3DWindow)


![image](classes/SpinnerDoubleWidget.png)

Class Widget spinner double



![image](pictures/WidgetSpinnerFloat.png)


#### spinner_value_changed(param, value)
Change value spinner


#### class windows.views.renders_config_widgets.SpinnerIntWidget(param, title3DWindow)


![image](classes/SpinnerIntWidget.png)

Class Widget spinner int



![image](pictures/WidgetSpinnerFloat.png)


#### spinner_value_changed(param, value)
Change value spinner


#### class windows.views.renders_config_widgets.SpinnerWidget()


![image](classes/SpinnerWidget.png)

Class Widget spinner



![image](pictures/WidgetSpinnerFloat.png)


#### class windows.views.renders_config_widgets.TextWidget(param, title3DWindow)


![image](classes/TextWidget.png)

Class Widget text



![image](pictures/WidgetText.png)


#### text_value_changed(widget, param, value=None)
Change text


#### class windows.views.renders_config_widgets.OpenFileConfig(parentWindow, parameters)


![image](classes/OpenFileConfig.png)

Test real present files from blender plugin xml configs


#### getRealsFiles()
Return list of reals files from blender plugin xml parameters


#### class windows.views.renders_config_widgets.OpenFontFile(param, widget, title3DWindow)


![image](classes/OpenFontFile.png)

Open font file widget with test player


#### chooseFont()
Return selected file font


#### chooseFontFromList(item)
Font file choose from list


#### closeEvent(event)
Exit from close title3DWindow choose font


#### getFileFont()
Get font file selected


#### importSystemFonts()
Import fonts files from system


#### initUI()
Add title3DWindow UI


#### openFontFile()
Open file font


#### previewFont(fontname)
Font file choose from list


#### reject()
Exit from button cancel choose font title3DWindow


#### stylePreviewFont(fontstyle)
Font file choose from list


#### updateFontsList(default=None)
Update list of fonts and default font


#### class windows.views.renders_config_widgets.OpenMovieFile(param, widget, title3DWindow)


![image](classes/OpenMovieFile.png)

Open movie file widget with test player


#### addControls()
Add widgets to QMainWindow object


#### changeVolume(value)
Set volume player from slider volume


#### chooseMovie()
Return selected file movie


#### clearhandler()
Stop play movie


#### closeEvent(self, QCloseEvent)

#### durationChanged(duration)
Update range from media duration movie


#### getFileMovie()
Get movie file from selected movie with GUI


#### initUI()
Add menu bar and defaults sets to QMainWindow object


#### movieChanged(media)
Set select movie, and display into status bar to OpenMovieFile widget


#### openFile()
Open file movie


#### playStateChanged(state)
Set state of play movie : play or pause


#### playhandler()
Play or pause movie if playing


#### positionChanged(position)
Set position slider from movie position play


#### setPosition(position)
Set position movie play from slider


#### stophandler()
Stop play movie


#### class windows.views.renders_config_widgets.OpenSoundFile(param, widget, title3DWindow)


![image](classes/OpenSoundFile.png)

Open sound file widget with test player


#### addControls()
Add widgets to QMainWindow object


#### changeVolume(value)
Set volume player from slider volume


#### chooseSong()
Return selected file song


#### clearhandler()
Stop play music


#### closeEvent(self, QCloseEvent)

#### durationChanged(duration)
Update range from media duration song


#### getFileSound()
Get sound file from selected sound with GUI


#### initUI()
Add menu bar and defaults sets to QMainWindow object


#### openFile()
Open file song


#### playStateChanged(state)
Set state of play song : play or pause


#### playhandler()
Play or pause music if playing


#### positionChanged(position)
Set position slider from song position play


#### setPosition(position)
Set position song play from slider


#### songChanged(media)
Set select song, and display into status bar to OpenSoundFile widget


#### stophandler()
Stop play music
