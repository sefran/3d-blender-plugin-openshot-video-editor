#!/bin/bash

echo generate diagrams classes
( exec "./creatediagrams.bash" )

echo create docs files
echo create html
make html
echo create latex
make latex > /dev/null
echo create epub
make epub
echo create pdf
make latexpdf > /dev/null
echo create man
make man
echo create texinfo
make texinfo
echo create plein text
make text
echo create xml
make xml
echo create info
make info
echo create markdown
make markdown

cp ./build/markdown/index.md ../README.md
sed -i 's/classes\//docs\/source\/classes\//g' ../README.md
sed -i 's/pictures\//docs\/source\/pictures\//g' ../README.md
