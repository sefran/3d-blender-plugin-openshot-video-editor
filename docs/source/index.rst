.. Openshot Render 3D Title Gesture documentation master file, created by
   sphinx-quickstart on Wed Aug 28 17:15:10 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Welcome to Openshot Render 3D Title Gesture's documentation!

    .. image:: pictures/OpenShotBlender3DTitlePluginsGesture.png
        :alt: Look like
        :align: center
        :width: 500px

How to create Animated 3D plugin
================================

Minimal 3D Title Animation plugin
---------------------------------

    Name: name_blender_file_plugin.xml
    ----------------------------------
    
    Minimal xml file to put into path_to_openshot_directory/openshot_folder/blender
    
    .. code-block:: XML

        <?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <!DOCTYPE openshot-effect>
        <effect>
            <title translatable="True">User text plugin</title>
            <description translatable="True">Tooltip text plugin</description>
            <icon>icon_plugin.png</icon>
            <category>Video</category>
            <plugintype>Type animation plugin</plugintype>
            <render name="blender">
                <service>name_blender_file_plugin.blend</service>
                <engine>blender</engine>
            </render>
            <param name="widget_blender_engines" type="blender_engines" title="Text blender_engines" description="Tooltip blender_engines" visibility="hidden">
                <default>BLENDER</default>
            </param>
            <param name="widget_range_frames" type="rangeframes" title="Blender Animation Range Frames" description="Tooltip rangeframes" visibility="hidden">
                <min>1</min>
                <max>200</max>
                <start>1</start>
                <end>200</end>
                <step>1</step>
                <default>100</default>
            </param>
        </effect>

    .. raw:: latex
    
        \clearpage
    
    
    Name: name_blender_file_plugin.py
    ---------------------------------

    Minimal python script file to path_to_openshot_directory/openshot_folder/blender/scripts
    
    .. code-block:: python
    
        #	OpenShot Video Editor is a program that creates, modifies, and edits video files.
        #   Copyright (C) 2009  Jonathan Thomas
        #
        #	This file is part of OpenShot Video Editor (http://launchpad.net/openshot/).
        #
        #	OpenShot Video Editor is free software: you can redistribute it and/or modify
        #	it under the terms of the GNU General Public License as published by
        #	the Free Software Foundation, either version 3 of the License, or
        #	(at your option) any later version.
        #
        #	OpenShot Video Editor is distributed in the hope that it will be useful,
        #	but WITHOUT ANY WARRANTY; without even the implied warranty of
        #	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        #	GNU General Public License for more details.
        #
        #	You should have received a copy of the GNU General Public License
        #	along with OpenShot Video Editor.  If not, see <http://www.gnu.org/licenses/>.

    .. code-block:: python

        # Debug Info:
        # ./blender -b test.blend -P demo.py
        # -b = background mode
        # -P = run a Python script within the context of the project file

    .. code-block:: python
    
        ###########################################################
        #####                Get python library               #####
        # Import Blender's python API.  This only works when the script is being
        # run from the context of Blender.  Blender contains it's own version of Python
        # with this library pre-installed.
        import bpy, colorsys, os, sys
        from math import pi, radians
        #####           End Import Get python library         #####
        ###########################################################

    .. raw:: latex
    
        \clearpage
    
    .. code-block:: python
    
        ###########################################################
        #####            Get python Blender library           #####
        directory = os.path.dirname(bpy.data.filepath)[:-5] + 'scripts/openshot'
        if not directory in sys.path:
            sys.path.append(directory)
        from debug import gest_blender_error
        #####          End Get python Blender library         #####
        ###########################################################

    .. code-block:: python
    
        ###########################################################
        #####            Openshot parameters plugin           #####
        # Init all of the variables needed by this script.
        params = {}
        # Specific Blender python script parameters
        # put yours specifics variables Blender script like params['My param'] = Value 
        # Because Blender executes
        # this script, OpenShot will inject a dictionary of the required parameters
        # before this script is executed.
        #INJECT_PARAMS_HERE
        # The remainder of this script will modify the current Blender .blend project
        # file, and adjust the settings.  The .blend file is specified in the XML file
        # that defines this template in OpenShot.
        #####          End Openshot parameters plugin         #####
        ###########################################################

    .. code-block:: python

        ###########################################################
        #####              Blender errors gesture             #####
        # interpretor errors code gesture
        def excepthook(type, value, traceback):
            sys.exit(gest_blender_error(params, sys.exc_info(), 'Error script code', params["output_path"] + "debug.txt"))
        sys.excepthook = excepthook
        #####            End Blender errors gesture           #####
        ###########################################################

    .. code-block:: python

        # Init render engine
        if params["scene_render"] == 'CYCLES':
            bpy.context.scene.render.engine = 'CYCLES'
        elif params["scene_render"] == 'BLENDER_GAME':
            bpy.context.scene.render.engine = 'BLENDER_GAME'
        elif params["scene_render"] == 'POVRAY_RENDER':
            bpy.context.scene.render.engine = 'POVRAY_RENDER'
        elif params["scene_render"] == 'EEVEE':
            bpy.context.scene.render.engine = 'EEVEE'
        else: #if params["scene_render"] == 'BLENDER'
            if bpy.app.version < (2, 80, 0):
                bpy.context.scene.render.engine = 'BLENDER_RENDER'
            else:
                bpy.context.scene.render.engine = 'BLENDER_WORKBENCH'

    .. code-block:: python

        ###########################################################
        #####      Define Blender python script functions     #####
        flag_error = True
        try:
            flag_error = False
        except Exception as E:
            gest_blender_error(params, sys.exc_info(), 'Error define blender python functions', params["output_path"] + "debug.txt", E)
        #####    End Define Blender python script functions   #####
        ###########################################################

    .. code-block:: python

        ###########################################################
        #####             Start create scene plugin           #####
        #print(str(params), file=open(os.path.join(os.path.expanduser("~"), "debug.txt"), "a"))
        if not flag_error:
            #-----------------------------------------------------#
            #----                  Clean Scene                ----#
            flag_error = True
            try:
                flag_error = False
                pass
            except Exception as E:
                gest_blender_error(params, sys.exc_info(), 'Error clean scene', params["output_path"] + "debug.txt", E)
            #----                End Clean Scene              ----#
            #-----------------------------------------------------#
        if not flag_error:
            #-----------------------------------------------------#
            #----                  3D Modeling                ----#
            flag_error = True
            try:
               # Get font object
                flag_error = False
            except Exception as E:
                gest_blender_error(params, sys.exc_info(), 'Error 3d modeling', params["output_path"] + "debug.txt", E)
            #----                End 3D Modeling              ----#
            #-----------------------------------------------------#
        if not flag_error:
            #-----------------------------------------------------#
            #----                  UV Mapping                 ----#
            flag_error = True
            try:
                flag_error = False
            except Exception as E:
                gest_blender_error(params, sys.exc_info(), 'Error uv mapping', params["output_path"] + "debug.txt", E)
            #----                End UV Mapping               ----#
            #-----------------------------------------------------#
        if not flag_error:
            #-----------------------------------------------------#
            #----             Texturing and Shaders           ----#
            flag_error = True
            try:
                flag_error = False
            except Exception as E:
                gest_blender_error(params, sys.exc_info(), 'Error texturing and shading', params["output_path"] + "debug.txt", E)
            #----           End Texturing and Shaders         ----#
            #-----------------------------------------------------#
        if not flag_error:
            #-----------------------------------------------------#
            #----                    Rigging                  ----#
            flag_error = True
            try:
                flag_error = False
            except Exception as E:
                gest_blender_error(params, sys.exc_info(), 'Error rigging', params["output_path"] + "debug.txt", E)
            #----                  End Rigging                ----#
            #-----------------------------------------------------#
        if not flag_error:
            #-----------------------------------------------------#
            #----                   Animation                 ----#
            flag_error = True
            try:
                flag_error = False
            except Exception as E:
                gest_blender_error(params, sys.exc_info(), 'Error animation', params["output_path"] + "debug.txt", E)
            #----                 End Animation               ----#
            #-----------------------------------------------------#
        if not flag_error:
            #-----------------------------------------------------#
            #----                   Lighting                  ----#
            flag_error = True
            try:
                flag_error = False
            except Exception as E:
                gest_blender_error(params, sys.exc_info(), 'Error lightning', params["output_path"] + "debug.txt", E)
            #----                 End Lighting                ----#
            #-----------------------------------------------------#
        if not flag_error:
            #-----------------------------------------------------#
            #----                Camera Setting               ----#
            flag_error = True
            try:
                flag_error = False
            except Exception as E:
                gest_blender_error(params, sys.exc_info(), 'Error camera setting', params["output_path"] + "debug.txt", E)
            #----              End Camera Setting             ----#
            #-----------------------------------------------------#
        if not flag_error:
            #-----------------------------------------------------#
            #----                  Renderring                 ----#
            flag_error = True
            try:
                flag_error = False
            except Exception as E:
                gest_blender_error(params, sys.exc_info(), 'Error set renderring', params["output_path"] + "debug.txt", E)
            #----                End Renderring               ----#
            #-----------------------------------------------------#
        if not flag_error:
            #-----------------------------------------------------#
            #----           Compositing & Special VFX         ----#
            flag_error = True
            try:
                flag_error = False
            except Exception as E:
                gest_blender_error(params, sys.exc_info(), 'Error compositing ans special vfx', params["output_path"] + "debug.txt", E)
            #----         End Compositing & Special VFX       ----#
            #-----------------------------------------------------#
        if not flag_error:
            #-----------------------------------------------------#
            #----                Music and Foley              ----#
            flag_error = True
            try:
                flag_error = False
            except Exception as E:
                gest_blender_error(params, sys.exc_info(), 'Error music and foley', params["output_path"] + "debug.txt", E)
            #----              End Music and Foley            ----#
            #-----------------------------------------------------#
        #####              End create scene plugin            #####
        ###########################################################

    .. code-block:: python

        ##############################################################################
        #####                      Editing and Final output                      #####
        # Set the render options.  It is important that these are set
        # to the same values as the current OpenShot project.  These
        # params are automatically set by OpenShot
        # Render the current animation to the params["output_path"] folder
        bpy.context.scene.render.resolution_x = params["resolution_x"]
        bpy.context.scene.render.resolution_y = params["resolution_y"]
        bpy.context.scene.render.resolution_percentage = params["resolution_percentage"]
        #bpy.context.scene.render.quality = params["quality"]
        if flag_error or not params["animation"] :
            # Render the current animation to the params["output_path"] folder
            bpy.context.scene.render.filepath = params["output_path"] + str(params["current_frame"])
            if flag_error:
                params["current_frame"] = params["start_frame"]
                try:
                    bpy.context.scene.render.file_format = 'PNG'
                except:
                    bpy.context.scene.render.image_settings.file_format = 'PNG'
            bpy.data.scenes[0].frame_current = params["current_frame"]
            bpy.data.scenes[0].frame_start = params["current_frame"]
            bpy.data.scenes[0].frame_end = params["current_frame"]
            bpy.ops.render.render(animation=params["animation"], write_still=True)
        else:
            # Render the current animation to the params["output_path"] folder
            bpy.context.scene.render.filepath = params["output_path"]
            bpy.context.scene.render.fps = params["fps"]
            bpy.data.scenes[0].frame_current = params['current_frame']
            bpy.data.scenes[0].frame_start = params['start_frame']
            bpy.data.scenes[0].frame_end = params['end_frame']
            # Animation Speed (use Blender's time remapping to slow or speed up animation)
            #bpy.context.scene.render.frame_map_new = bpy.context.scene.render.frame_map_old * int(params["animation_speed"])
            bpy.ops.render.render(animation=params["animation"])
        #####                    End Editing and Final output                    #####
        ##############################################################################


Blender plugin version gesture
------------------------------

    Valide plugin for Blender version ≥ 2.8

    .. code-block:: XML

        <render name="blender">
            <service>name_blender_file_plugin.blend</service>
            <engine>blender</engine>
            <min>2.8</min>
        </render>


    Valide plugin for Blender version ≤ 2.79

    .. code-block:: XML

        <render name="blender">
            <service>name_blender_file_plugin.blend</service>
            <engine>blender</engine>
            <max>2.79</max>
        </render>


    Valide plugin for Blender version > 2.79

    .. code-block:: XML

        <render name="blender">
            <service>name_blender_file_plugin.blend</service>
            <engine>blender</engine>
            <sup>2.79</sup>
        </render>


    Valide plugin for Blender version < 2.8

    .. code-block:: XML

        <render name="blender">
            <service>name_blender_file_plugin.blend</service>
            <engine>blender</engine>
            <inf>2.8</inf>
        </render>


    Multi validation plugin for Blender version < 2.8 and > 2.7

    .. code-block:: XML

        <render name="blender">
            <service>name_blender_file_plugin.blend</service>
            <engine>blender</engine>
            <sup>2.7</sup>
            <inf>2.8</inf>
        </render>


    Sample for plugin support Blender less than 2.8 version.

    .. image:: pictures/ListPluginsBlender2.7Compatibles.png
        :alt: Look like
        :align: center
        :width: 500px

    .. raw:: latex
    
        \clearpage
    
    Sample for plugins support Blender 2.8 version.

    .. image:: pictures/ListPluginsBlender2.8Compatibles.png
        :alt: Look like
        :align: center
        :width: 500px


Blender plugin engine gesture
-----------------------------

    Set list of blender engines choose

    .. code-block:: XML

        <render name="blender">
            <service>name_blender_file_plugin.blend</service>
            <engine>blender</engine>
            <engine>cycles</engine>
            <engine>eevee</engine>
        </render>
        
    Widget select engine render

    .. code-block:: XML

        <param name="widget_blender_engine_set" type="blender_engines" title="Blender engine" description="Choose Blender_engine">
            <default>CYCLES</default>
        </param>

    .. image:: pictures/WidgetBlenderEngineSelect.png
        :alt: Look like
        :align: center
        :width: 500px
    
    Features of hide/show widgets select case render
    
    .. code-block:: XML

        <param name="widget_blender_engine_set" type="blender_engines" title="Blender engine" description="Choose Blender_engine" on_select_show="blender:widget_1; cycles:widget_2; eevee:widget_2, widget_3" on_select_hide="blender:widget_2, widget_3; cycles:widget_1, widget_3; eevee:widget_1">
            <default>CYCLES</default>
        </param>


        
Blender plugin frames gesture
-----------------------------

    Widget select frames

    .. code-block:: XML

        <param name="widget_range_frames_set" type="rangeframes" title="Range Frames" description="Choose a range frames">
            <min>1</min>
            <max>200</max>
            <start>34</start>
            <end>163</end>
            <step>1</step>
            <default>50</default>
        </param>

    .. image:: pictures/RangeFrameWidget.png
        :alt: Look like
        :align: center
        :width: 500px

    Widget select frames by steps into a group 'group_render'

    .. code-block:: XML

        <groups>
            <group name="group_render" title="Render" type="static" description="Select render options"></group>
        </groups>
        <param name="widget_range_frames_set" type="rangeframes" title="Range Frames" description="Choose a range frames" group="group_render">
            <min>1</min>
            <max>240</max>
            <start>50</start>
            <end>200</end>
            <step>1</step>
            <steps>
                <start name="0" num="0"/>
                <start name="50" num="50"/>
                <start name="80" num="80"/>
                <start name="150" num="150"/>
                <end name="80" num="80"/>
                <end name="200" num="200"/>
                <end name="220" num="220"/>
                <end name="240" num="240"/>
            </steps>
            <default>100</default>
        </param>

    .. image:: pictures/RangeFrameWidgetByStep.png
        :alt: Look like
        :align: center
        :width: 500px


Blender plugin texts features
-----------------------------

    Widget Text
    
    .. code-block:: XML

        <param name="widget_text" type="text" title="Text label" description="Tooltip text">
            <default>DefaultText</default>
        </param>

    .. image:: pictures/WidgetText.png
        :alt: Look like
        :align: center
        :width: 500px
        
    Widget Text Multiline
    
    .. code-block:: XML

        <param name="widget_multiline_text" type="multiline" title="Multiline text label" description="Tooltip multiline text">
            <default>Default
            Multiline
            Text
            </default>
        </param>

    .. image:: pictures/WidgetMultiline.png
        :alt: Look like
        :align: center
        :width: 500px

Blender plugin booleans features
--------------------------------

    Widget Boolean
    
    .. code-block:: XML
    
        <param name="widget_boolean" type="boolean" title="Boolean Label" description="Tooltip boolean">
            <default>off</default>
        </param>

    .. image:: pictures/WidgetBoolean1.png
        :alt: Look like
        :align: center
        :width: 500px

    .. image:: pictures/WidgetBoolean2.png
        :alt: Look like
        :align: center
        :width: 500px
    
    Widget Group boolean
    
    .. code-block:: XML
    
        <groups>
            <group name="group_boolean" title="Group boolean" type="off"  description="Tooltipn group boolean"></group>
        </groups>
    
    .. image:: pictures/BooleanGroupWidgetFeature1.png
        :alt: Look like
        :align: center
        :width: 500px
    
    .. image:: pictures/BooleanGroupWidgetFeature2.png
        :alt: Look like
        :align: center
        :width: 500px
    
    Conditionnal boolean choice
    
    .. code-block:: XML
    
        <param name="widget_boolean" type="boolean" title="Label boolean" description="Tooltip boolean" on_active_show="widget2" on_active_hide="widget1" on_inactive_show="widget1" on_inactive_hide="widget2">
            <default>off</default>
        </param>
        <param name="widget1" type="any_type_off_widget" title="Label of widget1" description="Tooltip of widget1">
            <values>
                <value name="" num=""/>
            </values>
            <default></default>
        </param>
        <param name="widget2" type="any_type_off_widget" title="Label of widget2" description="Tooltip of widget2">
            <values>
                <value name="" num=""/>
            </values>
            <default></default>
        </param>

    .. image:: pictures/BooleanSwitchConditionnalWidgetVisible1.png
        :alt: Look like
        :align: center
        :width: 500px
    
    .. image:: pictures/BooleanSwitchConditionnalWidgetVisible2.png
        :alt: Look like
        :align: center
        :width: 500px


Blender plugin Widget dropdown
------------------------------

    Define a dropdown list of selectables items
    
    .. code-block:: XML
    
        <param name="widget_dropdown" type="dropdown" title="Text dropdown" description="Tooltip dropdown">
            <values>
                <value name="Label item 1" num="item1"/>
                <value name="Label item 2" num="item2"/>
                <value name="Label item 3" num="item3"/>
            </values>
            <default>item2</default>
        </param>
        
    .. image:: pictures/WidgetDropdown.png
        :alt: Look like
        :align: center
        :width: 500px

    
Blender plugin Widget spinner
-----------------------------

    Choose a numerical spinner value.
    
    Example for integers

    .. code-block:: XML
    
        <param name="widget_spiner" type="spinner" title="Label spinner" description="Tooltip spinner" data_type="int">
            <min>0</min>
            <max>10</max>
            <step>1</step>
            <default>5</default>
        </param>

    or for float

    .. code-block:: XML
    
        <param name="widget_spiner" type="spinner" title="Label spinner" description="Tooltip spinner" data_type="float">
            <min>0.0</min>
            <max>10.0</max>
            <step>0.1</step>
            <default>0.5</default>
        </param>

    .. image:: pictures/WidgetSpinnerFloat.png
        :alt: Look like
        :align: center
        :width: 500px


Blender plugin Widget color
---------------------------

    Choose a color
    
    .. code-block:: XML
    
        <param name="widget_color" type="color" title="Label color" description="Tooltip color">
            <default>#B20000</default>
        </param>

    .. image:: pictures/WidgetColor1.png
        :alt: Look like
        :align: center
        :width: 500px

    .. image:: pictures/WidgetColor2.png
        :alt: Look like
        :align: center
        :width: 500px


Blender plugin Widget file
--------------------------

    Choose a file

    .. code-block:: XML
    
        <param name="widget_file" type="file" title="Label file" description="Tooltip file">
            <values>
                <value name="my_file1.txt" num=":folder/my_file.txt"/>
                <value name="my_file1.txt" num="/static_path/my_file.txt"/>
            </values>
            <default>my_file1.txt</default>
        </param>
        
    ':' character for path to blender plugins Openshot folder
    
    .. image:: pictures/WidgetChooseFile.png
        :alt: Look like
        :align: center
        :width: 500px

    .. image:: pictures/WidgetChooseFileWindow.png
        :alt: Look like
        :align: center
        :width: 500px

Blender plugin Widget font
--------------------------

    Choose a file font

    .. code-block:: XML

        <param name="widget_font" type="font" title="Lanel font" description="Tooltip font">
            <values>
                <value name="Leander.ttf" num=":fonts/Leander.ttf"/>
            </values>
            <default>Leander.ttf</default>
        </param>
        
    ':' character for path to blender plugins Openshot folder
    
    .. image:: pictures/FontSelectWidget.png
        :alt: Look like
        :align: center
        :width: 500px

    .. image:: pictures/FontSelectWidgetWindowChooseFont.png
        :alt: Look like
        :align: center
        :width: 500px
    

Blender plugin Widget picture
-----------------------------

    Choose a file picture

    .. code-block:: XML

        <param name="widget_picture" type="picture" title="Label picture" description="Tooltip picture">
            <values>
                <value name="My-picture.png" num=":pictures/My-picture.png"/>
            </values>
            <default>My-picture.png</default>
        </param>
        
    ':' character for path to blender plugins Openshot folder
    
    .. image:: pictures/WidgetChoosePicture.png
        :alt: Look like
        :align: center
        :width: 500px

    .. image:: pictures/WidgetChoosePictureWindow.png
        :alt: Look like
        :align: center
        :width: 500px


Blender plugin Widget sound
---------------------------

    Choose a file sound

    .. code-block:: XML

        <param name="widget_sound" type="sound" title="Label sound" description="Tooltip sound">
            <values>
                <value name="My-sound.ogg" num=":sounds/My-sound.png"/>
            </values>
            <default>My-sound.png</default>
        </param>
        
    ':' character for path to blender plugins Openshot folder
    
    .. image:: pictures/WidgetSound1.png
        :alt: Look like
        :align: center
        :width: 500px

    .. image:: pictures/WidgetSound2.png
        :alt: Look like
        :align: center
        :width: 500px


Blender plugin Widget movie
---------------------------

    Choose a file movie

    .. code-block:: XML

        <param name="widget_movie" type="movie" title="Label movie" description="Tooltip movie">
            <values>
                <value name="My-movie.mpeg" num=":movies/My-movie.mpeg"/>
            </values>
            <default>My-movie.mpeg</default>
        </param>
        
    ':' character for path to blender plugins Openshot folder
    
    .. image:: pictures/WidgetMovie1.png
        :alt: Look like
        :align: center
        :width: 500px

    .. image:: pictures/WidgetMovie2.png
        :alt: Look like
        :align: center
        :width: 500px


Python scripts gestures Animated 3D plugin
==========================================

Debuggin features
-----------------

    .. code-block:: python

        import bpy, platform, textwrap, traceback
        from math import pi, radians


    .. code-block:: python

        # Clear scene
        def erase_all_forms():
            """Erase all objects exept camera, lamps and speaker"""
            if bpy.app.version < (2, 80, 0):
                bpy.ops.wm.read_factory_settings()
            objects = bpy.context.scene.objects
            operation = bpy.ops.object
            operation.select_all(action='DESELECT')
            for obj in objects:
                if bpy.app.version < (2, 80, 0):
                    obj.select = obj.type in ['EMPTY', 'MESH', 'CURVE', 'SURFACE', 'META', 'FONT', 'ARMATURE', 'LATTICE']
                else:
                    obj.select_set(obj.type in ['EMPTY', 'MESH', 'CURVE', 'SURFACE', 'META', 'FONT', 'ARMATURE', 'LATTICE'])
            operation.delete(use_global=True)

    .. code-block:: python

        # Define camera for show error into blender
        def camera_error():
            """Capture text generated by print_error()"""
            camera = bpy.data.scenes["Scene"].camera
            camera.location.x = 10
            camera.location.y = 0
            camera.location.z = 0
            camera.data.angle = 50*(pi/180.0)
            camera.rotation_euler = (radians(90), 0, radians(90))

    .. code-block:: python

        # Define light for show error into blender
        def light_error():
            """Light text generated by print_error()"""
            lamp = bpy.data.objects["Sun"]
            lamp.data.energy = 300
            lamp.location.x = 4.0
            lamp.location.y = 1.0
            lamp.location.z = 6.0

    .. code-block:: python

        # Add debugging text to Blender width error
        def print_error(error_info, text_error):
            """Print error into generated picture"""
            erase_all_forms()
            # Scene set
            operate = bpy.ops.object
            if bpy.app.version < (2, 80, 0):
                operate.camera_add()
            bpy.context.scene.camera = bpy.data.objects['Camera']
            if bpy.app.version < (2, 80, 0):
                operate.lamp_add(type='SUN')
            else:
                operate.light_add(type='SUN')
            light_error()
            camera_error()
            if bpy.app.version < (2, 80, 0):
                bpy.data.worlds[0].horizon_color = [0.9254901960784314, 0.9254901960784314, 0.9254901960784314]
            # Message error info
            operate.text_add()
            obj_text = bpy.data.objects["Text"]
            text = obj_text.data
            #exc_type = str(error_info[0])
            tb = traceback.extract_tb(error_info[2])[-1]
            message = 'ERROR\n' + platform.dist()[1] + ' ' + platform.architecture()[0] + '\n' + platform.system() + ' ' + platform.release() + '\n' + 'Python ' + platform.python_version() + '\n' + 'Blender ' + bpy.app.version_string + '\nFile: ' + tb[0].split('/')[-1] + ' Line: ' + str(tb[1]) + '\n' + "\n".join(textwrap.wrap(text_error,40))
            text.body = message
            text.size=0.5
            text.align_x = 'CENTER'
            text.align_y = 'CENTER'
            operate.origin_set(type='ORIGIN_GEOMETRY')
            obj_text.location.x = 0.0
            obj_text.location.y = 0.0
            obj_text.location.z = 0.0
            obj_text.rotation_euler = (radians(90), 0, radians(90))

    .. code-block:: python

        # Blender error gesture
        def gest_blender_error(params, error, text_error, path_error, E=None):
            """Blender errors gesture"""
            if E:
                print_error(error, text_error + ': <' + str(E) + '>')
            else:
                print_error(error, text_error)
            tb = traceback.extract_tb(error[2])[-1]
            print(text_error, file=open(path_error, "a"))
            if E:
                print('File ' + tb[0].split('/')[-1] + ' Line=' + str(tb[1]) + ' :' + str(E), file=open(path_error, "a"))
            else:
                print('File ' + tb[0].split('/')[-1] + ' Line=' + str(tb[1]), file=open(path_error, "a"))
            print(str(params), file=open(path_error, "a"))

    
    How to use ?
    ------------
    
    Example of debug blender render code use
    
    .. code-block:: python

        directory = os.path.dirname(bpy.data.filepath)[:-5] + 'scripts/openshot'
        if not directory in sys.path:
            sys.path.append(directory)
        from debug import gest_blender_error
        flag_error = False
        if not flag_error:
            flag_error = True
            try:
                # Your job script
                flag_error = False
            except Exception as E:
                gest_blender_error(params, sys.exc_info(), 'Error clean scene', params["output_path"] + "debug.txt", E)
    
    This code generate a picture error Blender information and put a debuggin file into temporry folder pluggin
    

Blender features
----------------


Modules OpenShot Animated 3D Title plugins
==========================================

.. automodule:: windows.animated_title
   :undoc-members:
       
--------------------------------------

.. automodule:: windows.controlers.renders_worker
   :undoc-members:

--------------------------------------

.. automodule:: windows.models.renders_model
   :undoc-members:

--------------------------------------

.. automodule:: windows.views.renders_plugins_views
   :undoc-members:

--------------------------------------

.. automodule:: windows.views.renders_config_widgets
   :undoc-members:

--------------------------------------


Classes OpenShot Animated 3D Title plugins
==========================================


Class Manager Animated 3D Title plugins
---------------------------------------

.. autoclass:: windows.animated_title.Animated3DTitleManager
    :members:



Classes Controlers Animated 3D Title plugins
--------------------------------------------

.. autoclass:: windows.controlers.threads_worker.ThreadsManager
    :members:

    
.. autoclass:: windows.controlers.threads_worker.Worker
    :members:

    
.. autoclass:: windows.controlers.threads_worker.WorkerSignals
    :members:

    
.. autoclass:: windows.controlers.renders_worker.PluginsManager
    :members:

    
.. autoclass:: windows.controlers.renders_worker.ManageTaskViewPlugin
    :members:


.. autoclass:: windows.controlers.blender_worker.QBlenderEvent
    :members:

    
.. autoclass:: windows.controlers.blender_worker.BlenderRequest
    :members:



Classes Datas Models 3D Animated Title plugins
----------------------------------------------

.. autoclass:: windows.models.renders_model.ConfigurationPluginModel
    :members:

    
.. autoclass:: windows.models.renders_model.ListPluginsModel
    :members:

    
.. autoclass:: windows.models.renders_model.ParametersPluginModel
    :members:



Classes Views Window Animated 3D Title plugins
----------------------------------------------

.. autoclass:: windows.views.renders_plugins_views.ListPluginsView
    :members:

    
.. autoclass:: windows.views.renders_plugins_views.RendersPluginsViews
    :members:

    
.. autoclass:: windows.views.renders_plugins_views.ManagerPanelPluginsView
    :members:


.. autoclass:: windows.views.renders_plugins_views.TaskManagerPluginView
    :members:

    

Classes Views Configurations Animated 3D Title plugin
-----------------------------------------------------

.. autoclass:: windows.views.renders_config_widgets.CreateWidgetsParametersRenderPlugin
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.BlenderEnginesWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.BooleanWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.BooleanGroupWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.ColorWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.DropdownWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.FileWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.FontWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.GroupWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.LabelWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.MovieWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.MultilineWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.PictureWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.RangeFramesWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.SoundWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.SpinnerDoubleWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.SpinnerIntWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.SpinnerWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.TextWidget
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.OpenFileConfig
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.OpenFontFile
    :members:

    
.. autoclass:: windows.views.renders_config_widgets.OpenMovieFile
    :members:


.. autoclass:: windows.views.renders_config_widgets.OpenSoundFile
    :members:
    
