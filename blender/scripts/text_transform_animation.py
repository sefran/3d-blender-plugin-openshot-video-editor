#	OpenShot Video Editor is a program that creates, modifies, and edits video files.
#   Copyright (C) 2009  Jonathan Thomas
#
#	This file is part of OpenShot Video Editor (http://launchpad.net/openshot/).
#
#	OpenShot Video Editor is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	OpenShot Video Editor is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with OpenShot Video Editor.  If not, see <http://www.gnu.org/licenses/>.


# Debug Info:
# ./blender -b test.blend -P demo.py
# -b = background mode
# -P = run a Python script within the context of the project file


##############################################################################
#####                         Get python library                         #####
# Import Blender's python API.  This only works when the script is being
# run from the context of Blender.  Blender contains it's own version of Python
# with this library pre-installed.
import bpy, colorsys, os, sys, traceback
from math import pi, radians
#####                    End Import Get python library                   #####
##############################################################################

##############################################################################
#####                     Get python Blender library                     #####
directory = os.path.dirname(bpy.data.filepath)[:-5] + 'scripts/openshot'
if not directory in sys.path:
    sys.path.append(directory)

from debug import gest_blender_error
#####                   End Get python Blender library                   #####
##############################################################################


##############################################################################
#####                     Openshot parameters plugin                     #####
params = {}

# Init all of the variables needed by this script.  Because Blender executes
# this script, OpenShot will inject a dictionary of the required parameters
# before this script is executed.
#INJECT_PARAMS_HERE

# The remainder of this script will modify the current Blender .blend project
# file, and adjust the settings.  The .blend file is specified in the XML file
# that defines this template in OpenShot.
#####                   End Openshot parameters plugin                   #####
##############################################################################


##############################################################################
#####               Define Blender plugin script functions               #####
flag_error = True
try:
    # Clear scene
    def erase_all_scene():
        """Erase all objects"""
        if bpy.ops.object.mode_set.poll():
            bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')
        for obj in bpy.context.scene.objects:
            obj.select = obj.type in ['EMPTY', 'MESH', 'CURVE', 'DATA', 'SURFACE', 'META', 'FONT', 'ARMATURE', 'LATTICE', 'CAMERA', 'LAMP', 'PLANE', 'SPEAKER']
        bpy.ops.object.delete(use_global=True)
        for material in bpy.data.materials:
            bpy.data.materials.remove(material)
    # Set camera
    def set_camera(camera, position_camera=(10, 0, 0), rotation_camera=(90, 0, 90), angle_camera=50):
        """Capture text generated"""
        camera.location.x = position_camera[0]
        camera.location.y = position_camera[1]
        camera.location.z = position_camera[2]
        camera.data.angle = angle_camera*(pi/180.0)
        camera.rotation_euler = (radians(rotation_camera[0]), radians(rotation_camera[1]), radians(rotation_camera[2]))
    def left_camera():
        set_camera(camera, (-5, 0, 0), (90, 0, -90), 50) # axe left view
    def front_left_camera():
        set_camera(camera, (-3.5, -3, 0), (90, 0, -45), 50) # axe -x-y front left
    def front_camera():
        set_camera(camera, (0, -5, 0), (90, 0, 0), 50) # axe -y front view
    def front_right_camera():
        set_camera(camera, (3.5, -3, 0), (90, 0, 45), 50) # axe x-y front right
    def right_camera():
        set_camera(camera, (5, 0, 0), (90, 0, 90), 50) # axe x right view
    def back_right_camera():
        set_camera(camera, (3.5, 3, 0), (90, 0, 135), 50) # axe xy back right
    def back_camera():
        set_camera(camera, (0, 5, 0), (90, 0, 180), 50) # axe y back view
    def back_left_camera():
        set_camera(camera, (-3.5, 3, 0), (90, 0, -135), 50) # axe -xy back left
    def top_camera():
        set_camera(camera, (0, 0, 5), (0, 0, 0), 50) # axe z top view
    def bottom_camera():
        set_camera(camera, (0, 0, -5), (180, 0, 0), 50) # axe z bottom view
    def left_top_camera():
        set_camera(camera, (-5, 0, 3), (55, 0, -90), 50) # axe -x left top
    def front_left_top_camera():
        set_camera(camera, (-5, -5, 3), (65, 0, -45), 50) # axe -x-y front left top
    def front_left_bottom_camera():
        set_camera(camera, (-5, -5, -3), (113, 0, -45), 50) # axe -x fron left bottom
    def front_right_top_camera():
        set_camera(camera, (5, -5, 3), (65, 0, 45), 50) # axe x-y front right top
    def front_right_bottom_camera():
        set_camera(camera, (5, -5, -3), (113, 0, 45), 50) # axe xy fron right bottom
    def front_top_camera():
        set_camera(camera, (0, -5, 3), (60, 0, 0), 50) # axe x-y front top
    def front_bottom_camera():
        set_camera(camera, (0, -5, -3), (120, 0, 0), 50) # axe xy front bottom
    def my_camera():
        set_camera(camera, (-4, -3.5, 0), (90, 0, -45), 50) # my view
    def animation_camera():
        """animation camera set positions"""
        camera.animation_data_create()
        camera.animation_data.action = bpy.data.actions.new(name="MyCamera")
        fcu_camera_position_x = camera.animation_data.action.fcurves.new(data_path='location', index=0)
        fcu_camera_position_y = camera.animation_data.action.fcurves.new(data_path='location', index=1)
        fcu_camera_position_z = camera.animation_data.action.fcurves.new(data_path='location', index=2)
        fcu_camera_position_x.keyframe_points.add(4)
        fcu_camera_position_y.keyframe_points.add(4)
        fcu_camera_position_z.keyframe_points.add(4)
        fcu_camera_position_x.keyframe_points[0].co = 0, -5
        fcu_camera_position_y.keyframe_points[0].co = 0, 0
        fcu_camera_position_z.keyframe_points[0].co = 0, 3
        fcu_camera_position_x.keyframe_points[1].co = 50, 0
        fcu_camera_position_y.keyframe_points[1].co = 50, -5
        fcu_camera_position_z.keyframe_points[1].co = 50, 0
        fcu_camera_position_x.keyframe_points[2].co = 220, 0
        fcu_camera_position_y.keyframe_points[2].co = 220, -5
        fcu_camera_position_z.keyframe_points[2].co = 220, 0
        fcu_camera_position_x.keyframe_points[3].co = 240, 75
        fcu_camera_position_y.keyframe_points[3].co = 240, -75
        if bool(params['floor_on_off']):
            fcu_camera_position_z.keyframe_points[3].co = 240, 3
        else:
            fcu_camera_position_z.keyframe_points[3].co = 240, -3
        #smooth curve from keyframes
        fcu_camera_position_x.update()
        fcu_camera_position_y.update()
        fcu_camera_position_z.update()
        # Camera track Text_begin
        track_object = camera.constraints.new(type='TRACK_TO')
        track_object.target = text_begin
        track_object.track_axis = 'TRACK_NEGATIVE_Z'
        track_object.up_axis = 'UP_Y'
        bpy.ops.object.select_all(action='DESELECT')
        camera.select = True
        bpy.ops.object.visual_transform_apply()
    def camera_switcher(camera_set):
        """Select camera view"""
        switcher = {'lateral_tracking': animation_camera, 'left': left_camera, 'front_left': front_left_camera, 'front': front_camera, 'front_right': front_right_camera, 'right': right_camera, 'back_right': back_right_camera, 'back': back_camera, 'back_left': back_left_camera, 'top_left': left_top_camera, 'top_front_left': front_left_top_camera, 'top_front': front_top_camera, 'top': top_camera, 'top_front_right': front_right_top_camera, 'bottom_front_left': front_left_bottom_camera, 'bottom': bottom_camera, 'bottom_front': front_bottom_camera, 'bottom_front_right': front_right_bottom_camera, 'other_view': my_camera}
        funcexec = switcher.get(camera_set, lambda:"Invalid set camera position")
        return funcexec()
    # Define world
    def world_scene(render=None, color_scene=None):
        """World scene select"""
        # erase all prewiews worlds
        for world in bpy.data.worlds.values():
            bpy.data.worlds.remove(world)
        # create new world
        world = bpy.data.worlds.new("World")
        bpy.context.scene.world = world
        if render == 'BLENDER':
            bpy.context.scene.render.engine = 'BLENDER_RENDER'
            bpy.data.worlds[0].light_settings.use_ambient_occlusion = True
            bpy.data.worlds[0].use_sky_blend = True
            if color_scene:
                bpy.data.worlds[0].ambient_color = color_scene['rgb']
                bpy.data.worlds[0].horizon_color = color_scene['rgb']
                bpy.data.worlds[0].zenith_color = color_scene['rgb']
        if render == 'CYCLES':
            bpy.context.scene.use_nodes = False
            bpy.context.scene.render.engine = 'CYCLES'
            # Active world nodes
            bpy.context.scene.world.use_nodes = True
            #select world node tree
            world = bpy.context.scene.world
            node_tree = bpy.data.worlds[world.name].node_tree
            # Get World output
            world_output_node = node_tree.nodes.values()[0]
            # Get node background node
            background_node = node_tree.nodes.values()[1]
            # Set strong environment
            background_node.inputs[1].default_value = 1
            # World ambian color
            if color_scene:
                background_node.inputs[0].default_value = color_scene['rgba']
        return world
    # Define sky scene
    def sky_scene(world, render, color_sky=None):
        """Set the sky of scene"""
        if render == 'BLENDER':
            if color_sky:
                bpy.data.worlds[0].horizon_color = color_sky[0]['rgb']
                bpy.data.worlds[0].zenith_color = color_sky[1]['rgb']
            bpy.data.worlds[0].light_settings.use_ambient_occlusion = True
            bpy.data.worlds[0].use_sky_blend = True
        if render == 'CYCLES':
            if color_sky:
                #select world node tree
                node_tree_sky = bpy.data.worlds[world.name].node_tree
                # Get World output
                world_output_node_sky = node_tree_sky.nodes.values()[0]
                world_output_node_sky.location = 1300,300
                # Get node background node
                background_node_sky = node_tree_sky.nodes.values()[1]
                background_node_sky.location = 1100,300
                # Set Ambian light
                #background_node.inputs[0].default_value = color_sky[0]['rgba']
                # Add color ramp
                colorramp_node_sky = node_tree_sky.nodes.new(type="ShaderNodeValToRGB")
                colorramp_node_sky.location = 800,300
                colorramp_node_sky.color_ramp.elements.new(position=2)
                colorramp_node_sky.color_ramp.elements[0].position = 0.0
                color_dark = list(colorsys.hsv_to_rgb(color_sky[1]['hsv'][0], color_sky[1]['hsv'][1], 0))
                color_dark.append(1.0)
                colorramp_node_sky.color_ramp.elements[0].color = color_dark
                colorramp_node_sky.color_ramp.elements[1].position = 0.5
                colorramp_node_sky.color_ramp.elements[1].color = color_sky[1]['rgba']
                colorramp_node_sky.color_ramp.elements[2].position = 1.0
                colorramp_node_sky.color_ramp.elements[2].color = color_sky[0]['rgba']
                # Link node color ramp
                node_tree_sky.links.new(background_node_sky.inputs[0], colorramp_node_sky.outputs[0])
                # Add math multiply
                multiply_node_sky = node_tree_sky.nodes.new(type="ShaderNodeMath")
                multiply_node_sky.location = 600,300
                multiply_node_sky.operation = 'MULTIPLY'
                # Link node multiply
                node_tree_sky.links.new(colorramp_node_sky.inputs[0], multiply_node_sky.outputs[0])
                # Add math add
                add_node_sky = node_tree_sky.nodes.new(type="ShaderNodeMath")
                add_node_sky.location = 400,300
                add_node_sky.inputs[1].default_value = 1.0
                # Link node add
                node_tree_sky.links.new(multiply_node_sky.inputs[0], add_node_sky.outputs[0])
                # Add separate XYZ
                separateXYZ_node_sky = node_tree_sky.nodes.new(type="ShaderNodeSeparateXYZ")
                separateXYZ_node_sky.location = 200,300
                # Link node separate XYZ
                node_tree_sky.links.new(add_node_sky.inputs[0], separateXYZ_node_sky.outputs[2])
                # Add texture coordinate
                texturecoordinate_node_sky = node_tree_sky.nodes.new(type="ShaderNodeTexCoord")
                texturecoordinate_node_sky.location = 0,300
                # Link node texture coordinate
                node_tree_sky.links.new(separateXYZ_node_sky.inputs[0], texturecoordinate_node_sky.outputs[0])
    # Define scenery picture
    def scenery(render, path=None, color_scene=None):
        if render == 'BLENDER':
            if path :
                try:
                    image = bpy.data.images.load(path)
                except:
                    raise NameError("Cannot load image %s" % path)
                else:
                    # Show background when rendering
                    bpy.context.scene.render.alpha_mode ='TRANSPARENT'
                    bpy.context.scene.use_nodes = True
                    # Clear previews nodes
                    for node_scene in bpy.context.scene.node_tree.nodes.items():
                        bpy.context.scene.node_tree.nodes.remove(node_scene[1])
                    # Create news nodes
                    scene_node_composite = bpy.context.scene.node_tree.nodes.new(type="CompositorNodeComposite")
                    scene_node_alpha = bpy.context.scene.node_tree.nodes.new(type="CompositorNodeAlphaOver")
                    scene_node_layer = bpy.context.scene.node_tree.nodes.new(type="CompositorNodeRLayers")
                    scene_node_scale = bpy.context.scene.node_tree.nodes.new(type="CompositorNodeScale")
                    scene_node_image = bpy.context.scene.node_tree.nodes.new(type="CompositorNodeImage")
                    # Set node positions
                    scene_node_composite.location = 600, 425
                    scene_node_alpha.location = 400, 400
                    scene_node_scale.location = 200, 450
                    scene_node_layer.location = 200, 280
                    scene_node_image.location = 0, 400
                    # Link nodes
                    bpy.context.scene.node_tree.links.new(scene_node_composite.inputs[0], scene_node_alpha.outputs[0])
                    bpy.context.scene.node_tree.links.new(scene_node_alpha.inputs[1], scene_node_scale.outputs[0])
                    bpy.context.scene.node_tree.links.new(scene_node_alpha.inputs[2], scene_node_layer.outputs[0])
                    bpy.context.scene.node_tree.links.new(scene_node_scale.inputs[0], scene_node_image.outputs[0])
                    # Set scale picture
                    scene_node_scale.space = 'RENDER_SIZE'
                    # Set picture background
                    scene_node_image.image = image
            else:
                # Dont show picture background
                bpy.context.scene.render.alpha_mode = 'SKY'
        if render == 'CYCLES':
            if path :
                try:
                    image = bpy.data.images.load(path)
                except:
                    raise NameError("Cannot load image %s" % path)
                else:
                    # Clear previews nodes
                    #for node_scene inbpy.context.scene.world.node_tree.nodes.items():
                    #    bpy.context.scene.world.node_tree.nodes.remove(node_scene[1])
                    # Render environment for HDRi
                    bpy.context.scene.render.layers.values()[0].use_pass_environment = True
                    # Get World output
                    world_output_node = bpy.context.scene.world.node_tree.nodes.values()[0]
                    # Get node background
                    background_node = bpy.context.scene.world.node_tree.nodes.values()[1]
                    #create new texture environment node
                    texture_environment_node = bpy.context.scene.world.node_tree.nodes.new(type="ShaderNodeTexEnvironment")
                    texture_environment_node.image = image
                    #create new node mixer with color
                    mix_color_node = bpy.context.scene.world.node_tree.nodes.new(type="ShaderNodeMixRGB")
                    mix_color_node.blend_type = 'MULTIPLY' #'SCREEN'
                    mix_color_node.inputs[0].default_value = 1
                    # Set Ambian light
                    if color_scene:
                        mix_color_node.inputs[1].default_value = color_scene['rgba']
                    #Set nodes positions
                    mix_color_node.location = 200,300
                    texture_environment_node.location = 0,300
                    world_output_node.location = 600,300
                    background_node.location = 400,300
                    # link nodes
                    bpy.context.scene.world.node_tree.links.new(mix_color_node.inputs[2], texture_environment_node.outputs[0])
                    bpy.context.scene.world.node_tree.links.new(background_node.inputs[0], mix_color_node.outputs[0])
    # Define lights scene
    def lights_area(render=None):
        """Set area lights"""
        bpy.ops.object.lamp_add(type='AREA')
        light_area = bpy.context.active_object
        light_area.location.x = 4.0
        light_area.location.y = -2.0
        light_area.location.z = 6.0
        if render == 'BLENDER':
            light_area.data.energy = 1
            light_area.data.shadow_method = 'RAY_SHADOW'
        if render == 'CYCLES':
            light_area.data.use_nodes = True
            light_area.data.node_tree.nodes[1].inputs[1].default_value = 30000
    def lights_spot(render=None):
        """Set spot lights"""
        bpy.ops.object.lamp_add(type='SPOT')
        light_area = bpy.context.active_object
        light_area.location = (0, -2.5, 5)
        light_area.rotation_euler = (radians(25.5), 0, 0)
        light_area.data.shadow_soft_size = 1
        light_area.data.spot_size = radians(45)
        if render == 'BLENDER':
            light_area.data.energy = 3
            light_area.data.spot_blend = 0.5
        if render == 'CYCLES':
            light_area.data.node_tree.nodes[1].inputs[1].default_value = 3000
    def lights_studio(render=None):
        """Set studio light"""
        # Create Back Light Small
        bpy.ops.mesh.primitive_plane_add()
        panel_light1 = bpy.context.scene.objects.active
        panel_light1.name = 'BackLightSmall'
        panel_light1.data.name = 'Data'
        panel_light1.location = (-6, 5, 0.5)
        panel_light1.rotation_euler = (radians(180), radians(-65), radians(135))
        panel_light1.scale = (0.212, 0.212, 0.106)
        # Create a material emission.
        matemission = bpy.data.materials.new(name = 'X-Light 1')
        if render == 'BLENDER':
            matemission.emit = 500
        if render == 'CYCLES':
            # create node emmission
            matemission.use_nodes = True
            # Remove default material
            matemission.node_tree.nodes.remove(matemission.node_tree.nodes.active)
            # Get output node material
            matemission_input = matemission.node_tree.nodes[0]
            matemission_input.location = 800,295
            # Set emission shader
            emissionshader = matemission.node_tree.nodes.new('ShaderNodeEmission')
            emissionshader.location = 0,300
            emissionshader.inputs['Strength'].default_value = 500 #14.4
            # link mix shader output to material input surface
            matemission.node_tree.links.new(matemission_input.inputs['Surface'], emissionshader.outputs[0])
            # Add modifier subsurface
        # set material oblect to our new material
        panel_light1.active_material = matemission
        panel_light1.select = True
        light1_subsurface_modifier = panel_light1.modifiers.new(name="Subsurfaces", type='SUBSURF')
        light1_subsurface_modifier.levels = 2
        # Create Light Plane Big
        bpy.ops.mesh.primitive_plane_add()
        panel_light2 = bpy.context.scene.objects.active
        panel_light2.name = 'LightPlaneBig'
        panel_light2.data.name = 'Data'
        panel_light2.location = (-3, -2, 2)
        panel_light2.rotation_euler = (radians(180), radians(-65), radians(25))
        panel_light2.scale = (0.212, 0.212, 0.106)
        # Create a material emission.
        matemission2 = bpy.data.materials.new(name = 'X-Light 2')
        if render == 'BLENDER':
            matemission.emit = 100
        if render == 'CYCLES':
            # create node emmission
            matemission2.use_nodes = True
            # Remove default material
            matemission2.node_tree.nodes.remove(matemission2.node_tree.nodes.active)
            # Get output node material
            matemission2_input = matemission2.node_tree.nodes[0]
            matemission2_input.location = 800,295
            # Set emission shader
            emissionshader2 = matemission2.node_tree.nodes.new('ShaderNodeEmission')
            emissionshader2.location = 0,300
            emissionshader2.inputs['Strength'].default_value = 100 #5
            # link mix shader output to material input surface
            matemission2.node_tree.links.new(matemission2_input.inputs['Surface'], emissionshader2.outputs[0])
        # set material oblect to our new material
        panel_light2.active_material = matemission2
        # Add modifier subsurface
        panel_light2.select = True
        light2_subsurface_modifier = panel_light2.modifiers.new(name="Subsurfaces", type='SUBSURF')
        light2_subsurface_modifier.levels = 2
        # Create Light Plane Big 2
        bpy.ops.mesh.primitive_plane_add()
        panel_light3 = bpy.context.scene.objects.active
        panel_light3.name = 'LightPlaneBig2'
        panel_light3.data.name = 'Data'
        panel_light3.location = (3, -2, 2)
        panel_light3.rotation_euler = (radians(180), radians(-53.2), radians(166))
        panel_light3.scale = (0.212, 0.212, 0.106)
        # Create a material emission.
        matemission3 = bpy.data.materials.new(name = 'X-Light 3')
        if render == 'BLENDER':
            matemission.emit = 100
        if render == 'CYCLES':
            # create node emmission
            matemission3.use_nodes = True
            # Remove default material
            matemission3.node_tree.nodes.remove(matemission3.node_tree.nodes.active)
            # Get output node material
            matemission3_input = matemission3.node_tree.nodes[0]
            matemission3_input.location = 800,295
            # Set emission shader
            emissionshader3 = matemission3.node_tree.nodes.new('ShaderNodeEmission')
            emissionshader3.location = 0,300
            emissionshader3.inputs['Strength'].default_value = 100 #8
            # link mix shader output to material input surface
            matemission3.node_tree.links.new(matemission3_input.inputs['Surface'], emissionshader3.outputs[0])
        # set material oblect to our new material
        panel_light3.active_material = matemission3
        # Add modifier subsurface
        panel_light3.select = True
        light3_subsurface_modifier = panel_light3.modifiers.new(name="Subsurfaces", type='SUBSURF')
        light3_subsurface_modifier.levels = 2
        # Create Light Plane Small
        bpy.ops.mesh.primitive_plane_add()
        panel_light4 = bpy.context.scene.objects.active
        panel_light4.name = 'LightPlaneSmall'
        panel_light4.data.name = 'Data'
        panel_light4.location = (30, -30, 25)
        panel_light4.rotation_euler = (radians(180), radians(-65), radians(135))
        panel_light4.scale = (2, 2, 1)
        # Create a material emission.
        matemission4 = bpy.data.materials.new(name = 'X-Light 4')
        if render == 'BLENDER':
            matemission.emit = 1000
        if render == 'CYCLES':
            # create node emmission
            matemission4.use_nodes = True
            # Remove default material
            matemission4.node_tree.nodes.remove(matemission4.node_tree.nodes.active)
            # Get output node material
            matemission4_input = matemission4.node_tree.nodes[0]
            matemission4_input.location = 800,295
            # Set emission shader
            emissionshader4 = matemission4.node_tree.nodes.new('ShaderNodeEmission')
            emissionshader4.location = 0,300
            emissionshader4.inputs['Strength'].default_value = 1000 #14.4
            # link mix shader output to material input surface
            matemission4.node_tree.links.new(matemission4_input.inputs['Surface'], emissionshader4.outputs[0])
        # set material oblect to our new material
        panel_light4.active_material = matemission4
        # Add modifier subsurface
        panel_light4.select = True
        light4_subsurface_modifier = panel_light4.modifiers.new(name="Subsurfaces", type='SUBSURF')
        light4_subsurface_modifier.levels = 2
    # Text object functions
    def add_text(text_to_add, text_size=1, align_x='CENTER', align_y='CENTER'):
        """Add text"""
        bpy.ops.object.text_add()
        obj_text = bpy.context.scene.objects.active
        text = obj_text.data
        text.body = text_to_add
        text.size = text_size
        text.align_x = align_x
        text.align_y = align_y
        bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY')
        obj_text.location.x = 0.0
        obj_text.location.y = 0.0
        obj_text.location.z = 0.0
        obj_text.rotation_euler = (radians(90), 0, radians(0))
    # Load a font text
    def load_font(font_path):
        """ Load a new TTF font into Blender, and return the font object """
        # get the original list of fonts (before we add a new one)
        original_fonts = bpy.data.fonts.keys()
        # load new font
        bpy.ops.font.open(filepath=font_path)
        # get the new list of fonts (after we added a new one)
        for font_name in bpy.data.fonts.keys():
            if font_name not in original_fonts:
                return bpy.data.fonts[font_name]
        # no new font was added
        return None
    def text_to_3D(text_to_operate, extrude=0.1, bevel_depth=0.02, bevel_resolution=2, space_character=1.1):
        """Convert flat text to 3D"""
        text_to_operate.data.extrude = extrude
        text_to_operate.data.bevel_depth = bevel_depth
        text_to_operate.data.bevel_resolution = bevel_resolution
        text_to_operate.data.space_character = space_character
    def text_to_mesh(text_to_operate):
        """Transform text to mesh"""
        text_to_operate.select = True
        bpy.context.scene.objects.active = text_to_operate
        bpy.ops.object.convert(target='MESH')
        bpy.context.scene.objects.active = text_to_operate
        bpy.ops.object.modifier_add(type='REMESH')
        # Config Remesh modifier
        text_to_operate.modifiers[0].use_remove_disconnected = False
        text_to_operate.modifiers[0].use_smooth_shade = True
        text_to_operate.modifiers[0].octree_depth = 7
    flag_error = False
except Exception as E:
    gest_blender_error(params, sys.exc_info(), 'Error define blender python functions', params["output_path"] + "debug.txt", E)
#####             End Define Blender plugin script functions             #####
##############################################################################


##############################################################################
#####                      Start create scene plugin                     #####

#print(str(params), file=open(os.path.join(os.path.expanduser("~"), "debug.txt"), "a"))

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           Clean Scene                          ----#
    flag_error = True
    try:
        # init scene plugin
        erase_all_scene()
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error clean scene', params["output_path"] + "debug.txt", E)
    #----                         End Clean Scene                        ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           3D Modeling                          ----#
    flag_error = True
    try:
        # create camera
        bpy.ops.object.camera_add()
        camera = bpy.context.scene.objects.active
        bpy.context.scene.camera = camera
        # Create objects scene plugin
        #text begin
        add_text(text_to_add=params["text_begin"], text_size=params["text_size"], align_x=params["spacemode"])
        text_begin = bpy.context.scene.objects.active
        text_begin.name = 'Text begin'
        text_begin.data.name = 'Data'
        #text finish
        add_text(text_to_add=params["text_finish"], text_size=params["text_size"], align_x=params["spacemode"])
        text_finish = bpy.context.scene.objects.active
        text_finish.name = 'Text finish'
        text_finish.data.name = 'Data'
        # set font text objects
        if params["fontname"] != "":
            # Add font so it's available to Blender
            font = load_font(params["fontname"])
            text_begin.data.font = font
            text_finish.data.font = font
        # Convert text objects from flat to 3D
        text_to_3D(text_begin)
        text_to_3D(text_finish)
        text_finish.select = False
        text_to_mesh(text_begin)
        text_begin.select = False
        text_to_mesh(text_finish)
        text_finish.select = False
        # Get max height text
        if bool(int(params["floor_on_off"])):
            # Get tail text
            size_text = text_begin.dimensions[1]
            if text_finish.dimensions[1] > size_text:
                size_text = text_finish.dimensions[1]
        # add floor scene
        if bool(int(params["floor_on_off"])):
            # add plane floor
            bpy.ops.mesh.primitive_plane_add()
            floor = bpy.context.scene.objects.active
            floor.name = 'floor'
            floor.scale.x = 10000
            floor.scale.y = 10000
            floor.location.x = 0
            floor.location.y = 0
            floor.location.z = 0
            floor.location.z = -size_text/2 -0.01
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error 3d modeling', params["output_path"] + "debug.txt", E)
    #----                         End 3D Modeling                        ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           UV Mapping                           ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error uv mapping', params["output_path"] + "debug.txt", E)
    #----                         End UV Mapping                         ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                      Texturing and Shaders                     ----#
    flag_error = True
    try:
        # Set material text
        if params["scene_render"] == 'BLENDER':
            material_text = bpy.data.materials.new(name='Text material')
            material_text.diffuse_color = params["text_color"]['rgb'] #(0.8,0,0)
            material_text.raytrace_mirror.use = True
            material_text.raytrace_mirror.reflect_factor = 0.1
            text_begin.active_material = material_text
            text_finish.active_material = material_text
            if bool(int(params["floor_on_off"])):
                material_plane = bpy.data.materials.new(name='Plane material')
                material_plane.diffuse_color = (0.25,0.25,0.25)
                material_plane.diffuse_intensity = 1
                material_plane.specular_shader = 'BLINN'
                material_plane.specular_intensity = 1
                material_plane.specular_hardness = 5
                material_plane.specular_ior = 10.0
                material_plane.raytrace_mirror.use = True
                material_plane.raytrace_mirror.reflect_factor = 1
                material_plane.raytrace_mirror.fresnel = 1
                material_plane.raytrace_mirror.fresnel_factor = 1.3
                material_plane.raytrace_mirror.distance = 3.0
                floor.active_material = material_plane
        if params["scene_render"] == 'CYCLES':
            # Create material text
            material_text = bpy.data.materials.new(name='Text material')
            # Create node text
            material_text.use_nodes = True
            # Remove default nodes material
            material_text.node_tree.nodes.remove(material_text.node_tree.nodes.active)
            # Get output material node output
            material_text_node_output = material_text.node_tree.nodes[0]
            material_text_node_output.location = 400, 295
            # Set Mix shader node
            material_text_node_mix = material_text.node_tree.nodes.new('ShaderNodeMixShader')
            material_text_node_mix.location = 200, 300
            material_text_node_mix.inputs[0].default_value = 0.15
            # Set Diffuse BSDF shader node
            material_text_node_diffuse = material_text.node_tree.nodes.new('ShaderNodeBsdfDiffuse')
            material_text_node_diffuse.location = 0, 300
            material_text_node_diffuse.inputs[0].default_value = params["text_color"]['rgba'] #(0.445, 0, 0, 1)
            # Set Glossy BSDF shader node
            material_text_node_glossy = material_text.node_tree.nodes.new('ShaderNodeBsdfGlossy')
            material_text_node_glossy.location = 0, 150
            material_text_node_glossy.inputs[1].default_value = 0.05
            # link shades
            material_text.node_tree.links.new(material_text_node_output.inputs[0], material_text_node_mix.outputs[0])
            material_text.node_tree.links.new(material_text_node_mix.inputs[1], material_text_node_diffuse.outputs[0])
            material_text.node_tree.links.new(material_text_node_mix.inputs[2], material_text_node_glossy.outputs[0])
            text_begin.active_material = material_text
            text_finish.active_material = material_text
            if bool(int(params["floor_on_off"])):
                # Create material plane
                material_plane = bpy.data.materials.new(name='Plane material')
                # Create node plane
                material_plane.use_nodes = True
                floor.active_material = material_plane
                # Remove default nodes material
                material_plane.node_tree.nodes.remove(material_plane.node_tree.nodes.active)
                # Get output material node output
                material_plane_node_output = material_plane.node_tree.nodes[0]
                material_plane_node_output.location = 400, 295
                # Set Mix shader node
                material_plane_node_mix = material_plane.node_tree.nodes.new('ShaderNodeMixShader')
                material_plane_node_mix.location = 200, 300
                # Set Diffuse BSDF shader node
                material_plane_node_diffuse = material_plane.node_tree.nodes.new('ShaderNodeBsdfDiffuse')
                material_plane_node_diffuse.location = 0, 300
                material_plane_node_diffuse.inputs[0].default_value = (0.25, 0.25, 0.25, 1)
                # Set Glossy BSDF shader node
                material_plane_node_glossy = material_plane.node_tree.nodes.new('ShaderNodeBsdfGlossy')
                material_plane_node_glossy.location = 0, 150
                material_plane_node_glossy.inputs[1].default_value = 0
                # link shades
                material_plane.node_tree.links.new(material_plane_node_output.inputs[0], material_plane_node_mix.outputs[0])
                material_plane.node_tree.links.new(material_plane_node_mix.inputs[1], material_plane_node_diffuse.outputs[0])
                material_plane.node_tree.links.new(material_plane_node_mix.inputs[2], material_plane_node_glossy.outputs[0])
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error texturing and shading', params["output_path"] + "debug.txt", E)
    #----                    End Texturing and Shaders                   ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                             Rigging                            ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error rigging', params["output_path"] + "debug.txt", E)
    #----                           End Rigging                          ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                            Animation                           ----#
    flag_error = True
    try:
        # Create animations
        # Add particle system te text_begin
        text_begin.select = True
        bpy.context.scene.objects.active = text_begin
        text_begin.modifiers.new("particle", type='PARTICLE_SYSTEM')
        text_begin.modifiers[1].particle_system.settings.use_modifier_stack = True
        text_begin.modifiers[1].particle_system.settings.count = 500 * int(params["animation_speed"])
        text_begin.modifiers[1].particle_system.settings.frame_end = 50 * int(params["animation_speed"])
        text_begin.modifiers[1].particle_system.settings.lifetime = 100 * int(params["animation_speed"])
        text_begin.modifiers[1].particle_system.settings.normal_factor = 0
        text_begin.modifiers[1].particle_system.settings.effector_weights.gravity = 0
        text_begin.modifiers[1].particle_system.settings.render_type = 'NONE'
        # add modifier explode
        text_begin.modifiers.new("explode", type='EXPLODE')
        text_begin.modifiers[2].use_edge_cut = True
        text_begin.modifiers[2].show_unborn = False
        text_begin.modifiers[2].show_dead = False
        # Add modifier solidify
        text_begin.modifiers.new("solidify", type='SOLIDIFY')
        # Add particle system te text_finish
        text_begin.select = False
        text_finish.select = True
        bpy.context.scene.objects.active = text_finish
        text_finish.modifiers.new("particle", type='PARTICLE_SYSTEM')
        text_finish.modifiers[1].particle_system.settings.use_modifier_stack = True
        text_finish.modifiers[1].particle_system.settings.count = 500 * int(params["animation_speed"])
        text_finish.modifiers[1].particle_system.settings.frame_start = 100 * int(params["animation_speed"])
        text_finish.modifiers[1].particle_system.settings.frame_end = 150 * int(params["animation_speed"])
        text_finish.modifiers[1].particle_system.settings.lifetime = 50 * int(params["animation_speed"])
        text_finish.modifiers[1].particle_system.settings.render_type = 'NONE'
        text_finish.modifiers[1].particle_system.settings.physics_type = "KEYED"
        bpy.ops.particle.new_target({'particle_system': text_finish.modifiers[1].particle_system}) # Generate warning: Blender output with missing something : PyContext 'window', 'blend_data','region', 'area' and 'screen' not found
        text_finish.modifiers[1].particle_system.active_particle_target.object = text_begin
        bpy.ops.particle.new_target({'particle_system': text_finish.modifiers[1].particle_system}) # Generate warning like up line
        text_finish.modifiers[1].particle_system.active_particle_target.object = text_finish
        # add modifier explode
        text_finish.modifiers.new("explode", type='EXPLODE')
        text_finish.modifiers[2].use_edge_cut = True
        text_finish.modifiers[2].show_unborn = False
        # add modifier solidify
        text_finish.modifiers.new("solidify", type='SOLIDIFY')
        # add force field animation
        bpy.ops.object.effector_add(type='TURBULENCE')
        field_effect = bpy.context.scene.objects.active
        field_effect.animation_data_create()
        field_effect.animation_data.action = bpy.data.actions.new(name="MyStrength")
        fcu_strength = field_effect.animation_data.action.fcurves.new(data_path='field.strength')
        fcu_strength.keyframe_points.add(3)
        fcu_strength.keyframe_points[0].co = 89, 0.0
        fcu_strength.keyframe_points[1].co = 90, 1.0
        fcu_strength.keyframe_points[2].co = 120, 0.0
        # floor animation
        if bool(int(params["floor_on_off"])):
            text_finish.select = False
            floor.select = True
            bpy.context.scene.objects.active = floor
            # add colision modifier to plane
            bpy.ops.object.modifier_add(type='COLLISION')
            floor.select = False
        # Set frame 0
        bpy.data.scenes[0].frame_current = 0
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error animation', params["output_path"] + "debug.txt", E)
    #----                          End Animation                         ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                            Lighting                            ----#
    flag_error = True
    try:
        # create scene environment
        if params["scene_render"] == 'BLENDER':
            # create world
            #world_scene(render=params["scene_render"], color_scene=(params["horizon_color"],params["zenith_color"],params["ambian_color"]), path=params["blender_background"])
            world_plugin = world_scene(render=params["scene_render"], color_scene=params["ambian_color"])
            if params["group_sky"]:
                sky_scene(world=world_plugin, render=params["scene_render"], color_sky=(params["horizon_color"], params["zenith_color"]))
            if params["group_decor"]:
                scenery(render=params["scene_render"], path=params["blender_background"], color_scene=params["ambian_color"])
        if params["scene_render"] == 'CYCLES':
            # create world
            #world_scene(render=params["scene_render"], color_scene=(params["horizon_color"],params["zenith_color"],params["ambian_color"]), camera_scene=params["camera"], path=params["cycles_hdr"])
            world_plugin = world_scene(render=params["scene_render"], color_scene=params["ambian_color"])
            if params["group_sky"]:
                sky_scene(world=world_plugin, render=params["scene_render"], color_sky=(params["horizon_color"], params["zenith_color"]))
            if params["group_decor"]:
                scenery(render=params["scene_render"], path=params["cycles_hdr"], color_scene=params["ambian_color"])
        #if params["scene_render"] == '':
        #    world_scene()
        # create lights environment
        #print(lights_switcher(params["light_scene"]))
        if params["light_scene"] == 'SPOT':
            lights_spot(render=params["scene_render"])
        if params["light_scene"] == 'AREA':
            lights_area(render=params["scene_render"])
        if params["light_scene"] == 'STUDIO':
            lights_studio(render=params["scene_render"])

        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error lightning', params["output_path"] + "debug.txt", E)
    #----                          End Lighting                          ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                         Camera Setting                         ----#
    flag_error = True
    try:
        # Set camera
        print(camera_switcher(params["camera"]))
        bpy.context.scene.objects.active = camera
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error camera setting', params["output_path"] + "debug.txt", E)
    #----                       End Camera Setting                       ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           Renderring                           ----#
    flag_error = True
    try:
        # Frame render
        if params["scene_render"] == 'CYCLES':
            bpy.context.scene.cycles.samples = 50
            bpy.context.scene.cycles.sample_clamp_indirect = 1.0
        try:
            bpy.context.scene.render.file_format = params['file_format']
        except:
            bpy.context.scene.render.image_settings.file_format = params['file_format']
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error set renderring', params["output_path"] + "debug.txt", E)
    #----                         End Renderring                         ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                    Compositing & Special VFX                   ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error compositing ans special vfx', params["output_path"] + "debug.txt", E)
    #----                  End Compositing & Special VFX                 ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                         Music and Foley                        ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error music and foley', params["output_path"] + "debug.txt", E)
    #----                       End Music and Foley                      ----#
    #------------------------------------------------------------------------#

#####                       End create scene plugin                      #####
##############################################################################


##############################################################################
#####                      Editing and Final output                      #####
# Set the render options.  It is important that these are set
# to the same values as the current OpenShot project.  These
# params are automatically set by OpenShot
# Render the current animation to the params["output_path"] folder
bpy.context.scene.render.resolution_x = params["resolution_x"]
bpy.context.scene.render.resolution_y = params["resolution_y"]
bpy.context.scene.render.resolution_percentage = params["resolution_percentage"]
#bpy.context.scene.render.quality = params["quality"]

if flag_error or not params["animation"] :
    # Render the current animation to the params["output_path"] folder
    bpy.context.scene.render.filepath = params["output_path"] + str(params["current_frame"])
    if flag_error:
        params["current_frame"] = params['start_frame']
        try:
            bpy.context.scene.render.file_format = 'PNG'
        except:
            bpy.context.scene.render.image_settings.file_format = 'PNG'
    bpy.data.scenes[0].frame_current = params["current_frame"]
    bpy.data.scenes[0].frame_start = params["current_frame"]
    bpy.data.scenes[0].frame_end = params["current_frame"]
    bpy.ops.render.render(animation=params["animation"], write_still=True)
else:
    # Render the current animation to the params["output_path"] folder
    bpy.context.scene.render.filepath = params["output_path"]
    bpy.context.scene.render.fps = params["fps"]
    bpy.data.scenes[0].frame_current = params['current_frame']
    bpy.data.scenes[0].frame_start = params['start_frame']
    bpy.data.scenes[0].frame_end = params['end_frame']
    # Animation Speed (use Blender's time remapping to slow or speed up animation)
    bpy.context.scene.render.frame_map_new = bpy.context.scene.render.frame_map_old * int(params["animation_speed"])
    bpy.ops.render.render(animation=params["animation"])
#####                    End Editing and Final output                    #####
##############################################################################

