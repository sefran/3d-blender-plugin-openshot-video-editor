#	OpenShot Video Editor is a program that creates, modifies, and edits video files.
#   Copyright (C) 2009  Jonathan Thomas
#
#	This file is part of OpenShot Video Editor (http://launchpad.net/openshot/).
#
#	OpenShot Video Editor is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	OpenShot Video Editor is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with OpenShot Video Editor.  If not, see <http://www.gnu.org/licenses/>.


# Debug Info:
# ./blender -b test.blend -P demo.py
# -b = background mode
# -P = run a Python script within the context of the project file


##############################################################################
#####                         Get python library                         #####
# Import Blender's python API.  This only works when the script is being
# run from the context of Blender.  Blender contains it's own version of Python
# with this library pre-installed.
import bpy, colorsys, os, sys
from math import pi, radians
#####                    End Import Get python library                   #####
##############################################################################

##############################################################################
#####                     Get python Blender library                     #####
directory = os.path.dirname(bpy.data.filepath)[:-5] + 'scripts/openshot'
if not directory in sys.path:
    sys.path.append(directory)

from debug import gest_blender_error

#####                   End Get python Blender library                   #####
##############################################################################

##############################################################################
#####                     Openshot parameters plugin                     #####
params = {}
params['alpha'] = 1.0
params['horizon_color'] = {'rgb': [0.57, 0.57, 0.57]}

# Init all of the variables needed by this script.  Because Blender executes
# this script, OpenShot will inject a dictionary of the required parameters
# before this script is executed.
#INJECT_PARAMS_HERE

# The remainder of this script will modify the current Blender .blend project
# file, and adjust the settings.  The .blend file is specified in the XML file
# that defines this template in OpenShot.
#####                   End Openshot parameters plugin                   #####
##############################################################################

##############################################################################
#####                       Blender errors gesture                       #####
# interpretor errors code gesture
def excepthook(type, value, traceback):
    sys.exit(gest_blender_error(params, sys.exc_info(), 'Error script code', params["output_path"] + "debug.txt"))

sys.excepthook = excepthook
#####                     End Blender errors gesture                     #####
##############################################################################


# Init render engine
if bpy.app.version < (2, 80, 0):
    bpy.context.scene.render.engine = 'BLENDER_RENDER'
#    bpy.context.scene.render.engine = 'CYCLES'
#    bpy.context.scene.render.engine = 'BLENDER_GAME'
#    bpy.context.scene.render.engine = 'POVRAY_RENDER'
else:
    bpy.context.scene.render.engine = 'BLENDER_WORKBENCH'
            

##############################################################################
#####               Define Blender python script functions               #####
flag_error = True
try:
    # Load a font
    def load_font(font_path):
        """ Load a new TTF font into Blender, and return the font object """
        # get the original list of fonts (before we add a new one)
        original_fonts = bpy.data.fonts.keys()
        # load new font
        bpy.ops.font.open(filepath=font_path)
        # get the new list of fonts (after we added a new one)
        for font_name in bpy.data.fonts.keys():
            if font_name not in original_fonts:
                return bpy.data.fonts[font_name]
        # no new font was added
        return None
    flag_error = False
except Exception as E:
    gest_blender_error(params, sys.exc_info(), 'Error define blender python functions', params["output_path"] + "debug.txt", E)
#####             End Define Blender python script functions             #####
##############################################################################


##############################################################################
#####                      Start create scene plugin                     #####

#print(str(params), file=open(os.path.join(os.path.expanduser("~"), "debug.txt"), "a"))

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           Clean Scene                          ----#
    flag_error = True
    try:
        flag_error = False
        pass
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error clean scene', params["output_path"] + "debug.txt", E)
    #----                         End Clean Scene                        ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           3D Modeling                          ----#
    flag_error = True
    try:
        # Modify Text / Curve settings
        #print (bpy.data.curves.keys())
        text_object = bpy.data.curves["Title"]
        text_object.extrude = params["extrude"]
        text_object.bevel_depth = params["bevel_depth"]
        text_object.body = params["title"]
        text_object.align_x = params["spacemode"]
        text_object.size = params["text_size"]
        text_object.space_character = params["width"]

        # Get font object
        font = None
        if params["fontname"] != "Bfont":
            # Add font so it's available to Blender
            font = load_font(params["fontname"])
        else:
            # Get default font
            font = bpy.data.fonts["Bfont"]
            
        text_object.font = font

        text_object = bpy.data.curves["Subtitle"]
        text_object.extrude = params["extrude"]
        text_object.bevel_depth = params["bevel_depth"]
        text_object.body = params["sub_title"]
        text_object.align_x = params["spacemode"]
        text_object.size = params["text_size"]
        text_object.space_character = params["width"]

        # set the font
        text_object.font = font

        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error 3d modeling', params["output_path"] + "debug.txt", E)
    #----                         End 3D Modeling                        ----#
    #------------------------------------------------------------------------#


if not flag_error:
    #------------------------------------------------------------------------#
    #----                           UV Mapping                           ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error uv mapping', params["output_path"] + "debug.txt", E)
    #----                         End UV Mapping                         ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                      Texturing and Shaders                     ----#
    flag_error = True
    try:
        # Change the material settings (color, alpha, etc...)
        if "Text" in bpy.data.materials:
            material_object = bpy.data.materials["Text"]
            if bpy.app.version < (2, 80, 0):
                material_object.diffuse_color = params["diffuse_color"]['rgb']
            else:
                material_object.diffuse_color = params["diffuse_color"]['rgba']
            material_object.specular_color = params["specular_color"]['rgb']
            material_object.specular_intensity = params["specular_intensity"]
            if bpy.app.version < (2, 80, 0):
                material_object.alpha = params["alpha"]

        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error texturing and shading', params["output_path"] + "debug.txt", E)
    #----                    End Texturing and Shaders                   ----#
    #------------------------------------------------------------------------#


if not flag_error:
    #------------------------------------------------------------------------#
    #----                             Rigging                            ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error rigging', params["output_path"] + "debug.txt", E)
    #----                           End Rigging                          ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                            Animation                           ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error animation', params["output_path"] + "debug.txt", E)
    #----                          End Animation                         ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                            Lighting                            ----#
    flag_error = True
    try:
        if bpy.app.version < (2, 80, 0):
            bpy.context.scene.render.alpha_mode = params["alpha_mode"]
            bpy.data.worlds[0].horizon_color = params["horizon_color"]["rgb"]
        try:
            bpy.context.scene.render.color_mode = params["color_mode"]
        except:
            bpy.context.scene.render.image_settings.color_mode = params["color_mode"]
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error lightning', params["output_path"] + "debug.txt", E)
    #----                          End Lighting                          ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                         Camera Setting                         ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error camera setting', params["output_path"] + "debug.txt", E)
    #----                       End Camera Setting                       ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           Renderring                           ----#
    flag_error = True
    try:
        try:
            bpy.context.scene.render.file_format = params["file_format"]
        except:
            bpy.context.scene.render.image_settings.file_format = params["file_format"]
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error set renderring', params["output_path"] + "debug.txt", E)
    #----                         End Renderring                         ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                    Compositing & Special VFX                   ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error compositing ans special vfx', params["output_path"] + "debug.txt", E)
    #----                  End Compositing & Special VFX                 ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                         Music and Foley                        ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error music and foley', params["output_path"] + "debug.txt", E)
    #----                       End Music and Foley                      ----#
    #------------------------------------------------------------------------#

#####                       End create scene plugin                      #####
##############################################################################


##############################################################################
#####                      Editing and Final output                      #####
# Set the render options.  It is important that these are set
# to the same values as the current OpenShot project.  These
# params are automatically set by OpenShot
# Render the current animation to the params["output_path"] folder
bpy.context.scene.render.resolution_x = params["resolution_x"]
bpy.context.scene.render.resolution_y = params["resolution_y"]
bpy.context.scene.render.resolution_percentage = params["resolution_percentage"]
#bpy.context.scene.render.quality = params["quality"]

if flag_error or not params["animation"] :
    # Render the current animation to the params["output_path"] folder
    bpy.context.scene.render.filepath = params["output_path"] + str(params["current_frame"])
    if flag_error:
        params["current_frame"] = params['start_frame']
        try:
            bpy.context.scene.render.file_format = 'PNG'
        except:
            bpy.context.scene.render.image_settings.file_format = 'PNG'
    bpy.data.scenes[0].frame_current = params["current_frame"]
    bpy.data.scenes[0].frame_start = params["current_frame"]
    bpy.data.scenes[0].frame_end = params["current_frame"]
    bpy.ops.render.render(animation=params["animation"], write_still=True)
else:
    # Render the current animation to the params["output_path"] folder
    bpy.context.scene.render.filepath = params["output_path"]
    bpy.context.scene.render.fps = params["fps"]
    bpy.data.scenes[0].frame_current = params['current_frame']
    bpy.data.scenes[0].frame_start = params['start_frame']
    bpy.data.scenes[0].frame_end = params['end_frame']
    # Animation Speed (use Blender's time remapping to slow or speed up animation)
    #bpy.context.scene.render.frame_map_new = bpy.context.scene.render.frame_map_old * int(params["animation_speed"])
    bpy.ops.render.render(animation=params["animation"])
#####                    End Editing and Final output                    #####
##############################################################################
