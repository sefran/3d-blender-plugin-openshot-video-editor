#	OpenShot Video Editor is a program that creates, modifies, and edits video files.
#   Copyright (C) 2009  Jonathan Thomas
#
#	This file is part of OpenShot Video Editor (http://launchpad.net/openshot/).
#
#	OpenShot Video Editor is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	OpenShot Video Editor is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with OpenShot Video Editor.  If not, see <http://www.gnu.org/licenses/>.


# Debug Info:
# ./blender -b test.blend -P demo.py
# -b = background mode
# -P = run a Python script within the context of the project file


##############################################################################
#####                         Get python library                         #####
# Import Blender's python API.  This only works when the script is being
# run from the context of Blender.  Blender contains it's own version of Python
# with this library pre-installed.
import bpy, colorsys, os, sys, traceback
from math import pi, radians
#####                    End Import Get python library                   #####
##############################################################################

##############################################################################
#####                     Get python Blender library                     #####
directory = os.path.dirname(bpy.data.filepath)[:-5] + 'scripts/openshot'
if not directory in sys.path:
    sys.path.append(directory)

from debug import gest_blender_error
#####                   End Get python Blender library                   #####
##############################################################################


##############################################################################
#####                     Openshot parameters plugin                     #####
params = {}

# Init all of the variables needed by this script.  Because Blender executes
# this script, OpenShot will inject a dictionary of the required parameters
# before this script is executed.
#INJECT_PARAMS_HERE

# The remainder of this script will modify the current Blender .blend project
# file, and adjust the settings.  The .blend file is specified in the XML file
# that defines this template in OpenShot.
#####                   End Openshot parameters plugin                   #####
##############################################################################


##############################################################################
#####                       Blender errors gesture                       #####
# interpretor errors code gesture
def excepthook(type, value, traceback):
    sys.exit(gest_blender_error(params, sys.exc_info(), 'Error script code', params["output_path"] + "debug.txt"))
sys.excepthook = excepthook
#####                     End Blender errors gesture                     #####
##############################################################################


##############################################################################
#####               Define Blender python script functions               #####
flag_error = False
try:
    # My Blender python functions 
    flag_error = True
except Exception as E:
    gest_blender_error(params, sys.exc_info(), 'Error define blender python functions', params["output_path"] + "debug.txt", E)
#####             End Define Blender python script functions             #####
##############################################################################


##############################################################################
#####                      Start create scene plugin                     #####

#print(str(params), file=open(os.path.join(os.path.expanduser("~"), "debug.txt"), "a"))

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           Clean Scene                          ----#
    flag_error = False
    try:
        flag_error = True
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error clean scene', params["output_path"] + "debug.txt", E)
    #----                         End Clean Scene                        ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           3D Modeling                          ----#
    flag_error = False
    try:
        flag_error = True
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error 3d modeling', params["output_path"] + "debug.txt", E)
    #----                         End 3D Modeling                        ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           UV Mapping                           ----#
    flag_error = False
    try:
        flag_error = True
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error uv mapping', params["output_path"] + "debug.txt", E)
    #----                         End UV Mapping                         ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                      Texturing and Shaders                     ----#
    flag_error = False
    try:
        flag_error = True
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error texturing and shading', params["output_path"] + "debug.txt", E)
   #----                    End Texturing and Shaders                   ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                             Rigging                            ----#
    flag_error = False
    try:
        flag_error = True
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error rigging', params["output_path"] + "debug.txt", E)
    #----                           End Rigging                          ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                            Animation                           ----#
    flag_error = False
    try:
        flag_error = True
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error animation', params["output_path"] + "debug.txt", E)
    #----                          End Animation                         ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                            Lighting                            ----#
    flag_error = False
    try:
        flag_error = True
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error lightning', params["output_path"] + "debug.txt", E)
    #----                          End Lighting                          ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                         Camera Setting                         ----#
    flag_error = False
    try:
        flag_error = True
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error camera setting', params["output_path"] + "debug.txt", E)
    #----                       End Camera Setting                       ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           Renderring                           ----#
    flag_error = False
    try:
        flag_error = True
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error set renderring', params["output_path"] + "debug.txt", E)
    #----                         End Renderring                         ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                    Compositing & Special VFX                   ----#
    flag_error = False
    try:
        flag_error = True
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error compositing ans special vfx', params["output_path"] + "debug.txt", E)
    #----                  End Compositing & Special VFX                 ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                         Music and Foley                        ----#
    flag_error = False
    try:
        flag_error = True
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error music and foley', params["output_path"] + "debug.txt", E)
    #----                       End Music and Foley                      ----#
    #------------------------------------------------------------------------#

#####                       End create scene plugin                      #####
##############################################################################


##############################################################################
#####                      Editing and Final output                      #####
# Set the render options.  It is important that these are set
# to the same values as the current OpenShot project.  These
# params are automatically set by OpenShot
# Render the current animation to the params["output_path"] folder
bpy.context.scene.render.resolution_x = params["resolution_x"]
bpy.context.scene.render.resolution_y = params["resolution_y"]
bpy.context.scene.render.resolution_percentage = params["resolution_percentage"]
# Render the current animation to the params["output_path"] folder
bpy.context.scene.render.filepath = params["output_path"]

if 'preview_frame' in params:
    bpy.data.scenes[0].frame_current = params["preview_frame"]
    bpy.data.scenes[0].frame_start = params["preview_frame"]
    bpy.data.scenes[0].frame_end = params["preview_frame"]
    bpy.ops.render.render(animation=False, write_still=True)
else:
    bpy.data.scenes[0].frame_current = params['start_frame']
    bpy.data.scenes[0].frame_start = params['start_frame']
    bpy.data.scenes[0].frame_end = params['end_frame']
    bpy.ops.render.render(animation=params["animation"])
#####                    End Editing and Final output                    #####
##############################################################################

