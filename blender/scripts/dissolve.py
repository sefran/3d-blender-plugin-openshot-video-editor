#	OpenShot Video Editor is a program that creates, modifies, and edits video files.
#   Copyright (C) 2009  Jonathan Thomas
#
#	This file is part of OpenShot Video Editor (http://launchpad.net/openshot/).
#
#	OpenShot Video Editor is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	OpenShot Video Editor is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with OpenShot Video Editor.  If not, see <http://www.gnu.org/licenses/>.


# Debug Info:
# ./blender -b test.blend -P demo.py
# -b = background mode
# -P = run a Python script within the context of the project file


##############################################################################
#####                         Get python library                         #####
# Import Blender's python API.  This only works when the script is being
# run from the context of Blender.  Blender contains it's own version of Python
# with this library pre-installed.
import bpy, colorsys, os, sys
from math import pi, radians
#####                    End Import Get python library                   #####
##############################################################################


##############################################################################
#####                     Get python Blender library                     #####
from bpy.props import *

directory = os.path.dirname(bpy.data.filepath)[:-5] + 'scripts/openshot'
if not directory in sys.path:
    sys.path.append(directory)

from debug import gest_blender_error

#####                   End Get python Blender library                   #####
##############################################################################


##############################################################################
#####                     Openshot parameters plugin                     #####
params = {}
params['alpha'] = 1.0
params['horizon_color'] = {'rgb': [0.57, 0.57, 0.57]}

# Init all of the variables needed by this script.  Because Blender executes
# this script, OpenShot will inject a dictionary of the required parameters
# before this script is executed.
#INJECT_PARAMS_HERE

# The remainder of this script will modify the current Blender .blend project
# file, and adjust the settings.  The .blend file is specified in the XML file
# that defines this template in OpenShot.
#####                   End Openshot parameters plugin                   #####
##############################################################################


##############################################################################
#####                       Blender errors gesture                       #####
# interpretor errors code gesture
def excepthook(type, value, traceback):
    sys.exit(gest_blender_error(params, sys.exc_info(), 'Error script code', params["output_path"] + "debug.txt"))

sys.excepthook = excepthook
#####                     End Blender errors gesture                     #####
##############################################################################


# Init render engine
if bpy.app.version < (2, 80, 0):
    bpy.context.scene.render.engine = 'BLENDER_RENDER'
#    bpy.context.scene.render.engine = 'CYCLES'
#    bpy.context.scene.render.engine = 'BLENDER_GAME'
#    bpy.context.scene.render.engine = 'POVRAY_RENDER'
else:
    bpy.context.scene.render.engine = 'BLENDER_WORKBENCH'


##############################################################################
#####               Define Blender python script functions               #####
flag_error = True
try:
    # Load a font
    def load_font(font_path):
        """ Load a new TTF font into Blender, and return the font object """
        # get the original list of fonts (before we add a new one)
        original_fonts = bpy.data.fonts.keys()
        # load new font
        bpy.ops.font.open(filepath=font_path)
        # get the new list of fonts (after we added a new one)
        for font_name in bpy.data.fonts.keys():
            if font_name not in original_fonts:
                return bpy.data.fonts[font_name]
        # no new font was added
        return None

    def createDissolveText(title,extrude,bevel_depth,spacemode,textsize,width,font):
        """ Create aned animate the exploding texte """

        newText = title
        #create text
        if bpy.app.version < (2, 80, 0):
            bpy.ops.object.text_add(view_align=False, enter_editmode=False,location=(0, 0, 0), rotation=(0, 0, 0))
        else:
            bpy.ops.object.text_add(enter_editmode=False,location=(0, 0, 0), rotation=(0, 0, 0))
        if bpy.app.version < (2, 80, 0):
            ActiveObjectText = bpy.context.scene.objects.active
        else:
            ActiveObjectText = bpy.context.view_layer.objects.active

        #erasing previous objects
        if ActiveObjectText.name != 'Text':
            bpy.ops.object.select_all(action='DESELECT')
            #selecting and erasing Text
            if bpy.app.version < (2, 80, 0):
                bpy.context.scene.objects.active  = bpy.data.objects['Text']
                bpy.context.scene.objects.active.select = True
            else:
                bpy.context.view_layer.objects.active = bpy.data.objects["Text"]
                bpy.data.objects["Text"].select_set(True)
            bpy.ops.object.delete(use_global=False)
            bpy.ops.object.select_all(action='DESELECT')
            
            #need to delete other objects
            bpy.ops.object.select_all(action='DESELECT')
            #selecting and erasing Turbulence field
            if bpy.app.version < (2, 80, 0):
                bpy.context.scene.objects.active  = bpy.data.objects['TurbulenceField']
                bpy.context.scene.objects.active.select = True
            else:
                bpy.context.view_layer.objects.active  = bpy.data.objects['TurbulenceField']
                bpy.data.objects['TurbulenceField'].select_set(True)
            bpy.ops.object.delete(use_global=False)

            bpy.ops.object.select_all(action='DESELECT')
            #selecting and erasing Plane
            if bpy.app.version < (2, 80, 0):
                bpy.context.scene.objects.active = bpy.data.objects['Plane']
                bpy.context.scene.objects.active.select = True
            else:
                bpy.context.view_layer.objects.active = bpy.data.objects['Plane']
                bpy.data.objects['Plane'].select_set(True)
            bpy.ops.object.delete(use_global=False)

            bpy.ops.object.select_all(action='DESELECT')
            #selecting and erasing WindField
            if bpy.app.version < (2, 80, 0):
                bpy.context.scene.objects.active  = bpy.data.objects['WindField']
                bpy.context.scene.objects.active.select = True
            else:
                bpy.context.view_layer.objects.active  = bpy.data.objects['WindField']
                bpy.data.objects['WindField'].select_set(True)
            bpy.ops.object.delete(use_global=False)

            #selecting newText
            if bpy.app.version < (2, 80, 0):
                bpy.context.scene.objects.active  = ActiveObjectText
                bpy.context.scene.objects.active.select = True
            else:
                bpy.context.view_layer.objects.active  = ActiveObjectText
                ActiveObjectText.select_set(True)
        #naming/renaming the text
        ActiveObjectText.name = 'Text';
        ActiveObjectText = bpy.data.objects['Text']
        
        #placing text in position
        ActiveObjectText.rotation_euler[0]=pi/2 #xaxis
        ActiveObjectText.rotation_euler[1]=0.0  #yaxis
        ActiveObjectText.rotation_euler[2]=0.0  #zaxis
        ActiveObjectText.location[0]=0
        ActiveObjectText.location[1]=0
        ActiveObjectText.location[2]=0 
        #changing text
        ActiveObjectText.data.body = title
        
        #text size 
        ActiveObjectText.data.size = textsize
        ActiveObjectText.data.space_character = width
        ActiveObjectText.data.font = font
        #centering text
        #ActiveObjectText.data.align_x='CENTER'
        ActiveObjectText.data.align_x=spacemode
        #extrude text
        ActiveObjectText.data.extrude=extrude #0.04

        #bevel text
        ActiveObjectText.data.bevel_depth = bevel_depth #0.005
        ActiveObjectText.data.bevel_resolution = 5
        #adjust text position
        ActiveObjectText.location.z= -ActiveObjectText.dimensions[1]/3
        
        #convert to mesh to apply effect
        bpy.ops.object.convert(target='MESH', keep_original=False)
        
        #affect dissolve material
        ActiveObjectText.data.materials.append(bpy.data.materials['DissolveMaterial'])
        if bpy.app.version < (2, 80, 0):
            ActiveObjectText = bpy.context.scene.objects.active
        else:
            ActiveObjectText = bpy.context.view_layer.objects.active
        
        #Don't forget to deselect before select!
        bpy.ops.object.select_all(action='DESELECT')
        
        #selecting Text
        if bpy.app.version < (2, 80, 0):
            bpy.context.scene.objects.active = ActiveObjectText
            bpy.context.scene.objects.active.select = True
        else:
            bpy.context.view_layer.objects.active = ActiveObjectText
            ActiveObjectText.select_set(True)
        
        #add remesh modifier to text
        bpy.ops.object.modifier_add(type='REMESH')
        #modifying parameters
        ActiveObjectText.modifiers['Remesh'].octree_depth = 9 #10 best quality but vertices number too high
        ActiveObjectText.modifiers['Remesh'].scale=0.99
        ActiveObjectText.modifiers['Remesh'].mode='SMOOTH'
        ActiveObjectText.modifiers['Remesh'].use_remove_disconnected=False
        #apply this modifier
        bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Remesh")
        
        #Nb quads for particle system be careful of API version
        if bpy.app.version[1] < 62 :
            NbQuads = len(ActiveObjectText.data.faces)
        if bpy.app.version[1] == 62 :
            if bpy.app.version[2] >= 2 :
                NbQuads = len(ActiveObjectText.data.polygons.values()) #API 2.62.2 and up
            else:
                NbQuads = len(ActiveObjectText.data.faces)
        if bpy.app.version[1] > 62 :
                NbQuads = len(ActiveObjectText.data.polygons.values()) #API 2.63 and up

        #Add Particle System
        bpy.ops.object.particle_system_add()
        if bpy.app.version < (2, 80, 0):
            #Particle parameters
            ActiveObjectText.particle_systems['ParticleSystem'].settings.count = NbQuads
            ActiveObjectText.particle_systems['ParticleSystem'].settings.frame_start = 10
            ActiveObjectText.particle_systems['ParticleSystem'].settings.frame_end = 60
            ActiveObjectText.particle_systems['ParticleSystem'].settings.lifetime = 80
            ActiveObjectText.particle_systems['ParticleSystem'].point_cache.frame_step = 1
            ActiveObjectText.particle_systems['ParticleSystem'].settings.normal_factor = 0.0
            #not useful
            ActiveObjectText.particle_systems['ParticleSystem'].settings.use_dynamic_rotation = True
            ActiveObjectText.particle_systems['ParticleSystem'].settings.render_type='NONE'
            ActiveObjectText.particle_systems['ParticleSystem'].settings.draw_method='DOT'
            ActiveObjectText.particle_systems['ParticleSystem'].settings.effector_weights.gravity = 0
            ActiveObjectText.particle_systems['ParticleSystem'].settings.use_adaptive_subframes = True
            ActiveObjectText.particle_systems['ParticleSystem'].settings.courant_target = 0.2
        else:
            #Particle parameters
            ActiveObjectText.particle_systems['ParticleSettings'].settings.count = NbQuads
            ActiveObjectText.particle_systems['ParticleSettings'].settings.frame_start = 10
            ActiveObjectText.particle_systems['ParticleSettings'].settings.frame_end = 60
            ActiveObjectText.particle_systems['ParticleSettings'].settings.lifetime = 80
            ActiveObjectText.particle_systems['ParticleSettings'].point_cache.frame_step = 1
            ActiveObjectText.particle_systems['ParticleSettings'].settings.normal_factor = 0.0
            #not useful
            ActiveObjectText.particle_systems['ParticleSettings'].settings.use_dynamic_rotation = True
            ActiveObjectText.particle_systems['ParticleSettings'].settings.render_type='NONE'
            #ActiveObjectText.particle_systems['ParticleSettings'].settings.draw_method='DOT'
            ActiveObjectText.particle_systems['ParticleSettings'].settings.effector_weights.gravity = 0
            ActiveObjectText.particle_systems['ParticleSettings'].settings.use_adaptive_subframes = True
            ActiveObjectText.particle_systems['ParticleSettings'].settings.courant_target = 0.2

        bpy.ops.object.select_all(action='DESELECT')

        #Adding Wind force field on center and rotate it -90 on Y
        if bpy.app.version < (2, 80, 0):
            bpy.ops.object.effector_add(type='WIND', view_align=False, enter_editmode=False, location=(0, 0, 0), rotation=(0, -pi/2, 0), layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
            ActiveObjectWindField = bpy.context.scene.objects.active
        else:
            bpy.ops.object.effector_add(type='WIND', enter_editmode=False, location=(0, 0, 0), rotation=(0, -pi/2, 0))
            ActiveObjectWindField = bpy.context.view_layer.objects.active
        ActiveObjectWindField.name = 'WindField'
        #settings
        ActiveObjectWindField.field.strength = 1.0
        ActiveObjectWindField.field.flow = 1.0
        ActiveObjectWindField.field.noise = 0.0
        ActiveObjectWindField.field.seed = 27
        ActiveObjectWindField.field.apply_to_location = True
        ActiveObjectWindField.field.apply_to_rotation = True
        ActiveObjectWindField.field.use_absorption = False

        #Adding Turbulence Force Field
        if bpy.app.version < (2, 80, 0):
            bpy.ops.object.effector_add(type='TURBULENCE', view_align=False, enter_editmode=False, location=(0, 0, 0), rotation=(0, 0, 0), layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
            ActiveObjectTurbulenceField = bpy.context.scene.objects.active
        else:
            bpy.ops.object.effector_add(type='TURBULENCE', enter_editmode=False, location=(0, 0, 0), rotation=(0, 0, 0))
            ActiveObjectTurbulenceField = bpy.context.view_layer.objects.active
        ActiveObjectTurbulenceField.name = 'TurbulenceField'
        #settings
        ActiveObjectTurbulenceField.field.strength = 15
        ActiveObjectTurbulenceField.field.size = 0.75
        ActiveObjectTurbulenceField.field.flow = 0.5
        ActiveObjectTurbulenceField.field.seed = 23
        ActiveObjectTurbulenceField.field.apply_to_location = True
        ActiveObjectTurbulenceField.field.apply_to_rotation = True
        ActiveObjectTurbulenceField.field.use_absorption = False


        #Don't forget to deselect before select!
        bpy.ops.object.select_all(action='DESELECT')

        #selecting Text
        if bpy.app.version < (2, 80, 0):
            bpy.context.scene.objects.active  = ActiveObjectText
            bpy.context.scene.objects.active.select = True
        else:
            bpy.context.view_layer.objects.active  = ActiveObjectText
            ActiveObjectText.select_set(True)

        #adding wipe texture to text

        sTex = bpy.data.textures.new('Wipe', type = 'BLEND')
        sTex.use_color_ramp = True

        if bpy.app.version < (2, 80, 0):
            TexSlot=ActiveObjectText.particle_systems['ParticleSystem'].settings.texture_slots.add()
        else:
            TexSlot=ActiveObjectText.particle_systems['ParticleSettings'].settings.texture_slots.add()
        TexSlot.texture = sTex

        bpy.ops.object.select_all(action='DESELECT')

        #Create plane for controlling action of particle system (based on time)
        #if text is created on the fly 'Wipe' texture does not work! don't know really why!
        # so use of an existing plane, and resize it to the text x dimension
        if bpy.app.version < (2, 80, 0):
            bpy.ops.mesh.primitive_plane_add(view_align=False, enter_editmode=False, location=(0, 0, 0), rotation=(pi/2, 0, 0), layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
            ActiveObjectPlane = bpy.context.scene.objects.active
        else:
            bpy.ops.mesh.primitive_plane_add(enter_editmode=False, location=(0, 0, 0), rotation=(pi/2, 0, 0))
            ActiveObjectPlane = bpy.context.view_layer.objects.active
        ActiveObjectPlane.name = 'Plane'
        #Change dimensions
        ActiveObjectPlane.dimensions = ((ActiveObjectText.dimensions[0]*1.2),(ActiveObjectText.dimensions[1]*1.2),0)
        #hide plane for render
        ActiveObjectPlane.hide_render = True
        #show as wire in 3D
        if bpy.app.version < (2, 80, 0):
            ActiveObjectPlane.draw_type = 'WIRE'
        else:
            ActiveObjectPlane.display_type = 'WIRE'

        bpy.ops.object.select_all(action='DESELECT')

        #selecting Text
        if bpy.app.version < (2, 80, 0):
            bpy.context.scene.objects.active  = ActiveObjectText
            bpy.context.scene.objects.active.select = True
        else:
            bpy.context.view_layer.objects.active  = ActiveObjectText
            ActiveObjectText.select_set(True) 

        TexSlot.texture_coords = 'OBJECT'
        TexSlot.object = ActiveObjectPlane
        TexSlot.use_map_time = True


        ActiveObjectText.data.update()

        bpy.ops.object.modifier_add(type='EXPLODE')
        bpy.ops.mesh.uv_texture_add() #name UVMap by default
        #ActiveObjectText.data.materials['DissolveMaterial'].texture_slots['Texture'].texture_coords = 'UV'
        #ActiveObjectText.data.materials['DissolveMaterial'].texture_slots['Texture'].uv_layer = 'UVMap'
        #ActiveObjectText.data.materials['DissolveMaterial'].texture_slots['Texture'].use_map_alpha = True
        #ActiveObjectText.data.materials['DissolveMaterial'].texture_slots['Texture'].alpha_factor = 1.0

        ActiveObjectText.modifiers['Explode'].particle_uv = 'UVMap'
        ActiveObjectText.data.update()


        #Don't forget to deselect before select!
        bpy.ops.object.select_all(action='DESELECT')

        #selecting Text
        if bpy.app.version < (2, 80, 0):
            bpy.context.scene.objects.active  = ActiveObjectText
            bpy.context.scene.objects.active.select = True
        else:
            bpy.context.view_layer.objects.active  = ActiveObjectText
            ActiveObjectText.select_set(True)
        TexSlot.texture_coords = 'OBJECT'
        TexSlot.object = ActiveObjectPlane

        TexSlot.use_map_time = False
        TexSlot.use_map_time = True

        ActiveObjectText.data.update()

    flag_error = False
except Exception as E:
    gest_blender_error(params, sys.exc_info(), 'Error define blender python functions', params["output_path"] + "debug.txt", E)
#####             End Define Blender python script functions             #####
##############################################################################


##############################################################################
#####                      Start create scene plugin                     #####

#print(str(params), file=open(os.path.join(os.path.expanduser("~"), "debug.txt"), "a"))

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           Clean Scene                          ----#
    flag_error = True
    try:
        flag_error = False
        pass
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error clean scene', params["output_path"] + "debug.txt", E)
    #----                         End Clean Scene                        ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           3D Modeling                          ----#
    flag_error = True
    try:
        # Modify Text / Curve settings
        #print (bpy.data.curves.keys())

        #text_object = bpy.data.curves["txtName1"]
        #text_object.extrude = params["extrude"]
        #text_object.bevel_depth = params["bevel_depth"]
        #text_object.body = params["title"]
        #text_object.align = params["spacemode"]
        #text_object.size = params["text_size"]
        #text_object.space_character = params["width"]

        # Get font object
        font = None
        if params["fontname"] != "Bfont":
            # Add font so it's available to Blender
            font = load_font(params["fontname"])
        else:
            # Get default font
            font = bpy.data.fonts["Bfont"]

        createDissolveText(params["title"],params["extrude"],params["bevel_depth"],params["spacemode"],params["text_size"],params["width"], font)

        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error 3d modeling', params["output_path"] + "debug.txt", E)
    #----                         End 3D Modeling                        ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           UV Mapping                           ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error uv mapping', params["output_path"] + "debug.txt", E)
    #----                         End UV Mapping                         ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                      Texturing and Shaders                     ----#
    flag_error = True
    try:
        # Change the material settings (color, alpha, etc...)
        if "DissolveMaterial" in bpy.data.materials:
            material_object = bpy.data.materials["DissolveMaterial"]
            if bpy.app.version < (2, 80, 0):
                material_object.diffuse_color = params["diffuse_color"]['rgb']
            else:
                material_object.diffuse_color = params["diffuse_color"]['rgba']
            material_object.specular_color = params["specular_color"]['rgb']
            material_object.specular_intensity = params["specular_intensity"]
            #don't modify alpha!!!!!
            #if bpy.app.version < (2, 80, 0):
            #    material_object.alpha = params["alpha"]
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error texturing and shading', params["output_path"] + "debug.txt", E)
    #----                    End Texturing and Shaders                   ----#
    #------------------------------------------------------------------------#


if not flag_error:
    #------------------------------------------------------------------------#
    #----                             Rigging                            ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error rigging', params["output_path"] + "debug.txt", E)
    #----                           End Rigging                          ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                            Animation                           ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error animation', params["output_path"] + "debug.txt", E)
    #----                          End Animation                         ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                            Lighting                            ----#
    flag_error = True
    try:
        if bpy.app.version < (2, 80, 0):
            bpy.context.scene.render.alpha_mode = params["alpha_mode"]
            bpy.data.worlds[0].horizon_color = params["horizon_color"]["rgb"]
        try:
            bpy.context.scene.render.color_mode = params["color_mode"]
        except:
            bpy.context.scene.render.image_settings.color_mode = params["color_mode"]
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error lightning', params["output_path"] + "debug.txt", E)
    #----                          End Lighting                          ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                         Camera Setting                         ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error camera setting', params["output_path"] + "debug.txt", E)
    #----                       End Camera Setting                       ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                           Renderring                           ----#
    flag_error = True
    try:
        try:
            bpy.context.scene.render.file_format = params["file_format"]
        except:
            bpy.context.scene.render.image_settings.file_format = params["file_format"]
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error set renderring', params["output_path"] + "debug.txt", E)
    #----                         End Renderring                         ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                    Compositing & Special VFX                   ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error compositing ans special vfx', params["output_path"] + "debug.txt", E)
    #----                  End Compositing & Special VFX                 ----#
    #------------------------------------------------------------------------#

if not flag_error:
    #------------------------------------------------------------------------#
    #----                         Music and Foley                        ----#
    flag_error = True
    try:
        flag_error = False
    except Exception as E:
        gest_blender_error(params, sys.exc_info(), 'Error music and foley', params["output_path"] + "debug.txt", E)
    #----                       End Music and Foley                      ----#
    #------------------------------------------------------------------------#

#####                       End create scene plugin                      #####
##############################################################################


##############################################################################
#####                      Editing and Final output                      #####
# Set the render options.  It is important that these are set
# to the same values as the current OpenShot project.  These
# params are automatically set by OpenShot
# Render the current animation to the params["output_path"] folder
bpy.context.scene.render.resolution_x = params["resolution_x"]
bpy.context.scene.render.resolution_y = params["resolution_y"]
bpy.context.scene.render.resolution_percentage = params["resolution_percentage"]
#bpy.context.scene.render.quality = params["quality"]

if flag_error or not params["animation"] :
    # Render the current animation to the params["output_path"] folder
    bpy.context.scene.render.filepath = params["output_path"] + str(params["current_frame"])
    if flag_error:
        params["current_frame"] = params["start_frame"]
        try:
            bpy.context.scene.render.file_format = 'PNG'
        except:
            bpy.context.scene.render.image_settings.file_format = 'PNG'
    bpy.data.scenes[0].frame_current = params["current_frame"]
    bpy.data.scenes[0].frame_start = params["current_frame"]
    bpy.data.scenes[0].frame_end = params["current_frame"]
    bpy.ops.render.render(animation=params["animation"], write_still=True)
else:
    # Render the current animation to the params["output_path"] folder
    bpy.context.scene.render.filepath = params["output_path"]
    bpy.context.scene.render.fps = params["fps"]
    bpy.data.scenes[0].frame_current = params['current_frame']
    bpy.data.scenes[0].frame_start = params['start_frame']
    bpy.data.scenes[0].frame_end = params['end_frame']
    # Animation Speed (use Blender's time remapping to slow or speed up animation)
    #bpy.context.scene.render.frame_map_new = bpy.context.scene.render.frame_map_old * int(params["animation_speed"])
    bpy.ops.render.render(animation=params["animation"])
#####                    End Editing and Final output                    #####
##############################################################################
